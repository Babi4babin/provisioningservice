#!/bin/bash

set -e
set -x

function usage(){
	if [ "$1" == "" ]; then
		echo "$0 <datacenter name>"
		echo 
		echo "	datacenter name: e.g. qe, st, ab..."
		echo 
	else
		 echo "$1. try --help." 
	fi
	exit 1
}

export DC=$1

[ "$DC" == "" ] && usage "Datacenter argument missing"

export APPNAME=provisioningservice

export SCRIPT_DIR=`pwd`
export APP_DEPLOY_DIR=deploy_${APPNAME}
export CONFIG_DIR=$SCRIPT_DIR/gcp_deployment
export PROPFILE_DIR=$SCRIPT_DIR/

echo "SCRIPT_DIR="$SCRIPT_DIR
echo "APP_DEPLOY_DIR="$APP_DEPLOY_DIR

#Clean up any old deployment artifacts
rm -rf $SCRIPT_DIR/${APPNAME}.tar.gz
rm -rf ${APP_DEPLOY_DIR}

#Create the the deployment directory
mkdir ${APP_DEPLOY_DIR}
mkdir ${APP_DEPLOY_DIR}/conf

#Copy the warfile just built; also copy the properties file and logback.xml,
# which we get from the prod gcp_deployment directory.
cp subscriptionservice/target/${APPNAME}*.war ${APP_DEPLOY_DIR}/

cp $SCRIPT_DIR/config/${APPNAME}.properties ${APP_DEPLOY_DIR}/conf/
cp $CONFIG_DIR/logback.xml ${APP_DEPLOY_DIR}/conf/
cp $SCRIPT_DIR/config/newrelic.yml ${APP_DEPLOY_DIR}/conf/

#Configure the package for the specified datacenter and create the archive
$CONFIG_DIR/${APPNAME}/${APPNAME}_configure.sh $DC
tar cvzf ${APPNAME}.tgz ${APP_DEPLOY_DIR}
