#!/usr/bin/env bash

## ENV variables
set -e
REBASE_REQUIRED="false"
COMMITS_AHEAD=0
MASTER_BRANCH_COUNT=0
TARGET_BRANCH_COUNT=0


## to verify the target branch is up to date with the master branch
echo "current branch is : ${CI_COMMIT_REF_NAME}"
TARGET_BRANCH_COUNT=$(git rev-list --count origin/$CI_COMMIT_REF_NAME)
MASTER_BRANCH_COUNT=$(git rev-list --count origin/$MASTER_BRANCH)
COUNT=$(( $MASTER_BRANCH_COUNT - $TARGET_BRANCH_COUNT))

if [[ $COUNT -gt 0 ]]; then
    echo "Please up to date branch with master";
elif [[ $COUNT -lt 0 ]]; then
    echo "fast-forward"
else
    echo "up to date with master"
fi