package com.marketo.packageupdater.listener;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.common.ObjectMapperFactory;
import com.marketo.packageupdater.model.PackageOperation;
import com.marketo.packageupdater.model.SubscriptionOperation;
import com.marketo.provisioningservice.service.FeatureProvisioningService;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.updatetracker.service.PackageUpdateTrackerService;

public class PackageUpdateListener implements MessageListener {

    private static final Logger logger = LoggerFactory.getLogger(PackageUpdateListener.class);

    @Autowired
    private FeatureProvisioningService featureProvisioningService;
    
    @Autowired
    private PackageUpdateTrackerService packageUpdateTrackerService;
        
    @Override
    public void onMessage(Message message) {
               
        if (message instanceof TextMessage) {

            String tenantUUID = null;
            int trackerId = 0;
            
            try {

                ObjectMapper om =  ObjectMapperFactory.getInstance();
                
                String json = ((TextMessage) message).getText();
                
                logger.info("Message " + json);
                
                SubscriptionOperation subOper = om.readValue(json, SubscriptionOperation.class);
                
                logger.info(subOper.toString());
                
                tenantUUID = subOper.getTenantUUID();
                trackerId = subOper.getTrackerId();
                
                
                ProvisioningContext provisioningContext = new ProvisioningContext();
                provisioningContext.setTenantId(tenantUUID);                
                
                for (PackageOperation oper : subOper.getPackageOperations()) {
                    
                    String log = String.format("Applying operation: %s on feature %s for Subscription %s with config %s", 
                            oper.getOperation(),
                            oper.getFeature().getName(), 
                            subOper.getTenantUUID(),
                            oper.getMarketingFeatureConfig());
                                    
                    logger.info(log);                               
                    
                    oper.getFeature().setMarketingFeatureConfig(oper.getMarketingFeatureConfig());
                    
                    featureProvisioningService.doOperation(oper.getFeature(), oper.getOperation(), provisioningContext);
                    
                    log = String.format("Applied operation: %s on feature %s for Subscription %s", 
                            oper.getOperation(), 
                            oper.getFeature().getName(), 
                            tenantUUID);

                    logger.info(log);                    
                }
                
                packageUpdateTrackerService.addSubscriptionEntry(subOper.getTenantUUID(), subOper.getTrackerId(), true);
            }
            catch (Exception ex) {
                logger.error("Failed applying operation " + ex.getMessage());               
                // Mark it as failed
                if (!StringUtils.isEmpty(tenantUUID)) {
                    packageUpdateTrackerService.addSubscriptionEntry(tenantUUID, trackerId, false);                    
                }
            }            
        }
        else {
            throw new IllegalArgumentException("Message must be of type TextMessage");
        }
    }

}
