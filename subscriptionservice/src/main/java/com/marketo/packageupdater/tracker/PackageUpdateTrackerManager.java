package com.marketo.packageupdater.tracker;

import java.util.Enumeration;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.marketo.packageupdater.model.UpdateTrackResponse;
import com.marketo.updatetracker.service.PackageUpdateTrackerService;

@Component
public class PackageUpdateTrackerManager {
    
    @Autowired
    private TaskExecutor taskExecutor;
    
    @Autowired
    private JmsTemplate jmsTemplate;
    
    @Autowired
    private PackageUpdateTrackerService packageUpdateTrackerService;
    
    @Autowired
    private Queue packageUpdateQueue;
    
    /**
     * 
     * @param trackerId
     */
    public void createUpdateTracker(int trackerId, boolean isLastUpdate) {
        
        PackageUpdateTracker updateTracker = new PackageUpdateTracker(packageUpdateTrackerService, packageUpdateQueue, jmsTemplate, trackerId, isLastUpdate);
        taskExecutor.execute(updateTracker);   
    }
    
    /**
     * 
     * @param trackerId
     * @return
     */
    public UpdateTrackResponse trackPackage(int trackerId) {
        
        int count = jmsTemplate.browseSelected(packageUpdateQueue, "trackerId=" + trackerId, new BrowserCallback<Integer>() {
            
            @Override
            public Integer doInJms(Session session, QueueBrowser browser) throws JMSException {
                
                Enumeration enumeration = browser.getEnumeration();
                int counter = 0;
                while (enumeration.hasMoreElements()) {
                    enumeration.nextElement();
                    counter++;
                }
                return counter;              
            }
        });
                
        return new UpdateTrackResponse(count);
    }
}
