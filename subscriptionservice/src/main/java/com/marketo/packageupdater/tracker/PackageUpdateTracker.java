package com.marketo.packageupdater.tracker;

import java.util.Enumeration;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;

import com.marketo.updatetracker.service.PackageUpdateTrackerService;

public class PackageUpdateTracker implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(PackageUpdateTracker.class);
    
    private static final int MAX_WAIT_TIME = 60 * 60 * 1000;
    
    private Queue packageUpdateQueue;
    
    private JmsTemplate jmsTemplate;
    
    private PackageUpdateTrackerService packageUpdateTrackerService;
    
    private int trackerId;
    
    private boolean isLastUpdate;
    
    public PackageUpdateTracker(PackageUpdateTrackerService packageUpdateTrackerService, Queue queue, JmsTemplate jmsTemplate, int trackerId, boolean isLastUpdate) {
        
        this.packageUpdateTrackerService = packageUpdateTrackerService;
        this.packageUpdateQueue = queue;
        this.jmsTemplate = jmsTemplate;
        this.trackerId = trackerId;
        this.isLastUpdate = isLastUpdate;
    }
    
    @Override
    public void run() {
        
        int numIterations = MAX_WAIT_TIME / 20 * 1000;
        
        int count = 0;
        try {
            while(numIterations-- > 0) {
               
                count = jmsTemplate.browseSelected(packageUpdateQueue, "trackerId=" + trackerId, new BrowserCallback<Integer>() {
        
                    @Override
                    public Integer doInJms(Session session, QueueBrowser browser) throws JMSException {
                        
                        Enumeration enumeration = browser.getEnumeration();
                        int counter = 0;
                        while (enumeration.hasMoreElements()) {
                            enumeration.nextElement();
                            counter++;
                        }
                        return counter;              
                    }
                });
                
                if (count == 0) {
                    String log = String.format("All messages dequeued for queue: %s for trackerID: %d", packageUpdateQueue.toString(), trackerId);
                    logger.info(log);
                    break;
                }
                
                logger.info("Messages left to be dequeued: " + count);
                
                Thread.sleep(20 * 1000);
            }            
        } catch (Exception e) {
            logger.error("Error in Update Thread:", e);
        }
        
        
        
        if (count == 0) {
            logger.info("Updating Success Run for TrackerID: " + trackerId);
            if (isLastUpdate) {
                packageUpdateTrackerService.updateSuccessRun(trackerId);
            }
        } else {
            if (isLastUpdate) {
                packageUpdateTrackerService.updateFailureRun(trackerId);
            }
            logger.info("Updating Failure Run for TrackerID: " + trackerId);
        }        
    }    
}
