package com.marketo.packageupdater.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.packageupdater.model.PackageOperation;
import com.marketo.provisioningservice.service.Operation;

@Component
public class PackageOperationsService {
    
    private static final Logger logger = LoggerFactory.getLogger(PackageOperationsService.class);

    @Autowired
    private BundleMetadataService bundleMetadataService;
    
    public List<PackageOperation> getOperations(List<MarketingBundleFeature> bundleFeatures, Date lastRun) {
        
        List<PackageOperation> packageOperations = new ArrayList<>();
        for (MarketingBundleFeature bundleFeature : bundleFeatures) {
            
            // We are only interested in changes in feature.
            if (bundleFeature.getFeature() != null) {
                
                Operation oper;
                if (bundleFeature.getCreatedAt().after(lastRun) && bundleFeature.isActive()) {
                    oper = Operation.PROVISION;
                } else {
                    if (bundleFeature.isActive()) {
                        //oper = Operation.UPDATE;
                        logger.info("Not handling update for now, as we do not know the correct way to do it");
                        continue;
                    } else {
                        oper = Operation.DEPROVISION;
                    }
                }
                PackageOperation packageOper = new PackageOperation();
                packageOper.setFeature(bundleFeature.getFeature());
                packageOper.setRootBundle(bundleMetadataService.getRootBundle(bundleFeature.getParentBundle()));
                packageOper.setMarketingFeatureConfig(bundleFeature.getMarketingFeatureConfig());
                packageOper.setOperation(oper);
                packageOperations.add(packageOper);
            }
        }
        return packageOperations;
    }
}
