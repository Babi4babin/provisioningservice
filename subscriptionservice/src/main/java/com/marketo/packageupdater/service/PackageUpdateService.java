package com.marketo.packageupdater.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.packageupdater.model.PackageOperation;
import com.marketo.packageupdater.model.PackageUpdateRequest;
import com.marketo.packageupdater.model.PackageUpdateResponse;
import com.marketo.packageupdater.model.SubscriptionOperation;
import com.marketo.packageupdater.model.UpdateTrackResponse;
import com.marketo.packageupdater.tracker.PackageUpdateTrackerManager;
import com.marketo.updatetracker.model.PackageUpdateTracker;
import com.marketo.updatetracker.service.PackageUpdateTrackerService;

@Component
public class PackageUpdateService {

    @Autowired
    private PackageUpdateTrackerService packageUpdateTrackerService;
    
    @Autowired
    private BundleMetadataService bundleMetadataService;
    
    @Autowired
    private PackageOperationsService packageOperationsService;
    
    @Autowired
    private SubscriptionOperationsService subscriptionOperationsService;
    
    @Autowired
    private SubscriptionOperationsPublishService subscriptionOperationsPublishService;
    
    @Autowired
    private PackageUpdateTrackerManager packageUpdateTrackerManager;
    
    @Autowired
    private HttpServletRequest request;
    
    /**
     * 
     * @param packageUpdateRequest
     * @return
     */
    public PackageUpdateResponse updatePackages(PackageUpdateRequest packageUpdateRequest) {
                
        // get the last run
        Date lastRun = getLastRunTime(packageUpdateRequest);
        // get the bundles updated since last run
        Date currDate = new Date();
        
        List<MarketingBundleFeature> bundleFeatures = bundleMetadataService.getBundleUpdatesBetween(lastRun, currDate);
        
        // Get all operations
        List<PackageOperation> packageOperations = packageOperationsService.getOperations(bundleFeatures, lastRun);

        int trackerId = 0;
        int numPackageUpdates = 0;
        int numSubscriptionUpdates = 0;
        
        if (packageOperations.size() != 0) {
            
            numPackageUpdates = packageOperations.size();
            
            // Get the tracker id
            trackerId = getExistingTrackerId(packageUpdateRequest);
            
            // Get all qualifying subscription operations
            List<SubscriptionOperation> qualifyingSubOperations = subscriptionOperationsService.
                    getQualifyingSubscriptionOperations(packageOperations, trackerId);
                                                        
            int numQualifyingSubs = qualifyingSubOperations.size();
            
            List<SubscriptionOperation> filteredSubOperations = subscriptionOperationsService.filterSubscriptions(qualifyingSubOperations, packageUpdateRequest);
            
            numSubscriptionUpdates = filteredSubOperations.size();

            if (!packageUpdateRequest.isDryRun()) {
                
                if (trackerId == 0) {
                    PackageUpdateTracker updateTracker = packageUpdateTrackerService.addUpdateRun(packageUpdateRequest.getTag());
                    trackerId = updateTracker.getId();
                }
                
                subscriptionOperationsPublishService.publishSubscriptionOperations(filteredSubOperations, trackerId);
                
                packageUpdateTrackerManager.createUpdateTracker(trackerId, numQualifyingSubs == numSubscriptionUpdates);                
            }           
        }
        
        return buildResponse(trackerId, numSubscriptionUpdates, numPackageUpdates);
    }
    
    private PackageUpdateResponse buildResponse(int trackerId, int totalOperations, int numPackageUpdates) {        
        
        String updateTrackerUrl = buildResponseUrl(trackerId);
        
        return new PackageUpdateResponse(updateTrackerUrl, totalOperations, numPackageUpdates);
    }
    
    /**
     * 
     * @param trackerId
     * @return
     */
    public UpdateTrackResponse trackUpdate(int trackerId) {
        
        UpdateTrackResponse updateTrackerResponse = packageUpdateTrackerManager.trackPackage(trackerId);
        return updateTrackerResponse;    
    }
    
    /**
     * Get Last Completed Run
     * @param request
     */
    private Date getLastRunTime(PackageUpdateRequest request) {
        
        PackageUpdateTracker packageUpdateTracker = null;
        if (request.getTrackerId() > 0) {
            // Tracker Id
            packageUpdateTracker = packageUpdateTrackerService.getLastCompletedRun();            
        } else if (request.isNewRun()) {
            // this is used to create a new run when a current run is not finished
            packageUpdateTracker = packageUpdateTrackerService.getLastRun();            
        } else {
            // this is the normal case
            packageUpdateTracker = packageUpdateTrackerService.getLastCompletedRun();
        }
        
        Date lastRunTime = null;
        if (packageUpdateTracker != null) {
            lastRunTime = packageUpdateTracker.getStartTime();
        }
        return lastRunTime;
    }
    
    /**
     * Get trackerId for existing unfinished run if it exists 
     * @param request
     * @return
     */
    private int getExistingTrackerId(PackageUpdateRequest request) {
        
        int trackerId = 0;
        if (request.getTrackerId() == 0) {
            if (!request.isNewRun()) {
                PackageUpdateTracker lastTrackerRun = packageUpdateTrackerService.getLastRun();
                if (lastTrackerRun != null && lastTrackerRun.getEndTime() == null) {
                    trackerId = lastTrackerRun.getId();
                }
            }
        } else {
            trackerId = request.getTrackerId();
        }
        return trackerId;
    }
    
    /**
     * 
     * @param trackerId
     * @return
     */
    private String buildResponseUrl(int trackerId) {
        
        String trackerUrl = null;
        
        if (trackerId > 0) {
            String url = request.getRequestURL().toString();
            String uri = request.getRequestURI();
            String ctx = request.getContextPath();
            String base = url.substring(0, url.length() - uri.length()) + ctx;
            
            trackerUrl = String.format("%s/rest/v1/packageupdater/track?trackerId=%s", base, trackerId);
        }
        
        return trackerUrl;
     }
}
