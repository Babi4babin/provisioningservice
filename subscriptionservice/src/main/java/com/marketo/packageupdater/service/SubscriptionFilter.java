package com.marketo.packageupdater.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.marketo.locatorservice.client.SubscriptionServiceManager;
import com.marketo.locatorservice.entities.SubscriptionId;
import com.marketo.locatorservice.entities.SubscriptionPodInfo;
import com.marketo.packageupdater.model.PackageUpdateRequest;
import com.marketo.packageupdater.model.SubscriptionOperation;

@Component
public class SubscriptionFilter {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionFilter.class);
    
    @Autowired
    private SubscriptionServiceManager subscriptionServiceManager;
    
    public List<SubscriptionOperation> filter(List<SubscriptionOperation> subscriptionOperations, PackageUpdateRequest request) {
        
        // for each subscription
        int maxSubscriptions = request.getMaxSubscriptions();
        String pod = request.getPod();
        
        List<SubscriptionOperation> filteredSubOperations = new ArrayList<>();

        logger.info("Request is " + request);
        
        try {
                        
            int count = 0;
            for (SubscriptionOperation subOper : subscriptionOperations) {
                
                SubscriptionPodInfo subPodInfo = null;
                try {
                    subPodInfo = subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId(subOper.getTenantUUID()));                    
                } catch (Exception e) {
                    logger.error("Error Retrieving Subscription: " + subOper.getTenantUUID());
                }
                
                boolean filtered = true;
                
                if (maxSubscriptions > 0) {
                    filtered &= filterMaxSubscriptions(count++, maxSubscriptions);
                    if (!filtered) {
                        break;
                    }
                }

                if (subPodInfo == null) {
                    logger.error("Subscription Pod Info not received for Subscription: \" + subOper.getTenantUUID()");
                    break;
                }

                if (!StringUtils.isEmpty(pod) && !StringUtils.isEmpty(subPodInfo.getPodName())) {
                    filtered &= filterPod(subPodInfo.getPodName(), pod);  
                } 
                
                if (filtered) {
                    filteredSubOperations.add(subOper);
                }
            }
            
        } catch (Exception e) {
            
            logger.error("Error filtering. " + e);
            throw new RuntimeException(e);
        }
        return filteredSubOperations;
    }
    
    private boolean filterMaxSubscriptions(int count, int max) {
        return count < max;
    }
    
    private boolean filterPod(String subPod, String filterPod) {
        return subPod.equals(filterPod);
    }
        
}
