package com.marketo.packageupdater.service;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.common.ObjectMapperFactory;
import com.marketo.packageupdater.model.SubscriptionOperation;

@Component
public class SubscriptionOperationsPublishService {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionOperationsPublishService.class);
    
    @Autowired
    private JmsTemplate jmsTemplate;
    
    @Autowired
    private Queue packageUpdateQueue;
    
    /**
     * 
     * @param operations
     * @param trackerId
     */
    public void publishSubscriptionOperations(List<SubscriptionOperation> operations, int trackerId) {
        
        setTracker(operations, trackerId);
        
        for (final SubscriptionOperation operation : operations) {
            
            final String message = buildMessage(operation);
            
            jmsTemplate.send(packageUpdateQueue, 
                    new MessageCreator() {
                        
                        @Override
                        public Message createMessage(Session session) throws JMSException {
                            
                            TextMessage textMessage = session.createTextMessage();
                            textMessage.setText(message);
                            textMessage.setIntProperty("trackerId", operation.getTrackerId());
                            return textMessage;
                        }
                    });
        }        
    }
    
    private void setTracker(List<SubscriptionOperation> operations, int trackerId) {
        for (SubscriptionOperation operation : operations) {
            operation.setTrackerId(trackerId);
        }
    }
    
    /**
     * Build message
     * @param subOperation
     * @return
     */
    private String buildMessage(SubscriptionOperation subOperation) {
        
        ObjectMapper om = ObjectMapperFactory.getInstance();
        
        String json = null;
        
        try {
            json = om.writeValueAsString(subOperation);
        } catch (Exception e) {
            
            String msg = String.format("Unable to serialize trackerId %d for subscripton: %s",
                    subOperation.getTrackerId(), subOperation.getTenantUUID());
            logger.error(msg);
            throw new RuntimeException(msg, e);
        }
        return json;
    }    
    
}
