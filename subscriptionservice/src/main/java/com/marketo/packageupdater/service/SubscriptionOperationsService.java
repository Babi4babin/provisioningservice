package com.marketo.packageupdater.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.marketo.packageupdater.model.PackageOperation;
import com.marketo.packageupdater.model.PackageUpdateRequest;
import com.marketo.packageupdater.model.SubscriptionOperation;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.updatetracker.service.PackageUpdateTrackerService;

@Component
public class SubscriptionOperationsService {
    
    private static final Logger logger = LoggerFactory.getLogger(SubscriptionOperationsService.class);
    
    @Autowired
    private SubscriptionMetadataService subscriptionMetadataService;
    
    @Autowired
    private PackageUpdateTrackerService packageUpdateTrackerService;
    
    @Autowired
    private SubscriptionFilter subscriptionFilter;

    /**
     * 
     * @param packageOperations
     * @param request
     * @return
     */
    public List<SubscriptionOperation> getQualifyingSubscriptionOperations(List<PackageOperation> packageOperations, int trackerId) {
                        
        List<SubscriptionOperation> subscriptionOperations = getSubscriptionOperations(packageOperations);
        
        Set<String> completedSubs = new HashSet<>(getCompletedSubscriptions(trackerId));
        
        List<SubscriptionOperation> qualifyingSubOperations = new ArrayList<>();
        for(SubscriptionOperation subOperation : subscriptionOperations) {
            if (!completedSubs.contains(subOperation.getTenantUUID())) {
                qualifyingSubOperations.add(subOperation);
            }
        }
        
        return qualifyingSubOperations;
    }
    
    private List<SubscriptionOperation> getSubscriptionOperations(List<PackageOperation> packageOperations) {
       
        Map<String, List<PackageOperation>> subPackageOperations = new HashMap<>();
        
        /*
         * 1. Create Subscription Package Operation Map.
         * 2. 
         */       
        // Associate operations with subscriptions on which they are to applied
        for (PackageOperation packageOper : packageOperations) {
            
            logger.info("Package Operation: " + packageOper);            
            List<String> subs = subscriptionMetadataService.getSubscriptionsForBundle(packageOper.getRootBundle().getId());
            
            for (String tenantUUID : subs) {
                logger.info("Filtered Tenant UUID: " + tenantUUID);                
                if (subPackageOperations.containsKey(tenantUUID)) {
                    subPackageOperations.get(tenantUUID).add(packageOper);
                } else {
                    subPackageOperations.put(tenantUUID, Lists.newArrayList(packageOper));
                }
            }
        }
        
        List<SubscriptionOperation> subOperations = new ArrayList<>();
        for (String tenantUUID : subPackageOperations.keySet()) {
            SubscriptionOperation subOperation = new SubscriptionOperation();
            subOperation.setTenantUUID(tenantUUID);
            subOperation.setPackageOperations(subPackageOperations.get(tenantUUID));
            subOperations.add(subOperation);
        }
        
        return subOperations;
    }

    
    /**
     * Filter Subscriptions based on the request criteria
     * @param subscriptionOperations
     * @param request
     * @return
     */
    public List<SubscriptionOperation> filterSubscriptions(List<SubscriptionOperation> subscriptionOperations, PackageUpdateRequest request) {
        
        return subscriptionFilter.filter(subscriptionOperations, request);
    }
    
    private List<String> getCompletedSubscriptions(int trackerId) {
        
        List<String> subscriptions = Collections.emptyList();
        if (trackerId > 0) {
            subscriptions = packageUpdateTrackerService.getCompletedSubscriptionsForTracker(trackerId);
        }
        return subscriptions;
    }
}
