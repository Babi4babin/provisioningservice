package com.marketo.packageupdater.model;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.provisioningservice.service.Operation;

public class PackageOperation {
    
    private MarketingBundle rootBundle;
    
    private MarketingFeature feature;
    
    private Operation operation;
    
    private String marketingFeatureConfig;

    public MarketingBundle getRootBundle() {
        return rootBundle;
    }

    public void setRootBundle(MarketingBundle rootBundle) {
        this.rootBundle = rootBundle;
    }

    public MarketingFeature getFeature() {
        return feature;
    }

    public void setFeature(MarketingFeature feature) {
        this.feature = feature;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public String getMarketingFeatureConfig() {
        return marketingFeatureConfig;
    }

    public void setMarketingFeatureConfig(String marketingFeatureConfig) {
        this.marketingFeatureConfig = marketingFeatureConfig;
    }
    
    @Override
    public String toString() {
        
        StringBuffer sb = new StringBuffer();
        if (rootBundle != null) {
            sb.append(String.format("Root Bundle Name: %s\n", rootBundle.getName()));
        }
        
        if (feature != null) {
            sb.append(String.format("Feature Name: %s\n", feature.getName()));
        }
        
        if (operation != null) {
            sb.append(String.format("Operation : %s\n", operation.toString()));
        }
        
        return sb.toString();
    }
}
