package com.marketo.packageupdater.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class PackageUpdateResponse {
    
    private final String updateTrackerUrl;
    
    private final int numSubscriptionUpdates;
    
    private final int numPackageUpdates;
}
