package com.marketo.packageupdater.model;

import lombok.Data;

@Data
public class UpdateTrackResponse {
    private final int numObjectsRemaining;
    
    public UpdateTrackResponse(int numObjectsRemaining) {
        this.numObjectsRemaining = numObjectsRemaining;
    }
}
