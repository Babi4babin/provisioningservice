package com.marketo.packageupdater.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import lombok.Data;

@Data
public class SubscriptionOperation {
    
    private int trackerId;
    
    private String tenantUUID;
    
    private List<PackageOperation> packageOperations = new ArrayList<>();
    
    @Override
    public String toString() {
        
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("TrackedId : %s\n", trackerId));
        sb.append(String.format("TenantUUID : %s\n", tenantUUID));
        for (PackageOperation oper : packageOperations) {
            sb.append(String.format("Operation : %s\n", oper.getOperation()));
            sb.append(String.format("Feature : %s\n", oper.getFeature().getName()));
            if (!StringUtils.isEmpty(oper.getMarketingFeatureConfig())) sb.append(
                    String.format("Config : %s\n", oper.getMarketingFeatureConfig()));            
        }
        
        return sb.toString();
    }
    
}
