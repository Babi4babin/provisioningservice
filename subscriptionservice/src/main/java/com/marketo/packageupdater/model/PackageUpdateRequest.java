package com.marketo.packageupdater.model;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.StringUtils;

import lombok.Data;

@Data
public class PackageUpdateRequest {

    @NotEmpty
    @Length(max = 255)
    private String tag;
    
    private boolean dryRun;
    
    private int maxSubscriptions;
    
    private String pod;
    
    private String dataCenter;
    
    private int trackerId;
    
    private boolean isNewRun;
    
    @Override
    public String toString() {
        
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("Tag = %s\n", tag));
        sb.append(String.format("Dry Run = %b\n", dryRun));
        sb.append(String.format("Max Subscriptions = %s\n", maxSubscriptions));
        if(!StringUtils.isEmpty(pod)) {
            sb.append(String.format("Pod = %s\n", pod));
        }
        
        if (!StringUtils.isEmpty(dataCenter)) {
            sb.append(String.format("Data Center = %s\n", dataCenter));
        }
        
        if (trackerId > 0) {
            sb.append(String.format("TrackerId = %d\n", trackerId));
        }
        
        if (isNewRun) {
            sb.append(String.format("isNewRun = %b\n", isNewRun));
        }
        
        return sb.toString();
    }
}
