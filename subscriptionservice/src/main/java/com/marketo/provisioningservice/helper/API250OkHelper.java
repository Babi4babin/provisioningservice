package com.marketo.provisioningservice.helper;


import com.marketo.subscriptionservice.model.ChildAccount;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import com.marketo.subscriptionservice.utility.SecurityUtils;
import com.marketo.subscriptionservice.utility.SubscriptionConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;


@Component
@Configuration
public class API250OkHelper {

	private static final Logger logger = LoggerFactory.getLogger(API250OkHelper.class);


	@Value("${250okapiService.url}")
	private String serviceUrl;


	@Value("${250okapiService.apiKey}")
	private String apiKey;


	@Value("${250okapiService.cmntyToken}")
	private String communityToken;


	@Value("${250okapiService.appSecurityHashMethod}")
	private String hashMethod;


	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private SecurityUtils securityUtils;

	private static String statusURI = "/api/1.0/account/childaccount";



	/***
	 *
	 * Method to fetch child account from 250okapi service
	 *
	 * @param external_id
	 * @param relativeUrl
	 * @return
	 */
	public ChildAccount[] fetchChildAccountForTenant(String external_id, String relativeUrl) {

		String url = serviceUrl.concat(relativeUrl);
		UriComponents builder = null;
		ResponseEntity<ChildAccount[]> response = null;
		try {
			builder = UriComponentsBuilder.fromHttpUrl(url)
					.queryParam("external_id", securityUtils.hashPassword(external_id, communityToken, hashMethod))
					.build();
			HttpHeaders headers = new HttpHeaders();
			headers.add("x-api-key", apiKey);
			response = restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<String>(headers), ChildAccount[].class);

		} catch (RestClientException e) {

			logger.error(e.getMessage());

			if (e instanceof HttpClientErrorException) {
				HttpClientErrorException ex = (HttpClientErrorException) e;
				validateResponse(ex);
			}
			if (e instanceof ResourceAccessException)
				throw new ProvisioningException(SubscriptionConstants.API250OK_UNABLE_TO_FETCH_CHILD_ACCOUNT, e);


		} catch (Exception e) {

			logger.error(e.getMessage());

			throw new ProvisioningException(SubscriptionConstants.API250OK_UNABLE_TO_FETCH_CHILD_ACCOUNT, e);
		}
		return response != null ? response.getBody() : null;
	}

	/*****
	 *
	 * @param munchkinId
	 * @param requestBody
	 * @param relativeUrl
	 */
	public void provisionModulesForChildAccount(String munchkinId, HttpMethod httpMethod, MultiValueMap<String, String> requestBody, String relativeUrl) {

		String url = serviceUrl.concat(relativeUrl);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED.toString());
		headers.add("X-API-KEY", apiKey);
		HttpEntity formEntity = new HttpEntity<MultiValueMap<String, String>>(requestBody, headers);

		ResponseEntity response = null;

		try {


			response = restTemplate.exchange(url, httpMethod, formEntity, Object.class);

		} catch (RestClientException e) {

			logger.error("Caught exception during provisioning module ", e);
			HttpClientErrorException ex = (HttpClientErrorException) e;
			validateResponse(ex);

		} catch (Exception e) {

			logger.error("Caught exception during provisioning module ", e);
			throw new ProvisioningException(SubscriptionConstants.API250OK_UNABLE_TO_PROVISION_BUNDLE, e);
		}

	}


	/***
	 *
	 * Method to fetch all child accounts from 250okapi service
	 *
	 **/
	public ChildAccount[] fetchAllChildAccounts() {
		String url = serviceUrl.concat(statusURI);
		UriComponents builder = null;
		ResponseEntity<ChildAccount[]> response = null;
		try {
			builder = UriComponentsBuilder.fromHttpUrl(url)
					.build();
			HttpHeaders headers = new HttpHeaders();
			headers.add("x-api-key", apiKey);
			response = restTemplate.exchange(builder.toString(), HttpMethod.GET, new HttpEntity<String>(headers), ChildAccount[].class);

		} catch (RestClientException e) {

			logger.error(e.getMessage());

			if (e instanceof HttpClientErrorException) {
				HttpClientErrorException ex = (HttpClientErrorException) e;
				validateResponse(ex);
			}
			if (e instanceof ResourceAccessException)
				throw new ProvisioningException(SubscriptionConstants.API250OK_UNABLE_TO_FETCH_CHILD_ACCOUNT, e);


		} catch (Exception e) {

			logger.error(e.getMessage());

			throw new ProvisioningException(SubscriptionConstants.API250OK_UNABLE_TO_FETCH_CHILD_ACCOUNT, e);
		}

		return response != null ? response.getBody() : null;
	}


	@Bean
	public RestTemplate restTemplate() {
		return this.restTemplate == null
				? new RestTemplate() : this.restTemplate;
	}


	/**
	 * Method to validate response from 250ok api service
	 *
	 * @param ex
	 */
	private void validateResponse(HttpClientErrorException ex) {


		if (ex.getStatusCode().equals(HttpStatus.FORBIDDEN))
			throw new ProvisioningException(SubscriptionConstants.API250OK_ACCESS_DENIED);

		if (ex.getStatusCode().equals(HttpStatus.UNAUTHORIZED))
			throw new ProvisioningException(SubscriptionConstants.API250OK_AUTHENTICATION_FAILURE);

		if (ex.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR))
			throw new ProvisioningException(SubscriptionConstants.API250OK_INTERNAL_SERVER_ERROR);


	}

	/**
	 * @param tenantUUID
	 * @return
	 */
	public String generateExternalIdForTenant(String tenantUUID) {

		return securityUtils.hashPassword(tenantUUID, communityToken, hashMethod);
	}


	/**
	 * @return
	 */
	public List<String> getApi250okFields() {
		return Arrays.asList(new String[]{SubscriptionConstants.API250OK_FIELD_INBOX, SubscriptionConstants.API250OK_FIELD_BLACKLIST, SubscriptionConstants.API250OK_FIELD_REPUTATION
				, SubscriptionConstants.API250OK_FIELD_DESIGN, SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT, SubscriptionConstants.API250OK_FIELD_DMARC});
	}


}




