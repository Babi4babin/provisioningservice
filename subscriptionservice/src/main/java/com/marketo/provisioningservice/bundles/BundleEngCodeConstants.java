package com.marketo.provisioningservice.bundles;

/**
 * This class defines all the possible module eng codes.
 * This is used in SpaToProvisionConverter class.
 * Created by rmalani on 5/24/17.
 */
public class BundleEngCodeConstants {

    public static final String BASE_MODULE = "baseModule";
    public static final String SEARCH_ENGINE_OPTIMIZATION = "searchEngineOptimization";
    public static final String MARKETING_PERFORMANCE_INSIGHTS = "marketingPerformanceInsights";
    public static final String PROGRAM_ANALYSIS = "programAnalysis";
    public static final String ADVANCED_REPORT_BUILDER = "advancedReportBuilder";
    public static final String LIFE_CYCLE_MODELER = "lifeCycleModeler";
    public static final String MARKETING_CALENDAR = "marketingCalendar";
    public static final String SALES_INSIGHT = "salesInsight";
    public static final String EMAIL_PLUGIN = "emailPlugin";
    public static final String AD_TARGETING = "adTargeting";
    public static final String WEBSITE_RETARGETING = "websiteRetargeting";
    public static final String SOCIAL_MARKETING = "socialMarketing";
    public static final String EVENTS_AND_WEBINARS = "eventsAndWebinars";
    public static final String MOBILE_ENGAGEMENT = "mobileEngagement";
    public static final String WORKSPACES_AND_PARTITIONS = "workspacesAndPartitions";
    public static final String CUSTOM_OBJECTS = "customObjects";
    public static final String NATIVE_SFDC_MSD_CRM_CONNECTOR = "nativeSfdcMsdCrmConnector";
    public static final String WEB_PERSONALIZATION = "webPersonalization";
    public static final String ACCOUNT_BASED_MARKETING_CORE = "accountBasedMarketingCore";
    public static final String PREDICTIVE_CONTENT = "predictiveContent";
    public static final String CONTENT_AI_ANALYTICS = "contentAiAnalytics";
    public static final String ACCOUNT_BASED_MARKETING = "accountBasedMarketing";
    public static final String CUSTOM_OBJECT_TYPE_API = "customObjectTypeApi";

}
