package com.marketo.provisioningservice.util;

import com.marketo.provisioningservice.bundles.BundleEngCodeConstants;
import com.marketo.provisioningservice.features.ConfigCodeConstants;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rmalani on 5/23/17.
 */

/**
 * Utility Class to convert spa values that are passed in newBundle parameter
 * to provisioning service values
 */
public class SpaToProvisionConverter {

    private static final Logger logger = LoggerFactory.getLogger(SpaToProvisionConverter.class);

    private static final String DEFAULT = "default";
    private static final String SPA_SEO_SKU_500 = "500.000";
    private static final String PROV_SEO_SKU_500 = "SEO 500";
    private static final String SPA_SEO_SKU_1000 = "1000.000";
    private static final String PROV_SEO_SKU_1000 = "SEO 1000";
    private static final String PROV_SEO_SKU_1000PLUS = "SEO 1000 Plus";

    /**
     * Map that contains the module:config as key and value as another
     * map that maps spa value to corresponding provisioning value
     */
    private static final Map<String, Map<String, String>> SPA_TO_PROV_VALUES_MAP;
    static {
        SPA_TO_PROV_VALUES_MAP = new HashMap<String, Map<String, String>>()
        {{
            put(BundleEngCodeConstants.SEARCH_ENGINE_OPTIMIZATION+":"+ ConfigCodeConstants.SKU, new HashMap<String, String>()
            {{
                put(SPA_SEO_SKU_500, PROV_SEO_SKU_500);
                put(SPA_SEO_SKU_1000, PROV_SEO_SKU_1000);
                put(DEFAULT, PROV_SEO_SKU_1000PLUS);
            }});
        }};
    }

    /**
     * Function to convert spa values to provisioning values
     * in the newBundle parameter
     * @param newBundleConfig
     */
    public static void convertSpaToProvisionValues (JSONObject newBundleConfig) {
        // Iterate over the spaToProvValuesMap
        for (Map.Entry valueMapEntry : SPA_TO_PROV_VALUES_MAP.entrySet()) {
            String moduleConfigKey = (String) valueMapEntry.getKey();
            String moduleKey = moduleConfigKey.split(":")[0];
            String configKey = moduleConfigKey.split(":")[1];
            /* Check if the newBundle contains the same module and same config
             * If yes then, change the value of that config to corresponding
             * provision value using the spaToProvValue */
            if (!newBundleConfig.isNull(moduleKey) && newBundleConfig.has(moduleKey) && !newBundleConfig.get(moduleKey).toString().equals("null") && ((JSONObject)newBundleConfig.get(moduleKey)).has(configKey)) {
                String log = String.format("Converting Spa to provisioning service value for : %s", moduleConfigKey);
                logger.info(log);
                Map<String, String> valuesMap = (HashMap)valueMapEntry.getValue();
                JSONObject moduleConfigs = (JSONObject)newBundleConfig.get(moduleKey);
                try {
                    if (moduleConfigs.isNull(configKey)) {
                        logger.info("Null value encountered while converting SPA to Provisioning for : " + moduleConfigKey);
                    } else {
                        String spaValue = moduleConfigs.getString(configKey);
                        if (!spaValue.equals("null")) {
                            if(valuesMap.containsKey(spaValue)) {
                                moduleConfigs.put(configKey, valuesMap.get(spaValue));
                            } else {
                                moduleConfigs.put(configKey, valuesMap.get(DEFAULT));
                            }
                        }
                    }
                } catch (ClassCastException | JSONException ex) {
                    logger.info(String.format("Incorrect format encountered from spa for : %s", moduleConfigKey));
                }
            }
        }

    }
}
