package com.marketo.provisioningservice.features;

import java.util.Map;

import org.parboiled.common.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.common.ObjectMapperFactory;
import com.marketo.provisioningservice.service.FeatureLocatorService;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionservice.rest.RestClient;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;

@Component
public class MLMProvisioningHandler extends BaseFeatureProvisioningHandler {

    private static final Logger logger = LoggerFactory.getLogger(MLMProvisioningHandler.class);

    @Autowired
    private RestClient restClient;
    
    @Autowired
    private FeatureLocatorService featureLocatorService;
    
    /**
     * Do base subscription provisioning 
     */
    @Override
    public void provision(MarketingFeature feature, ProvisioningContext provisioningContext) {
        
        MultiValueMap<String, String> params = provisioningContext.getParams();
        
        // get pod name
        if (!params.containsKey("activePod") || params.get("activePod").size() == 0) {
            throw new ProvisioningInputException("Invalid Active Pod Recieved");
        }
        
        String activePod = params.get("activePod").get(0);
        // add the active pod to the context.
        provisioningContext.setActivePod(activePod);
        
        params.add("feature_config", feature.getMarketingFeatureConfig());

        logger.info("Calling MLM for base feature provsioning | Context: " + provisioningContext.toString());
        String host = featureLocatorService.getPodFeatureLocation(activePod, feature, null);
        
        String url = buildFeatureUrl(host, feature.getProvisioningURI(), null);
        
        String response = restClient.postForm(url, params);
        
        if (StringUtils.isEmpty(url)) {
            throw new ProvisioningException("Invalid response recieved while provisioning: " + feature.getName());
        }
        
        String munchkinId = parseProvisioningResponseJSON(response);
        provisioningContext.setTenantId(munchkinId);
    }
        
    @SuppressWarnings("unchecked")
    protected String parseProvisioningResponseJSON(String response) {
        
        ObjectMapper mapper = ObjectMapperFactory.getInstance();
        Map<String, Object> responseObj = null;
        
        if (StringUtils.isEmpty(response)) {
            throw new ProvisioningException("Invalid Response Recieved. " + response);
        }
        
        try {
            responseObj = mapper.readValue(response, Map.class);
        } catch (Exception e) {
            throw new ProvisioningException(e);
        }
        
        if (responseObj.containsKey("JSONResults")) {
            
            Map<String, Object> jsonSection = (Map<String, Object>)responseObj.get("JSONResults");
            
            if (jsonSection.containsKey("isError")) {
                if (jsonSection.containsKey("errorMessage")) {
                    throw new ProvisioningException((String)jsonSection.get("errorMessage"));                    
                } else {
                    throw new ProvisioningException("Error provisioning feature");
                }
            }   
            return (String)jsonSection.get("munchkinId");
        } else {
            throw new ProvisioningException("Invalid Response Recieved. " + response);
        }
    }        

    
    @Override
    public void deprovision(MarketingFeature feature, ProvisioningContext provisioningContext) {
        throw new ProvisioningException("Deprovisioning option on Base MLM feature not supported.");
    }

    /**
     * Call MLM to migrate features from V2 to Rubiks
     * @param feature
     * @param provisioningContext
     */
    public String migrate(MarketingFeature feature, ProvisioningContext provisioningContext) {

        if (org.springframework.util.StringUtils.isEmpty(provisioningContext.getTenantId())) {
            throw new ProvisioningException("Tenant ID not found.");
        }

        String host = getFeatureLocator(feature, provisioningContext);
        String url =  buildFeatureUrl(host, "/rest/v1/featureHandler/operation/migrateFeatures/munchkinId/{tenantUUID}", provisioningContext.getTenantId());

        String response = restClient.postJson(url, "");
        
        return parseProvisioningResponseJSON(response);
    }
}
