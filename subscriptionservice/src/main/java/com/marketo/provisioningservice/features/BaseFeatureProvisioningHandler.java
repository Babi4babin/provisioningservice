package com.marketo.provisioningservice.features;

import com.marketo.subscriptionservice.model.ClientType;
import com.marketo.subscriptionservice.model.FeatureCode;
import com.marketo.subscriptionservice.model.ProvisionSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.common.ObjectMapperFactory;
import com.marketo.provisioningservice.service.Operation;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.provisioningservice.service.FeatureLocatorService;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.RestClient;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;

import java.util.List;
import java.util.Map;

@Component
public class BaseFeatureProvisioningHandler {

    private static final Logger logger = LoggerFactory.getLogger(BaseFeatureProvisioningHandler.class);

    public static final String IS_ASYNC = "isAsync";

    @Autowired
    FeatureLocatorService subFeatureLocator;

    @Autowired
    private RestClient restClient;

    @Autowired
    private SubscriptionMetadataService subscriptionMetadataService;

    /**
     * Do operation
     * 
     * @param feature
     * @param operation
     */
    public void doOperation(MarketingFeature feature, Operation operation, ProvisioningContext provisioningContext) {
        switch (operation) {
        case PROVISION:
            provision(feature, provisioningContext);
            break;
        case DEPROVISION:
            deprovision(feature, provisioningContext);
            break;
        case UPDATE:
            update(feature, provisioningContext);
            break;
        }
    }

    /**
     * Get Feature Locator
     * 
     * @param feature
     * @return
     */
    protected String getFeatureLocator(MarketingFeature feature, ProvisioningContext provisioningContext) {

        logger.info("Provisioning Context: " + provisioningContext + " for feature " + feature.getCode() + " for Subscription " + provisioningContext.getTenantId());
        String uri = subFeatureLocator.getSubscriptionFeatureLocation(provisioningContext.getTenantId(), feature);
        logger.info("Feature location call URL: " + uri);
        if (StringUtils.isEmpty(uri) && !StringUtils.isEmpty(provisioningContext.getActivePod())) {
            logger.info("Unable to retrieve uri from Subscription Feature Location. Retrieving from Pod Feature Location for pod: " + provisioningContext.getActivePod() + " | Feature: " + feature.getCode() + " for Subscription " + provisioningContext.getTenantId());
            uri = subFeatureLocator.getPodFeatureLocation(provisioningContext.getActivePod(), feature, provisioningContext.getTenantId());
            logger.info("Get URL from Pod Feature location call URL: " + uri);
        }
        
        if (StringUtils.isEmpty(uri)) {
            throw new ProvisioningException("Unable to find location for the feature: " + feature.getName());
        }

        return uri;
    }


   /*
    *  Provision base feature
    */
    public void provision(MarketingFeature feature, ProvisioningContext provisioningContext) {

        if (StringUtils.isEmpty(provisioningContext.getTenantId())) {
            throw new ProvisioningException("Tenant ID not found. Either Provisioning Base Feature failed. "
                    + "Or it was not sent as part of addBundle/addFeature call");
        }

        String host = getFeatureLocator(feature, provisioningContext);

        String url = buildFeatureUrl(host, feature.getProvisioningURI(), provisioningContext.getTenantId());

        logger.info("About to provision Feature: " + feature.getName());

        String featureConfig = updateFeatureConfig(feature, provisioningContext) != null ?
                updateFeatureConfig(feature, provisioningContext) : feature.getMarketingFeatureConfig();
        logger.info("Feature Config for feature: "+ featureConfig);

        String response = restClient.postJson(url, featureConfig);

        if (StringUtils.isEmpty(response)) {
            throw new ProvisioningException("Empty response recieved from feature " + feature.getName());
        }

        FeatureResponse featureResponse = parseProvisioningResponse(response);
        
        // TODO: This needs to be reverted back in future
        // Reverting this changes until we fix V2 Bundle Ordering Issue (LM-91476)
        /*subscriptionMetadataService.addMarketingFeature(provisioningContext.getTenantId(),
                feature, featureResponse.getInfo());
        */
    }

    /**
     * This Method will update Feature Config for Marketing Feature. Update Feature Config will include
     * Provision Source (Provision Source Can be MLM, SPA or Null).
     *
     * @param feature
     * @param provisioningContext
     * @return
     */
    protected String updateFeatureConfig(MarketingFeature feature, ProvisioningContext provisioningContext) {

        // For Now, we will update feature config for RCA only. Based on new requirements
        // we will update this code.
        if (feature.getCode() == null || !feature.getCode().equalsIgnoreCase(FeatureCode.RCA.toString()))
            return null;

        ObjectMapper mapper = ObjectMapperFactory.getInstance();
        try {
            Map<String, String> configs = mapper.readValue(feature.getMarketingFeatureConfig(), Map.class);
            if (provisioningContext.getProvisionSource() != null && provisioningContext.getProvisionSource().equalsIgnoreCase(ProvisionSource.MLM.toString())) {
                configs.put(IS_ASYNC, "false");
            } else {
                configs.put(IS_ASYNC, "true");
            }
            return mapper.writeValueAsString(configs);
        } catch (Exception e) {
            throw new ProvisioningException(e);
        }
    }

    /**
     * 
     * @param response
     * @return
     */
    private FeatureResponse parseProvisioningResponse(String response) {

        ObjectMapper mapper = ObjectMapperFactory.getInstance();
        
        if (StringUtils.isEmpty(response)) {
            throw new ProvisioningException("Null Response Recieved from feature handler");
        }

        FeatureResponse featureResponse = null;
        try {
            featureResponse = mapper.readValue(response, FeatureResponse.class);
        } catch (Exception e) {
            throw new ProvisioningException(e);
        }

        if (featureResponse.isError()) {
            throw new ProvisioningException(featureResponse.getMessage());
        }
        
        return featureResponse;
    }

    /**
     * @param feature
     */
    public void deprovision(MarketingFeature feature, ProvisioningContext provisioningContext) {

        if (StringUtils.isEmpty(provisioningContext.getTenantId())) {
            throw new ProvisioningException("Tenant ID not found. Either Provisioning Base Feature failed. "
                    + "Or it was not sent as part of addBundle/addFeature call");
        }

        if (StringUtils.isEmpty(feature.getDeprovisioningURI())) {
            throw new ProvisioningException("Deprovisioning api not supported for feature " + feature.getName());
        }

        String host = getFeatureLocator(feature, provisioningContext);
        String url = buildFeatureUrl(host, feature.getDeprovisioningURI(), provisioningContext.getTenantId());

        String response = restClient.postJson(url, feature.getMarketingFeatureConfig());

        parseDeprovisioningResponse(response);
    }

    /**
     * 
     * @param response
     */
    private void parseDeprovisioningResponse(String response) {
        parseProvisioningResponse(response);
    }

    /**
     * 
     * @param feature
     */
    private void update(MarketingFeature feature, ProvisioningContext provisioningContext) {

        if (StringUtils.isEmpty(provisioningContext.getTenantId())) {
            throw new ProvisioningException("Tenant ID not found. Either Provisioning Base Feature failed. "
                    + "Or it was not sent as part of updateBundle/updateFeature call");
        }
        
        if (!subscriptionMetadataService.isFeatureProvisioned(provisioningContext.getTenantId(), feature.getId())) {
            provision(feature, provisioningContext);
        } else {
            // Do not update if there is no feature config.
            if (!StringUtils.isEmpty(feature.getMarketingFeatureConfig())) {
                if (StringUtils.isEmpty(feature.getUpdateURI())) {
                    logger.info("Update Feature api not supported for feature " + feature.getName());
                    // No update URI present, so do a deprovision and provision.
                    deprovision(feature, provisioningContext);
                    provision(feature, provisioningContext);
                } else {

                    logger.info("Update URI present for feature: " + feature.getName());
                    String host = getFeatureLocator(feature, provisioningContext);
                    String url =  buildFeatureUrl(host, feature.getUpdateURI(), provisioningContext.getTenantId());

                    // Add the tenantId as parameter to the url
                    String response = restClient.postJson(url, feature.getMarketingFeatureConfig());
                    parseUpdateResponse(response);
                }
            } else {
                logger.info("Not Updating the feature: " + feature.getMarketingFeatureConfig() + " as no config found");
            }
        }
    }

    /**
     * 
     * @param response
     */
    private void parseUpdateResponse(String response) {
        parseProvisioningResponse(response);
    }
    
    /**
     * 
     * @param appUrl
     * @param feature
     * @param tenantId
     * @return
     */
    public String buildFeatureUrl(String appUrl, String operationUri, String tenantId) {

        StringBuffer sb = new StringBuffer();
        sb.append(appUrl).append(operationUri);
        String url = sb.toString();
        // If the url has {tenantUUID} replace with it actual tenantId
        // This because our internal rest api expects tenantId in the endpoint path
        if (url.contains("{tenantUUID}")) {
            url = url.replace("{tenantUUID}", tenantId);
        }
        return url;
    }

}
