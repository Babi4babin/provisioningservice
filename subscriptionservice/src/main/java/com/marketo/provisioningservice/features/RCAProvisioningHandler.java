package com.marketo.provisioningservice.features;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.common.ObjectMapperFactory;
import com.marketo.provisioningservice.service.FeatureLocatorService;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.model.FeatureStatus;
import com.marketo.subscriptionservice.model.Response;
import com.marketo.subscriptionservice.model.SubscriptionFeatureConfig;
import com.marketo.subscriptionservice.rest.RestClient;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Created by asawhney on 6/1/17.
 */
@Component
public class RCAProvisioningHandler {

    @Autowired
    FeatureLocatorService subFeatureLocator;

    @Autowired
    private SubscriptionMetadataService subscriptionMetadataService;

    @Autowired
    private RestClient restClient;

    private static final Logger logger = LoggerFactory.getLogger(RCAProvisioningHandler.class);

    private static final String RCA_HOST = "http://{pod}fe1.marketo.org:8080";

    /**
     *
     * This Method will retreive featue status by calling rca rest api.
     *
     * @param tenantUUID
     * @param feature
     * @return
     */
    public String getFeatureStatus(String tenantUUID, MarketingFeature feature) {

        String config = subscriptionMetadataService.getMarketingFeature(tenantUUID, feature).getSubscriptionFeatureConfig();
        String podName = parseSubscriptionFeatureConfig(config).getPodName();
        String url = buildRCAUrl(feature.getStatusURI(), tenantUUID, podName);

        HttpHeaders headers = new HttpHeaders();
        String rcaResponse = restClient.getJson(url, headers);

        Response response = parseProvisioningResponse(rcaResponse);

        logger.info("RCA Feature Status: " + response.getStatus() + " for Munchkin : " +
                tenantUUID + " for feature code " + feature.getCode());

        feature.setMarketingFeatureConfig(config);
        subscriptionMetadataService.addMarketingFeature(tenantUUID, feature,
                getInfo(response.getStatus()));

        if (response.getStatus().equalsIgnoreCase(FeatureStatus.FAILED.toString())) {
            logger.info("Remove RCA Feature " + feature.getCode() + " for subscription bundle map" +
                    " for munchkin " + tenantUUID);
            subscriptionMetadataService.deleteMarketingFeature(tenantUUID, feature.getId());
        }

        return response.getStatus();
    }

    /**
     *
     * Build RCA Rest API Url
     *
     * @param operationUri
     * @param tenantId
     * @param pod
     * @return
     */
    protected String buildRCAUrl(String operationUri, String tenantId, String pod) {

        String host = RCA_HOST;
        host = host.replace("{pod}", pod);

        StringBuffer sb = new StringBuffer();
        sb.append(host).append(operationUri).append("?tenantUUID=").append(tenantId);
        String url = sb.toString();

        logger.info("RCA Url: " + url);
        return url;
    }

    /**
     * Parse Subscription Feature Config (Json Format)
     *
     * @param subscriptionFeatureConfig
     * @return
     */
    protected SubscriptionFeatureConfig parseSubscriptionFeatureConfig(String subscriptionFeatureConfig) {

        ObjectMapper mapper = ObjectMapperFactory.getInstance();

        if (StringUtils.isEmpty(subscriptionFeatureConfig)) {
            throw new ProvisioningException("Null Response Recieved from feature handler");
        }

        SubscriptionFeatureConfig config = null;
        try {
            config = mapper.readValue(subscriptionFeatureConfig, SubscriptionFeatureConfig.class);
        } catch (Exception e) {
            throw new ProvisioningException(e);
        }

        return config;
    }

    /**
     * Parse Provisioning Response
     *
     * @param rcaResponse
     * @return
     */
    protected Response parseProvisioningResponse(String rcaResponse) {

        if (StringUtils.isEmpty(rcaResponse)) {
            throw new ProvisioningException("Empty response recieved from Sirius feature ");
        }

        ObjectMapper mapper = ObjectMapperFactory.getInstance();

        Response response = null;
        try {
            response = mapper.readValue(rcaResponse, Response.class);
        } catch (Exception e) {
            throw new ProvisioningException(e);
        }

        if (response == null || response.getStatus().isEmpty()) {
            throw new ProvisioningException("RCA Provisioning Failed");
        }

        return response;
    }

    private String getInfo(String status) {
        JSONObject obj = new JSONObject();
        obj.put("status", status);
        return obj.toString();
    }

}
