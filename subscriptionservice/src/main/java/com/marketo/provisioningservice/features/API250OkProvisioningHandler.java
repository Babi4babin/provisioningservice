package com.marketo.provisioningservice.features;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.provisioningservice.helper.API250OkHelper;
import com.marketo.provisioningservice.service.Operation;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.model.ChildAccount;
import com.marketo.subscriptionservice.service.SubscriptionService;
import com.marketo.subscriptionservice.utility.SubscriptionConstants;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.*;

import static org.mockito.Mockito.spy;


@Component
@Configuration
public class API250OkProvisioningHandler {

	private String delChildAccountRelativeUrl = "/api/1.0/account/childAccount";

	@Autowired
	private API250OkHelper api250OkHelper;

	@Autowired
	private SubscriptionService subscriptionService;

	@Autowired
	private SubscriptionMetadataService subscriptionMetadataService;

	private static final String YES = "y";

	private static final String NO = "n";

	private static final Logger logger = LoggerFactory.getLogger(API250OkProvisioningHandler.class);


	/***
	 *x
	 * @param feature
	 * @param operation
	 * @param provisioningContext
	 */
	public void doOperation(MarketingFeature feature, Operation operation, ProvisioningContext provisioningContext) {
		switch (operation) {
			case PROVISION:
				provision(feature, provisioningContext);
				break;
			case DEPROVISION:
				deprovision(feature, provisioningContext);
				break;
			case UPDATE:
				update(feature, provisioningContext);
				break;
		}
	}


	/***
	 *
	 * @param feature
	 * @param provisioningContext
	 */
	public void provision(MarketingFeature feature, ProvisioningContext provisioningContext) {

		//check whether child account exists for given tenant
		ChildAccount account = fetchByMunchkinId(provisioningContext.getTenantId(), feature.getStatusURI());
		MultiValueMap<String, String> requestBodyMap = new LinkedMultiValueMap<String, String>();
		HttpMethod httpMethod = null;

		handleContextParam(provisioningContext,feature,SubscriptionConstants.API250OK_ACTION_PROVISION);

		if (Objects.isNull(account)) {
			//set client account specific fields if account is new

			String customerPrefix = subscriptionService.getTenantDetailsByMuchkinId(provisioningContext.getTenantId());
			requestBodyMap.add("email", customerPrefix.concat(
					SubscriptionConstants.API250OK_MARKETO_EMAIL_DOMAIN));
			requestBodyMap.add("name", customerPrefix);
			requestBodyMap.add("password", UUID.randomUUID().toString());

			String hashValue = api250OkHelper.
					generateExternalIdForTenant(provisioningContext.getTenantId());
			logger.info("hash value : " + hashValue);
			requestBodyMap.add("external_id", hashValue);
		}
		addFeaturesToRequestBody(feature, provisioningContext, requestBodyMap, account);

		if (Objects.nonNull(account)) {
			requestBodyMap.add("id", account.getId());
			requestBodyMap.add("email", account.getEmail());
			requestBodyMap.add("name", account.getCompany());
			requestBodyMap.add("external_id", account.getExternal_id());
		}
		logger.info("provisioning 250ok bundle :" + feature.getCode() + "  for tenant" +
				provisioningContext.getTenantId() + "with params \n" + requestBodyMap.toString());

		httpMethod = account == null ? HttpMethod.POST : HttpMethod.PUT;
		api250OkHelper.provisionModulesForChildAccount(provisioningContext.getTenantId(), httpMethod, requestBodyMap, feature.getProvisioningURI());

		//
		MultiValueMap<String, String> data = new LinkedMultiValueMap<>();
		data.add("status","enable");
		try {
			subscriptionService.updateDeliverabilityPartnerEnabled(provisioningContext.getTenantId(), data);
		}
		catch (Exception e) {
			logger.error("Error enabling Deliverability Partner Tile ", e);
			throw new ProvisioningException(SubscriptionConstants.API250OK_MARKETO_UNABLE_TO_ENABLE_DELIVERABILITY_TOOLS_TILE, e);
		}
	}


	/****
	 *
	 * @param feature
	 * @param provisioningContext
	 */
	public void deprovision(MarketingFeature feature, ProvisioningContext provisioningContext) {

		ChildAccount account = fetchByMunchkinId(provisioningContext.getTenantId(), feature.getStatusURI());
		MultiValueMap<String, String> requestBodyMap = new LinkedMultiValueMap<String, String>();

		handleContextParam(provisioningContext,feature,SubscriptionConstants.API250OK_ACTION_DEPROVISION);

		if (Objects.isNull(account)) {
			logger.error(String.format("%s for munchkin id: %s", SubscriptionConstants.API250OK_CHILD_ACCOUNT_DOES_NOT_EXIST, provisioningContext.getTenantId()));
			throw new ProvisioningException(SubscriptionConstants.API250OK_CHILD_ACCOUNT_DOES_NOT_EXIST);
		} else {
			addFeaturesToRequestBody(feature, provisioningContext, requestBodyMap, account);
			requestBodyMap.add("id", account.getId());
			requestBodyMap.add("email", account.getEmail());
			requestBodyMap.add("name", account.getCompany());
			requestBodyMap.add("external_id", account.getExternal_id());

			logger.info(String.format("Deprovisioning 250ok bundle: %s for tenant: %s", feature.getCode(), provisioningContext.getTenantId()));

			api250OkHelper.provisionModulesForChildAccount(provisioningContext.getTenantId(), HttpMethod.PUT, requestBodyMap, feature.getUpdateURI());
		}

		subscriptionMetadataService.deleteMarketingFeature(provisioningContext.getTenantId(), feature.getId());

		//Fetch updated child account details
		ChildAccount updatedAccount = fetchByMunchkinId(provisioningContext.getTenantId(), feature.getStatusURI());

		//Verify if any 250ok features are active
		if(updatedAccount.isAny250okFeaturesActive()) return;

		//Deactivate Deliverability Tools tile, if no active 250ok features
		MultiValueMap<String, String> data = new LinkedMultiValueMap<>();
		data.add("status","disable");
		try {
			subscriptionService.updateDeliverabilityPartnerEnabled(provisioningContext.getTenantId(), data);
		}
		catch (Exception e) {
			logger.error("Error disabling Deliverability Partner Tile ", e);
			throw new ProvisioningException(SubscriptionConstants.API250OK_MARKETO_UNABLE_TO_DISABLE_DELIVERABILITY_TOOLS_TILE, e);
		}
	}

	/**
	 * Delete 250ok Child Account
	 *
	 * @param tenantUUID
	 */
	public void cancel250ok(String tenantUUID) {

		ChildAccount account = fetchByMunchkinId(tenantUUID, delChildAccountRelativeUrl);
		MultiValueMap<String, String> requestBodyMap = new LinkedMultiValueMap<String, String>();

		if (Objects.isNull(account)) {
			return;
		} else {
			requestBodyMap.add("id", account.getId());

			logger.info(String.format("Deleting 250ok Child Account: %s for tenant: %s", account.getId(), tenantUUID));

			api250OkHelper.provisionModulesForChildAccount(tenantUUID, HttpMethod.DELETE, requestBodyMap, delChildAccountRelativeUrl);
		}
	}

	/*****
	 *
	 * @param feature
	 * @param provisioningContext
	 */
	public void update(MarketingFeature feature, ProvisioningContext provisioningContext) {
		ChildAccount account = fetchByMunchkinId(provisioningContext.getTenantId(), feature.getStatusURI());
		MultiValueMap<String, String> requestBodyMap = new LinkedMultiValueMap<String, String>();

		handleContextParam(provisioningContext,feature,SubscriptionConstants.API250OK_ACTION_UPDATE);

		if (Objects.isNull(account)) {
			logger.error(SubscriptionConstants.API250OK_CHILD_ACCOUNT_DOES_NOT_EXIST + " for munchkin id: " + provisioningContext.getTenantId());
			throw new ProvisioningException(SubscriptionConstants.API250OK_CHILD_ACCOUNT_DOES_NOT_EXIST);
		} else {
			addFeaturesToRequestBody(feature, provisioningContext, requestBodyMap, account);
			requestBodyMap.add("id", account.getId());
			requestBodyMap.add("email", account.getEmail());
			requestBodyMap.add("name", account.getCompany());
			requestBodyMap.add("external_id", account.getExternal_id());

			logger.info("Update 250ok bundle: " + feature.getCode() + " for tenant: " + provisioningContext.getTenantId() + " with params: \n" + requestBodyMap.toString());

			api250OkHelper.provisionModulesForChildAccount(provisioningContext.getTenantId(), HttpMethod.PUT, requestBodyMap, feature.getUpdateURI());
		}


	}


	private void addFeaturesToRequestBody(MarketingFeature feature, ProvisioningContext provisioningContext,
	                                      MultiValueMap<String, String> requestBodyMap, ChildAccount account) {
		Map<String, JSONObject> featurePropertyMap = getFeatureMetadata(feature);
		JSONObject bundleConfig = new JSONObject();
		if (provisioningContext.getParams().getFirst("newBundle") != null) {
			bundleConfig = provisioningContext.getNewBundleConfig();
		} else if (provisioningContext.getParams().getFirst("bundle") != null) {
			net.sf.json.JSONArray jsonArray = new net.sf.json.JSONArray();
			String bundleStr = provisioningContext.getParams().getFirst("bundle");
			if (bundleStr != null && bundleStr.startsWith("[")) {
				jsonArray = net.sf.json.JSONArray.fromObject(bundleStr);
			} else {
				jsonArray.add(bundleStr);
			}
			String bundleKey = (String) jsonArray.get(0);
			bundleConfig = new JSONObject("{" + bundleKey + ":{\"quantity\":-1}}");
		}
		Set<String> keySet = featurePropertyMap.keySet();
		Object bundleKeyObject = bundleConfig.get(feature.getCode());
		keySet.forEach(s -> {

			if (Objects.nonNull(bundleKeyObject) && !bundleKeyObject.toString().isEmpty()
					&& !bundleKeyObject.toString().equalsIgnoreCase("null")) {
				JSONObject bundleKey = (JSONObject) bundleKeyObject;
				if (!bundleKey.has(s)) {
					JSONObject defaultConfig = featurePropertyMap.get(s);
					requestBodyMap.add(s, defaultConfig.get("value").toString());
				} else {
					Object newConfig = bundleKey.get(s);
					requestBodyMap.add(s, newConfig == null ? "" : newConfig.toString());
				}
			} else {
				JSONObject defaultConfig = featurePropertyMap.get(s);
				requestBodyMap.add(s, defaultConfig.get("value").toString());
			}

		});

		Integer quantity = Integer.parseInt(requestBodyMap.getFirst("quantity"));

		//configure modules
		if (feature.getCode().equalsIgnoreCase(SubscriptionConstants.API250OK_MARKETO_EMAIL_DELIVERABILITY)) {
			configureEmailDeliverability(requestBodyMap, account, quantity);
		} else if (feature.getCode().equalsIgnoreCase(SubscriptionConstants.API250OK_MARKETO_EMAIL_INFORMANT)) {
			configureEmailInformant(requestBodyMap, account, quantity);
		} else if (feature.getCode().equalsIgnoreCase(SubscriptionConstants.API250OK_MARKETO_EMAIL_REPUTATION)) {
			configureEmailReputation(requestBodyMap, account, quantity);
		}

		if (account != null) {
			copyBundleProperties(account, requestBodyMap);
		}
	}

	/****
	 *
	 * method to child account for given tenant/munchkin id
	 *
	 * @param munchkinId
	 * @param url
	 * @return
	 */
	ChildAccount fetchByMunchkinId(String munchkinId, String url) {
		ChildAccount account = null;
		ChildAccount[] accounts = api250OkHelper.fetchChildAccountForTenant(munchkinId, url);
		if (accounts != null && accounts.length == 1)
			account = accounts[0];
		return account;
	}

	/***
	 *
	 * @param marketingFeature
	 * @return
	 */
	Map<String, JSONObject> getFeatureMetadata(MarketingFeature marketingFeature) {

		String developerConfig = marketingFeature.getDeveloperConfig();
		org.json.JSONArray jsonObject = new JSONArray(developerConfig);
		Iterator<Object> itr = jsonObject.iterator();
		Map<String, JSONObject> featureFieldMap = new HashMap<>();
		while (itr.hasNext()) {
			JSONObject item = (JSONObject) itr.next();
			JSONObject field = (JSONObject) item.get("name");
			featureFieldMap.put(field.getString("field"), item);
		}
		return featureFieldMap;
	}

	/****
	 *
	 * @param requestBodyMap
	 * @param account
	 * @param quantity
	 */
	private void configureEmailDeliverability(MultiValueMap<String, String> requestBodyMap, ChildAccount account, Integer quantity) {
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_BLACKLIST, account == null ? NO : getValue(account.getHas_blacklistinformant()));
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_REPUTATION, account == null ? NO : getValue(account.getHas_reputation()));
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_DMARC, account == null ? NO : getValue(account.getHas_dmarc()));
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT, account == null ? NO : getValue(account.getHas_emailinformant()));
		if (quantity == -1) {
			requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_INBOX, NO);
			requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_DESIGN, NO);
			return;
		}
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_INBOX, YES);
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_DESIGN, YES);

		//setting quatity for emaildeliverability
		Integer max_inbox = Integer.parseInt(requestBodyMap.getFirst("max_inbox"));
		Integer max_design = Integer.parseInt(requestBodyMap.getFirst("max_design"));
		max_inbox = max_inbox * quantity;
		max_design = max_design * quantity;
		requestBodyMap.remove("max_inbox");
		requestBodyMap.remove("max_design");
		requestBodyMap.add("max_inbox", max_inbox.toString());
		requestBodyMap.add("max_design", max_design.toString());
	}

	/****
	 *
	 * @param requestBodyMap
	 * @param account
	 * @param quantity
	 */
	private void configureEmailInformant(MultiValueMap<String, String> requestBodyMap, ChildAccount account, Integer quantity) {
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_INBOX, account == null ? NO : getValue(account.getHas_inboxinformant()));
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_BLACKLIST, account == null ? NO : getValue(account.getHas_blacklistinformant()));
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_DESIGN, account == null ? NO : getValue(account.getHas_designinformant()));
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_REPUTATION, account == null ? NO : getValue(account.getHas_reputation()));
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_DMARC, account == null ? NO : getValue(account.getHas_dmarc()));
		if (quantity == -1) {
			requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT, NO);
			return;
		}
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT, YES);

		//setting quatity for email informant
		Integer max_emailinformant = Integer.parseInt(requestBodyMap.getFirst("max_emailinformant"));
		max_emailinformant = max_emailinformant * quantity;
		requestBodyMap.remove("max_emailinformant");
		requestBodyMap.add("max_emailinformant", max_emailinformant.toString());
	}

	/****
	 *
	 * @param requestBodyMap
	 * @param account
	 * @param quantity
	 */
	private void configureEmailReputation(MultiValueMap<String, String> requestBodyMap, ChildAccount account, Integer quantity) {
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_INBOX, account == null ? NO : getValue(account.getHas_inboxinformant()));
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_DESIGN, account == null ? NO : getValue(account.getHas_designinformant()));
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT, account == null ? NO : getValue(account.getHas_emailinformant()));
		if (quantity == -1) {
			requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_BLACKLIST, NO);
			requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_REPUTATION, NO);
			requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_DMARC, NO);
			return;
		}
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_BLACKLIST, YES);
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_REPUTATION, YES);
		requestBodyMap.add(SubscriptionConstants.API250OK_FIELD_DMARC, YES);

		//setting quatity for emailReputation
		Integer max_blacklist = Integer.parseInt(requestBodyMap.getFirst("max_blacklist"));
		Integer max_dmarc = Integer.parseInt(requestBodyMap.getFirst("max_dmarc"));
		max_blacklist = max_blacklist * quantity;
		max_dmarc = max_dmarc * quantity;

		requestBodyMap.remove("max_blacklist");
		requestBodyMap.remove("max_dmarc");
		requestBodyMap.add("max_blacklist", max_blacklist.toString());
		requestBodyMap.add("max_dmarc", max_dmarc.toString());
	}


	/**
	 * method to handle context param
	 *
	 * @param provisioningContext
	 * @param marketingFeature
	 * @param action
	 */
	private void handleContextParam(ProvisioningContext provisioningContext, MarketingFeature marketingFeature, String action) {

		if (provisioningContext.getParams() != null)
			return;

		//handle request from super admin UI
		MultiValueMap<String, String> requestBodyMap = new LinkedMultiValueMap<String, String>();

		switch (action) {
			case SubscriptionConstants.API250OK_ACTION_PROVISION:
			case SubscriptionConstants.API250OK_ACTION_UPDATE:
				String bundleConfig = "{'" + marketingFeature.getCode() + "':" + marketingFeature.getMarketingFeatureConfig() + "}";
				requestBodyMap.add("newBundle", bundleConfig);
				provisioningContext.parseNewBundleConfigFromParam(bundleConfig);
				break;
			case SubscriptionConstants.API250OK_ACTION_DEPROVISION:
				requestBodyMap.add("bundle", "[\""+ marketingFeature.getCode() + "\"]");
				break;
		}
		provisioningContext.setParams(requestBodyMap);
	}


	/***
	 *
	 * @param account
	 * @param featureCode
	 */
	private void copyBundleProperties(ChildAccount account, MultiValueMap<String, String> requestBodyMap) {
		api250OkHelper.getApi250okFields().forEach(field -> {
			if (field == SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT) {
				Integer emailInformantMax = Integer.parseInt(account.getEmailinformant_max());
				if (emailInformantMax > 0) {
					if (!requestBodyMap.containsKey("emailinformant_expiration"))
						requestBodyMap.add("emailinformant_expiration", account.getEmailinformant_expires());
					if (!requestBodyMap.containsKey("max_emailinformant"))
						requestBodyMap.add("max_emailinformant", account.getEmailinformant_max());
				}

			} else if (field == SubscriptionConstants.API250OK_FIELD_INBOX && getValue(account.getHas_inboxinformant()).equals(YES)) {

				if (!requestBodyMap.containsKey("inbox_expiration"))
					requestBodyMap.add("inbox_expiration", account.getInbox_expires());
				if (!requestBodyMap.containsKey("max_inbox"))
					requestBodyMap.add("max_inbox", account.getInbox_max());
				if (!requestBodyMap.containsKey("inbox_period"))
					requestBodyMap.add("inbox_period", "monthly");

			} else if (field == SubscriptionConstants.API250OK_FIELD_BLACKLIST && getValue(account.getHas_blacklistinformant()).equals(YES)) {

				if (!requestBodyMap.containsKey("blacklist_expiration"))
					requestBodyMap.add("blacklist_expiration", account.getBlacklist_expires());
				if (!requestBodyMap.containsKey("max_blacklist"))
					requestBodyMap.add("max_blacklist", account.getBlacklist_max());
			} else if (field == SubscriptionConstants.API250OK_FIELD_REPUTATION && getValue(account.getHas_reputation()).equals(YES)) {
				if (!requestBodyMap.containsKey("reputation_expiration"))
					requestBodyMap.add("reputation_expiration", account.getReputation_expires());
			} else if (field == SubscriptionConstants.API250OK_FIELD_DESIGN && getValue(account.getHas_designinformant()).equals(YES)) {
				if (!requestBodyMap.containsKey("design_expiration"))
					requestBodyMap.add("design_expiration", account.getDesign_expires());
				if (!requestBodyMap.containsKey("max_design"))
					requestBodyMap.add("max_design", account.getDesign_max());
			} else if (field == SubscriptionConstants.API250OK_FIELD_DMARC) {
				Integer dmarcMax = Integer.parseInt(account.getDmarc_max());
				if (dmarcMax > 0) {
					if (!requestBodyMap.containsKey("dmarc_expiration"))
						requestBodyMap.add("dmarc_expiration", account.getDmarc_expires());
					if (!requestBodyMap.containsKey("max_dmarc"))
						requestBodyMap.add("max_dmarc", account.getDmarc_max());
				}
			}


		});
	}

	/***
	 *
	 * @param in
	 * @return
	 */
	public String getValue(String in) {
		if (in == null || in.equalsIgnoreCase("no"))
			return "n";
		else if (in.equalsIgnoreCase("yes") || in.equalsIgnoreCase("y"))
			return "y";
		else
			return "n";
	}


}