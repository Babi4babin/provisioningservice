package com.marketo.provisioningservice.features;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.marketo.subscriptionservice.model.FeatureStatus;
import org.json.JSONObject;

public class FeatureResponse {

    private static final String SUCCESS_STATUS = "SUCCESS";
    
    private static final String ERROR_STATUS = "ERROR";
    
    private String status;
    private String message;
    private String errorCode;

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    
    @JsonIgnore
    public boolean isSuccess() {
        return SUCCESS_STATUS.equals(status);
    }

    @JsonIgnore
    public boolean isError() {
        return ERROR_STATUS.equals(status);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getInfo() {
        JSONObject obj = new JSONObject();
        if (isError()) {
            obj.put("status", FeatureStatus.FAILED.toString());
        } else {
            obj.put("status", FeatureStatus.COMPLETED.toString());
        }
        return obj.toString();
    }
}
