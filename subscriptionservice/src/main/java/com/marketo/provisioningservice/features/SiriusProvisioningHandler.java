package com.marketo.provisioningservice.features;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.common.ObjectMapperFactory;
import com.marketo.provisioningservice.service.FeatureLocatorService;
import com.marketo.provisioningservice.service.Operation;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.model.FeatureStatus;
import com.marketo.subscriptionservice.model.SubscriptionFeatureInfo;
import com.marketo.subscriptionservice.model.sirius.*;
import com.marketo.subscriptionservice.rest.RestClient;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Created by asawhney on 3/6/17.
 */
@Component
public class SiriusProvisioningHandler {

    @Autowired
    private RestClient restClient;

    @Autowired
    FeatureLocatorService subFeatureLocator;

    @Autowired
    private SubscriptionMetadataService subscriptionMetadataService;

    private static final Logger logger = LoggerFactory.getLogger(SiriusProvisioningHandler.class);

    private static final String ORION_BASE_SCENARIO = "orion_base";
    private static final String ORION_BASE_FEATURE_CODE = "orionBase";
    private static final String ORION_DESCRIPTION = "Orion Base Provision";
    private static final String ORION_PROVISIONING_USER = "bbconsole";
    private static final String ORION_COMPONENT = "scenarios";
    private static final String SIRIUS_STATUS_CHANGE_URI = "/rest/v2/backlog/jobs";
    private static final String ORION_BASE_DEPROVISION_FOR_CANCELLED = "orion_base_deprovision_for_cancelled";
    private static final String ORION_BASE_DEPROVISION_FOR_CANCELLED_DORMANT = "orion_base_deprovision_for_cancelled_dormant";
    private static final String ORION_BASE_REACTIVATION_FOR_CANCELLED_DORMANT = "orion_base_reactivation_for_cancelled_dormant";

    @Value("${orion.auth.token}")
    private String ORION_AUTH_TOKEN;
    private static final String SIRIUS_HOST = "https://{dc}-bbconsole-ws.marketo.org/console";


    public void doOperation(MarketingFeature feature, Operation operation, ProvisioningContext provisioningContext) {
        switch (operation) {
            case PROVISION:
                provision(feature, provisioningContext);
                break;
        }
    }

    /*
     * This Method will provision Sirius Components
     */
    public void provision(MarketingFeature feature, ProvisioningContext provisioningContext) {

        String mlmInternalEndPoint = getFeatureLocator(feature, provisioningContext);
        String payload = createPayload(feature, provisioningContext, mlmInternalEndPoint);

        String url = buildSiriusUrl(feature.getProvisioningURI(), provisioningContext.getTenantId(), null, provisioningContext.getActivePod(), null);

        logger.info("About to provision Sirius Feature: " + feature.getName());

        String response = postRestClient(url, payload);

        SiriusResponse siriusResponse = parseProvisioningResponse(response);
        subscriptionMetadataService.addMarketingFeature(provisioningContext.getTenantId(),
                feature, siriusResponse.getBacklogJob().getInfo());

    }

    /*
     * This Method will retreive featue status by calling sirius rest api.
     */
    public String getFeatureStatus(String tenantUUID, MarketingFeature feature, String jobId, String datacenter) {

        String url = buildSiriusUrl(feature.getStatusURI(), tenantUUID, jobId, null, datacenter);
        String response = getRestClient(url);

        SiriusResponse siriusResponse = parseProvisioningResponse(response);

        String status = siriusResponse.getBacklogJob().getStatus();
        logger.info("Sirius Feature Status: " + status + " for backlogId: " +
                siriusResponse.getBacklogJob().getBacklogId() + " for feature code " + feature.getCode());

        if (status.equalsIgnoreCase(SiriusJobStatus.SUCCEEDED.toString())) {
            subscriptionMetadataService.addMarketingFeature(tenantUUID, feature,
                    getInfo(SiriusJobStatus.SUCCEEDED.toString(), jobId));
            return FeatureStatus.COMPLETED.toString();
        }

        if (status.equalsIgnoreCase(SiriusJobStatus.FAILED.toString()) || status.equalsIgnoreCase(SiriusJobStatus.KILLED.toString())
                || status.equalsIgnoreCase(SiriusJobStatus.SUSPENDED.toString())) {
            return FeatureStatus.FAILED.toString();
        }

        return FeatureStatus.PROGRESS.toString();
    }

    /**
     * Change the sirius status to cancel dormant
     *
     * @param tenantUUID
     * @param podLocation
     */
    public boolean cancelDormantSirius(String tenantUUID, String podLocation) {
        logger.info("Starting sirius status change to Cancel Dormant: " + tenantUUID);
        String url = buildSiriusUrl(SIRIUS_STATUS_CHANGE_URI, tenantUUID, null, podLocation, null);
        String payload = createPayloadForChangeStatus(tenantUUID, ORION_BASE_DEPROVISION_FOR_CANCELLED_DORMANT);
        String response = postRestClient(url, payload);
        return validateSiriusStatusChangeResponse(response, tenantUUID);
    }

    /**
     * Change the sirius status to cancel
     *
     * @param tenantUUID
     * @param podLocation
     */
    public boolean cancelSirius(String tenantUUID, String podLocation) {
        logger.info("Starting sirius status change to Cancel: " + tenantUUID);
        String url = buildSiriusUrl(SIRIUS_STATUS_CHANGE_URI, tenantUUID, null, podLocation, null);
        String payload = createPayloadForChangeStatus(tenantUUID, ORION_BASE_DEPROVISION_FOR_CANCELLED);
        String response = postRestClient(url, payload);
        return validateSiriusStatusChangeResponse(response, tenantUUID);
    }

    /**
     * Change the sirius status to activate
     *
     * @param tenantUUID
     * @param podLocation
     */
    public boolean reactivateSirius(String tenantUUID, String podLocation) {
        logger.info("Starting sirius status change to Activate: " + tenantUUID);
        String url = buildSiriusUrl(SIRIUS_STATUS_CHANGE_URI, tenantUUID, null, podLocation, null);
        String payload = createPayloadForChangeStatus(tenantUUID, ORION_BASE_REACTIVATION_FOR_CANCELLED_DORMANT);
        String response = postRestClient(url, payload);
        return validateSiriusStatusChangeResponse(response, tenantUUID);
    }

    /**
     * Create payload for cancel, cancel_dormant, reactivate
     * @param tenantUUID
     * @return
     */
    protected String createPayloadForChangeStatus(String tenantUUID, String actionName) {
        String NONE = "none";
        SiriusProperties properties = new SiriusProperties.Builder()
                .munchkinId(tenantUUID).mlmInternalEndPoint(NONE)
                .podName(NONE).leadDbSize(NONE).build();

        SiriusAction action = new SiriusAction.Builder()
                .properties(properties).name(actionName).build();

        SiriusJob job = new SiriusJob.Builder().action(action).component(getComponent(ORION_BASE_FEATURE_CODE))
                .user(getUser(ORION_BASE_FEATURE_CODE)).build();

        SiriusRequest request = new SiriusRequest.Builder().job(job).build();

        ObjectMapper mapper = ObjectMapperFactory.getInstance();
        mapper.setVisibility(PropertyAccessor.FIELD,
                JsonAutoDetect.Visibility.ANY);
        try {
            String requestBody = mapper.writeValueAsString(request);
            logger.info("Sirius " + actionName + " Payload Sirius : " + requestBody);
            return requestBody;
        } catch (JsonProcessingException e) {
            throw new ProvisioningException(e.getMessage());
        }
    }


    /*
     * Create Payload for Sirius Console
     */
    protected String createPayload(MarketingFeature feature, ProvisioningContext provisioningContext, String mlmInternalEndPoint) {

        String leadDbSize = null;
        try {
            leadDbSize = provisioningContext.getParams().get("leadDbSize").get(0);
        } catch (NullPointerException e) {
            throw new ProvisioningException("Lead DB Size Cannot be Null");
        }

        SiriusProperties properties = new SiriusProperties.Builder()
                .munchkinId(provisioningContext.getTenantId()).mlmInternalEndPoint(mlmInternalEndPoint)
                .podName(provisioningContext.getActivePod()).leadDbSize(leadDbSize).build();

        SiriusAction action = new SiriusAction.Builder()
                .properties(properties).name(getScenario(feature.getCode())).build();

        SiriusJob job = new SiriusJob.Builder().action(action).component(getComponent(feature.getCode()))
                .description(getDescription(feature.getCode())).user(getUser(feature.getCode())).build();

        SiriusRequest request = new SiriusRequest.Builder().job(job).build();

        ObjectMapper mapper = ObjectMapperFactory.getInstance();
        mapper.setVisibility(PropertyAccessor.FIELD,
                JsonAutoDetect.Visibility.ANY);
        try {
            String requestBody = mapper.writeValueAsString(request);
            logger.info("Create Payload Sirius : " + requestBody);
            return requestBody;
        } catch (JsonProcessingException e) {
            throw new ProvisioningException(e.getMessage());
        }
    }

    /*
     * This Method will parse provisioning response and check the status is valid or not
     */
    protected boolean validateSiriusStatusChangeResponse(String response, String tenantUUID) {

        SiriusResponse siriusResponse = parseProvisioningResponse(response);
        String status = siriusResponse.getBacklogJob().getStatus();

        if (status.equalsIgnoreCase(SiriusJobStatus.SUCCEEDED.toString())
                || status.equalsIgnoreCase(SiriusJobStatus.SUBMITTED.toString())
                || status.equalsIgnoreCase(SiriusJobStatus.RUNNING.toString())
                || status.equalsIgnoreCase(SiriusJobStatus.BACKLOG.toString())) {
            return true;
        } else {
            logger.info("Subscription status change in sirius fails for subscription " + tenantUUID + " with the response " + response);
            throw new ProvisioningException("Subscription status change in sirius fails for subscription " + tenantUUID);
        }
    }

    /*
     * This Method will parse provisioning response and return Sirius Response Object
     */
    protected SiriusResponse parseProvisioningResponse(String response) {

        if (StringUtils.isEmpty(response)) {
            throw new ProvisioningException("Empty response recieved from Sirius feature ");
        }

        ObjectMapper mapper = ObjectMapperFactory.getInstance();

        if (StringUtils.isEmpty(response)) {
            throw new ProvisioningException("Null Response Recieved from feature handler");
        }

        SiriusResponse siriusResponse = null;
        try {
            siriusResponse = mapper.readValue(response, SiriusResponse.class);
        } catch (Exception e) {
            throw new ProvisioningException(e);
        }

        if (!siriusResponse.getSuccess()) {
            throw new ProvisioningException("Sirius Provisioning Failed");
        }

        return siriusResponse;
    }

    /**
     * Get Feature Locator
     *
     * @param feature
     * @return
     */
    private String getFeatureLocator(MarketingFeature feature, ProvisioningContext provisioningContext) {

        String uri = subFeatureLocator.getSubscriptionFeatureLocation(provisioningContext.getTenantId(), feature);
        logger.info("Feature location call URL: " + uri);
        if (StringUtils.isEmpty(uri) && !StringUtils.isEmpty(provisioningContext.getActivePod())) {
            uri = subFeatureLocator.getPodFeatureLocation(provisioningContext.getActivePod(), feature, provisioningContext.getTenantId());
            logger.info("Get URL from Pod Feature location call URL: " + uri);
        }

        if (StringUtils.isEmpty(uri)) {
            throw new ProvisioningException("Unable to find location for the feature: " + feature.getName());
        }

        return uri;
    }

    /*
     * This Method will build Sirius Url
     */
    private String buildSiriusUrl(String operationUri, String tenantId, String backlogId, String pod, String datacenter) {

        String dc = subFeatureLocator.getSubscriptionDC(tenantId).toLowerCase();
        if (StringUtils.isEmpty(dc) && pod != null) {
            dc = subFeatureLocator.getDCFeatureLocation(pod);
        }
        if (StringUtils.isEmpty(dc) && datacenter != null) {
            dc = datacenter;
        }
        if (StringUtils.isEmpty(dc)) {
            throw new ProvisioningException("Datacenter cannot be null for orion status call. Please send datacenter as url parameter");
        }
        String host = SIRIUS_HOST;
        host = host.replace("{dc}", dc);

        StringBuffer sb = new StringBuffer();
        sb.append(host).append(operationUri);
        String url = sb.toString();
        // If the url has {tenantUUID} replace with it actual tenantId
        // This because our internal rest api expects tenantId in the endpoint path
        if (url.contains("{backlogId}")) {
            url = url.replace("{backlogId}", backlogId);
        }
        logger.info("Sirius Url: " + url);

        return url;
    }

    /*
     * Get Scenario for Sirius Console
     */
    private String getScenario(String code) {
        if (code.equalsIgnoreCase(ORION_BASE_FEATURE_CODE))
            return ORION_BASE_SCENARIO;

        return null;
    }

    /*
     * Get Description for Sirius Console
     */
    private String getDescription(String code) {
        if (code.equalsIgnoreCase(ORION_BASE_FEATURE_CODE))
            return ORION_DESCRIPTION;
        return null;
    }

    /*
     * Get Component for Sirius Console
     */
    private String getComponent(String code) {
        if (code.equalsIgnoreCase(ORION_BASE_FEATURE_CODE))
            return ORION_COMPONENT;

        return ORION_COMPONENT;
    }

    /*
     * Get User for Sirius Console
     */
    private String getUser(String code) {
        if (code.equalsIgnoreCase(ORION_BASE_FEATURE_CODE))
            return ORION_PROVISIONING_USER;

        return null;
    }

    /**
     *
     * Get Rest Clinet
     * @return
     */
    private String getRestClient(String url) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
        return restClient.getJson(url, headers);
    }

    /**
     * Post Rest Client
     *
     * @return
     */
    private String postRestClient(String url, String data) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
        return restClient.postJson(url, data, headers);
    }

    private String getInfo(String status, String backlogId) {
        JSONObject obj = new JSONObject();
        obj.put("status", status);
        obj.put("backlogId", backlogId);
        return obj.toString();
    }

}