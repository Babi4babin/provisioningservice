package com.marketo.provisioningservice.features;

/**
 * This class defines all the possible config codes for all
 * the modules. These are used in SpaToProvisionConverter class
 * Created by rmalani on 5/24/17.
 */
public class ConfigCodeConstants {
    // baseModule configs codes
    public static final String SUBSCRIPTION_TYPE_CODE = "subscriptionTypeCode";
    public static final String DEDICATED_SENDING_IPS = "dedicatedSendingIPs";
    public static final String DELIVERABILITY_PARTNER_ENABLED = "deliverabilityPartnerEnabled";
    public static final String WEB_SERVICE_DAILY_QUOTA = "webServiceDailyQuota";
    public static final String DELIVERABILITY_PARTNER_USER = "deliverabilityPartnerUser";
    public static final String DELIVERABILITY_PARTNER_GROUP = "deliverabilityPartnerGroup";
    public static final String MK_DENIAL_ENABLED = "mkDenialEnabled";
    public static final String MTA_OPTIONS = "mtaOptions";
    public static final String NURTURE_PROGRAM_LIMIT = "nurtureProgramLimit";
    public static final String NURTURE_TRACKS_LIMIT = "nurtureTracksLimit";
    public static final String MAX_REPORTS_PER_SUB = "maxReportsPerSub";
    public static final String MAX_RECORDS_PER_REPORT = "maxRecordsPerReport";
    public static final String MAX_CUSTOM_ACTIVITY = "maxCustomActivity";
    public static final String MAX_CUSTOM_ACTIVITY_FIELDS = "maxCustomActivityFields";

    // searchEngineOptimization config codes
    public static final String SKU = "sku";

    // lifeCycleModeler config codes
    public static final String RCM_LIMIT = "rcmLimit";

    // marketingCalendar
    public static final String CALENDAR_SEATS = "calendarSeats";

    // salesInsight
    public static final String SIS_SEATS = "sisSeats";

    // salesInsight and emailPlugin common config code
    public static final String LISS_SEATS = "lissSeats";

    // customObjects config codes
    public static final String MAX_CUSTOM_OBJECTS = "maxCustomObjects";
    public static final String MAX_CUSTOM_OBJECT_FIELDS = "maxCustomObjectFields";
    public static final String MAX_CUSTOM_OBJECT_DATA = "maxCustomObjectData";

    // accountBasedMarketingCore config codes
    public static final String PAID_SEATS = "paidSeats";
    public static final String ACCOUNTS_LIMIT = "accountsLimit";

}
