package com.marketo.provisioningservice.service;

import java.util.List;
import java.util.regex.Pattern;

import com.marketo.subscriptionservice.model.NonProdDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.locatorservice.client.SubscriptionService;
import com.marketo.locatorservice.client.SubscriptionServiceManager;
import com.marketo.locatorservice.entities.ServiceId;
import com.marketo.locatorservice.entities.SubscriptionId;
import com.marketo.locatorservice.entities.SubscriptionPodInfo;
import com.marketo.locatorservice.entities.SubscriptionServiceId;
import com.marketo.locatorservice.entities.enums.SubscriptionServiceType;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;

/*
 * Wrapper over locator subscription service manager
 */
@Component
public class FeatureLocatorService {

    private static final Logger logger = LoggerFactory.getLogger(FeatureLocatorService.class);
    
    @Autowired
    private SubscriptionServiceManager subscriptionServiceManager;
               
    /**
     * If serviceid is mlm then just get the pod info and return the internal api url,
     * else get the cluster and assume the metadata has "internal_rest_endpoint_host" as key
     * @param tenantId
     * @param feature
     * @return
     */
    public String getSubscriptionFeatureLocation(String tenantId, MarketingFeature feature) {
        
        String serviceId = feature.getLocatorServiceId();
        logger.info("Service id: " + serviceId);
        
        String location = null;
        try {
        
            // Currently we do not have common interface to get cluster, given serviceid/subscriptionid
            if ("mlmpod".equalsIgnoreCase(serviceId)) {
                SubscriptionPodInfo podInfo = subscriptionServiceManager.
                        getSubscriptionPodInfo(new SubscriptionId(tenantId));
                logger.info("Internal API URL found: " + podInfo.getInternalApiUrl() + " for feature: " + feature.getName() + " for Subscription: " +tenantId);
                return podInfo.getInternalApiUrl();
            } else {
                
                SubscriptionServiceId subServiceId = new SubscriptionServiceId(new SubscriptionId(tenantId), new ServiceId(serviceId), SubscriptionServiceType.PRIMARY);
                
                SubscriptionService subService = subscriptionServiceManager.getSubscriptionService(subServiceId);
                
                if (subService != null) {
                    location = subService.getCluster().getMetaData("internal_rest_endpoint_host").getValue();
                    logger.info("internal_rest_endpoint_host: " + location + " for feature: " + feature.getName() + " for Subscription: " +tenantId);
                    return location;
                }
            }
        } catch(Exception e) {
            logger.error("Failed retrieving location for feature: " + feature.getName() + " for Subscription: " + tenantId + " Exception: " + e.getMessage());
        }
        
        return location;
    }

    /*
     * This Method will retreive Subscription Data Center.
     */
    public String getSubscriptionDC(String tenantId) {

        String dc = "";

        try {
            SubscriptionPodInfo podInfo = subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId(tenantId));
            String dataCenterId = podInfo.getDataCenterId().getDataCenterId();
            dc = getDC(dataCenterId, podInfo.getPodName());
        } catch(Exception e) {
            logger.error("Failed retrieving datacenter for Subscription: " + tenantId);
        }
        return dc;
    }

    /**
     *
     * @param pod
     * @return
     */
    public String getDCFeatureLocation(String pod) {

        String dc = null;

        try {
            List<SubscriptionPodInfo> pods = subscriptionServiceManager.getAllSubscriptionPodInfo();
            for (SubscriptionPodInfo podInfo : pods) {
                if (pod.equals(podInfo.getPodName())) {
                    String dataCenterId = podInfo.getDataCenterId().getDataCenterId();
                    dc = getDC(dataCenterId, podInfo.getPodName());
                }
            }

            return dc;
        } catch (Exception e) {
            logger.error("Failed retrieving datacenter for pod: " + pod);
        }

        return dc;
    }

    /**
     *
     * @param dc
     * @param podName
     * @return
     */
    private String getDC(String dc, String podName) {

        if (dc.equalsIgnoreCase(NonProdDC.SJ.toString())) {

            if (podName.matches("(?i)" + Pattern.quote(NonProdDC.SJINT.toString()) + "\\d?")
                || podName.matches("(?i)" + Pattern.quote(NonProdDC.GUSW1INT.toString()) + "\\d{3}"))
              return NonProdDC.INT.toString();

            if (podName.matches("(?i)" + Pattern.quote(NonProdDC.SJQE.toString()) + "\\d?")
                || podName.matches("(?i)" + Pattern.quote(NonProdDC.GUSW1QE.toString()) + "\\d{3}"))
              return NonProdDC.QE.toString();

            if (podName.equalsIgnoreCase(NonProdDC.SJST.toString())
                || podName.matches("(?i)" + Pattern.quote(NonProdDC.GUSW1ST.toString()) + "\\d{3}"))
              return NonProdDC.ST.toString();
        }

        if (dc.equalsIgnoreCase(NonProdDC.GUSW1.toString())) {

            if (podName.matches("(?i)" + Pattern.quote(NonProdDC.GUSW1INT.toString()) + "\\d{3}"))
                return NonProdDC.GUSW1INT.toString();

            if (podName.matches("(?i)" + Pattern.quote(NonProdDC.GUSW1QE.toString()) + "\\d{3}"))
                return NonProdDC.GUSW1QE.toString();

            if (podName.matches("(?i)" + Pattern.quote(NonProdDC.GUSW1ST.toString()) + "\\d{3}"))
                return NonProdDC.GUSW1ST.toString();
        }


        return dc;
    }
    
    /**
     * 
     * @param pod
     * @param feature
     * @param tenantId
     * @return
     */
    public String getPodFeatureLocation(String pod, MarketingFeature feature, String tenantId) {
        
        String location = null;
        
        try {
            List<SubscriptionPodInfo> pods = subscriptionServiceManager.getAllSubscriptionPodInfo();
            String internalApiUrl = null;
            for (SubscriptionPodInfo podInfo : pods) {
                if (pod.equals(podInfo.getPodName())) {
                    internalApiUrl = podInfo.getInternalApiUrl();
                    logger.info("Internal api url : " + internalApiUrl + " for feature: " + (feature != null ? feature.getName() : "subscriptionActivate") + " for subscription: " + tenantId);
                }
            }

            if (StringUtils.isEmpty(internalApiUrl)) {
                throw new ProvisioningException("Internal API URL not found for Pod: " + pod);
            }            
            
            return internalApiUrl;
        } catch (Exception e) {
            logger.error("Failed retrieving location for feature: " + (feature != null ? feature.getName() : "subscriptionActivate") + " for pod: " + pod + " Exception: " + e.getMessage());
        }
        
        return location;    
    }
    
}
