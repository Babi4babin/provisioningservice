package com.marketo.provisioningservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.marketo.bundlemetadata.dao.BundleMetadataDAO;
import com.marketo.bundlemetadata.exception.BundleMetadataException;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundles;
import com.marketo.common.ObjectMapperFactory;
import com.marketo.provisioningservice.features.MLMProvisioningHandler;
import com.marketo.provisioningservice.features.RCAProvisioningHandler;
import com.marketo.provisioningservice.features.SiriusProvisioningHandler;
import com.marketo.provisioningservice.helper.API250OkHelper;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import com.marketo.subscriptionservice.model.*;
import com.marketo.subscriptionservice.model.sirius.SiriusJobStatus;
import com.marketo.subscriptionservice.utility.SubscriptionConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import net.sf.json.JSONArray;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.rest.model.BundleDiffInfo;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundleFeatures;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.bundlemetadata.service.MarketingFeatureConfigurator;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.ProvisioningResponse;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;
import com.marketo.subscriptionservice.rest.exception.ModuleNotFoundException;

import java.util.*;

@Component
public class ProvisioningService {

    private static final Logger logger = LoggerFactory.getLogger(ProvisioningService.class);

    @Autowired
    private BundleMetadataService bundleMetadataService;

    @Autowired
    private BundleProvisioningService bundleProvisioningService;

    @Autowired
    private FeatureProvisioningService featureProvisioningService;

    @Autowired
    private ProvisioningContext provisioningContext;

    @Autowired
    private SubscriptionMetadataService subscriptionMetadataService;

    @Autowired
    private MarketingFeatureConfigurator featureConfigurator;

    @Autowired
    private SubscriptionMetadataDAO subscriptionMetadataDAO;

    @Autowired
    private BundleMetadataDAO bundleMetadataDAO;

    @Autowired
    private MLMProvisioningHandler mlmFeatureHandler;

    @Autowired
    private SiriusProvisioningHandler siriusProvisioningHandler;

    @Autowired
    private RCAProvisioningHandler rcaProvisioningHandler;

    @Autowired
    private API250OkHelper api250OkHelper;

    /**
     * Provision a subscription
     * @param configParams
     * @throws ProvisioningException
     */
    public ProvisioningResponse provisionSubscription(MultiValueMap<String, String> configParams) {

        // Look for token in params
        String token = configParams.getFirst("token");
        if (StringUtils.isEmpty(token)) {
            throw new ProvisioningInputException("Missing Token");
        }

        provisioningContext.setParams(configParams);
        if (provisioningContext.getParams().get("mlm") != null) {
            provisioningContext.setProvisionSource(ProvisionSource.MLM.toString());
        }


        // retrieve the bundle name from here
        String bundle = configParams.getFirst("bundle");
        if (StringUtils.isEmpty(bundle)) {
            throw new ProvisioningInputException("Bundle name not found in the parameters");
        }

        MarketingBundle marketingBundle = bundleMetadataService.getMarketingBundle(bundle);
        if (marketingBundle == null) {
            throw new ProvisioningInputException("Bundle " + bundle + " not found in the bundle metadata");
        }


        MarketingBundleFeature baseBundleFeature = marketingBundle.getChildBundleFeatures().get(0);
        MarketingFeature feature = baseBundleFeature.getFeature();
        feature.setMarketingFeatureConfig(baseBundleFeature.getMarketingFeatureConfig());

        mlmFeatureHandler.provision(feature, provisioningContext);
        return new ProvisioningResponse(provisioningContext.getTenantId());
    }


    /**
     * Rubicks - Provision a subscription
     * @param configParams
     * @throws ProvisioningException
     */
    public ProvisioningResponse newProvisionSubscription(MultiValueMap<String, String> configParams) {

        // Look for token in params
        String token = configParams.getFirst("token");
        if (StringUtils.isEmpty(token)) {
            throw new ProvisioningInputException("Missing Token");
        }
        String sbCopy = configParams.getFirst("sandboxCopyEnable");
        boolean sandboxCopyEnable = Boolean.parseBoolean(sbCopy) || sbCopy.equals("1");

        provisioningContext.setParams(configParams);
        if (provisioningContext.getParams().get("mlm") != null) {
            provisioningContext.setProvisionSource(ProvisionSource.MLM.toString());
        }

        JSONArray modules = parseModules(configParams);

        if (modules.indexOf("baseModule") != 0) {
            throw new ProvisioningInputException("Base Module (baseModule) should be included and should be the first in the list, parameters: " + configParams);
        }

        logger.info("Provision Rubiks subscription, context" + provisioningContext);

        MarketingBundle baseMarketingBundle = bundleMetadataService.getMarketingBundleByCode((String) modules.get(0));
        MarketingBundleFeature baseBundleFeature = baseMarketingBundle.getChildBundleFeatures().get(0);
        MarketingFeature feature = baseBundleFeature.getFeature();
        feature.setMarketingFeatureConfig(baseBundleFeature.getMarketingFeatureConfig());

        if (sandboxCopyEnable) {
            String sandBoxCopySrc = "";
            sandBoxCopySrc = (configParams.containsKey("sandBoxCopySrc") ? configParams.getFirst("sandBoxCopySrc") : sandBoxCopySrc);
            configureSandboxCopy(baseBundleFeature, modules, sandBoxCopySrc);
            feature.setMarketingFeatureConfig(baseBundleFeature.getMarketingFeatureConfig());
        }
        // Override the default base feature configuration.
        bundleProvisioningService.overrideDefaultFeatureConfig(feature, baseMarketingBundle, provisioningContext);
        baseBundleFeature.setMarketingFeatureConfig(feature.getMarketingFeatureConfig());
        mlmFeatureHandler.provision(feature, provisioningContext);

        return new ProvisioningResponse(provisioningContext.getTenantId());
    }


    /**
     * V2 & Rubicks - Confirming subscription created and base feature provisioned in MLM, time to provision rest of the features.
     * @param tenantUUID
     * @param configParams
     * @throws ProvisioningException
     */
    public ProvisioningResponse doPostSubscriptionCreated(String tenantUUID, MultiValueMap<String, String> configParams) {

        provisioningContext.setParams(configParams);
        provisioningContext.setTenantId(tenantUUID);

        if (provisioningContext.getParams().get("mlm") != null) {
            logger.info("Provisioning Source is MLM");
            provisioningContext.setProvisionSource(ProvisionSource.MLM.toString());
        }

        String activePod = configParams.get("activePod").get(0);
        // add the active pod to the context.
        provisioningContext.setActivePod(activePod);

        logger.info("Confirming Subscription creation. Context" + provisioningContext);

        JSONArray modules = parseModules(configParams);
        Integer errors;
        for (Object obj : modules) {
            String bundleKey = (String) obj;
            if(StringUtils.isEmpty(bundleKey)) {
                logger.info("Skipping empty bundle code, tenantUUID: " + provisioningContext.getTenantId());
                continue;
            }
            provisioningContext.addExecutedStep(bundleKey, ModuleStatus.STARTED.toString(), null);
            MarketingBundle marketingBundle = bundleMetadataDAO.getMarketingBundleByCode(bundleKey);
            if (marketingBundle == null) {
                marketingBundle = bundleMetadataDAO.getMarketingBundle(bundleKey);
                if (marketingBundle == null) {
                    String msg = "Bundle code (" + bundleKey + ") not found in the bundle metadata - skipped!!!";
                    provisioningContext.addError(new ProvisioningException(msg));
                    logger.info(msg);
                    provisioningContext.addExecutedStep(bundleKey, ModuleStatus.FAILURE.toString(), "Module Code not found in bundle metadata");
                    continue;
                }
            }

            /*
             * Bundle records if already exists in subscription bundle map then it's confirming the provision was fine.
             * so only retry to provision the modules and sub features if no map record exist
             */
            MarketingFeature feature = marketingBundle.getChildBundleFeatures().get(0).getFeature();
            if (feature.getName().equals(featureProvisioningService.BASE_FEATURE)) {
                // Save base config in bundle subscription map
                feature.setMarketingFeatureConfig(configParams.getFirst("feature_config"));
                List<MarketingBundleFeature> bundleFeatures = new ArrayList<>(marketingBundle.getChildBundleFeatures());
                // Remove base feature as it's already provisioned in MLM
                bundleFeatures.remove(0);
                marketingBundle.setChildBundleFeatures(bundleFeatures);
                provisionBundle(marketingBundle, provisioningContext);
                subscriptionMetadataService.addMarketingBundle(tenantUUID, marketingBundle);
                subscriptionMetadataService.addMarketingFeature(tenantUUID, feature);
                provisioningContext.addExecutedStep(bundleKey, ModuleStatus.SUCCESS.toString(), null);
            }
            else {
                errors = provisioningContext.getErrors().size();
                provisionBundle(marketingBundle, provisioningContext);
                if (provisioningContext.getErrors().size() == errors) {
                    subscriptionMetadataService.addMarketingBundle(tenantUUID, marketingBundle);
                    for (MarketingBundleFeature bundleFeature : marketingBundle.getChildBundleFeatures()) {
                        if (!StringUtils.isEmpty(bundleFeature.getFeature().getMarketingFeatureConfig())) {
                            subscriptionMetadataService.addMarketingFeature(tenantUUID, bundleFeature.getFeature());
                        }
                    }
                    provisioningContext.addExecutedStep(bundleKey, ModuleStatus.SUCCESS.toString(), null);
                }
                else {
                    try {
                        provisioningContext.addExecutedStep(bundleKey, ModuleStatus.FAILURE.toString(), "Check error section");
                        // Just to call MLM and disable the features
                        deprovisionBundle(marketingBundle, provisioningContext);
                    }
                    catch (Exception e) {
                        logger.info("Bundle deprovisioning failed with error msg: " + e.getMessage());
                    }
                }
            }
        }

        if (provisioningContext.getErrors().size() > 0) {
            throw new ProvisioningException(provisioningContext.getFormattedErrors() + " " + provisioningContext.getFormattedExecutedSteps());
        }

        return new ProvisioningResponse(provisioningContext.getTenantId());
    }

    public boolean isBundleRequirePodSelection(String bundleCode) {
        List <String> modulesRequirePodSelection =  Arrays.asList("marketingPerformanceInsights","programAnalysis", "advancedReportBuilder", "lifeCycleModeler",
                "searchEngineOptimization", "webPersonalization", "accountBasedMarketing", "predictiveContent", "websiteRetargeting", "contentAiAnalytics");

        return modulesRequirePodSelection.contains(bundleCode);
    }

    // Temp workaround till we get the Sandbox copy tool support Rubiks modules as of now it's not possible to copy from multiple sources
    // so we need to give one source for different cases as details in the story.
    private void configureSandboxCopy (MarketingBundleFeature baseBundleFeature, JSONArray modules, String sandBoxCopySrc) {
        boolean hasSalesInsight =  modules.contains("salesInsight"),
                hasLifeCycleModeler =  modules.contains("lifeCycleModeler"),
                hasABM = modules.contains("accountBasedMarketing") ? modules.contains("accountBasedMarketing") : modules.contains("accountBasedMarketingCore");

        if (StringUtils.isEmpty(sandBoxCopySrc) || sandBoxCopySrc == null) {
            if (hasABM && hasSalesInsight && hasLifeCycleModeler) {
                sandBoxCopySrc = "seededtier3";
            } else if (hasABM && hasLifeCycleModeler) {
                sandBoxCopySrc = "seededtier2";
            } else if (hasLifeCycleModeler) {
                sandBoxCopySrc = "seededtier1";
            } else {
                sandBoxCopySrc = "seededtier0";
            }
        }

        JSONObject featureConfig = new JSONObject(baseBundleFeature.getMarketingFeatureConfig());
        featureConfig.put("sandboxCopyFrom", sandBoxCopySrc);
        baseBundleFeature.setMarketingFeatureConfig(featureConfig.toString());
        logger.info("Configured : sandboxCopyFrom= " + sandBoxCopySrc);
    }

    /**
     * TODO - Remove after all customer migrated to Rubiks - Module packaging
     * Provision a bundle
     * @param tenantId
     * @param name
     * @param token
     */
    public void provisionBundle(String tenantId, String name, String token) {

        MarketingBundle marketingBundle = bundleMetadataService.getMarketingBundle(name);
        if (marketingBundle == null) {
            throw new ProvisioningInputException("Bundle: " + name + " not found in the bundle metadata");
        }

        provisioningContext.setTenantId(tenantId);

        provisionBundle(marketingBundle, provisioningContext);

        subscriptionMetadataService.addMarketingBundle(provisioningContext.getTenantId(), marketingBundle);
    }

    /**
     * Rubiks - Provision a bundle
     * @param tenantId
     * @param bundleId
     * @param bundleFeaturesRequest
     */
    public void provisionBundle(String tenantId, int bundleId, BundleFeaturesRequest bundleFeaturesRequest) {

        MarketingBundle marketingBundle = bundleMetadataService.getMarketingBundle(bundleId);

        // If bundleId is for a bundle which already provisioned then get it for update features conf
        SubscriptionBundleMap bundleMap = subscriptionMetadataDAO.getSubscriptionBundleEntry(tenantId, bundleId);

        if (bundleMap != null) {
            throw new ProvisioningInputException("Bundle with id = "+ bundleId +" already provisioned for TenantUUID " + tenantId);
        }

        // If we have features coming in the request then override featureConfig
        Map<Integer, MarketingFeature> featuresMap = new HashMap<>();
        for(MarketingFeature feature: bundleFeaturesRequest.getFeatures()) {
            featuresMap.put(feature.getId(), feature);
        }

        for(MarketingBundleFeature bundleFeature: marketingBundle.getChildBundleFeatures()) {
            MarketingFeature f = featuresMap.get(bundleFeature.getFeature().getId());
            if (f != null) {
                bundleFeature.setMarketingFeatureConfig(f.getMarketingFeatureConfig());
            }
        }

        provisioningContext.setTenantId(tenantId);

        provisionBundle(marketingBundle, provisioningContext);

        if (provisioningContext.getErrors().size() > 0) {
            String error = provisioningContext.getErrors().get(0).getMessage();
            logger.info("Bundle provisioning failed for some feature with error msg: " + error
                    + "\n About to deprovision the whole bundle now...");
            try {
                // Just to call MLM and disable the features
                deprovisionBundle(marketingBundle, provisioningContext);
            }
            catch (Exception e) {
                logger.info("Bundle deprovisioning failed with error msg: " + e.getMessage());
            }
            throw new ProvisioningException(error);
        }

        subscriptionMetadataService.addMarketingBundle(provisioningContext.getTenantId(), marketingBundle);

        // add features with config overrides in sub bundles map
        for(MarketingFeature feature: bundleFeaturesRequest.getFeatures()) {
            if(!StringUtils.isEmpty(feature.getMarketingFeatureConfig())) {
                subscriptionMetadataService.addMarketingFeature(provisioningContext.getTenantId(), feature);
            }
        }
    }

    /**
     * Provision a feature
     * @param tenantId
     * @param name
     * @param token
     * @param featureConfig
     */
    public void provisionFeature(String tenantUUID, int featureId, String featureConfig) {

        MarketingFeature marketingFeature = bundleMetadataService.getMarketingFeature(featureId);

        if (StringUtils.isEmpty(tenantUUID)) {
            throw new ProvisioningInputException("Invalid TenantUUID Recieved");
        }

        if (marketingFeature == null) {
            throw new ProvisioningInputException("Feature: " + featureId + " not found in the Bundle Metadata");
        }

        marketingFeature.setMarketingFeatureConfig(featureConfig);

        provisioningContext.setTenantId(tenantUUID);

        provisionFeature(marketingFeature, provisioningContext);

        subscriptionMetadataService.addMarketingFeature(provisioningContext.getTenantId(), marketingFeature);
    }

    /**
     * Deprovision Marketing Feature
     * @param tenantId
     * @param feature
     */
    public void deprovisionFeature(String tenantUUID, int featureId) {

        MarketingFeature marketingFeature = subscriptionMetadataService.getAddonFeatureInfoForSubsription(tenantUUID, featureId);
        if (marketingFeature == null) {
            throw new ProvisioningInputException("Feature: " + featureId + " not found in the Subscription Bundle Map");
        }

        if (StringUtils.isEmpty(tenantUUID)) {
            throw new ProvisioningInputException("Invalid TenantUUID Recieved");
        }

        provisioningContext.setTenantId(tenantUUID);

        deprovisionFeature(marketingFeature, provisioningContext);

        subscriptionMetadataService.deleteMarketingFeature(tenantUUID, marketingFeature.getId());
    }

    /**
     * Rubiks - Deprovision Marketing Bundle
     * TODO test case
     * @param tenantUUID
     * @param bundleId
     */
    public void deprovisionBundle(String tenantUUID, int bundleId) {

        if (StringUtils.isEmpty(tenantUUID)) {
            throw new ProvisioningInputException("Invalid TenantUUID Received");
        }

        // Verify bundle with bundleId is provisioned
        SubscriptionBundleMap bundleMap = subscriptionMetadataDAO.getSubscriptionBundleEntry(tenantUUID, bundleId);

        if (bundleMap == null) {
            throw new ModuleNotFoundException("TenantUUID " + tenantUUID + ", bundle with di "+ bundleId +
                    " is not provisioned for this subscription");
        }

        Map<Integer, Integer> featureUsedByCount = subscriptionMetadataService.getFeatureUsedByCount(tenantUUID);

        provisioningContext.setTenantId(tenantUUID);
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle(bundleId);
        ListIterator bundleFeatureItr = bundle.getChildBundleFeatures().listIterator(bundle.getChildBundleFeatures().size());

       // De-provision in reverse order.
        while(bundleFeatureItr.hasPrevious()) {
            MarketingBundleFeature bundleFeature = (MarketingBundleFeature) bundleFeatureItr.previous();
            MarketingFeature feature = bundleFeature.getFeature();
            // If the feature exists in subscription_bundle_map then use it as it has the overridden config
            MarketingFeature subscriptionFeature = subscriptionMetadataDAO.getFeatureInfoForSubscription(tenantUUID, feature.getId());
            if (subscriptionFeature != null) {
                feature =  subscriptionFeature;
            }

            if(featureUsedByCount.get(feature.getId()) > 1 ) {
                logger.info("Skipping deprovision for feature with Id = " + feature.getId() +", Name: "+ feature.getName() +
                        " - feature already provisioned part of another bundle");
            } else {
                deprovisionFeature(feature, provisioningContext);
                subscriptionMetadataService.deleteMarketingFeature(tenantUUID, feature.getId());
            }
        }
        subscriptionMetadataDAO.deleteMarketingBundle(tenantUUID, bundleId);
    }

    /*
     * Rubiks Get Marketing Bundle
     * @param tenantUUID
     * @param bundleId
     */
    public String getMarketingFeatureStatus(String tenantUUID, String featureCode, String datacenter) {

        MarketingFeature marketingFeature = bundleMetadataService.getMarketingFeatureByCode(featureCode);

        if (StringUtils.isEmpty(tenantUUID)) {
            throw new ProvisioningInputException("Invalid TenantUUID Recieved");
        }

        if (marketingFeature == null) {
            throw new ProvisioningInputException("Feature: " + featureCode + " not found in the Bundle Metadata");
        }

        String subscriptionFeatureInfo = subscriptionMetadataDAO.getMarketingFeatureStatus(tenantUUID, marketingFeature);
        return getFeatureCurrentStatus(tenantUUID, marketingFeature, subscriptionFeatureInfo, datacenter);
    }

    /*
     * This Method will check Feature Status and if Status is not successful then this method decide whether to
      * check Feature Current Status or not based on client Type.
      * For Eg, In Case of Sirius Client Type, If Status is not successful then provisioning will call sirius rest api
      * to check the latest status of feature.
     */
    protected String getFeatureCurrentStatus(String tenantUUID, MarketingFeature marketingFeature, String subscriptionFeatureInfo, String datacenter) {

        if (subscriptionFeatureInfo == null || subscriptionFeatureInfo.isEmpty()) {
            if (marketingFeature.getCode().equalsIgnoreCase(FeatureCode.ORIONBase.toString())) {
                return FeatureStatus.UNKNOWN.toString();
            }
        }

        SubscriptionFeatureInfo subscriptionInfo = null;
        if (subscriptionFeatureInfo != null && !StringUtils.isEmpty(subscriptionFeatureInfo)) {
            subscriptionInfo = parseSubscriptionFeatureInfo(subscriptionFeatureInfo);
            if(subscriptionInfo == null) {
                logger.error(String.format("Unable to parse Subscription Feature Info for %s subscriptionFeatureInfo", subscriptionFeatureInfo));
                return null;
            }
            if (subscriptionInfo.getStatus().equalsIgnoreCase(SiriusJobStatus.SUCCEEDED.toString())) {
                return FeatureStatus.COMPLETED.toString();
            }
        }

        // Check whether Client Type is Sirius
        if (marketingFeature.getClientType() != null && marketingFeature.getClientType().equalsIgnoreCase(ClientType.SIRIUS.toString())) {
            if(subscriptionInfo == null) {
                logger.error(String.format("Unable to parse Subscription Feature Info for %s subscriptionFeatureInfo", subscriptionFeatureInfo));
                return null;
            }
            return siriusProvisioningHandler.getFeatureStatus(tenantUUID, marketingFeature, subscriptionInfo.getBacklogId(), datacenter);
        }

        if (marketingFeature.getCode().equalsIgnoreCase(FeatureCode.RCA.toString())) {
            return rcaProvisioningHandler.getFeatureStatus(tenantUUID, marketingFeature);
        }

        if(subscriptionInfo == null) {
            logger.error("Unable to parse Subscription Feature Info for " + subscriptionFeatureInfo + " subscriptionFeatureInfo");
            return null;
        }

        return subscriptionInfo.getStatus();

    }

    /*
     * This Method will parse Subscription Feature Info (String) and return Subscription Feature Info Object
     */
    protected SubscriptionFeatureInfo parseSubscriptionFeatureInfo(String subscriptionFeatureInfo) {

        ObjectMapper mapper = ObjectMapperFactory.getInstance();

        if (StringUtils.isEmpty(subscriptionFeatureInfo)) {
            throw new ProvisioningException("Null Response Recieved from feature handler");
        }

        SubscriptionFeatureInfo subscriptionInfo = null;
        try {
            subscriptionInfo = mapper.readValue(subscriptionFeatureInfo, SubscriptionFeatureInfo.class);
        } catch (Exception e) {
            throw new ProvisioningException(e);
        }

        return subscriptionInfo;
    }

    /**
     * Update a feature
     * @param tenantUUID
     * @param featureId
     * @param featureConfig
     */
    public void updateFeature(String tenantUUID, int featureId, String featureConfig) {

        MarketingFeature marketingFeature = bundleMetadataService.getMarketingFeature(featureId);

        if (StringUtils.isEmpty(tenantUUID)) {
            throw new ProvisioningInputException("Invalid TenantUUID Recieved");
        }

        if (marketingFeature == null) {
            throw new ProvisioningInputException("Feature: " + featureId + " not found in the Bundle Metadata");
        }

        // Big Giant Hack. MLM Rest Client sends [] by default, fix that ASAP
        if (StringUtils.isEmpty(featureConfig) || featureConfig.equals("[]")) {
            featureConfig = featureConfigurator.getDefaultFeatureConfig(tenantUUID, marketingFeature);
        }

        featureConfig = updateFeatureConfig(featureConfig);

        marketingFeature.setMarketingFeatureConfig(featureConfig);

        provisioningContext.setTenantId(tenantUUID);

        updateFeature(marketingFeature, provisioningContext);

        subscriptionMetadataService.addMarketingFeature(provisioningContext.getTenantId(), marketingFeature);
    }

    /**
     * TODO - Remove after all customer migrated to Rubiks - Module packaging
     * Upgrade/Downgrade a bundle
     * @param tenantUUID
     * @param bundleId
     */
    public void updateBundle(String tenantUUID, int bundleId) {

        // get the bundle for this subscription
        SubscriptionBundleFeatures bundleFeatures = subscriptionMetadataService.getBundlesFeatures(tenantUUID);
        if (bundleFeatures != null) {
            MarketingBundle existingBundle = bundleFeatures.getBundle();

            MarketingBundle newBundle = bundleMetadataService.getMarketingBundle(bundleId);
            if (newBundle == null) {
                throw new ProvisioningInputException("Bundle " + bundleId + " not found in the bundle metadata");
            }

            String log = String.format("Update Bundle: %s for subscription: %s", newBundle.getName(), tenantUUID);

            logger.info(log);

            provisioningContext.setTenantId(tenantUUID);

            BundleDiffInfo bundleDiffInfo = bundleMetadataService.compareBundles(existingBundle, newBundle);

            for (MarketingFeature feature : bundleDiffInfo.getCommonFeatures()) {
                try {
                    logger.info("Common Feature between old and new bundles: " + feature.getName());
                    // if feature already exists as add on, do not do anything.
                    if (subscriptionMetadataService.getAddonFeatureInfoForSubsription(tenantUUID, feature.getId()) == null) {
                        updateFeature(feature, provisioningContext);
                    } else {
                        updateFeature(feature, provisioningContext);
                        subscriptionMetadataService.addMarketingFeature(tenantUUID, feature);
                    }

                } catch (Exception e) {
                    logger.error("Error Updating feature: " + feature.getName() , e);
                    provisioningContext.addError(e);
                }
            }
            for (MarketingFeature feature : bundleDiffInfo.getNewFeatures()) {
                try {
                    logger.info("Feature only in new bundle: " + feature.getName());
                    // if feature already exists as add on, do not do anything.
                    if (subscriptionMetadataService.getAddonFeatureInfoForSubsription(tenantUUID, feature.getId()) == null) {
                        updateFeature(feature, provisioningContext);
                    }
                } catch (Exception e) {
                    logger.error("Error Updating feature: " + feature.getName() , e);
                    provisioningContext.addError(e);
                }
            }

            for (MarketingFeature feature : bundleDiffInfo.getOldFeatures()) {
                try {
                    logger.info("Feature old in new bundle: " + feature.getName());
                    // if feature already exists as add on, do not do anything.
                    if (subscriptionMetadataService.getAddonFeatureInfoForSubsription(tenantUUID, feature.getId()) == null) {
                        deprovisionFeature(feature, provisioningContext);
                    }
                } catch (Exception e) {
                    logger.error("Error Deprovisioning feature: " + feature.getName() , e);
                    provisioningContext.addError(e);
                }
            }

            subscriptionMetadataService.updateMarketingBundle(provisioningContext.getTenantId(), bundleId, existingBundle.getId());

        } else {
            throw new ProvisioningInputException("TenantUUID " + tenantUUID + " does not exist in SubscriptionBundleMap");
        }
    }

    /**
     * TODO test case
     * Rubiks - This will update the child bundle features config (will not remove or add features)
     * @param tenantUUID
     * @param bundleId
     * @param bundleFeaturesRequest
     *
     */
    public void updateBundle(String tenantUUID, int bundleId, BundleFeaturesRequest bundleFeaturesRequest) {

        MarketingBundle bundle = bundleMetadataService.getMarketingBundle(bundleId);

        // If bundleId is for a bundle which already provisioned then get it for update features conf
        SubscriptionBundleMap bundleMap = subscriptionMetadataDAO.getSubscriptionBundleEntry(tenantUUID, bundleId);

        // Get all provisioned bundles for this subscription
        SubscriptionBundleFeatures bundlesFeatures = subscriptionMetadataService.getBundlesFeatures(tenantUUID);

        // Sub should be already provisioned with at least the base module
        if (bundlesFeatures == null) {
            throw new ProvisioningInputException("TenantUUID " + tenantUUID + " does not exist in SubscriptionBundleMap");
        }

        if (bundleMap == null) {
            throw new ProvisioningInputException("TenantUUID " + tenantUUID + ", bundle with di "+ bundleId +" is not provisioned for this subscription");
        }

        // update a provisioned  bundle features
        for(MarketingFeature feature: bundleFeaturesRequest.getFeatures()) {
            updateFeature(tenantUUID, feature.getId(), feature.getMarketingFeatureConfig());
        }
    }
    /**
     * Provision Marketing Bundle
     * @param tenantId
     * @param bundle
     */
    private void provisionBundle(MarketingBundle bundle, ProvisioningContext context) {
        bundleProvisioningService.doOperation(bundle, Operation.PROVISION, context);
    }

    /**
     * Provision Marketing Feature
     * @param tenantId
     * @param feature
     * @throws ProvisioningException
     */
    private void provisionFeature(MarketingFeature feature, ProvisioningContext context) {

        // Do provisioning operation on feature
        featureProvisioningService.doOperation(feature, Operation.PROVISION, context);
    }

    private void deprovisionFeature(MarketingFeature feature, ProvisioningContext context) {
        featureProvisioningService.doOperation(feature, Operation.DEPROVISION, context);
    }

    /**
     * Deprovision Marketing Bundle
     * @param tenantId
     * @param bundle
     */
    private void deprovisionBundle(MarketingBundle bundle, ProvisioningContext context) {
        bundleProvisioningService.doOperation(bundle, Operation.DEPROVISION, context);
    }

    private void updateFeature(MarketingFeature feature, ProvisioningContext context) {
        featureProvisioningService.doOperation(feature, Operation.UPDATE, context);
    }

    private void updateBundle(MarketingBundle bundle, ProvisioningContext context) {
        bundleProvisioningService.doOperation(bundle, Operation.UPDATE, context);
    }

    /**
     * Rubicks - migrate a subscription from v2 to Rubiks
     * @param configParams
     * @throws ProvisioningException
     */

    public ProvisioningResponse migrate(MultiValueMap<String, String> configParams) {

        String tenantUUID = configParams.getFirst("tenantUUID");
        if (StringUtils.isEmpty(tenantUUID)) {
            throw new ProvisioningInputException("Missing subscription tenantUUID");
        }

        List<SubscriptionBundleMap> subBundleMaps = subscriptionMetadataDAO.getAllBundleFeatures(tenantUUID);

        if (subBundleMaps.size() > 0) {
            throw new BundleMetadataException("Subscripton Bundle Mapping already exist in Rubiks for: " + tenantUUID);
        }

        provisioningContext.setTenantId(tenantUUID);
        provisioningContext.setParams(configParams);
        JSONArray modules = parseModules(configParams);
        if (modules.indexOf("baseModule") != 0) {
            throw new ProvisioningInputException("Base Module (baseModule) should be included and should be the first in the list, parameters: " + configParams);
        }

        try {
            addBundlesAndFeatures(tenantUUID, modules, provisioningContext.getFeaturePodSelection());

            // Call MLM for feature Migration
            logger.info("About to call MLM migration, configParams: "+ configParams.toString());
            mlmFeatureHandler.migrate(bundleMetadataService.getMarketingFeature(FeatureProvisioningService.BASE_FEATURE), provisioningContext);
        }
        catch (Exception ex) {
            logger.info("MLM feature migration failed, deleting all bundles and features from Rubiks");
            subscriptionMetadataDAO.deleteBundlesAndFeatures(tenantUUID);
            throw ex;
        }

        logger.info("Migration succeeded!!!");
        return new ProvisioningResponse(provisioningContext.getTenantId());
    }

    /**
     *  This function is part of update subscription apis. It adds new modules to
     *  the existing subscription.
     * @param configParams parameters from request
     * @return ProvisioningResponse
     */
    public ProvisioningResponse upgradeSubscription(String tenantUUID, MultiValueMap<String, String> configParams) {
        if (StringUtils.isEmpty(tenantUUID)) {
            throw new ProvisioningInputException("Missing subscription tenantUUID");
        }

        provisioningContext.setTenantId(tenantUUID);
        provisioningContext.setParams(configParams);

        logger.info("Upgrade Subscription started Context Params: " + provisioningContext.toString());

        JSONObject newBundleConfig = provisioningContext.getNewBundleConfig();
        if (newBundleConfig.has("baseModule")) {
            throw new ProvisioningInputException("Base Module cannot be present for addition to existing modules");
        }
        SubscriptionBundles provisionedModules = subscriptionMetadataService.getProvisionedModules(tenantUUID);
        if (provisionedModules.getBundles().size() == 0) {
            String errorMsg = String.format("TenantUUID [ %s ] has no modules provisioned", tenantUUID);
            logger.error(errorMsg);
            throw new ProvisioningException(errorMsg);
        }
        Integer errors;
        ArrayList<String> failedModules = new ArrayList<>();
        for (Object obj : newBundleConfig.keySet()) {
            String bundleKey = (String) obj;
            if (StringUtils.isEmpty(bundleKey)) {
                logger.info("Skipping addition of empty bundle code, tenantUUID: " + provisioningContext.getTenantId());
                failedModules.add("emptyModule");
                continue;
            }
            provisioningContext.addExecutedStep(bundleKey, ModuleStatus.STARTED.toString(), null);
            MarketingBundle marketingBundle = bundleMetadataDAO.getMarketingBundleByCode(bundleKey);
            if (marketingBundle == null) {
                marketingBundle = bundleMetadataDAO.getMarketingBundle(bundleKey);
                if (marketingBundle == null) {
                    String msg = "Bundle code (" + bundleKey + ") not found in the bundle metadata - skipped for addition to existing subscription!!!";
                    provisioningContext.addWarning(msg);
                    logger.info(msg);
                    failedModules.add(bundleKey);
                    provisioningContext.addExecutedStep(bundleKey, ModuleStatus.FAILURE.toString(), "Module code not present in metadata");
                    continue;
                }
            }
            SubscriptionBundleMap bundleMap = subscriptionMetadataDAO.getSubscriptionBundleEntry(tenantUUID, marketingBundle.getId());
            if (bundleMap != null) {
                String msg = String.format("Module [%s] is already provisioned for TenantUUID [%s]", bundleKey, tenantUUID);
                logger.info(msg);
                provisioningContext.addWarning(msg);
                provisioningContext.addExecutedStep(bundleKey, ModuleStatus.SUCCESS.toString(), "Module already provisioned");
                continue;
            }

            errors = provisioningContext.getErrors().size();
            provisionBundle(marketingBundle, provisioningContext);
            if (provisioningContext.getErrors().size() == errors) {
                subscriptionMetadataService.addMarketingBundle(tenantUUID, marketingBundle);
                for (MarketingBundleFeature bundleFeature : marketingBundle.getChildBundleFeatures()) {
                    if (!StringUtils.isEmpty(bundleFeature.getFeature().getMarketingFeatureConfig())) {
                        subscriptionMetadataService.addMarketingFeature(tenantUUID, bundleFeature.getFeature());
                    }
                }
                provisioningContext.addExecutedStep(bundleKey, ModuleStatus.SUCCESS.toString(), null);
            } else {
                try {
                    failedModules.add(marketingBundle.getName());
                    String msg = String.format("Addition of Module [%s] failed for tenantUUID: [%s]", bundleKey, tenantUUID);
                    logger.info(msg);
                    provisioningContext.addWarning(msg);
                    provisioningContext.addExecutedStep(bundleKey, ModuleStatus.FAILURE.toString(), "Check warnings");
                    deprovisionBundle(marketingBundle, provisioningContext);
                } catch (Exception ex) {
                    logger.error("Bundle deprovisioning failed with error msg: " + ex.getMessage());
                }
            }
        }
        // if all modules provisioning failed then its an error
        if (newBundleConfig.length() == failedModules.size()) {
            throw new ProvisioningException(provisioningContext.getFormattedWarnings());
        }
        return new ProvisioningResponse(tenantUUID);
    }

    /**
     *  This function is part of update subscription api. It modify the configs of existing modules for
     *  a subscription.
     * @param configParams parameters from request
     * @return
     */
    public ProvisioningResponse modifySubscriptionConfig(String tenantUUID, MultiValueMap<String, String> configParams) {
        if (StringUtils.isEmpty(tenantUUID)) {
            throw new ProvisioningInputException("Missing subscription tenantUUID");
        }

        provisioningContext.setTenantId(tenantUUID);
        provisioningContext.setParams(configParams);

        logger.info("Modify Subscription started Context Params: " + provisioningContext.toString());

        JSONObject newBundleConfig = provisioningContext.getNewBundleConfig();
        SubscriptionBundles provisionedModules = subscriptionMetadataService.getProvisionedModules(tenantUUID);
        if (provisionedModules.getBundles().size() == 0) {
            String msg = String.format("TenantUUID [%s] has no modules provisioned", tenantUUID);
            logger.error(msg);
            throw new ProvisioningException(msg);
        }

        List<String> failedModules = new ArrayList<>();

        for (Object obj : newBundleConfig.keySet()) {
            boolean isProvisioned = false;
            String bundleKey = (String) obj;
            if (StringUtils.isEmpty(bundleKey)) {
                logger.info("Skipping modification of empty bundle code, tenantUUID: " + provisioningContext.getTenantId());
                failedModules.add("emptyModule");
                continue;
            }
            provisioningContext.addExecutedStep(bundleKey, ModuleStatus.STARTED.toString(), null);
            // get the default marketingBundle
            MarketingBundle marketingBundle = bundleMetadataDAO.getMarketingBundleByCode(bundleKey);
            if (marketingBundle == null) {
                marketingBundle = bundleMetadataDAO.getMarketingBundle(bundleKey);
                if (marketingBundle == null) {
                    String msg = String.format("Bundle code ( %s ) not found in the bundle metadata - skipped for modification to existing subscription!!!", bundleKey);
                    provisioningContext.addWarning(msg);
                    logger.info(msg);
                    failedModules.add(bundleKey);
                    provisioningContext.addExecutedStep(bundleKey, ModuleStatus.FAILURE.toString(), "Module code not found in metadata");
                    continue;
                }
            }
            for (MarketingBundle bundle : provisionedModules.getBundles()) {
                if (bundle.getCode().equals(bundleKey)) {
                    marketingBundle = bundle;
                    isProvisioned = true;
                    break;
                }
            }
            if (isProvisioned == false) {
                String log = String.format("Module [%s] not provisioned for tenantUUID: [%s]", bundleKey, tenantUUID);
                logger.info(log);
                provisioningContext.addWarning(log);
                failedModules.add(bundleKey);
                provisioningContext.addExecutedStep(bundleKey, ModuleStatus.FAILURE.toString(), "Module not provisioned for tenantUUID");
                continue;
            }
            boolean isFailed = false;
            for (MarketingBundleFeature bundleFeature : marketingBundle.getChildBundleFeatures()) {
                MarketingFeature feature = bundleFeature.getFeature();
                if (!StringUtils.isEmpty(feature.getMarketingFeatureConfig())) {
                    boolean isOverridden = bundleProvisioningService.overrideDefaultFeatureConfig(feature, marketingBundle, provisioningContext);
                    if (isOverridden == true) {
                        try {
                            logger.info("Starting update of module: " + bundleKey + " feature: " + feature.getCode() + " for tenantUUID: " + tenantUUID + " having new feature_config: " + feature.getMarketingFeatureConfig());
                            updateFeature(tenantUUID, feature.getId(), feature.getMarketingFeatureConfig());
                        } catch (Exception e) {
                            String msg = String.format("Update of feature [%s] module: [%s] failed with error: [%s]", feature.getCode(), bundleKey, e.getMessage());
                            logger.info(msg);
                            provisioningContext.addWarning(msg);
                            isFailed = true;
                            continue;
                        }
                    }
                }
            }
            if (isFailed == true) {
                failedModules.add(bundleKey);
                provisioningContext.addExecutedStep(bundleKey, ModuleStatus.FAILURE.toString(), "Check warnings");
            } else {
                provisioningContext.addExecutedStep(bundleKey, ModuleStatus.SUCCESS.toString(), null);
            }
        }
        if (newBundleConfig.length() == failedModules.size()) {
            throw new ProvisioningException(provisioningContext.getFormattedWarnings());
        }
        return new ProvisioningResponse(tenantUUID);
    }

    /**
     * This function is part of update subscription api. It removes some modules for a subscription
     * @param configParams
     * @return
     */
    public ProvisioningResponse downgradeSubscription(String tenantUUID, MultiValueMap<String, String> configParams) {
        if (StringUtils.isEmpty(tenantUUID)) {
            throw new ProvisioningInputException("Missing subscription tenantUUID");
        }
        provisioningContext.setTenantId(tenantUUID);
        provisioningContext.setParams(configParams);

        logger.info("Downgrade Subscription started Context Params: " + provisioningContext.toString());

        JSONArray modules = parseModules(configParams);
        List<String> failedModules = new ArrayList<>();
        for (Object obj : modules) {
            String bundleKey = (String) obj;
            if (StringUtils.isEmpty(bundleKey)) {
                logger.info("Skipping empty bundle code, tenantUUID: " + provisioningContext.getTenantId());
                failedModules.add("emptyModule");
                continue;
            }
            provisioningContext.addExecutedStep(bundleKey, ModuleStatus.STARTED.toString(), null);
            MarketingBundle marketingBundle = bundleMetadataDAO.getMarketingBundleByCode(bundleKey);
            if (marketingBundle == null) {
                marketingBundle = bundleMetadataDAO.getMarketingBundle(bundleKey);
                if (marketingBundle == null) {
                    String msg = String.format("Bundle code (%s) not found in the bundle metadata - skipped!!!", bundleKey);
                    provisioningContext.addWarning(msg);
                    logger.info(msg);
                    failedModules.add(bundleKey);
                    provisioningContext.addExecutedStep(bundleKey, ModuleStatus.FAILURE.toString(), "Module code not present in metadata");
                    continue;
                }
            }
            if (bundleKey.equals("baseModule")) {
                String warningMsg = "Base Module cannot be deprovisioned.";
                logger.info(warningMsg);
                provisioningContext.addWarning(warningMsg);
                failedModules.add(bundleKey);
                provisioningContext.addExecutedStep(bundleKey, ModuleStatus.FAILURE.toString(), "BaseModule cannot be deprovisioned");
                continue;
            }
            int bundleId = marketingBundle.getId();
            try {
                deprovisionBundle(tenantUUID, bundleId);
                provisioningContext.addExecutedStep(bundleKey, ModuleStatus.SUCCESS.toString(), null);
            } catch (ModuleNotFoundException ex) {
                String msg = String.format("Module: %s not found for the tenantUUID: [%s]", bundleKey, tenantUUID);
                logger.info(msg);
                provisioningContext.addWarning(msg);
                provisioningContext.addExecutedStep(bundleKey, ModuleStatus.SUCCESS.toString(), "Module not provisioned for tenantUUID");
            } catch (Exception ex) {
                String msg = String.format("Module: [%s] could not be deprovisioned for tenantUUID: [%s]", bundleKey, tenantUUID);
                logger.info(msg);
                provisioningContext.addWarning(msg);
                failedModules.add(bundleKey);
                provisioningContext.addExecutedStep(bundleKey, ModuleStatus.FAILURE.toString(), ex.getMessage());
            }
        }
        if (failedModules.size() == modules.size()) {
            throw new ProvisioningException(provisioningContext.getFormattedWarnings());
        }
        return new ProvisioningResponse(tenantUUID);
    }

    protected String updateFeatureConfig(String featureConfig) {

        ObjectMapper mapper = ObjectMapperFactory.getInstance();
        try {
            Map<String, String> configs = mapper.readValue(featureConfig, Map.class);
            if (configs.containsKey("mlm")) {

                logger.info("Provisioning Source is MLM");
                configs.remove("mlm");
                provisioningContext.setProvisionSource(ProvisionSource.MLM.toString());
                return mapper.writeValueAsString(configs);
            }
        } catch (Exception e) {
            throw new ProvisioningException(e);
        }
        return featureConfig;
    }


    /**
     * Rubicks Migration, add bundles and features to subscription_bundle_map without calling MLM provision handler (Handled by MLM itself)
     * @param tenantUUID
     * @param modules
     * @param podSelection
     * @throws ProvisioningException
     */

    private void addBundlesAndFeatures(String tenantUUID, JSONArray modules, HashMap<String, String> podSelection) {

        for (Object obj : modules) {
            String bundleCode = (String) obj;
            MarketingBundle marketingBundle = bundleMetadataService.getMarketingBundleByCode(bundleCode);

            logger.info("Adding Bundle " + marketingBundle.getName() + " to bundle map");
            subscriptionMetadataService.addMarketingBundle(provisioningContext.getTenantId(), marketingBundle);

            for (MarketingBundleFeature bundleFeature : marketingBundle.getChildBundleFeatures()) {
                String featureConfig = bundleFeature.getMarketingFeatureConfig();
                MarketingFeature feature = bundleFeature.getFeature();

                if (StringUtils.isEmpty(featureConfig)) {
                    featureConfigurator.initializeFeature(tenantUUID, feature, podSelection);
                    featureConfig = feature.getMarketingFeatureConfig();
                }

                if (!StringUtils.isEmpty(featureConfig)) {
                    feature.setMarketingFeatureConfig(featureConfig);
                    subscriptionMetadataService.addMarketingFeature(provisioningContext.getTenantId(), feature);
                    logger.info("Feature config added to bundle map, feature name: "+ feature.getName() +", config: " + featureConfig);
                }
            }
        }
    }


    /****
     * Adding bundles and features for 250 ok bundles
     * @param data
     */
    public ProvisioningResponse migrateTo250okBundles(MultiValueMap<String, String> data) {

        Map<String, org.json.JSONArray> modules = parseModules250ok(data);

        Set<String> tenantkeySet = modules.keySet();

        for (String tenant : tenantkeySet) {
            org.json.JSONArray tenantmodules = modules.get(tenant);

            Iterator<Object> moduleItr = tenantmodules.iterator();

            while (moduleItr.hasNext()) {

                JSONObject levelOneJson = (JSONObject) moduleItr.next();

                Set<String> keys = levelOneJson.keySet();

                for (String bundleCode : keys) {

                    MarketingBundle marketingBundle = bundleMetadataService.getMarketingBundleByCode(bundleCode);

                    logger.info("Adding Bundle " + marketingBundle.getName() + " to bundle map");

                    //setting new bundle param here
                    provisioningContext.parseNewBundleConfigFromParam(levelOneJson.toString());

                    if (subscriptionMetadataDAO.getSubscriptionBundleEntry(tenant, marketingBundle.getId()) == null) {
                        subscriptionMetadataService.addMarketingBundle(tenant, marketingBundle);
                    }

                    for (MarketingBundleFeature bundleFeature : marketingBundle.getChildBundleFeatures()) {
                        MarketingFeature feature = bundleFeature.getFeature();
                        feature.setMarketingFeatureConfig(levelOneJson.get(bundleCode).toString());

                        bundleProvisioningService.overrideDefaultFeatureConfig(feature, marketingBundle, provisioningContext);

                        String featureConfig = feature.getMarketingFeatureConfig();

                        if (!StringUtils.isEmpty(featureConfig)) {
                            feature.setMarketingFeatureConfig(featureConfig);
                            if (subscriptionMetadataService.getMarketingFeature(tenant, feature) == null) {
                                subscriptionMetadataService.addMarketingFeature(tenant, feature);
                            } else {
                                subscriptionMetadataService.updateMarketingFeatureConfig(tenant, feature.getCode(), feature.getMarketingFeatureConfig());
                            }
                            logger.info("Feature config added to bundle map, feature name: " + feature.getName() + ", config: " + featureConfig);
                        }
                    }

                    //removing new bundle param once processed for current bundle code
                    provisioningContext.parseNewBundleConfigFromParam("{}");

                }

            }


        }

        return new ProvisioningResponse("Completed migrating 250ok accounts");

    }

        /***
         * method to parse modules for 250ok bundles
         * @param data
         * @return
         */
        private Map<String,org.json.JSONArray> parseModules250ok (MultiValueMap < String, String > data){


            Map<String,org.json.JSONArray> tenantJsonData = new HashMap<>();

            ChildAccount[] accounts = api250OkHelper.fetchAllChildAccounts();

            for (ChildAccount account : accounts) {

                JSONObject tenantData = new JSONObject();

                org.json.JSONArray params = new org.json.JSONArray();

                String munchkinId = data.getFirst(account.getExternal_id());

                if (munchkinId != null) {

                    logger.info(" parsing data for munchkin Id : " + munchkinId);

                    //Email Reputation
                    JSONObject emailReputation = prepareDataForEmailReputation(account);

                    if (Objects.nonNull(emailReputation))
                        params.put(emailReputation);

                    //Email Informant
                    JSONObject emailInformant = prepareDataForEmailInformant(account);

                    if (Objects.nonNull(emailInformant))
                        params.put(emailInformant);

                    //Email Deliverability
                    JSONObject emailDeliverability = prepareDataForEmailDeliverability(account);

                    if (Objects.nonNull(emailDeliverability))
                        params.put(emailDeliverability);

                    logger.info("JSON for munchkin :" + munchkinId + " is  : " + params.toString());

                    tenantJsonData.put(munchkinId, params);

                }

            }

            return tenantJsonData;
        }

        /***
         *
         * @param account
         * @return
         */
        private JSONObject prepareDataForEmailReputation (ChildAccount account){

            JSONObject emailReputationJson = null;

            MarketingBundle marketingBundle = bundleMetadataService.getMarketingBundleByCode(SubscriptionConstants.API250OK_MARKETO_EMAIL_REPUTATION);

            JsonObject bundle_feature_config_json = getBundleConfigDataFor(marketingBundle);
            //Email Reputation
            if (account.getHas_dmarc().equals("yes") ||
                    account.getHas_reputation().equals("yes") ||
                    account.getHas_blacklistinformant().equals("yes")) {

                emailReputationJson = new JSONObject();

                JSONObject emailReputationFeatureJson = new JSONObject();

                Integer max_blacklist = Integer.parseInt(account.getBlacklist_max());

                Integer quantitydef = Integer.parseInt(bundle_feature_config_json.get("max_blacklist").getAsString());

                emailReputationFeatureJson.put("quantity", max_blacklist / quantitydef);
                emailReputationFeatureJson.put("reputation_expiration", account.getReputation_expires());
                emailReputationFeatureJson.put("max_blacklist", account.getBlacklist_max());
                emailReputationFeatureJson.put("blacklist_expiration", account.getBlacklist_expires());
                emailReputationFeatureJson.put("max_dmarc", account.getDmarc_max());
                emailReputationFeatureJson.put("dmarc_expiration", account.getDmarc_expires());

                emailReputationJson.put("emailReputation", emailReputationFeatureJson);
            }

            return emailReputationJson;
        }

        /***
         *
         * @param account
         * @return
         */
        private JSONObject prepareDataForEmailInformant (ChildAccount account){

            JSONObject emailInformantJson = null;

            MarketingBundle marketingBundle = bundleMetadataService.getMarketingBundleByCode(SubscriptionConstants.API250OK_MARKETO_EMAIL_INFORMANT);

            JsonObject bundle_feature_config_json = getBundleConfigDataFor(marketingBundle);

            if (account.getHas_emailinformant().equals("yes")) {

                emailInformantJson = new JSONObject();

                JSONObject emailInformantFeatureJson = new JSONObject();

                Integer email_informant_max = Integer.parseInt(account.getEmailinformant_max());

                Integer quantitydef = Integer.parseInt(bundle_feature_config_json.get("max_emailinformant").getAsString());

                emailInformantFeatureJson.put("quantity", email_informant_max / quantitydef);
                emailInformantFeatureJson.put("max_emailinformant", account.getEmailinformant_max());
                emailInformantFeatureJson.put("emailinformant_expiration", account.getEmailinformant_expires());

                emailInformantJson.put("emailInformant", emailInformantFeatureJson);
            }

            return emailInformantJson;
        }

        /***
         *
         * @param account
         * @return
         */
        private JSONObject prepareDataForEmailDeliverability (ChildAccount account){

            JSONObject emailDeliverabilityJson = null;

            MarketingBundle marketingBundle = bundleMetadataService.getMarketingBundleByCode(SubscriptionConstants.API250OK_MARKETO_EMAIL_DELIVERABILITY);

            JsonObject bundle_feature_config_json = getBundleConfigDataFor(marketingBundle);


            if (account.getHas_designinformant().equals("yes") || account.getHas_inboxinformant().equals("yes")) {

                emailDeliverabilityJson = new JSONObject();

                JSONObject emailDeliverabilityFeatureJson = new JSONObject();

                Integer email_informant_max = Integer.parseInt(account.getInbox_max());

                Integer quantitydef = Integer.parseInt(bundle_feature_config_json.get("max_inbox").getAsString());

                emailDeliverabilityFeatureJson.put("quantity", email_informant_max / quantitydef);
                emailDeliverabilityFeatureJson.put("max_inbox", account.getInbox_max());
                emailDeliverabilityFeatureJson.put("inbox_expiration", account.getInbox_expires());
                emailDeliverabilityFeatureJson.put("inbox_period", "monthly");
                emailDeliverabilityFeatureJson.put("max_design", account.getDesign_max());
                emailDeliverabilityFeatureJson.put("design_expiration", account.getDesign_expires());

                emailDeliverabilityJson.put("emailDeliverability", emailDeliverabilityFeatureJson);

            }
            return emailDeliverabilityJson;
        }


        /***
         *
         * @param marketingBundle
         * @return
         */
        private JsonObject getBundleConfigDataFor (MarketingBundle marketingBundle){
            JsonParser jsonParser = new JsonParser();
            String bundle_feature_config = "";
            for (MarketingBundleFeature bundleFeature : marketingBundle.getChildBundleFeatures())
                bundle_feature_config = bundleFeature.getMarketingFeatureConfig();
            JsonObject bundle_feature_config_json = jsonParser.parse(bundle_feature_config).getAsJsonObject();
            return bundle_feature_config_json;
        }

        private JSONArray parseModules (MultiValueMap < String, String > configParams) throws ProvisioningInputException
        {
            String bundleStr = configParams.getFirst("bundle");
            if (StringUtils.isEmpty(bundleStr)) {
                throw new ProvisioningInputException("bundle is not found in the parameters: " + configParams);
            }
            JSONArray jsonArray = new JSONArray();
            if (bundleStr.startsWith("[")) {
                jsonArray = JSONArray.fromObject(bundleStr);
            } else {
                jsonArray.add(bundleStr);
            }
            return jsonArray;
        }


    }
