package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.service.MarketingFeatureConfigurator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;

import java.util.ArrayList;
import java.util.List;

@Component
public class BundleProvisioningService {

    private static final Logger logger = LoggerFactory.getLogger(BundleProvisioningService.class);
    
    @Autowired
    private FeatureProvisioningService featureProvisioningService;
    
    @Autowired
    private SubscriptionMetadataService subscriptionMetadataService;

    @Autowired
    private MarketingFeatureConfigurator featureConfigurator;
    
    public void doOperation(MarketingBundle bundle, Operation operation, ProvisioningContext context) {
                        
        for (MarketingBundleFeature bundleFeature : bundle.getChildBundleFeatures()) {
    
            if (bundleFeature.isBundle()) {
                doOperation(bundleFeature.getBundle(), operation, context);
            } else if (bundleFeature.isFeature()) {
                String featureConfig = bundleFeature.getMarketingFeatureConfig();
                MarketingFeature feature = bundleFeature.getFeature();
                
                if (!StringUtils.isEmpty(featureConfig)) {
                    feature.setMarketingFeatureConfig(featureConfig);
                    /*
                     check if the newBundle is present in the request and
                      if present then override the default feature config
                      */
                    overrideDefaultFeatureConfig(feature, bundle, context);
                    bundleFeature.setMarketingFeatureConfig(feature.getMarketingFeatureConfig());
                }
                else if (!StringUtils.isEmpty(feature.getDeveloperConfig())) {
                    featureConfigurator.initializeFeature(context.getTenantId(), feature, context.getFeaturePodSelection());
                    /*
                    override default developer config from newBundle
                     */
                    overrideDefaultFeatureConfig(feature, bundle, context);
                    bundleFeature.setMarketingFeatureConfig(feature.getMarketingFeatureConfig());
                    logger.info("Feature configuration initialized, feature name:"
                            + feature.getName() + " config: " + feature.getMarketingFeatureConfig());
                }
                else {
                    logger.info("Feature configuration not present for feature: " 
                            + feature.getName() + " for parent bundle " + bundle.getName());
                }
                
                try {
                    featureProvisioningService.doOperation(feature, operation, context);
                } catch(ProvisioningException e) {
                    logger.error("Failed Provisoning Marketing Feature: "
                            + feature.getName() + ". Context: " + context, e);                                        

                    if (FeatureProvisioningService.BASE_FEATURE.equals(feature.getName())) {
                        throw e;
                    } else {
                        context.addError(e);                
                    }            
                }
                
            }                
        }
    }

    /**
     * Function to override the default configuration for a feature by the new
     * configuration received in the newBundle Param
     * @param feature MarketingFeature for which the default config needs to be overridden
     * @param bundle MarktingBundle Bundle to which the feature belongs
     * @param provisioningContext
     * @return MarketingFeature after overriding the default configuration
     */
    public boolean overrideDefaultFeatureConfig(MarketingFeature feature, MarketingBundle bundle, ProvisioningContext provisioningContext) {
        boolean isOverridden = false;
        if (StringUtils.isEmpty(feature.getMarketingFeatureConfig())) {
            return false;
        }
        JSONObject defaultFeatureConfigList = new JSONObject(feature.getMarketingFeatureConfig());
        JSONObject newBundleConfig = provisioningContext.getNewBundleConfig();
        if(newBundleConfig.length() != 0 && !newBundleConfig.isNull(bundle.getCode()) && newBundleConfig.has(bundle.getCode()) && !newBundleConfig.get(bundle.getCode()).toString().equals("null")) {
            JSONObject newFeatureConfigList = newBundleConfig.getJSONObject(bundle.getCode());
            List<String> warningMessages = validateNewBundleConfig(newFeatureConfigList, bundle);
            if (warningMessages.size() != 0) {
                for (String warningMsg : warningMessages) {
                    provisioningContext.addWarning(warningMsg);
                }
            }
            for (Object defaultFeatureConfigObj : defaultFeatureConfigList.keySet()) {
                String defaultFeatureConfigKey = (String) defaultFeatureConfigObj;
                try {
                    if (!newFeatureConfigList.isNull(defaultFeatureConfigKey) && newFeatureConfigList.has(defaultFeatureConfigKey) && !newFeatureConfigList.get(defaultFeatureConfigKey).equals("null")) {
                        logger.info("Overriding default configurations for bundle: " + bundle.getCode() + " | feature: " + defaultFeatureConfigObj.toString());
                        defaultFeatureConfigList.put(defaultFeatureConfigKey, newFeatureConfigList.get(defaultFeatureConfigKey));
                        isOverridden = true;
                    }
                } catch (JSONException ex) {
                    logger.info("Incorrect format of newBundle while overriding bundle: " + bundle.getCode() + " | feature: " + defaultFeatureConfigKey);
                }
            }
            feature.setMarketingFeatureConfig(defaultFeatureConfigList.toString());
        }
        return isOverridden;
    }

    /**
     * Function to validate the newBundle config codes
     * @param newFeatureConfigList List of configs from newBundleConfig
     * @param bundle bundle for which the config codes need to be checked for
     */
    public List<String> validateNewBundleConfig(JSONObject newFeatureConfigList, MarketingBundle bundle) {
        List<MarketingBundleFeature> allBundleFeatures = bundle.getChildBundleFeatures();
        List<String> featureConfigCodes = new ArrayList<>();
        List<String> warningMessages = new ArrayList<>();
        for (MarketingBundleFeature bundleFeature : allBundleFeatures) {
            if (bundleFeature.getMarketingFeatureConfig() != null) {
                JSONObject featureConfigs = new JSONObject(bundleFeature.getMarketingFeatureConfig());
                for (Object featureConfigObj : featureConfigs.keySet()) {
                    featureConfigCodes.add((String) featureConfigObj);
                }
            } else if (bundleFeature.getFeature().getDeveloperConfig() != null) {
                JSONArray devConfigs = new JSONArray(bundleFeature.getFeature().getDeveloperConfig());
                for (int i = 0; i < devConfigs.length(); i++) {
                    JSONObject config = devConfigs.getJSONObject(i);
                    String featureCode = config.getJSONObject("name").getString("field");
                    featureConfigCodes.add(featureCode);
                }
            }
        }
        for (Object newFeatureConfigObj : newFeatureConfigList.keySet()) {
            if (!featureConfigCodes.contains((String) newFeatureConfigObj)) {
                String msg = "Incorrect config code in newBundle for: Bundle: " + bundle.getCode() + " | config: " + newFeatureConfigObj.toString();
                warningMessages.add(msg);
                logger.info(msg);
            }
        }
        return warningMessages;
    }
}
