package com.marketo.provisioningservice.service;

import java.util.*;

import com.marketo.provisioningservice.util.SpaToProvisionConverter;
import com.marketo.subscriptionservice.model.ModuleStatus;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

@Component
public class ProvisioningContext {

    public final String PROVISION_SOURCE = "provisionSource";
    
    // RequestId can be used to track a request
    private final String requestUUID = UUID.randomUUID().toString();
    
    private String tenantId;
    
    // Form parameters sent by SPA as input to initial subscription creation.
    private MultiValueMap<String, String> params;

    // newBundleConfig parameter from request
    private JSONObject newBundleConfig = new JSONObject();

    // Active Pod 
    private String activePod;

    // Check whether SPA called provisioning service or MLM UI
    private String provisionSource;

    private List<String> executedSteps = new ArrayList<>();

    private HashMap<String, String> featurePodSelection =  new HashMap<>();
    
    private List<Exception> errors = new ArrayList<>();

    // warnings
    private List<String> warnings = new ArrayList<>();
    
    public List<Exception> getErrors() {
        return errors;
    }

    public void addError(Exception e) {
        errors.add(e);
    }

    public String getFormattedErrors() {
        String errorStr = "";
        for(Exception e : errors) {
            errorStr += e.getMessage() + "\n";
        }
        return errorStr;
    }

    public List<String> getExecutedSteps() {return executedSteps; }

    public void addExecutedStep(String moduleKey, String status, String reason) {
        String step = String.format("Module [%s] : %s", moduleKey, status);
        if (status.equalsIgnoreCase(ModuleStatus.FAILURE.toString())) {
            step += " Reason: " + reason;
        }
        executedSteps.add(step);
    }

    public String getFormattedExecutedSteps() {
        String executedStepFormattedString = "Progress Steps: ";
        for (String executedStepString : executedSteps) {
            executedStepFormattedString += executedStepString + "\n";
        }
        return executedStepFormattedString;
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public void addWarning(String warningMsg) {
        warnings.add(warningMsg);
    }

    public String getFormattedWarnings() {
        String warningMsg = "";
        for (String warning : warnings) {
            warningMsg += warning + "\n";
        }
        return warningMsg;
    }
    /*
    private ProvisioningContext(ProvisioningContextBuilder builder) {
        this.requestUUID = builder.requestUUID;
        this.tenantId = builder.tenantId;
        this.params = builder.params;
        this.token = builder.token;
        this.activePod = builder.activePod;
    }
    */
    
    public String getRequestUUID() {
        return requestUUID;
    }
    
    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getProvisionSource() {
        return provisionSource;
    }

    public void setProvisionSource(String provisionSource) {
        this.provisionSource = provisionSource;
    }
    
    public MultiValueMap<String, String> getParams() {
        return params;
    }

    public String getActivePod() {
        return activePod;
    }
    
    public void setActivePod(String activePod) {
        this.activePod = activePod;
    }
     
    public void setParams(MultiValueMap<String, String> params) {
        this.params = params;
    }
        
    /*
    public static class ProvisioningContextBuilder {
        
        private String requestUUID = UUID.randomUUID().toString();;
        
        private String tenantId;
        
        private MultiValueMap<String, String> params;
        
        private String token;
        
        private String activePod;
        
        public ProvisioningContextBuilder setTenantId(String tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public ProvisioningContextBuilder setParams(MultiValueMap<String, String> params) {
            this.params = params;
            return this;
        }

        public ProvisioningContextBuilder setToken(String token) {
            this.token = token;
            return this;
        }
        
        public ProvisioningContext build() {
            return new ProvisioningContext(this);
        }
    }
    */

    public HashMap<String, String> getFeaturePodSelection() {
        HashMap<String, String> podsMap = featurePodSelection;

        if(podsMap.size() == 0 && params != null) {
            podsMap = parsePodSelectionFromParam(params.getFirst("pods"));
        }
        return podsMap;
    }

    public HashMap<String, String> parsePodSelectionFromParam(String podsStr) {
        HashMap<String, String> podsMap = new HashMap<>();
        if (StringUtils.isEmpty(podsStr)) {
            return podsMap;
        }

        JSONObject podsJson = new JSONObject(podsStr);
        for (Iterator<String> podsItr = podsJson.keys(); podsItr.hasNext();) {
            String podKey = podsItr.next();
            podsMap.put(podKey, (String) podsJson.get(podKey));
        }

        this.featurePodSelection = podsMap;
        return podsMap;
    }

    /**
     * Function to return the newBundleConfig JSONObject if it is
     * already present else parse the params to get newBundleConfig.
     * @return newBundleConfig JSONObject
     */
    public JSONObject getNewBundleConfig() {
        JSONObject newBundleConfig = this.newBundleConfig;
        if ( params != null && params.containsKey("newBundle") && newBundleConfig.length() == 0 ) {
            String newBundleConfigString = params.getFirst("newBundle");
            if (newBundleConfigString.trim().length()>0 && !newBundleConfigString.trim().equals("null")) {
                newBundleConfig = parseNewBundleConfigFromParam(newBundleConfigString);
            }
        }
        return newBundleConfig;
    }

    /**
     * Function to parse the configParams to generate the
     * newBundleConfig JSONObject and setting in the provisioningContext
     * @param newBundleConfigString
     * @return newBundleConfig JSONObject
     */
    public JSONObject parseNewBundleConfigFromParam(String newBundleConfigString) {
        JSONObject newBundleConfigParam = new JSONObject(newBundleConfigString);
        SpaToProvisionConverter.convertSpaToProvisionValues(newBundleConfigParam);
        this.newBundleConfig = newBundleConfigParam;
        return newBundleConfig;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        
        sb.append("RequestID: ").append(requestUUID).append("\n");
        
        if (!StringUtils.isEmpty(tenantId)) {
            sb.append("Tenant ID: ").append(tenantId).append("\n");
        }
        
        if (params != null) {
            sb.append("Context parameters: " + params.toString());
        }
        
        return sb.toString();
    }
}
