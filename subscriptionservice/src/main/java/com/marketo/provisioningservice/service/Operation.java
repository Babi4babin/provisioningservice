package com.marketo.provisioningservice.service;

public enum Operation {
    PROVISION,
    DEPROVISION,
    UPDATE
}
