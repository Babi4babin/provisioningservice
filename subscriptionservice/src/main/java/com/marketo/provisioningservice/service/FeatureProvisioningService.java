package com.marketo.provisioningservice.service;

import com.marketo.provisioningservice.features.API250OkProvisioningHandler;
import com.marketo.provisioningservice.features.SiriusProvisioningHandler;
import com.marketo.subscriptionservice.model.ClientType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.provisioningservice.features.DefaultFeatureProvisioningHandler;
import com.marketo.provisioningservice.features.MLMProvisioningHandler;

@Component
public class FeatureProvisioningService {
   
    private static final Logger logger = LoggerFactory.getLogger(FeatureProvisioningService.class);
    
    public static final String BASE_FEATURE = "Base"; 
    
    @Autowired
    private MLMProvisioningHandler mlmFeatureHandler;
    
    @Autowired
    private DefaultFeatureProvisioningHandler defaultFeatureHandler;

    @Autowired
    private SiriusProvisioningHandler siriusProvisioningHandler;

    @Autowired
    private API250OkProvisioningHandler api250OkProvisioningHandler;

    public void doOperation(MarketingFeature feature, Operation operation, ProvisioningContext context) {
        // If feature is base feature then do special provisioning
        
        if (BASE_FEATURE.equals(feature.getName())) {

            String log = String.format("About to %s Base MLM feature for Subscription: %s", 
                    operation, feature.getName(), context.getTenantId());

            logger.info(log);
            mlmFeatureHandler.doOperation(feature, operation, context);
            logger.info("MLM Base feature provisioned successfully.");
        } else {
            
            String log = String.format("About to %s feature: %s for Subscription: %s", 
                    operation, feature.getName(), context.getTenantId());
            
            logger.info(log);

            if (feature.getClientType() == null ||
                    (feature.getClientType() != null && feature.getClientType().equalsIgnoreCase(ClientType.MLM.toString()))) {
                defaultFeatureHandler.doOperation(feature, operation, context);
            }

            if (feature.getClientType() != null && feature.getClientType().equalsIgnoreCase(ClientType.SIRIUS.toString()))
                siriusProvisioningHandler.doOperation(feature, operation, context);

            if(feature.getClientType() != null && feature.getClientType().equalsIgnoreCase(ClientType.API250OK.toString()))
                api250OkProvisioningHandler.doOperation(feature,operation,context);

        }            
    }
}
