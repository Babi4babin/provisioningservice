package com.marketo.subscriptionservice.utility;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.stereotype.Component;

/**
 * This is the Utility class which can be used for any json operations
 */
@Component
public class JsonUtility{

    /**
     * Get the field value from json String as a Boolean.
     * @param jsonString
     * @return
     */
    public boolean getFieldValueFromJsonString(String jsonString, String parentNode, String childNode) {
      JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
      return  jsonObject.getAsJsonObject(parentNode).get(childNode).getAsBoolean();
    }
}
