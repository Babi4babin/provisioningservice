package com.marketo.subscriptionservice.utility;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


@Component
public class SecurityUtils {


	private static final Logger logger = LoggerFactory.getLogger(SecurityUtils.class);

	/****
	 *
	 * Hashing method
	 *
	 * @param password
	 * @param type
	 * @return
	 */
	public String hash(String type, String password)
			throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance(type);
		byte[] shaByteArr = mDigest.digest(password.getBytes(Charset.forName("UTF-8")));
		StringBuilder hexStrBuilder = new StringBuilder();
		for (int i = 0; i < shaByteArr.length; i++)
			hexStrBuilder.append(String.format("%02x", shaByteArr[i]));

		logger.info("hash value generated : " + hexStrBuilder.toString());

		return hexStrBuilder.toString();
	}

	/***
	 *
	 *
	 * Method to hash the given input
	 *
	 * @param password
	 * @param communityToken
	 * @param hashMethod
	 * @return
	 */
	public String hashPassword(String password, String communityToken, String hashMethod) {
		String encrypted = "";
		try {
			logger.info("community token :" + communityToken + " & hashMethod :" + hashMethod);
			encrypted = hash(hashMethod, communityToken);
			encrypted = hash(hashMethod, password.concat(encrypted));
		} catch (Exception e) {
			logger.error("Error while encrypting data", e);
		}
		return encrypted;
	}

}
