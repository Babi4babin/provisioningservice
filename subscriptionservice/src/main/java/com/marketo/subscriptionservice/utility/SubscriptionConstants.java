package com.marketo.subscriptionservice.utility;

import java.util.Arrays;
import java.util.List;

/**
 * This class be used for having the Constants and Error messages
 */
public class SubscriptionConstants {

    public static final String UNABLE_TO_CANCEL_SUB = "Unable to delete the Subscription, Subscription should be in the Cancel Dormant state to Cancel";

    //250okapi service
    public static final String API250OK_UNABLE_TO_CREATE_CHILD_ACCOUNT = "Failed to created child account";
    public static final String API250OK_UNABLE_TO_PROVISION_BUNDLE = "Failed to provision requested bundle";
    public static final String API250OK_UNABLE_TO_FETCH_CHILD_ACCOUNT = "Failed to fetch child account details";
    public static final String API250OK_CHILD_ACCOUNT_DOES_NOT_EXIST = "250ok child account does not exist";
    public static final String API250OK_ACCESS_DENIED = "Access denied";
    public static final String API250OK_AUTHENTICATION_FAILURE = "Authentication failed at 250ok api";
    public static final String API250OK_INTERNAL_SERVER_ERROR = "Internal server error";
    public static final String API250OK_MARKETO_EMAIL_DOMAIN = "@marketo.com";
    public static final String API250OK_MARKETO_EMAIL_DELIVERABILITY = "emailDeliverability";
    public static final String API250OK_MARKETO_EMAIL_INFORMANT = "emailInformant";
    public static final String API250OK_MARKETO_EMAIL_REPUTATION = "emailReputation";
    public static final String API250OK_MARKETO_UNABLE_TO_ENABLE_DELIVERABILITY_TOOLS_TILE = "Failed to enable Deliverability Tools Tile in MLM";
    public static final String API250OK_MARKETO_UNABLE_TO_DISABLE_DELIVERABILITY_TOOLS_TILE = "Failed to disable Deliverability Tools Tile in MLM";

    //250ok api fields
    public static final String API250OK_FIELD_INBOX = "inbox";
    public static final String API250OK_FIELD_BLACKLIST = "blacklist";
    public static final String API250OK_FIELD_REPUTATION = "reputation";
    public static final String API250OK_FIELD_DESIGN = "design";
    public static final String API250OK_FIELD_EMAIL_INFORMANT = "emailinformant";
    public static final String API250OK_FIELD_DMARC = "dmarc";

    //250ok api actions
    public static final String API250OK_ACTION_PROVISION ="provision";
    public static final String API250OK_ACTION_DEPROVISION ="deprovision";
    public static final String API250OK_ACTION_UPDATE ="update";




}
