package com.marketo.subscriptionservice.rest;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ProvisioningResponse extends BaseResponse {
    private String tenantUUID;

    public ProvisioningResponse(String tenantUUID) {
        this.tenantUUID = tenantUUID;
    }

}
