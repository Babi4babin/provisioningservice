package com.marketo.subscriptionservice.rest.exception;

public class ProvisioningException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 6594004704365679672L;

    public ProvisioningException(String msg) {
        super(msg);
    }
    
    public ProvisioningException(Throwable cause) {
        super(cause);
    }
    
    public ProvisioningException(String message, Throwable cause) {
        super(message, cause);
    }
}
