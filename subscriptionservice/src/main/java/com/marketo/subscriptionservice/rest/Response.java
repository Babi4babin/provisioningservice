package com.marketo.subscriptionservice.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.marketo.provisioningservice.service.ProvisioningContext;
import lombok.ToString;

@ToString
public class Response<S> {
    
    private String requestId;

	private boolean success;
    
    private List<ErrorResponse> errors = null;
    
    private List<String> warnings = null;
    
    private List<S> result = null;
    
    public Response() {}

    public Response(boolean success, S s) {
        this(success, s, null);        
    }

    public Response(S s, ProvisioningContext context) {
        this(true, s, context);
    }
    
    public Response(boolean success, S s, ProvisioningContext context) {
        
        if (context != null) {
            this.requestId = context.getRequestUUID();            
        } else {
            this.requestId = UUID.randomUUID().toString();
        }

        this.success = success;
        result = new ArrayList<S>();
        result.add(s);
        warnings = new ArrayList<>();

        if (context != null && context.getErrors() != null && context.getErrors().size() > 0) {
            for (Exception e : context.getErrors()) {
                warnings.add(e.getLocalizedMessage());
            }
        }
        if (context != null && context.getWarnings() != null && context.getWarnings().size() > 0) {
            warnings.add("Warnings to be looked upon: \n");
            for (String warningMessage : context.getWarnings()) {
                warnings.add(warningMessage + "\n");
            }
        }
        if (context != null && context.getExecutedSteps() != null && context.getExecutedSteps().size()>0) {
            warnings.add("Progress Steps: \n");
            for (String executedStep : context.getExecutedSteps()) {
                warnings.add(executedStep + "\n");
            }
        }
    }
    
    public Response(boolean success) {
        this.success = success;
    }
    
    public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

    public List<ErrorResponse> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorResponse> errors) {
        this.errors = errors;
    }
    
    public List<S> getResult() {
        return result;
    }

    public void setResult(List<S> result) {
        this.result = result;
    }

    public List<String> getWarnings() {
        return warnings;
    }

}
