package com.marketo.subscriptionservice.rest;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.marketo.subscriptionservice.rest.exception.CustomErrorResponseHandler;

@Component
public class RestClient {
   
    private static Logger logger = LoggerFactory.getLogger(RestClient.class);
    
    private RestTemplate restTemplate;
    
    public RestClient() {
        this.restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new CustomErrorResponseHandler());
    }
    
    /**
     * Do post request with url encoded form data
     * @param url
     * @param data
     * @return
     * TODO: Implement retries here
     */
    public String postForm(String url, MultiValueMap<String, String> data) {
                
        HttpHeaders headers = new HttpHeaders();
        final Map<String, String> parameterMap = new HashMap<String, String>(4);
        String response = null;
        
        parameterMap.put("charset", "utf-8");
                
        headers.setContentType(new MediaType("application", "x-www-form-urlencoded",
                parameterMap));
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(data,
                headers);
        
        logger.info("Sending request to url: " + url);
                
        response = restTemplate.postForObject(url,
                entity, String.class);

        logger.info("Response recieved: " + response);

        return response;
    }
    
    /**
     * Do post request with json data
     * 
     * @param url
     * @param data
     * @return
     * TODO: Implement retries here
     */
    public String postJson(String url, String data) {
        
        HttpHeaders headers = new HttpHeaders();
        final Map<String, String> parameterMap = new HashMap<String, String>(4);
        String response = null;
                
        parameterMap.put("charset", "utf-8");
                
        headers.setContentType(new MediaType("application", "json",
                parameterMap));
        
        HttpEntity<String> entity = null;
        
        // Due to peculiarities of MLM internal rest api, 
        // we need to send some json/form data for POST request 
        if (StringUtils.isEmpty(data)) {
            entity = new HttpEntity<String>("{}", headers);        
        } else {
            entity = new HttpEntity<String>(data,
                    headers);
        }
        
        
        logger.info("Sending request to url: " + url);
        
        logger.info("Config data: " + data);
        
        response = restTemplate.postForObject(url,
                entity, String.class);
        
        logger.info("Recieved response: " + response);

        return response;
        
    }

    /**
     * Do Post Request with Json Data and Auth Token
     *
     * @param url
     * @param data
     * @param headers
     * @return
     */
    public String postJson(String url, String data, HttpHeaders headers) {

        final Map<String, String> parameterMap = new HashMap<String, String>(4);
        String response = null;

        parameterMap.put("charset", "utf-8");

        headers.setContentType(new MediaType("application", "json",
                parameterMap));

        HttpEntity<String> entity = null;

        // Due to peculiarities of MLM internal rest api,
        // we need to send some json/form data for POST request
        if (StringUtils.isEmpty(data)) {
            entity = new HttpEntity<String>("{}", headers);
        } else {
            entity = new HttpEntity<String>(data,
                    headers);
        }


        logger.info("Sending request to url: " + url);

        logger.info("Config data: " + data);

        response = restTemplate.postForObject(url,
                entity, String.class);

        logger.info("Recieved response: " + response);

        return response;

    }

    /**
     *
     * Do Get Request with Authentication Header
     * @param url
     * @param headers
     * @return
     */
    public String getJson(String url, HttpHeaders headers) {

        final Map<String, String> parameterMap = new HashMap<String, String>(4);

        parameterMap.put("charset", "utf-8");

        headers.setContentType(new MediaType("application", "json",
                parameterMap));

        HttpEntity<String> entity = new HttpEntity<String>("{}", headers);

        logger.info("Sending request to url: " + url);
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        logger.info("Recieved response: " + response.getBody().toString());
        return response.getBody().toString();
    }
}
