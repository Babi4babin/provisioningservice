package com.marketo.subscriptionservice.rest.exception;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

public class CustomErrorResponseHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
                
        return response.getStatusCode() != HttpStatus.OK; 
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        throw new ProvisioningException(IOUtils.toString(response.getBody()));
    }
}
