package com.marketo.subscriptionservice.rest.exception;

/**
 * Created by rmalani on 6/8/17.
 */
public class ModuleNotFoundException extends ProvisioningInputException {



    public ModuleNotFoundException(String message) {
        super(message);
    }

    public ModuleNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ModuleNotFoundException(Throwable cause) {
        super(cause);
    }

}
