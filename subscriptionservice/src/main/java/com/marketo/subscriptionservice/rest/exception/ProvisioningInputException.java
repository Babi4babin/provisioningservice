package com.marketo.subscriptionservice.rest.exception;

public class ProvisioningInputException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 6594004704365679672L;

    public ProvisioningInputException(String msg) {
        super(msg);
    }
    
    public ProvisioningInputException(Throwable cause) {
        super(cause);
    }
    
    public ProvisioningInputException(String message, Throwable cause) {
        super(message, cause);
    }
}
