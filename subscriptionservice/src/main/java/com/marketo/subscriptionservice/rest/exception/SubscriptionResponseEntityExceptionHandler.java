package com.marketo.subscriptionservice.rest.exception;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.marketo.bundlemetadata.exception.BundleMetadataException;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionservice.rest.BaseResponse;
import com.marketo.subscriptionservice.rest.ErrorResponse;
import com.marketo.subscriptionservice.rest.Response;

@ControllerAdvice
public class SubscriptionResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    
    @Autowired
    private ProvisioningContext provisioningContext;
    
    @ExceptionHandler(value = {ProvisioningInputException.class, ProvisioningInputException.class})
    protected ResponseEntity<Response<BaseResponse>> handleProvisioningInputException(RuntimeException ex, WebRequest req) {
        
        // Get error object
        ErrorResponse error = new ErrorResponse("1", ex.getMessage());
        
        Response<BaseResponse> response = new Response<>(false, null);
        response.setErrors(Collections.singletonList(error));
        
        return new ResponseEntity<Response<BaseResponse>>(response, HttpStatus.OK);        
    }
    
    @ExceptionHandler(value = {ProvisioningException.class, ProvisioningException.class})
    protected ResponseEntity<Response<BaseResponse>> handleProvisioningException(RuntimeException ex, WebRequest req) {
        
        // Get error object
        ErrorResponse error = new ErrorResponse("4", ex.getMessage());
        
        Response<BaseResponse> response = new Response<>(false, null, provisioningContext);
        response.setErrors(Collections.singletonList(error));
        
        return new ResponseEntity<Response<BaseResponse>>(response, HttpStatus.OK);        
    }
    
    @ExceptionHandler(value = {BundleMetadataException.class, BundleMetadataException.class})
    protected ResponseEntity<Response<BaseResponse>> handleBundleMetadataException(RuntimeException ex, WebRequest req) {
        
        // Get error object
        ErrorResponse error = new ErrorResponse("1", ex.getMessage());
        
        Response<BaseResponse> response = new Response<>(false, null);
        response.setErrors(Collections.singletonList(error));
        
        return new ResponseEntity<Response<BaseResponse>>(response, HttpStatus.OK);        
    }
 
    @ExceptionHandler(value = {Exception.class, Exception.class})
    protected ResponseEntity<Response<BaseResponse>> handleException(RuntimeException ex, WebRequest req) {
        
        // Get error object
        logger.error("Exception while processing request", ex);
        
        ErrorResponse error = new ErrorResponse("4", ex.getMessage());
        
        Response<BaseResponse> response = new Response<>(false, null, provisioningContext);
        response.setErrors(Collections.singletonList(error));
        
        return new ResponseEntity<Response<BaseResponse>>(response, HttpStatus.OK);
    }  
}
