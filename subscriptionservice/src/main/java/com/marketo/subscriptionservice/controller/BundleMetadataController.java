package com.marketo.subscriptionservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionservice.rest.Response;

@Controller
@RequestMapping(value = "/rest/v1/bundlemetadata")
public class BundleMetadataController {

    @Autowired
    private BundleMetadataService bundleMetadataService;
    
    @Autowired
    ProvisioningContext provisioningcontext;
    
    @RequestMapping(value = "/bundles/{name}.json", method = RequestMethod.GET)
    @ResponseBody
    public Response<MarketingBundle> getMarketingBundle(@PathVariable String name) {
        
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle(name);
        
        return new Response<MarketingBundle>(bundle, provisioningcontext);        
    }
    
    @RequestMapping(value = "/bundles.json", method = RequestMethod.GET)
    @ResponseBody
    public Response<List<MarketingBundle>> getBundles(@RequestParam(value = "root", required = false) boolean root) {
        
        List<MarketingBundle> bundles;
        if (root) {
            bundles = bundleMetadataService.getRootBundles();
        } else {
            bundles = bundleMetadataService.getAllBundles();
        }        
        
        return new Response<List<MarketingBundle>>(bundles, provisioningcontext);
    }
}
