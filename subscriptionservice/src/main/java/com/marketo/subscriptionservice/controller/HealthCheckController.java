package com.marketo.subscriptionservice.controller;

import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.marketo.subscriptionservice.model.AppInfo;
import com.marketo.subscriptionservice.model.BuildInfo;
import com.marketo.subscriptionservice.model.Status;

/*
 * import com.marketo.treeservice.model.AppInfo; import
 * com.marketo.treeservice.model.BuildInfo; import
 * com.marketo.treeservice.model.Status;
 */

@Controller
@RequestMapping("/healthcheck")
public class HealthCheckController {
    /**
     * Logger for this class
     */
    private static final Logger logger = LoggerFactory.getLogger(HealthCheckController.class);

    @ResponseBody
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String status() {
        Status status = null;
        try {
            // TO DO: You need to have business logic here to determine whether
            // status is up or down.
            // For now setting to up.
            status = Status.UP;
        } catch (Exception ex) {
            status = Status.DOWN;
        }

        return status.toString();
    }

    @ResponseBody
    @RequestMapping(value = "/version", method = RequestMethod.GET, produces = "application/json")
    public BuildInfo version() {
        Properties props = getBuildInfoProperties();

        BuildInfo buildInfo = new BuildInfo();

        buildInfo.setJobName(props.getProperty("build.jobName"));
        buildInfo.setBuildNumber(props.getProperty("build.number"));
        buildInfo.setReleaseName(props.getProperty("build.releaseName"));
        buildInfo.setBuildTimestamp(props.getProperty("build.timestamp"));

        return buildInfo;
    }

    @ResponseBody
    @RequestMapping(value = "/info", method = RequestMethod.GET, produces = "application/json")
    public AppInfo info() {
        // TO DO: AppInfo is right now dummy. We need to add necessary fields in
        // there and populate it here
        AppInfo appInfo = new AppInfo();

        return appInfo;
    }

    @RequestMapping(value = "/smoketest", method = RequestMethod.GET)
    @ResponseBody
    public String smokeTest() {
        String status = "Success";
        try {
            // Do some smoke tests here
        } catch (Exception ex) {
            logger.error(ex.getMessage(), (Throwable) ex);
            status = "Failed";
        }

        return status;
    }

    private Properties getBuildInfoProperties() {
        try {
            InputStream in = getClass().getClassLoader().getResourceAsStream("build.properties");
            Properties props = new Properties();
            props.load(in);
            return props;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), (Throwable) ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
}