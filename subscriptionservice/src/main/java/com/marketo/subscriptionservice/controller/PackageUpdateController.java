package com.marketo.subscriptionservice.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.marketo.packageupdater.model.PackageUpdateRequest;
import com.marketo.packageupdater.model.PackageUpdateResponse;
import com.marketo.packageupdater.model.UpdateTrackResponse;
import com.marketo.packageupdater.service.PackageUpdateService;
import com.marketo.subscriptionservice.rest.Response;

@Controller
@RequestMapping(value = "/rest/v1/packageupdater")
public class PackageUpdateController {

    @Autowired
    private PackageUpdateService packageUpdateService;
    
    @RequestMapping(value = "update.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<PackageUpdateResponse> updatePackages(@Valid @RequestBody PackageUpdateRequest packageUpdateRequest) {
        
        PackageUpdateResponse packageUpdateResponse = packageUpdateService.updatePackages(packageUpdateRequest);
       
        return new Response<PackageUpdateResponse>(true, packageUpdateResponse);
    }

    @RequestMapping(value = "track", method = RequestMethod.GET)
    @ResponseBody
    public Response<UpdateTrackResponse> trackUpdate(@RequestParam int trackerId) {
    
        UpdateTrackResponse updateTrackResponse = packageUpdateService.trackUpdate(trackerId);
        
        return new Response<UpdateTrackResponse>(true, updateTrackResponse);
    }    
}
