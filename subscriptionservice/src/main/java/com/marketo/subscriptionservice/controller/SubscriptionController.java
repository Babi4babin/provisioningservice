package com.marketo.subscriptionservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.provisioningservice.service.ProvisioningService;
import com.marketo.subscriptionservice.model.BundleFeaturesRequest;
import com.marketo.subscriptionservice.model.ProvisionSource;
import com.marketo.subscriptionservice.model.SubscriptionStatus;
import com.marketo.subscriptionservice.rest.BaseResponse;
import com.marketo.subscriptionservice.rest.ProvisioningResponse;
import com.marketo.subscriptionservice.rest.Response;
import com.marketo.subscriptionservice.service.SubscriptionService;

@Controller
@RequestMapping(value = "/rest/v1/subscriptions")
public class SubscriptionController {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionController.class);
	
    @Autowired
    ProvisioningService provisioningService;
        
    @Autowired
    ProvisioningContext provisioningcontext;
    
    @Autowired
    SubscriptionService subscriptionService;

    /**
     * Provision a subscription.
     * @param data
     * @return
     */
    @RequestMapping(value = "/provision.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<ProvisioningResponse> provisionSubscription(@RequestBody MultiValueMap<String, String> data) {
        
        Long start = System.currentTimeMillis();
        
        logger.info("Start Provisioning for context:" + provisioningcontext);
        
        ProvisioningResponse response = provisioningService.provisionSubscription(data);
        
        String info = String.format("[%s] Subscription Provisioning computation time(ms) %s", 
                provisioningcontext.getRequestUUID(), (System.currentTimeMillis()-start));
        
        logger.info(info);
        
        return new Response<ProvisioningResponse>(response, provisioningcontext);
    }

    /**
     * Provision a subscription - Rubicks.
     * @param data
     * @return
     */
    @RequestMapping(value = "/newProvision.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<ProvisioningResponse> newProvisionSubscription(@RequestBody MultiValueMap<String, String> data) {

        Long start = System.currentTimeMillis();

        logger.info("Start Provisioning for context:" + provisioningcontext);

        ProvisioningResponse response = provisioningService.newProvisionSubscription(data);

        String info = String.format("[%s] Subscription Provisioning computation time(ms) %s",
                provisioningcontext.getRequestUUID(), (System.currentTimeMillis()-start));

        logger.info(info);

        return new Response<ProvisioningResponse>(response, provisioningcontext);
    }

    /**
     * V2 & Rubicks - MLM Signal Subscription created, so proceed with provisioning rest of the features and modules
     * @param tenantUUID
     * @param data
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/postCreated.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<ProvisioningResponse> postSubscriptionCreated(@PathVariable String tenantUUID, @RequestBody MultiValueMap<String, String> data) {

        ProvisioningResponse response = provisioningService.doPostSubscriptionCreated(tenantUUID, data);

        logger.info(String.format("tenantUUID = [%s], Provisioning confirmed context: %s",
                provisioningcontext.getRequestUUID(), provisioningcontext));

        return new Response<>(response, provisioningcontext);
    }

    /**
     * Migrate a subscription to Rubiks.
     * @param data
     * @return
     */
    @RequestMapping(value = "/migrate.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<ProvisioningResponse> migrate(@RequestBody MultiValueMap<String, String> data) {

        Long startTime = System.currentTimeMillis();

        logger.info("Start Migrating for context:" + provisioningcontext);

        ProvisioningResponse response = provisioningService.migrate(data);

        String info = String.format("[%s] Subscription migration computation time(ms) %s",
                provisioningcontext.getRequestUUID(), (System.currentTimeMillis()-startTime));

        logger.info(info);

        return new Response<>(response, provisioningcontext);
    }

    /**
     * Provision a feature.
     * @param tenantId
     * @param id
     * @param token
     * @param configParams
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/features/{id}/provision.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<BaseResponse> provisionFeature(@PathVariable String tenantUUID, 
            @PathVariable int id, @RequestBody String configParams) {
        
        provisioningService.provisionFeature(tenantUUID, id, configParams);
        
        return new Response<BaseResponse>(new BaseResponse(), provisioningcontext);        
    }

    /**
     * Disable  a feature.
     * @param tenantId
     * @param id
     * @param token
     * @param configParams
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/features/{id}/delete.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<BaseResponse> deleteFeature(@PathVariable String tenantUUID, 
            @PathVariable int id) {
        
        provisioningService.deprovisionFeature(tenantUUID, id);
        
        return new Response<BaseResponse>(new BaseResponse(), provisioningcontext);        
    }    

    /**
     * Disable  a feature.
     * @param tenantId
     * @param id
     * @param token
     * @param configParams
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/features/{id}/update.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<BaseResponse> updateFeature(@PathVariable String tenantUUID, 
            @PathVariable int id, @RequestBody String configParams) {
                        
        provisioningService.updateFeature(tenantUUID, id, configParams);
        
        return new Response<BaseResponse>(new BaseResponse(), provisioningcontext);        
    }    
    
    /**
     * TODO - Remove after all customer migrated to Rubiks - Module packaging
     * Update the top level bundle for this subscription
     * @param tenantUUID
     * @param bundleId
     * @param token
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/updatebundle.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<BaseResponse> updateBundle(@PathVariable String tenantUUID, 
            @RequestParam(value = "bundleId", required = true) int bundleId) {
        
        // Do an update
        provisioningService.updateBundle(tenantUUID, bundleId);
        
        return new Response<BaseResponse>(new BaseResponse(), provisioningcontext);
    }

    /**
     *
     *  Update the top level bundle for this subscription bundle
     * @param tenantUUID
     * @param bundleId
     * @param bundleFeaturesRequest
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/bundles/{bundleId}/update.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<BaseResponse> updateBundle(@PathVariable String tenantUUID, @PathVariable int bundleId,
                                               @RequestBody BundleFeaturesRequest bundleFeaturesRequest) {

        provisioningService.updateBundle(tenantUUID, bundleId, bundleFeaturesRequest);

        return new Response<BaseResponse>(new BaseResponse(), provisioningcontext);
    }


    /**
     *
     *  Provision a bundle for this subscription (upgrade)
     * @param tenantUUID
     * @param bundleId
     * @param bundleFeaturesRequest
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/bundles/{bundleId}/provision.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<BaseResponse> provisionBundle(@PathVariable String tenantUUID, @PathVariable int bundleId,
                                            @RequestBody BundleFeaturesRequest bundleFeaturesRequest) {

        provisioningcontext.setProvisionSource(ProvisionSource.MLM.toString());
        provisioningService.provisionBundle(tenantUUID, bundleId, bundleFeaturesRequest);

        return new Response<BaseResponse>(new BaseResponse(), provisioningcontext);
    }

    /**
     * Update the existing subscription by adding new modules to it.
     * @param data
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/upgrade.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<ProvisioningResponse> upgradeSubscription(@PathVariable String tenantUUID, @RequestBody MultiValueMap<String, String> data) {

        Long startTime = System.currentTimeMillis();

        logger.info(String.format("Starting subscription update for addition of new modules. Context: %s", provisioningcontext));

        ProvisioningResponse response = provisioningService.upgradeSubscription(tenantUUID, data);

        logger.info(String.format("[%s] Addition of modules to subscription computation time(ms): %s", provisioningcontext.getRequestUUID(), (System.currentTimeMillis()-startTime)));

        return new Response<> (response, provisioningcontext);
    }


    @RequestMapping(value = "/{tenantUUID}/downgrade.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<ProvisioningResponse> downgradeSubscription(@PathVariable String tenantUUID, @RequestBody MultiValueMap<String, String> data) {

        Long startTime = System.currentTimeMillis();

        logger.info(String.format("Starting subscription update for removal of existing modules. Context: %s", provisioningcontext));

        ProvisioningResponse response = provisioningService.downgradeSubscription(tenantUUID, data);

        logger.info(String.format("[%s] Removal of modules from subscription computation time(ms): %s", provisioningcontext.getRequestUUID(), (System.currentTimeMillis()-startTime)));

        return new Response<>(response, provisioningcontext);
    }

    @RequestMapping(value = "/{tenantUUID}/modify.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<ProvisioningResponse> modifySubscription(@PathVariable String tenantUUID, @RequestBody MultiValueMap<String, String> data) {

        Long startTime = System.currentTimeMillis();

        logger.info(String.format("Starting subscription update for existing modules configuration update. Context: %s", provisioningcontext));

        ProvisioningResponse response = provisioningService.modifySubscriptionConfig(tenantUUID, data);

        logger.info(String.format("[%s] Updation of modules configuration for subscription computation time(ms): %s", provisioningcontext.getRequestUUID(), (System.currentTimeMillis()-startTime)));

        return new Response<>(response, provisioningcontext);
    }

    /**
     *
     *  Deprovision a bundle for this subscription (downgrade)
     * @param tenantUUID
     * @param bundleId
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/bundles/{bundleId}/deprovision.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<BaseResponse> deprovisionBundle(@PathVariable String tenantUUID, @PathVariable int bundleId) {

        provisioningService.deprovisionBundle(tenantUUID, bundleId);

        return new Response<BaseResponse>(new BaseResponse(), provisioningcontext);
    }

    /*
     * Get bundle for this subscription with all provisioned features
     * @param tenantUUID
     * @param bundleId
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/features/{code}/get.json", method = RequestMethod.GET)
    @ResponseBody
    public Response<?> getMarketingFeatureStatus(@PathVariable String tenantUUID, @PathVariable String code, @RequestParam (required = false) String datacenter) {
        logger.info("Starting to retrieve status for feature: " + code + " with tenantUUID: " + tenantUUID + " and datacenter: " + datacenter);
        String response = provisioningService.getMarketingFeatureStatus(tenantUUID, code, datacenter);
        return new Response<>(response, provisioningcontext);
    }
    
    /*
     * CANCEL the subscription permanently
     * @param tenantUUID
     * @return the Response with the success parameter
     */
    @RequestMapping(value = "/{tenantUUID}/cancelPermanent", method = RequestMethod.POST)
    @ResponseBody
    public Response<?> cancelSub(@PathVariable String tenantUUID, @RequestParam (required = true) String podLocation, @RequestParam (defaultValue="false", required = false) Boolean siriusCancelled) {
    	  logger.info("Starting cancellation of subscription tenantUUID: " + tenantUUID + " at pod(from spa request): " + podLocation);
        return subscriptionService.editSubscriptionStatus(tenantUUID, SubscriptionStatus.CANCEL.getStatusCode(), podLocation, siriusCancelled);
    }
    
    /*
     * CANCEL_DORMANT the subscription
     * @param tenantUUID
     * @return the Response with the success parameter
     */
    @RequestMapping(value = "/{tenantUUID}/cancelDormant", method = RequestMethod.POST)
    @ResponseBody
    public Response<?> cancelDormantSub(@PathVariable String tenantUUID, @RequestParam(required = false) String podLocation) {
        logger.info("Starting cancel dormant of subscription tenantUUID: " + tenantUUID + " at pod(from spa request): " + podLocation);
        return subscriptionService.editSubscriptionStatus(tenantUUID, SubscriptionStatus.CANCEL_DORMANT.getStatusCode(), podLocation, false);
    }
    
    /*
     * Activate the subscription
     * @param tenantUUID
     * @return the Response with the success parameter and error messages
     */
    @RequestMapping(value = "/{tenantUUID}/activate", method = RequestMethod.POST)
    @ResponseBody
    public Response<?> activateSub(@PathVariable String tenantUUID, @RequestParam String podLocation) {
        logger.info("Starting activation of subscription tenantUUID: " + tenantUUID + " at pod(from spa request): " + podLocation);
    	  return subscriptionService.editSubscriptionStatus(tenantUUID, SubscriptionStatus.ACTIVE.getStatusCode(), podLocation, false);
    }

    /*
     * Enable or disable data encryption flag for given tenant
     * @param tenantUUID
     * @return the Response with the success parameter and error messages
     */
    @RequestMapping(value = "/{tenantUUID}/dataEncryption",method = RequestMethod.POST)
    @ResponseBody
    public Response<?> updateEncryptionFlag(@PathVariable String tenantUUID, @RequestBody MultiValueMap<String, String> data) {
        logger.info(String.format("Updating data encryption flag of subscription tenantUUID: %s",tenantUUID));
        return subscriptionService.updateEncryptionFlag(tenantUUID,data);
    }

    /**
     * Only used for migration of 250ok from base into separate modules.
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/migrate250ok.json", method = RequestMethod.POST)
    @ResponseBody
    public Response<ProvisioningResponse> migrate250ok(@RequestBody MultiValueMap<String, String> data) {

        Long startTime = System.currentTimeMillis();

        logger.info("Start Migrating if 250ok Bundles for all 250ok accounts");

        ProvisioningResponse response = provisioningService.migrateTo250okBundles(data);

        String info = String.format("[%s] Subscription migration computation time(ms) %s",
                provisioningcontext.getRequestUUID(), (System.currentTimeMillis() - startTime));

        logger.info(info);

        return new Response<>(response, provisioningcontext);
    }


    
}
