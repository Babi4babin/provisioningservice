package com.marketo.subscriptionservice.controller;

import com.marketo.bundlemetadata.rest.model.SubscriptionBundles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.marketo.bundlemetadata.rest.model.BundlesFeatures;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundleFeatures;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.BaseResponse;
import com.marketo.subscriptionservice.rest.Response;

@Controller
@RequestMapping(value = "/rest/v1/subscriptionbundlemap")
public class SubscriptionBundleMapController {

    @Autowired
    private SubscriptionMetadataService subscriptionMetadataService;
    
    @Autowired
    ProvisioningContext provisioningcontext;
        
    /**
     * Get all the bundles/features for this subscription.
     * @param tenantUUID
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/provisioned.json", method = RequestMethod.GET)
    @ResponseBody    
    public Response<SubscriptionBundleFeatures> getProvisionedBundlesFeatures(@PathVariable String tenantUUID) {

        SubscriptionBundleFeatures bundleFeatures = subscriptionMetadataService.getBundlesFeatures(tenantUUID);

        return new Response<SubscriptionBundleFeatures>(bundleFeatures, provisioningcontext);
    }
    
    /**
     * Get all the bundles/features available for this subscription.
     * @param tenantUUID
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/available.json", method = RequestMethod.GET)
    @ResponseBody    
    public Response<BundlesFeatures> getAvailableBundlesFeatures(@PathVariable String tenantUUID) {
        
        BundlesFeatures bundleFeatures = subscriptionMetadataService.getAvailablesBundlesFeatures(tenantUUID);
        
        return new Response<BundlesFeatures>(bundleFeatures, provisioningcontext);
    }


    /**
     * Rubiks -Get all the available modules(bundles) with features for this subscription.
     * @param tenantUUID
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/availableModules.json", method = RequestMethod.GET)
    @ResponseBody
    public Response<SubscriptionBundles> getAvailableModulesFeatures(@PathVariable String tenantUUID) {

        SubscriptionBundles bundles = subscriptionMetadataService.getAvailableModules(tenantUUID);

        return new Response<SubscriptionBundles>(bundles, provisioningcontext);
    }

    /**
     * Rubiks -Get all the provisioned modules(bundles) with features for this subscription.
     * @param tenantUUID
     * @return
     */
    @RequestMapping(value = "/{tenantUUID}/provisionedModules.json", method = RequestMethod.GET)
    @ResponseBody
    public Response<SubscriptionBundles> getProvisionedModulesFeatures(@PathVariable String tenantUUID) {

        SubscriptionBundles bundles = subscriptionMetadataService.getProvisionedModules(tenantUUID);

        return new Response<SubscriptionBundles>(bundles, provisioningcontext);
    }

    /**
     * Add bundle to subscription_bundle_map
     * @param tenantUUID
     * @param name
     * @return
     */
    @RequestMapping(value = "{tenantUUID}/bundle/{name}/add.json")
    @ResponseBody
    public Response<BaseResponse> addBundle(@PathVariable String tenantUUID, @PathVariable String name) {
        
        subscriptionMetadataService.addMarketingBundle(tenantUUID, name);
        
        return new Response<BaseResponse>(true, new BaseResponse());
    }

    /**
     * Add bundle to subscription_bundle_map
     * @param tenantUUID
     * @param name
     * @return
     */
    @RequestMapping(value = "{tenantUUID}/feature/{name}/add.json")
    @ResponseBody
    public Response<BaseResponse> addFeature(@PathVariable String tenantUUID, @PathVariable String name, @RequestBody(required = false) String featureConfig) {
        
        subscriptionMetadataService.addMarketingFeature(tenantUUID, name, featureConfig, null);
        
        return new Response<BaseResponse>(true, new BaseResponse());
    }

    /**
     * Delete all bundles and feature in subscription_bundle_map for tenantUUID
     * @param tenantUUID
     * @return
     */
    @RequestMapping(value = "{tenantUUID}/deleteAll.json")
    @ResponseBody
    public Response<BaseResponse> addFeature(@PathVariable String tenantUUID) {

        subscriptionMetadataService.deleteBundlesAndFeatures(tenantUUID);

        return new Response<BaseResponse>(true, new BaseResponse());
    }

    /**
     * updateConfig provisioned feature config in subscription_bundle_map,
     * e.g. used to update the RCA pod after move, could be called from CPT move tool
     * @param tenantUUID
     * @param code, feature code
     * @return BaseResponse
     */
    @RequestMapping(value = "{tenantUUID}/feature/{code}/updateConfig.json")
    @ResponseBody
    public Response<BaseResponse> updateFeatureConfig(@PathVariable String tenantUUID, @PathVariable String code, @RequestBody String featureConfig) {

        subscriptionMetadataService.updateMarketingFeatureConfig(tenantUUID, code, featureConfig);

        return new Response<BaseResponse>(true, new BaseResponse());
    }

}
