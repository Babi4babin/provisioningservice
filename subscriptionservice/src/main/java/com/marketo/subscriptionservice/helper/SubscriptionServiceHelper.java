package com.marketo.subscriptionservice.helper;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class SubscriptionServiceHelper {

    public static final String ENABLE = "enable";
    public static final String DISABLE = "disable";


    /**
     * Method to validate given Deliverability Partner Enabled parameters
     *
     * @param status
     **/
    public void validateInputParameters(String status) {
        if (StringUtils.isEmpty(status) || (!status.equals(ENABLE) &&
                !status.equals(DISABLE))) {
            throw new RuntimeException(String.format("Invalid Status for Deliverability Partner Enabled : %s", status));
        }
    }

}
