package com.marketo.subscriptionservice.model;

/**
 * Created by asawhney on 6/1/17.
 */
public enum FeatureCode {

    ORIONBase("orionBase"),
    RCA("revenueCycleAnalytics");

    private final String code;

    private FeatureCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}
