package com.marketo.subscriptionservice.model.sirius;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.json.JSONObject;

/**
 * Created by asawhney on 3/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SiriusBacklogJob {

    private String backlogId;

    private String status;

    public String getBacklogId() {
        return backlogId;
    }

    public void setBacklogId(String backlogId) {
        this.backlogId = backlogId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInfo() {
        JSONObject obj = new JSONObject();
        obj.put("status", status);
        obj.put("backlogId", backlogId);
        return obj.toString();
    }
}
