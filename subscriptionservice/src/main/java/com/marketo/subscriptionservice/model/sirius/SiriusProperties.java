package com.marketo.subscriptionservice.model.sirius;

/**
 * Created by asawhney on 3/7/17.
 */
public class SiriusProperties {

    private String munchkinId;

    private String mlmInternalEndPoint;

    private String leadDbSize;

    private String podName;

    public static class Builder {

        private String munchkinId;

        private String mlmInternalEndPoint;

        private String leadDbSize;

        private String podName;

        public Builder munchkinId(String munchkinId) {
            this.munchkinId = munchkinId;
            return this;
        }

        public Builder mlmInternalEndPoint(String mlmInternalEndPoint) {
            this.mlmInternalEndPoint = mlmInternalEndPoint;
            return this;
        }

        public Builder leadDbSize(String leadDbSize) {
            this.leadDbSize = leadDbSize;
            return this;
        }

        public Builder podName(String podName) {
            this.podName = podName;
            return this;
        }

        public SiriusProperties build() {
            return new SiriusProperties(this);
        }
    }

    private SiriusProperties(Builder builder) {
        munchkinId = builder.munchkinId;
        mlmInternalEndPoint = builder.mlmInternalEndPoint;
        leadDbSize = builder.leadDbSize;
        podName = builder.podName;
    }

}
