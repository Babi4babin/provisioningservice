package com.marketo.subscriptionservice.model;

/**
 * Created by asawhney on 3/9/17.
 */
public enum ClientType {
    SIRIUS,
    MLM,
    API250OK;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
