package com.marketo.subscriptionservice.model;

public enum Status {
    UP,
    DOWN,
    OUT_OF_SERVICE,
    STARTING; 
}