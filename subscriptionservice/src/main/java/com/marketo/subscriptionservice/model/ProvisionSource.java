package com.marketo.subscriptionservice.model;

/**
 * Created by asawhney on 6/1/17.
 */
public enum ProvisionSource {

    MLM("mlm"),
    SPA("spa");

    private final String code;

    private ProvisionSource(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}
