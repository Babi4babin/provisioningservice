package com.marketo.subscriptionservice.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class BuildInfo {
    private String jobName;
    private String buildNumber;
    private String releaseName;
    private String buildTimestamp;
}
