package com.marketo.subscriptionservice.model;

import lombok.Data;

import org.springframework.util.MultiValueMap;

@Data
public class ProvisioningRequest {
    
    private String activePod;
    
    private String bundle;
    
    private String token;

    private MultiValueMap<String, String> data;
    
}
