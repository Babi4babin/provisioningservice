package com.marketo.subscriptionservice.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Transient;
import java.io.Serializable;

@Data
@ToString
public class ChildAccount implements Serializable {

	private String id;
	private String api_key;
	private String company;
	private String has_inboxinformant;
	private String inbox_max;
	private String inbox_used;
	private String inbox_expires;
	private String has_blacklistinformant;
	private String blacklist_max;
	private String blacklist_used;
	private String blacklist_expires;
	private String has_reputation;
	private String reputation_expires;
	private String has_designinformant;
	private String design_max;
	private String design_used;
	private String design_expires;
	private String emailinformant_expires;
	private String has_emailinformant;
	private String emailinformant_max;
	private String emailinformant_used;
	private String has_dmarc;
	private String dmarc_max;
	private String dmarc_used;
	private String dmarc_expires;
	private String has_validation;
	private String validation_start;
	private String validation_expires;
	private int validationmax;
	private String validationmaxtype;
	private int validation_used;
	private int validation_used_unique;

	//account specific fields
	private String email;
	private String password;
	private String contact;
	private String title;
	private String phone;
	private boolean forcepasswordreset = false;
	private boolean inherit_expiration = false;
	private String external_id;
	private Integer account_volume;
	private boolean emailcredentials = false;
	private String timezone;


	@Override
	public String toString() {
		return "ChildAccount{" +
				"id='" + id + '\'' +
				", email='" + email + '\'' +
				", external_id='" + external_id + '\'' +
				", api_key='" + api_key + '\'' +
				", company='" + company + '\'' +
				", has_inboxinformant='" + has_inboxinformant + '\'' +
				", inbox_max='" + inbox_max + '\'' +
				", inbox_used='" + inbox_used + '\'' +
				", inbox_expires='" + inbox_expires + '\'' +
				", has_blacklistinformant='" + has_blacklistinformant + '\'' +
				", blacklist_max='" + blacklist_max + '\'' +
				", blacklist_used='" + blacklist_used + '\'' +
				", blacklist_expires='" + blacklist_expires + '\'' +
				", has_reputation='" + has_reputation + '\'' +
				", reputation_expires='" + reputation_expires + '\'' +
				", has_designinformant='" + has_designinformant + '\'' +
				", design_max='" + design_max + '\'' +
				", design_used='" + design_used + '\'' +
				", design_expires='" + design_expires + '\'' +
				", emailinformant_expires='" + emailinformant_expires + '\'' +
				", has_emailinformant='" + has_emailinformant + '\'' +
				", emailinformant_max='" + emailinformant_max + '\'' +
				", emailinformant_used='" + emailinformant_used + '\'' +
				", has_dmarc='" + has_dmarc + '\'' +
				", dmarc_max='" + dmarc_max + '\'' +
				", dmarc_used='" + dmarc_used + '\'' +
				", dmarc_expires='" + dmarc_expires + '\'' +
				", has_validation='" + has_validation + '\'' +
				", validation_start='" + validation_start + '\'' +
				", validation_expires='" + validation_expires + '\'' +
				", validationmax=" + validationmax +
				", validationmaxtype='" + validationmaxtype + '\'' +
				", validation_used=" + validation_used +
				", validation_used_unique=" + validation_used_unique +
				'}';
	}

	public boolean isAny250okFeaturesActive(){
		if(this.has_inboxinformant.equals("n") && this.has_blacklistinformant.equals("n") &&
				this.has_reputation.equals("n") && this.has_designinformant.equals("n") &&
				this.has_emailinformant.equals("n") && this.has_dmarc.equals("n") && this.has_validation.equals("n") ) {
			return false;
		}
		return true;
	}

}
