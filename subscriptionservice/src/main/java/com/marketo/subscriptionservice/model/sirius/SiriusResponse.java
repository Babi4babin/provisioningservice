package com.marketo.subscriptionservice.model.sirius;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.json.JSONObject;

/**
 * Created by asawhney on 3/8/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SiriusResponse {

    private boolean success;

    private SiriusBacklogJob backlogJob;

    public SiriusBacklogJob getBacklogJob() {
        return backlogJob;
    }

    public void setBacklogJob(SiriusBacklogJob backlogJob) {
        this.backlogJob = backlogJob;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
