package com.marketo.subscriptionservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by asawhney on 6/1/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
