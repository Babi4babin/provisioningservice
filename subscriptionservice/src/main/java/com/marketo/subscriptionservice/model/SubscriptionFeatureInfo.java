package com.marketo.subscriptionservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by asawhney on 3/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionFeatureInfo {

    private String status;

    private String backlogId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBacklogId() {
        return backlogId;
    }

    public void setBacklogId(String backlogId) {
        this.backlogId = backlogId;
    }

}
