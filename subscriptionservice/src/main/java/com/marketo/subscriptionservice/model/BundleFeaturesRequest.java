package com.marketo.subscriptionservice.model;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;
import lombok.Data;
import org.springframework.util.StringUtils;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import java.util.ArrayList;
import java.util.List;

@Data
public class BundleFeaturesRequest {

    private String bundleId;

    private List<MarketingFeature> features = new ArrayList<>();

}
