package com.marketo.subscriptionservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by asawhney on 3/7/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionFeatureConfig {

    private String podName;

    public String getPodName() {
        return podName;
    }

    public void setPodName(String podName) {
        this.podName = podName;
    }

}
