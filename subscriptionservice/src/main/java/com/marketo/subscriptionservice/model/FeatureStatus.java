package com.marketo.subscriptionservice.model;

public enum FeatureStatus {
    PROGRESS,
    COMPLETED,
    FAILED,
    UNKNOWN;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}