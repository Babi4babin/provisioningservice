package com.marketo.subscriptionservice.model.sirius;

/**
 * Created by asawhney on 3/7/17.
 */
public class SiriusRequest {

    private SiriusJob job;

    public static class Builder {

        private SiriusJob job;

        public Builder job(SiriusJob job) {
            this.job = job;
            return this;
        }

        public SiriusRequest build() {
            return new SiriusRequest(this);
        }
    }

    private SiriusRequest(Builder builder) {
        job = builder.job;
    }

}
