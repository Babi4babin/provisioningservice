package com.marketo.subscriptionservice.model.sirius;

/**
 * Created by asawhney on 3/7/17.
 */
public class SiriusAction {


    private String name;

    private SiriusProperties properties;

    public static class Builder {

        private String name;

        private SiriusProperties properties;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder properties(SiriusProperties properties) {
            this.properties = properties;
            return this;
        }

        public SiriusAction build() {
            return new SiriusAction(this);
        }
    }

    private SiriusAction(Builder builder) {
        name = builder.name;
        properties = builder.properties;
    }
}
