package com.marketo.subscriptionservice.model;

/**
 * Created by rmalani on 6/30/17.
 */
public enum ModuleStatus {
    STARTED,
    SUCCESS,
    FAILURE;

    @Override
    public String toString() {
        return name().toString();
    }
}
