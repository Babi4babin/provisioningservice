package com.marketo.subscriptionservice.model;

/**
 * Created by asawhney on 3/9/17.
 */
public enum NonProdDC {
    SJINT,
    INT,
    SJQE,
    QE,
    SJST,
    ST,
    SJ,
    GUSW1QE,
    GUSW1INT,
    GUSW1ST,
    GUSW1;


    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
