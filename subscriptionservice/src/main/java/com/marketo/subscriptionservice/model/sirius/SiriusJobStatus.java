package com.marketo.subscriptionservice.model.sirius;

public enum SiriusJobStatus {
    BACKLOG,
    SUBMITTED,
    RUNNING,
    SUCCEEDED,
    FAILED,
    KILLED,
    SUSPENDED;
}