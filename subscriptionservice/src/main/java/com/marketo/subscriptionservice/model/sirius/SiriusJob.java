package com.marketo.subscriptionservice.model.sirius;

/**
 * Created by asawhney on 3/7/17.
 */
public class SiriusJob {

    private String component;

    private SiriusAction action;

    private String description;

    private String user;

    public static class Builder {

        private String component;
        private SiriusAction action;
        private String description;
        private String user;

        public Builder component(String component) {
            this.component = component;
            return this;
        }

        public Builder action(SiriusAction action) {
            this.action = action;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder user(String user) {
            this.user = user;
            return this;
        }

        public SiriusJob build() {
            return new SiriusJob(this);
        }
    }

    private SiriusJob(Builder builder) {
        component = builder.component;
        action = builder.action;
        description = builder.description;
        user = builder.user;
    }

}

