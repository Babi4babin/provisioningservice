package com.marketo.subscriptionservice.model;

/*
 * Subscription Status Information
 */
public enum SubscriptionStatus {
	CANCEL(-1),
	CANCEL_DORMANT(-4),
	ACTIVE(2);
	
	private int statusCode;
	
	private SubscriptionStatus(int statusCode){
		this.statusCode = statusCode;
	}
	
	public int getStatusCode(){
		return statusCode;
	}
	
}
