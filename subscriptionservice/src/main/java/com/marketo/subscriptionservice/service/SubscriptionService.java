package com.marketo.subscriptionservice.service;

import java.util.*;

import com.google.gson.JsonSyntaxException;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundles;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.provisioningservice.features.API250OkProvisioningHandler;
import com.marketo.provisioningservice.service.FeatureLocatorService;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.helper.SubscriptionServiceHelper;
import com.marketo.subscriptionservice.model.SubscriptionStatus;
import com.marketo.subscriptionservice.rest.RestClient;
import com.marketo.subscriptionservice.utility.JsonUtility;
import com.marketo.subscriptionservice.utility.SubscriptionConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.locatorservice.client.SubscriptionServiceManager;
import com.marketo.locatorservice.entities.SubscriptionId;
import com.marketo.locatorservice.entities.SubscriptionPodInfo;
import com.marketo.subscriptionservice.rest.ErrorResponse;
import com.marketo.subscriptionservice.rest.Response;
import com.marketo.provisioningservice.features.SiriusProvisioningHandler;
import org.springframework.util.StringUtils;

@Component
public class SubscriptionService {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionService.class);

    private static final String EDIT_DELIVERABILITY_PARTNER_ENABLED_URL = "%s/administrator/editDeliverabilityPartnerEnabled?tenantId=%s";

    private static final String EDIT_SUB_STATUS_URL = "%s/administrator/editSubStatus?tenantId=%s&status=%s";

    private static final String CHECK_IS_SUB_ACTIVE = "%s/administrator/isSubActive?tenantId=%s";

    public static final String DATA_ENCRYPTION_URL = "%s/administrator/updateDataEncryptionFlag?tenantId=%s";

    public static final String FETCH_TENANT_DETAILS = "%s/administrator/fetchTenantDetails?tenantId=%s";

    public static final String ENABLE_DATA_ENCRYPTION = "enable";

    public static final String DISABLE_DATA_ENCRYPTION = "disable";

    @Autowired
    private SubscriptionServiceManager subscriptionServiceManager;

    @Autowired
    private FeatureLocatorService featureLocatorService;

    @Autowired
    private SiriusProvisioningHandler siriusProvisioningHandler;

    @Autowired
    private JsonUtility jsonUtility;

    @Autowired
    private SubscriptionMetadataService subscriptionMetadataService;

    @Autowired
    private SubscriptionMetadataDAO subscriptionMetadataDAO;

    @Autowired
    private BundleMetadataService bundleMetadataService;

    @Autowired
    private RestClient restClient;

    @Autowired
    private API250OkProvisioningHandler api250OkProvisioningHandler;

    @Autowired
    private SubscriptionServiceHelper subscriptionServiceHelper;

    /**
     * @param tenantUUID munchkinId of subscription
     * @param subStatus  the new subscription status which needs to be updated
     * @return the Response with the success parameter
     */
    public Response<?> editSubscriptionStatus(String tenantUUID, int subStatus, String podLocation, boolean siriusCancelled) {
        Response<?> resp = new Response<>();
        boolean result = false;
        try {
            String url = null;
            String subIsActiveUrl = null;
            if (subStatus == SubscriptionStatus.ACTIVE.getStatusCode() || subStatus == SubscriptionStatus.CANCEL.getStatusCode()) {
                if (podLocation == null) {
                    throw new Exception("Podlocation for " + subStatus + " subscription " + tenantUUID + " is null");
                }
                String host = featureLocatorService.getPodFeatureLocation(podLocation, null, tenantUUID);
                if (host == null) {
                    throw new Exception("Null host returned for pod: " + podLocation + " from locatorservice");
                }
                url = buildUrl(host, tenantUUID, subStatus);
                subIsActiveUrl = buildIsActiveUrl(host, tenantUUID);
            } else {
                SubscriptionPodInfo subPod = subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId(tenantUUID));
                url = buildUrl(subPod.getInternalApiUrl(), tenantUUID, subStatus);
                subIsActiveUrl = buildIsActiveUrl(subPod.getInternalApiUrl(), tenantUUID);
            }
            if (url == null) {
                throw new Exception("URL for mlm endpoint is null.");
            }
            String response = null;
            boolean status = false;
            if (subStatus == SubscriptionStatus.CANCEL_DORMANT.getStatusCode() || subStatus == SubscriptionStatus.ACTIVE.getStatusCode()) {
                status = editSubscriptionStatusInSirius(subStatus, tenantUUID, podLocation);
                if (status == true) {
                    response = sendRequest(url);
                    logger.info("MLM response for changing sub status for subscription " + tenantUUID + " response : " + response);
                    result = parseResponse(response, tenantUUID);
                } else {
                    logger.info("Subscription status change in sirius fails for subscription " + tenantUUID);
                    throw new Exception("Subscription status change in sirius fails for subscription " + tenantUUID);
                }
            } else if (subStatus == SubscriptionStatus.CANCEL.getStatusCode()) {
                String current_subscription_statusJsonString = sendRequest(subIsActiveUrl);
                boolean isSubscriptionActive;
                try {
                    logger.info("currentSubscriptionStatus json String :" + current_subscription_statusJsonString);
                    isSubscriptionActive = jsonUtility.getFieldValueFromJsonString(current_subscription_statusJsonString, "JSONResults", "isActive");
                } catch (JsonSyntaxException | NullPointerException ex) {
                    logger.error("Exception Occurred: " + ex.getMessage());
                    throw ex;
                }
                  /*
                    Scenario: If the Current sub is in active state and request is trying to Cancel the sub permanently.
                    This conditional Check have introduced as part of JIRA  LM-136548.
                  */
                if (isSubscriptionActive) {
                    ErrorResponse errorResponse = new ErrorResponse(null, SubscriptionConstants.UNABLE_TO_CANCEL_SUB);
                    List<ErrorResponse> errorResponseList = new ArrayList<>();
                    errorResponseList.add(errorResponse);
                    resp.setErrors(errorResponseList);
                    logger.info("Subscription status is " + isSubscriptionActive + ", hence not permanently cancelling the Subscription:" + tenantUUID);
                    return resp;
                }
                if (siriusCancelled == true) {
                    response = sendRequest(url);
                    logger.info("MLM response for changing sub status for subscription " + tenantUUID + " response : " + response);
                    result = parseResponse(response, tenantUUID);
                } else if (siriusCancelled == false) {
                    status = editSubscriptionStatusInSirius(subStatus, tenantUUID, podLocation);
                    result = status;
                }
                // Delete 250ok Child Account
                api250OkProvisioningHandler.cancel250ok(tenantUUID);
            }
        } catch (Exception e) {
            logger.error("Error in changing subscription status for tenantUUID: " + tenantUUID + ". " + e.getMessage());
            ErrorResponse err = new ErrorResponse(null, e.getMessage());
            List<ErrorResponse> errorList = new ArrayList<ErrorResponse>();
            errorList.add(err);
            resp.setErrors(errorList);
        }
        resp.setSuccess(result);
        logger.info("response from editSubscriptionStatus for subscription " + tenantUUID + " response : " + resp);
        return resp;

    }


    /**
     * Update the subscription Sirius status
     *
     * @param subStatus
     * @param tenantUUID
     * @param podLocation
     */
    private boolean editSubscriptionStatusInSirius(int subStatus, String tenantUUID, String podLocation) {
        if (subStatus == SubscriptionStatus.CANCEL.getStatusCode()) {
            return siriusProvisioningHandler.cancelSirius(tenantUUID, podLocation);
        } else if (subStatus == SubscriptionStatus.CANCEL_DORMANT.getStatusCode()) {
            return siriusProvisioningHandler.cancelDormantSirius(tenantUUID, podLocation);
        } else if (subStatus == SubscriptionStatus.ACTIVE.getStatusCode()) {
            return siriusProvisioningHandler.reactivateSirius(tenantUUID, podLocation);
        }
        return false;
    }

    /**
     * @param appUrl     appUrl of subscription
     * @param tenantUUID munchkinID of subscription
     * @return the complete formatted URL
     */
    private String buildIsActiveUrl(String appUrl, String tenantUUID) {
        return String.format(CHECK_IS_SUB_ACTIVE, appUrl, tenantUUID);
    }


    /**
     * @param appUrl     appUrl of subscription
     * @param tenantUUID munchkinID of subscription
     * @param subStatus  the subscription status
     * @return the complete formatted URL
     */
    private String buildUrl(String appUrl, String tenantUUID, int subStatus) {

        return String.format(EDIT_SUB_STATUS_URL, appUrl, tenantUUID, subStatus);
    }

    /**
     * @param appUrl     appUrl of subscription
     * @param tenantUUID munchkinID of subscription
     * @return the complete formatted URL
     */
    private String buildDataEncryptionUrl(String appUrl, String tenantUUID) {

        return String.format(DATA_ENCRYPTION_URL, appUrl, tenantUUID);
    }

    /**
     * @param appUrl     appUrl of subscription
     * @param tenantUUID munchkinID of subscription
     * @return the complete formatted URL
     */
    private String buildDeliverabilityPartnerEnabledUrl(String appUrl, String tenantUUID) {

        return String.format(EDIT_DELIVERABILITY_PARTNER_ENABLED_URL, appUrl, tenantUUID);
    }


    /**
     * @param appUrl     appUrl of subscription
     * @param tenantUUID munchkinID of subscription
     * @return the complete formatted URL
     */
    private String buildFetchTenantDetailsUrl(String appUrl, String tenantUUID) {

        return String.format(FETCH_TENANT_DETAILS, appUrl, tenantUUID);
    }

    /**
     * Do post request with url encoded form data
     *
     * @param url
     * @return
     */
    private String sendRequest(String url) {

        String response = null;
        RestTemplate restTemplate = new RestTemplate();

        try {

            logger.info("Sending request to URL: " + url);

            response = restTemplate.getForObject(url, String.class);

            logger.info("Response recieved: " + response);

        } catch (Exception e) {
            logger.error("Error retrieving response: for " + url + ". " + e.getMessage());
        }
        return response;
    }


    /**
     * @param response the response string needs to be parsed
     * @return true if there are no error in the response
     */
    @SuppressWarnings("unchecked")
    private boolean parseResponse(String response, String tenantUUID) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> responseObj = null;
        if (response != null) {
            try {
                responseObj = mapper.readValue(response, Map.class);
            } catch (Exception e) {
                throw new RuntimeException("Unabled to parse response received");
            }

            if (responseObj.containsKey("JSONResults")) {

                Map<String, Object> jsonSection = (Map<String, Object>) responseObj.get("JSONResults");

                if (jsonSection.containsKey("errorMessage")) {
                    throw new RuntimeException((String) jsonSection.get("errorMessage"));
                }

                if (jsonSection.containsKey("cleanUpRTP")) {
                    if (jsonSection.containsKey("V2Sub")) {
                        handleDeleteFeatureV2Sub(tenantUUID);
                        logger.info("Deleted AddOn RTP feature from V2 Subscription " + tenantUUID);
                    } else if (jsonSection.containsKey("RubiksSub")) {
                        handleDeleteBundleRubiksSub(tenantUUID);
                        logger.info("Deleted RTP Bundles and features from Rubiks Subscription " + tenantUUID);
                    }
                }

                return true;

            } else {
                throw new RuntimeException("Invalid Response Recieved. " + response);
            }
        }
        return true;
    }


    private void handleDeleteFeatureV2Sub(String tenantUUID) {

        String featureRTP = "RTP";
        MarketingFeature marketingFeature = bundleMetadataService.getMarketingFeature(featureRTP);
        subscriptionMetadataService.deleteMarketingFeature(tenantUUID, marketingFeature.getId());
    }


    private void handleDeleteBundleRubiksSub(String tenantUUID) {

        String bundleRTP[] = {"webPersonalization", "accountBasedMarketing", "websiteRetargeting", "predictiveContent", "contentAiAnalytics"};
        SubscriptionBundles subscriptionBundles = subscriptionMetadataService.getProvisionedModules(tenantUUID);

        if (subscriptionBundles.getBundles().size() != 0) {
            for (MarketingBundle marketingBundle : subscriptionBundles.getBundles()) {
                if (Arrays.asList(bundleRTP).contains(marketingBundle.getCode())) {
                    marketingBundle.getChildBundleFeatures().stream().forEach(marketingBundleFeature -> {
                        subscriptionMetadataService.deleteMarketingFeature(tenantUUID, marketingBundleFeature.getFeature().getId());
                    });
                    subscriptionMetadataDAO.deleteMarketingBundle(tenantUUID, marketingBundle.getId());
                }
            }
        }

    }

    /**
     * Method used to validate given encryption parameters
     *
     * @param params
     **/
    private void validateInputParameters(MultiValueMap<String, String> params) {
        String status = params.getFirst("status");
        String token = params.getFirst("token");

        if (StringUtils.isEmpty(status) || (!status.equalsIgnoreCase(ENABLE_DATA_ENCRYPTION) &&
                !status.equalsIgnoreCase(DISABLE_DATA_ENCRYPTION))) {
            throw new RuntimeException(String.format("Invalid encryption status : %s", status));
        }

        if (StringUtils.isEmpty(token)) {
            throw new RuntimeException("Missing Token");
        }
    }


    /**
     * @param tenantUUID munchkinId of subscription
     * @param params     encryption flag params
     * @return the Response with the success parameter
     */
    public Response updateEncryptionFlag(String tenantUUID, MultiValueMap<String, String> params) {
        Response resp = new Response();
        boolean result = false;
        List<String> responseData = new ArrayList<>();
        try {
            //input validations here
            validateInputParameters(params);

            //sending request to mlm rest end point
            SubscriptionPodInfo subPod = subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId(tenantUUID));
            String url = buildDataEncryptionUrl(subPod.getInternalApiUrl(), tenantUUID);
            String mlmResponse = restClient.postForm(url, params);

            result = parseResponse(mlmResponse, tenantUUID);
            String message = String.format("Successfully updated encryption flag for : %s", tenantUUID);
            responseData.add(message);

            logger.info(message);
        } catch (Exception e) {
            logger.error("Error while setting data encryption flag for tenantUUID: " + tenantUUID + ". " + e.getMessage());
            List<ErrorResponse> errorList = new ArrayList<>();
            ErrorResponse err = new ErrorResponse(null, e.getMessage());
            errorList.add(err);
            resp.setErrors(errorList);
        }
        resp.setRequestId(UUID.randomUUID().toString());
        resp.setSuccess(result);
        resp.setResult(responseData);
        return resp;
    }


    /***
     *
     * method to fetch the tenant details
     *
     * @param tenantUUID
     * @return
     */
    public String getTenantDetailsByMuchkinId(String tenantUUID) {
        String customerName = null;
        Map<String, Object> responseObj = null;
        try {

            //sending request to mlm rest end point
            SubscriptionPodInfo subPod = subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId(tenantUUID));
            String url = buildFetchTenantDetailsUrl(subPod.getInternalApiUrl(), tenantUUID);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            String mlmResponse = restClient.getJson(url, headers);
            ObjectMapper mapper = new ObjectMapper();
            if (mlmResponse != null) {
                responseObj = mapper.readValue(mlmResponse, Map.class);
                if (responseObj.containsKey("JSONResults")) {
                    Map<String, Object> jsonSection = (Map<String, Object>) responseObj.get("JSONResults");
                    customerName = (String) jsonSection.get("data");
                }
            }
        } catch (Exception e) {
            logger.error("Error while fetching details for tenantUUID: " + tenantUUID + ". " + e.getMessage());
        }
        return customerName;
    }


    /**
     * @param tenantUUID munchkinId of subscription
     * @param params     Deliverability Partner Enabled params
     * @return the Response with the success parameter
     */
    public Response updateDeliverabilityPartnerEnabled(String tenantUUID, MultiValueMap<String, String> params) {
        Response resp = new Response();
        boolean result = false;
        String status = params.getFirst("status");
        List<String> responseData = new ArrayList<>();
        try {
            //validate input params
            subscriptionServiceHelper.validateInputParameters(status);

            //build and call mlm rest end point
            SubscriptionPodInfo subPod = subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId(tenantUUID));
            String url = buildDeliverabilityPartnerEnabledUrl(subPod.getInternalApiUrl(), tenantUUID);
            String mlmResponse = restClient.postForm(url, params);

            result = parseResponse(mlmResponse, tenantUUID);
            String message = String.format("Successfully updated Deliverability Partner Enabled status to %s for : %s", status, tenantUUID);
            responseData.add(message);

            logger.info(message);
        } catch (Exception e) {
            logger.error("Error while updating Deliverability Partner Enabled status for tenantUUID: " + tenantUUID + ". " + e.getMessage());
            List<ErrorResponse> errorList = new ArrayList<>();
            ErrorResponse err = new ErrorResponse(null, e.getMessage());
            errorList.add(err);
            resp.setErrors(errorList);
        }
        resp.setRequestId(UUID.randomUUID().toString());
        resp.setSuccess(result);
        resp.setResult(responseData);
        return resp;
    }

}
