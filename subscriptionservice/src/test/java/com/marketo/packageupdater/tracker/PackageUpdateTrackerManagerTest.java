package com.marketo.packageupdater.tracker;

import com.marketo.packageupdater.model.UpdateTrackResponse;
import org.junit.Assert;
import org.mockito.*;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.jms.Queue;

import static org.mockito.Mockito.*;

public class PackageUpdateTrackerManagerTest {

  @Mock
  private JmsTemplate jmsTemplate;

  @Mock
  private TaskExecutor taskExecutor;

  @InjectMocks
  private PackageUpdateTrackerManager packageUpdateTrackerManager;

  @BeforeClass
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testCreateUpdateTracker() {
    doNothing().when(taskExecutor).execute(Mockito.any(Runnable.class));
    packageUpdateTrackerManager.createUpdateTracker(0, true);
    verify(taskExecutor, atLeastOnce()).execute(Mockito.any(Runnable.class));
  }

  @Test
  public void testTrackPackage() {
    when(jmsTemplate.browseSelected(Mockito.any(Queue.class), Mockito.anyString(), Mockito.any(BrowserCallback.class))).thenReturn(10);
    UpdateTrackResponse updateTrackResponse = packageUpdateTrackerManager.trackPackage(0);
    Assert.assertEquals(updateTrackResponse.getNumObjectsRemaining(), 10);
  }

}
