package com.marketo.packageupdater.service;

import com.marketo.provisioningservice.service.ProvisioningContext;
import net.sf.ezmorph.test.ArrayAssertions;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

public class ProvisioningContextMockTest {

    @InjectMocks
    private ProvisioningContext provisioningContext;

    @BeforeClass
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetFormattedError() {
        provisioningContext.addError(new NullPointerException("NullPointerException"));
       String formattedError =   provisioningContext.getFormattedErrors();
        Assert.assertEquals(formattedError, "NullPointerException\n");
    }

    @Test
    public void testGetExecutedSteps() {
      provisioningContext.addExecutedStep("moduleKey", "onGoing", "stopped");
      System.out.println(provisioningContext);
      List<String> executedSteps =  provisioningContext.getExecutedSteps();

      Assert.assertEquals(executedSteps.size(),1);
    }

    @Test
    public void testGetWarning() {
      provisioningContext.addWarning("Warning message");
      List<String> warningMsg = provisioningContext.getWarnings();

      Assert.assertEquals(warningMsg.size(), 1);
      ArrayAssertions.assertNotNull(provisioningContext.getRequestUUID());
      Assert.assertEquals(warningMsg.get(0),"Warning message" );
    }

    @Test
    public void testGetFormattedExecutedSteps() {
      Map map = provisioningContext.getFeaturePodSelection();
      Assert.assertEquals(map.size(),0);
    }

    @Test
    public void testParsePodSelectionFromParam() {
      Map map = provisioningContext.parsePodSelectionFromParam("{map: 'config'}");
      Assert.assertNotNull(map);
      Assert.assertEquals(map.size(), 1);
    }

    @Test
    public void testGetNewBundleConfig() {
      provisioningContext.parseNewBundleConfigFromParam("{outlook: 'config'}");
      JSONObject jsonObject = provisioningContext.getNewBundleConfig();
      Assert.assertNotNull(jsonObject);
    }
}