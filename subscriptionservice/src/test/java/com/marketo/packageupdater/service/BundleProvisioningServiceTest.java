package com.marketo.packageupdater.service;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.service.MarketingFeatureConfigurator;
import com.marketo.provisioningservice.service.BundleProvisioningService;
import com.marketo.provisioningservice.service.FeatureProvisioningService;
import com.marketo.provisioningservice.service.Operation;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;

import org.json.JSONObject;

import org.junit.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BundleProvisioningServiceTest {

    @Mock
    private MarketingBundle marketingBundle;

    @Mock
    private MarketingBundleFeature marketingBundleFeature;

    @Mock
    private MarketingFeature marketingFeature;

    @Mock
    private FeatureProvisioningService featureProvisioningService;

    @Mock
    private MarketingFeatureConfigurator featureConfigurator;

    private Operation operation;

    @Mock
    private ProvisioningContext provisioningContext;

    @InjectMocks
    private BundleProvisioningService bundleProvisioningService;

    private static final String CONFIG_DATA = "{\n" +
            "\t\"name\": \"config\",\n" +
            "\t\"field\": \"deliverabilityPartnerEnabled\",\n" +
            "\t\"displayField\": \"Deliveribility Partner\",\n" +
            "\t\"marketing_bundle\": {\n" +
            "\t\"code\": \"200\"\n" +
            "\t},\n" +
            "\t\"code\": {\n" +
            "\t\"name\": \"config\",\n" +
            "\t\"field\": \"deliverabilityPartnerEnabled\",\n" +
            "\t\"displayField\": \"Deliveribility Partner\"\n" +
            "\t}\n" +
            "}";

    private static final String MARKETING_BUNDLE_FEATURE= "[{\n" +
            "\t\"name\": {\n" +
            "\t\t\"field\": \"name\"\n" +
            "\t}\n" +
            "}]";

    @BeforeMethod
    public void setUp() {
       MockitoAnnotations.initMocks(this);
    }

    @Test(expectedExceptions = ProvisioningException.class)
    public void testDoOperationException() {
        when(marketingBundle.getChildBundleFeatures()).thenThrow(ProvisioningException.class);
        bundleProvisioningService.doOperation(marketingBundle
                                             , operation
                                             , provisioningContext);
    }

    @Test
    public void testDoOperation() {
        List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList();
        marketingBundleFeatures.add(marketingBundleFeature);
        when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
        when(marketingBundleFeature.isFeature()).thenReturn(true);
        when(marketingBundleFeature.getMarketingFeatureConfig()).thenReturn("feature-config");
        when(marketingBundleFeature.getBundle()).thenReturn(marketingBundle);
        when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
        bundleProvisioningService.doOperation(marketingBundle, operation, provisioningContext);

        verify(featureProvisioningService, times(1)).doOperation(marketingFeature
                                                                , operation
                                                                , provisioningContext);
    }

    @Test
    public void testDoOperationFeatureConfigInitializeFeature() {
        List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList();
        marketingBundleFeatures.add(marketingBundleFeature);
        when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
        when(marketingBundleFeature.isFeature()).thenReturn(true);
        when(marketingBundleFeature.getBundle()).thenReturn(marketingBundle);
        when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
        when(marketingFeature.getDeveloperConfig()).thenReturn("developer-config");
        bundleProvisioningService.doOperation(marketingBundle, operation, provisioningContext);

        verify(featureConfigurator, times(1)).initializeFeature(null, marketingFeature, new HashMap<>());
    }

    @Test
    public void testOverrideDefaultFeatureConfigFailure() {
        when(marketingFeature.getMarketingFeatureConfig()).thenReturn("");
        boolean result = bundleProvisioningService.overrideDefaultFeatureConfig(marketingFeature
                                                                               , marketingBundle
                                                                               , provisioningContext);
        Assert.assertFalse(result);
    }

    @Test
    public void testValidateBundleConfig() {
        JSONObject featureConfigList = new JSONObject(CONFIG_DATA);
         List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();
        marketingBundleFeatures.add(marketingBundleFeature);
        when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
        when(marketingBundleFeature.getMarketingFeatureConfig()).thenReturn("feature");
        when(marketingBundleFeature.getMarketingFeatureConfig()).thenReturn(CONFIG_DATA);
        List<String> resultData = bundleProvisioningService.validateNewBundleConfig(featureConfigList, marketingBundle);

        Assert.assertEquals(resultData.size(), 0);
    }

    @Test
    public void testOverrideDefaultFeatureConfigSuccess() {
        when(marketingFeature.getMarketingFeatureConfig()).thenReturn(CONFIG_DATA);
        JSONObject jsonConfig = new JSONObject(CONFIG_DATA);
        when(provisioningContext.getNewBundleConfig()).thenReturn(jsonConfig);
        when(marketingBundle.getCode()).thenReturn("code");
        boolean result = bundleProvisioningService.overrideDefaultFeatureConfig(marketingFeature
                                                                               , marketingBundle
                                                                               , provisioningContext);
        Assert.assertTrue(result);
    }

    @Test
    public void testValidateBundleConfig1() {
        JSONObject featureConfigList = new JSONObject(CONFIG_DATA);
        List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();
        marketingBundleFeatures.add(marketingBundleFeature);
        when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
        when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
        when(marketingBundleFeature.getFeature().getDeveloperConfig()).thenReturn(MARKETING_BUNDLE_FEATURE);
        List<String> resultData = bundleProvisioningService.validateNewBundleConfig(featureConfigList, marketingBundle);

        Assert.assertEquals(resultData.size(), 4);
    }
}