package com.marketo.packageupdater.service;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.locatorservice.client.SubscriptionService;
import com.marketo.locatorservice.client.SubscriptionServiceManager;
import com.marketo.locatorservice.entities.*;
import com.marketo.locatorservice.exception.LocatorServiceException;
import com.marketo.provisioningservice.service.FeatureLocatorService;
import com.marketo.subscriptionservice.model.NonProdDC;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class FeatureLocatorServiceTest {

   @Mock
   private MarketingFeature marketingFeature;

   @Mock
   private SubscriptionPodInfo subscriptionPodInfo;

   @Mock
   private SubscriptionServiceManager subscriptionServiceManager;

   @Mock
   private SubscriptionService subscriptionService;

   @Mock
   private  SubscriptionServiceId subscriptionServiceId;

   @Mock
   private MetaData metaData;

   @Mock
   private Cluster cluster;

   private static final String INTERNAL_URL = "/internalApi/url";

   @InjectMocks
   private FeatureLocatorService featureLocatorService;

   @Mock
   private DataCenterId dataCenterId;

   @BeforeClass
   public void setUp() {
      MockitoAnnotations.initMocks(this);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testSubscriptionFeatureLocationWithException() {
      featureLocatorService.getSubscriptionFeatureLocation("tenantId", null);
    }

    @Test
    public void testSubscriptionFeatureLocation() throws LocatorServiceException {
      when( marketingFeature.getLocatorServiceId()).thenReturn("mlmpod");
      when(subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId("tenantId")))
                                                            .thenReturn(subscriptionPodInfo);
      when(subscriptionPodInfo.getInternalApiUrl()).thenReturn(INTERNAL_URL);

      String resultantLocation = featureLocatorService.getSubscriptionFeatureLocation("tenantId"
                                                                                     , marketingFeature);
      Assert.assertEquals(resultantLocation,INTERNAL_URL );
    }

    @Test
    public void testSubscriptionFeatureLocation1() throws LocatorServiceException {
        when( marketingFeature.getLocatorServiceId()).thenReturn("newPod");
        when(subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId("tenantId")))
                                                               .thenReturn(subscriptionPodInfo);

        when(subscriptionPodInfo.getInternalApiUrl()).thenReturn(INTERNAL_URL);
        when( subscriptionServiceManager.getSubscriptionService(any(SubscriptionServiceId.class)))
                                                                .thenReturn(subscriptionService);

        when(subscriptionService.getCluster()).thenReturn(cluster);
        when(subscriptionService.getCluster().getMetaData("internal_rest_endpoint_host")).thenReturn(metaData);
        when(subscriptionService.getCluster().getMetaData("internal_rest_endpoint_host").getValue())
                                                                                        .thenReturn(INTERNAL_URL);

        String resultantLocation = featureLocatorService.getSubscriptionFeatureLocation("tenantId", marketingFeature);
        Assert.assertEquals(resultantLocation, INTERNAL_URL);
    }

    @Test
    public void testSubscriptionDCInt() throws LocatorServiceException {
        when(subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId("LM-122-321")))
                                                               .thenReturn(subscriptionPodInfo);

        when(subscriptionPodInfo.getDataCenterId()).thenReturn(dataCenterId);
        when( subscriptionPodInfo.getDataCenterId().getDataCenterId()).thenReturn(NonProdDC.SJ.toString());
        when(subscriptionPodInfo.getPodName()).thenReturn(NonProdDC.SJINT.toString());

        String pod = featureLocatorService.getSubscriptionDC("LM-122-321");
        Assert.assertEquals(pod, "int");
    }

    @Test
    public void testSubscriptionDCQE() throws LocatorServiceException {
        when(subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId("LM-122-321")))
                                                               .thenReturn(subscriptionPodInfo);

        when(subscriptionPodInfo.getDataCenterId()).thenReturn(dataCenterId);
        when( subscriptionPodInfo.getDataCenterId().getDataCenterId()).thenReturn(NonProdDC.SJ.toString());
        when(subscriptionPodInfo.getPodName()).thenReturn(NonProdDC.SJQE.toString());

        String pod = featureLocatorService.getSubscriptionDC("LM-122-321");
        Assert.assertEquals(pod, "qe");
    }

    @Test
    public void testSubscriptionDCSJ() throws LocatorServiceException {
        when(subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId("LM-122-321")))
                                                               .thenReturn(subscriptionPodInfo);

        when(subscriptionPodInfo.getDataCenterId()).thenReturn(dataCenterId);
        when( subscriptionPodInfo.getDataCenterId().getDataCenterId()).thenReturn(NonProdDC.SJ.toString());
        when(subscriptionPodInfo.getPodName()).thenReturn(NonProdDC.SJST.toString());

        String pod = featureLocatorService.getSubscriptionDC("LM-122-321");
        Assert.assertEquals(pod, "st");
    }

    @Test
    public void testSubscriptionDcGUSW1INT() throws LocatorServiceException {
        when(subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId("LM-122-321")))
                                                                .thenReturn(subscriptionPodInfo);

        when(subscriptionPodInfo.getDataCenterId()).thenReturn(dataCenterId);
        when( subscriptionPodInfo.getDataCenterId().getDataCenterId()).thenReturn(NonProdDC.GUSW1.toString());
        when(subscriptionPodInfo.getPodName()).thenReturn("int"+NonProdDC.GUSW1INT.toString()+"324");

        String pod = featureLocatorService.getSubscriptionDC("LM-122-321");
        Assert.assertEquals(pod, "gusw1");
    }

    @Test
    public void testPodFeatureLocation() throws LocatorServiceException {
        List<SubscriptionPodInfo> subscriptionPodInfoList = new ArrayList<>();
        subscriptionPodInfoList.add(subscriptionPodInfo);
        when( subscriptionServiceManager.getAllSubscriptionPodInfo()).thenReturn(subscriptionPodInfoList);
        when(subscriptionPodInfo.getPodName()).thenReturn(NonProdDC.GUSW1INT.toString());
        when(subscriptionPodInfo.getInternalApiUrl()).thenReturn(INTERNAL_URL);

        String podLocation = featureLocatorService.getPodFeatureLocation(NonProdDC.GUSW1INT.toString()
                                                                        , marketingFeature, "LM-132-321" );
        Assert.assertEquals(podLocation, INTERNAL_URL);
    }

    @Test
    public void testPodFeatureLocationWithNull() throws LocatorServiceException {
        List<SubscriptionPodInfo> subscriptionPodInfoList = new ArrayList<>();
        subscriptionPodInfoList.add(subscriptionPodInfo);
        when( subscriptionServiceManager.getAllSubscriptionPodInfo()).thenReturn(subscriptionPodInfoList);
        when(subscriptionPodInfo.getPodName()).thenReturn(NonProdDC.GUSW1INT.toString());
        when(subscriptionPodInfo.getInternalApiUrl()).thenReturn("");

        String podLocation = featureLocatorService.getPodFeatureLocation(NonProdDC.GUSW1INT.toString()
                                                                        , marketingFeature, "LM-132-321" );
        Assert.assertNull(podLocation);
    }

    @Test
    public void testDCFeatureLocationWithNullValue() {
       Assert.assertNull(featureLocatorService.getDCFeatureLocation(null));
    }

    @Test
    public void testDCFeatureLocation() throws LocatorServiceException {
        List<SubscriptionPodInfo> subscriptionPodInfoList = new ArrayList();
        subscriptionPodInfoList.add(subscriptionPodInfo);
        when( subscriptionServiceManager.getAllSubscriptionPodInfo()).thenReturn(subscriptionPodInfoList);
        when(subscriptionPodInfo.getDataCenterId()).thenReturn(dataCenterId);
        when( subscriptionPodInfo.getDataCenterId().getDataCenterId()).thenReturn(NonProdDC.SJ.toString());
        when(subscriptionPodInfo.getPodName()).thenReturn(NonProdDC.INT.toString());
        when(subscriptionPodInfo.getInternalApiUrl()).thenReturn("");

        String featureLocation = featureLocatorService.getDCFeatureLocation("int");
        Assert.assertEquals(featureLocation, "sj");
    }
}