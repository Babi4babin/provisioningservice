package com.marketo.packageupdater.service;

import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;

import com.marketo.packageupdater.model.PackageOperation;
import com.marketo.provisioningservice.service.Operation;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.when;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PackageOperationServiceMockTest {

    @Mock
    private BundleMetadataService bundleMetadataService;

    @Mock
    private MarketingBundleFeature marketingBundleFeature;

    @Mock
    private MarketingFeature marketingFeature;

    @InjectMocks
    private PackageOperationsService packageOperationsService;

    private static final String DATE_FORMAT = "EEEE, MMM dd, yyyy HH:mm:ss a";

    @BeforeClass
    public void setUp() {
      MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetOperationsForDeProvision() {
      List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();
      marketingBundleFeatures.add(marketingBundleFeature);
      when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
      when(marketingBundleFeature.isActive()).thenReturn(false);
      when(marketingBundleFeature.getCreatedAt()).thenReturn(new Date());

      List<PackageOperation> packageOperationList =  packageOperationsService.getOperations(marketingBundleFeatures
                                                                                           ,new Date());
      Assert.assertEquals(packageOperationList.size(), 1);
      Assert.assertEquals(packageOperationList.get(0).getOperation(), Operation.DEPROVISION);
    }

    @Test
    public void testGetOperationsForProvision() throws Exception{
      List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();
      marketingBundleFeatures.add(marketingBundleFeature);
      when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
      when(marketingBundleFeature.isActive()).thenReturn(true);
      when(marketingBundleFeature.getCreatedAt()).thenReturn(new Date());

      SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
      String dateInString = "Friday, Jun 7, 2013 12:10:56 PM";
      Date lastRun = formatter.parse(dateInString);
      List<PackageOperation> packageOperationList =  packageOperationsService.getOperations(marketingBundleFeatures
                                                                                           ,lastRun);

      Assert.assertEquals(packageOperationList.size(), 1);
      Assert.assertEquals(packageOperationList.get(0).getOperation(), Operation.PROVISION);
    }
}