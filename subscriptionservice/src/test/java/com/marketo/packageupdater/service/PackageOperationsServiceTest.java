package com.marketo.packageupdater.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;

import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.packageupdater.model.PackageOperation;
import com.marketo.provisioningservice.service.Operation;

@ContextConfiguration(locations = { "classpath:subscription-test-config.xml" })
public class PackageOperationsServiceTest extends AbstractTestNGSpringContextTests {
    
    @Autowired
    private BundleMetadataService bundleMetadataService;
    
    @Autowired
    private PackageOperationsService packageOperationsService;

    private List<MarketingBundleFeature> bundleFeatures;
    
    private Date lastRun;
    
    @BeforeMethod
    private void setup() {
        
        Calendar cal = Calendar.getInstance();
        
        cal.set(2014, 9, 10);
        
        lastRun = cal.getTime();
        
        bundleFeatures = bundleMetadataService.getBundleUpdatesBetween(lastRun, new Date());
    }
    
    //@Test
    public void testPackageOperationsService() { 
        
        logger.info("Size of bundlefeatures " + bundleFeatures.size());
        
        List<PackageOperation> packageOperations = packageOperationsService.getOperations(bundleFeatures, lastRun);
        
        assertThat(packageOperations.size(), equalTo(3));

        PackageOperation deleteOperation = packageOperations.get(0);
        assertThat(deleteOperation.getFeature().getName(), equalTo("Dynamic Content"));
        assertThat(deleteOperation.getOperation(), equalTo(Operation.DEPROVISION));        

        
        PackageOperation addOperation = packageOperations.get(2);
        assertThat(addOperation.getFeature().getName(), equalTo("RCM"));
        assertThat(addOperation.getOperation(), equalTo(Operation.PROVISION));
        
               
        PackageOperation updateOperation = packageOperations.get(1);
        assertThat(updateOperation.getFeature().getName(), equalTo("Base"));
        assertThat(updateOperation.getOperation(), equalTo(Operation.UPDATE));
    }
    
}
