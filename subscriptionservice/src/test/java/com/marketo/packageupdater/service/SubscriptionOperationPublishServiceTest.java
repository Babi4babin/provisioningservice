package com.marketo.packageupdater.service;

import com.marketo.packageupdater.model.SubscriptionOperation;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.InjectMocks;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionOperationPublishServiceTest {

    @Mock
    private SubscriptionOperation subscriptionOperation;

    @InjectMocks
    private SubscriptionOperationsPublishService subscriptionOperationsService;

    @BeforeClass
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void testSubscriptionOperationsPublishService() {

        List<SubscriptionOperation> operations = new ArrayList<>();
        operations.add(subscriptionOperation);
        SubscriptionOperation subscriptionOperation = new SubscriptionOperation();
        subscriptionOperation.setTenantUUID("232-ABC-234");
        subscriptionOperation.setTrackerId(1);
        subscriptionOperation.setPackageOperations(new ArrayList<>());

        subscriptionOperationsService.publishSubscriptionOperations(operations, 1);
    }
}