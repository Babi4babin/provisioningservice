package com.marketo.packageupdater.service;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.packageupdater.model.PackageOperation;
import com.marketo.packageupdater.model.SubscriptionOperation;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.updatetracker.service.PackageUpdateTrackerService;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionOperationServiceTest {

    @Mock
    private PackageOperation packageOperation;

    @Mock
    private MarketingBundle marketingBundle;

    @Mock
    private SubscriptionOperation subscriptionOperation;

    @Mock
    private  SubscriptionMetadataService subscriptionMetadataService;

    @Mock
    private PackageUpdateTrackerService packageUpdateTrackerService;

    @InjectMocks
    private SubscriptionOperationsService subscriptionOperationsService;

    @BeforeClass
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetQualifyingSubscriptionOperations() {
        List<PackageOperation> packageOperationList = new ArrayList<>();
        packageOperationList.add(packageOperation);
        List<String> stringList = new ArrayList<>();
        stringList.add("123-LMU-764");
        stringList.add("123-DAR-765");

        when(packageOperation.getRootBundle()).thenReturn(marketingBundle);
        when(packageOperation.getRootBundle().getId()).thenReturn(1);
        when( subscriptionMetadataService.getSubscriptionsForBundle(packageOperation.getRootBundle().getId()))
                                         .thenReturn(stringList);
        when(packageUpdateTrackerService.getCompletedSubscriptionsForTracker(1)).thenReturn(stringList);
        List<SubscriptionOperation> subscriptionOperations  =  subscriptionOperationsService.
                                                               getQualifyingSubscriptionOperations(packageOperationList, 1);

        Assert.assertNotNull(subscriptionOperations);
        Assert.assertEquals(subscriptionOperations.size(), 0);
    }
}
