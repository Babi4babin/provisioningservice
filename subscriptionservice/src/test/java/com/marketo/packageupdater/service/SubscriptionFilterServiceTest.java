package com.marketo.packageupdater.service;

import com.marketo.locatorservice.client.SubscriptionServiceManager;
import com.marketo.locatorservice.entities.SubscriptionId;
import com.marketo.locatorservice.entities.SubscriptionPodInfo;
import com.marketo.locatorservice.exception.LocatorServiceException;
import com.marketo.packageupdater.model.PackageUpdateRequest;
import com.marketo.packageupdater.model.SubscriptionOperation;

import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class SubscriptionFilterServiceTest {

    @Mock
    private PackageUpdateRequest packageUpdateRequest;

    @Mock
    private SubscriptionOperation subscriptionOperation;

    @Mock
    private SubscriptionServiceManager subscriptionServiceManager;


    @InjectMocks
    private SubscriptionFilter subscriptionFilter;

    @Mock
    private SubscriptionPodInfo subscriptionPodInfo;

    @BeforeClass
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void testFilterWithException() {
        subscriptionFilter.filter(null, packageUpdateRequest);
    }

    @Test
    public void testFilter() throws LocatorServiceException {
        List<SubscriptionOperation> subscriptionOperationList = new ArrayList<>();
        subscriptionOperationList.add(subscriptionOperation);

        when(packageUpdateRequest.getMaxSubscriptions()).thenReturn(1);
        when(subscriptionOperation.getTenantUUID()).thenReturn("234-MLM-453");
        when(subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId("234-MLM-453")))
          .thenReturn(subscriptionPodInfo);

        List<SubscriptionOperation> subscriptionOperations = subscriptionFilter.filter(subscriptionOperationList, packageUpdateRequest);

        Assert.assertEquals(subscriptionOperations.size(), 1);
    }
}