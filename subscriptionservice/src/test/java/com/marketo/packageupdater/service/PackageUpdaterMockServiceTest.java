package com.marketo.packageupdater.service;

import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.packageupdater.model.*;
import com.marketo.packageupdater.tracker.PackageUpdateTrackerManager;
import com.marketo.updatetracker.model.PackageUpdateTracker;
import com.marketo.updatetracker.service.PackageUpdateTrackerService;

import org.junit.Assert;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PackageUpdaterMockServiceTest {

    @Mock
    private PackageUpdateRequest packageUpdateRequest;

    @Mock
    private PackageUpdateTrackerService packageUpdateTrackerService;

    @Mock
    private PackageUpdateTracker packageUpdateTracker;

    @Mock
    private BundleMetadataService bundleMetadataService;

    @Mock
    private MarketingBundleFeature marketingBundleFeature;

    @Mock
    private PackageOperationsService packageOperationsService;

    @Mock
    private SubscriptionOperationsService subscriptionOperationsService;

    @Mock
    private SubscriptionOperationsPublishService subscriptionOperationsPublishService;

    @Mock
    private PackageUpdateTrackerManager packageUpdateTrackerManager;

    @Mock
    private PackageOperation packageOperation;

    @Mock
    private  HttpServletRequest request;

    @InjectMocks
    private PackageUpdateService packageUpdateService;

    @BeforeClass
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUpdatePackages() {
        when(packageUpdateRequest.getTrackerId()).thenReturn(1);
        when(packageUpdateTrackerService.getLastCompletedRun()).thenReturn(packageUpdateTracker);
        when(packageUpdateTracker.getStartTime()).thenReturn(new Date());

        List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();
        marketingBundleFeatures.add(marketingBundleFeature);
        List<PackageOperation> packageOperationList = mock(ArrayList.class);
        packageOperationList.add(packageOperation);
        List<SubscriptionOperation> qualifyingSubOperations =  mock(ArrayList.class);

        when(bundleMetadataService.getBundleUpdatesBetween(any(Date.class), any(Date.class)))
                                                          .thenReturn(marketingBundleFeatures);
        when(packageOperationsService.getOperations(anyListOf(MarketingBundleFeature.class), any(Date.class)))
                                                   .thenReturn(packageOperationList);
        when( subscriptionOperationsService.getQualifyingSubscriptionOperations(packageOperationList, 1))
                                                                                .thenReturn(qualifyingSubOperations);
        when(packageOperationList.size()).thenReturn(1);
        when(request.getRequestURL()).thenReturn(new StringBuffer("/provision"));
        when(request.getRequestURI()).thenReturn("");
        when(request.getContextPath()).thenReturn("/update");

        PackageUpdateResponse packageUpdateResponse =  packageUpdateService.updatePackages(packageUpdateRequest);
        Assert.assertNotNull(packageUpdateResponse);
        Assert.assertEquals(packageUpdateResponse.getUpdateTrackerUrl()
                                                 , "/provision/update/rest/v1/packageupdater/track?trackerId=1");

        Assert.assertEquals(packageUpdateResponse.getNumPackageUpdates(), 1);
    }

    @Test
    public void testTrackUpdate() {
        UpdateTrackResponse updateTrackResponse  = packageUpdateService.trackUpdate(1);
        Assert.assertNull(updateTrackResponse);
    }

    @Test
    public void testUpdatePackagesWithTrackerId() {
        when(packageUpdateTrackerService.getLastCompletedRun()).thenReturn(packageUpdateTracker);
        when(packageUpdateTracker.getStartTime()).thenReturn(new Date());

        List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();
        marketingBundleFeatures.add(marketingBundleFeature);
        List<PackageOperation> packageOperationList = mock(ArrayList.class);
        packageOperationList.add(packageOperation);
        List<SubscriptionOperation> qualifyingSubOperations =  mock(ArrayList.class);

        when(bundleMetadataService.getBundleUpdatesBetween(any(Date.class), any(Date.class)))
                .thenReturn(marketingBundleFeatures);
        when(packageOperationsService.getOperations(anyListOf(MarketingBundleFeature.class), any(Date.class)))
                .thenReturn(packageOperationList);
        when( subscriptionOperationsService.getQualifyingSubscriptionOperations(packageOperationList, 0))
                .thenReturn(qualifyingSubOperations);
        when(packageOperationList.size()).thenReturn(1);
        when(request.getRequestURL()).thenReturn(new StringBuffer("/provision"));
        when(request.getRequestURI()).thenReturn("");
        when(request.getContextPath()).thenReturn("/update");
        when(packageUpdateRequest.getTrackerId()).thenReturn(0);
        when( packageUpdateTrackerService.getLastRun()).thenReturn(packageUpdateTracker);
        when(packageUpdateTracker.getId()).thenReturn(5);

        PackageUpdateResponse packageUpdateResponse =  packageUpdateService.updatePackages(packageUpdateRequest);
        Assert.assertNotNull(packageUpdateResponse);
        Assert.assertEquals(packageUpdateResponse.getUpdateTrackerUrl()
                                                 , "/provision/update/rest/v1/packageupdater/track?trackerId=5");

        Assert.assertEquals(packageUpdateResponse.getNumPackageUpdates(), 1);
    }

}
