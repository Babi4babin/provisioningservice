package com.marketo.packageupdater.listener;

import com.marketo.packageupdater.model.SubscriptionOperation;
import com.marketo.provisioningservice.service.FeatureProvisioningService;
import com.marketo.updatetracker.service.PackageUpdateTrackerService;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hornetq.jms.client.HornetQMessage;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.jms.Message;
import javax.jms.TextMessage;

import java.util.UUID;

import static org.mockito.Mockito.*;

public class PackageUpdateListenerTest {

    @Mock
    private FeatureProvisioningService featureProvisioningService;

    @Mock
    private PackageUpdateTrackerService packageUpdateTrackerService;


    @InjectMocks
    private PackageUpdateListener packageUpdateListener;


    private String munckinId = "";


    @BeforeClass
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeMethod
    public void beforeMethod() {
        munckinId=UUID.randomUUID().toString();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testWhenMessageIsNotTextMessage() {

        //message is not an instance of Textmessage
        Message message = mock(HornetQMessage.class);
        packageUpdateListener.onMessage(message);

    }

    @Test()
    public void testWhenMessageTypeIsText() throws Exception {

        Integer trackerId = RandomUtils.nextInt();
        SubscriptionOperation subscriptionOperation = new SubscriptionOperation();
        subscriptionOperation.setTenantUUID(munckinId);
        subscriptionOperation.setTrackerId(trackerId);

        TextMessage message = mock(TextMessage.class);
        when(message.getText()).thenReturn(new ObjectMapper().writeValueAsString(subscriptionOperation));
        packageUpdateListener.onMessage(message);
        verify(packageUpdateTrackerService,times(1)).addSubscriptionEntry(munckinId, trackerId, true);

    }

    @Test()
    public void testWhenMessageTypeIsTextWithPackageOperations() throws Exception {
        String subscOperationJSON = "{\"trackerId\":1,\"tenantUUID\":\""+munckinId+"\",\"packageOperations\":[{\"rootBundle\":null," +
                "\"operation\":\"PROVISION\",\"feature\":{\"name\":\"63899bcd-1b19-42d5-875d-dff0592110ed\",\"id\":0,\"code\":null,\"description\":null,\"active\":0," +
                "\"developerConfig\":null,\"provisioningURI\":null,\"statusURI\":null,\"clientType\":null,\"locatorServiceId\":null,\"deprovisioningURI\":null,\"updateURI\":null," +
                "\"marketingFeatureConfig\":null,\"updatedAt\":1590397711445,\"createdAt\":1590397711445},\"marketingFeatureConfig\":\"55300260-908b-4c6d-b77d-0a9699246d4b\"}]}";

        TextMessage message = mock(TextMessage.class);
        when(message.getText()).thenReturn(subscOperationJSON);
        packageUpdateListener.onMessage(message);
        verify(packageUpdateTrackerService,times(1)).addSubscriptionEntry(munckinId, 1, true);

    }

    @Test
    public void testWhenMessageTypeIsTextWithInvalidMessageData() throws Exception {

        String subscOperationJSON = "{\"trackerId\":1,\"tenantUUID\":\""+munckinId+"\",\"packageOperations\":[{\"rootBundle\":null," +
                "\"operation\":\"PROVISION\",\"feature\":{\"name\":\"63899bcd-1b19-42d5-875d-dff0592110ed\",\"id\":0,\"code\":null,\"description\":null,\"active\":0," +
                "\"developerConfig\":null,\"provisioningURI\":null,\"statusURI\":null,\"clientType\":null,\"locatorServiceId\":null,\"deprovisioningURI\":null,\"updateURI\":null," +
                "\"marketingFeatureConfig\":null,\"updatedAt\":1590397711445,\"createdAt\":1590397711445},\"marketingFeatureConfig\":\"55300260-908b-4c6d-b77d-0a9699246d4b\"}]}";

        TextMessage message = mock(TextMessage.class);
        when(message.getText()).thenReturn(subscOperationJSON);
        packageUpdateListener.onMessage(message);
        verify(packageUpdateTrackerService,times(1)).addSubscriptionEntry(munckinId, 1, true);



    }

    @Test
    public void testWhenMessageTypeIsTextExceptionScenario() throws Exception {

        String subscOperationJSON = "{\"trackerId\":1,\"tenantUUID\":\""+munckinId+"\",\"packageOperations\":[{\"rootBundle\":null," +
                "\"operation\":\"PROVISION\",\"feature\":{\"name\":\"63899bcd-1b19-42d5-875d-dff0592110ed\",\"id\":0,\"code\":null,\"description\":null,\"active\":0," +
                "\"developerConfig\":null,\"provisioningURI\":null,\"statusURI\":null,\"clientType\":null,\"locatorServiceId\":null,\"deprovisioningURI\":null,\"updateURI\":null," +
                "\"marketingFeatureConfig\":null,\"updatedAt\":1590397711445,\"createdAt\":1590397711445},\"marketingFeatureConfig\":\"55300260-908b-4c6d-b77d-0a9699246d4b\"}]}";

        TextMessage message = mock(TextMessage.class);
        when(message.getText()).thenReturn(subscOperationJSON);
        when(packageUpdateTrackerService.
                addSubscriptionEntry(munckinId, 1, true))
                .thenThrow(new NullPointerException());
            packageUpdateListener.onMessage(message);
        verify(packageUpdateTrackerService,times(1)).addSubscriptionEntry(munckinId, 1, false);

    }


}

