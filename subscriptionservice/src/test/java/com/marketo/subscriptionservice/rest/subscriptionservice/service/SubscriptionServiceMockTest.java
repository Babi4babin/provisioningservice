package com.marketo.subscriptionservice.rest.subscriptionservice.service;

import com.marketo.locatorservice.client.SubscriptionServiceManager;
import com.marketo.locatorservice.entities.SubscriptionId;
import com.marketo.locatorservice.entities.SubscriptionPodInfo;
import com.marketo.provisioningservice.features.SiriusProvisioningHandler;
import com.marketo.provisioningservice.service.FeatureLocatorService;
import com.marketo.subscriptionservice.helper.SubscriptionServiceHelper;
import com.marketo.subscriptionservice.model.SubscriptionStatus;
import com.marketo.subscriptionservice.rest.Response;
import com.marketo.subscriptionservice.rest.RestClient;
import com.marketo.subscriptionservice.service.SubscriptionService;
import com.marketo.subscriptionservice.utility.JsonUtility;
import org.apache.commons.lang.RandomStringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class SubscriptionServiceMockTest {

    @Mock
    private SubscriptionServiceManager subscriptionServiceManager;

    @Mock
    private FeatureLocatorService featureLocatorService;

    @Mock
    private SiriusProvisioningHandler siriusProvisioningHandler;

    @Mock
    private JsonUtility jsonUtility;

    @Mock
    private RestClient restClient;

    @Mock
    private SubscriptionServiceHelper subscriptionServiceHelper;

    @InjectMocks
    private SubscriptionService subscriptionService;

    private static String  MUNCHKIN_ID="123-ABC-456";

    private String podLocation;

    private static final String EDIT_SUB_STATUS_URL = "%s/administrator/editSubStatus?tenantId=%s&status=%s";


    @BeforeClass
    public void init(){
        MockitoAnnotations.initMocks(this);
        podLocation= RandomStringUtils.randomAlphabetic(5);
    }

    @Test(expectedExceptionsMessageRegExp = "Null host returned for pod")
    public void testEditSubscriptionStatusActive() throws Exception{
        when(featureLocatorService.getPodFeatureLocation(podLocation, null, MUNCHKIN_ID)).thenReturn(null);
        subscriptionService.editSubscriptionStatus(MUNCHKIN_ID, SubscriptionStatus.ACTIVE.getStatusCode(),podLocation,false);
    }

    @Test
    public void testEditSubscriptionStatusPodLocationIsEmpty() throws Exception{
        when(featureLocatorService.getPodFeatureLocation(podLocation, null, MUNCHKIN_ID)).thenReturn(null);
        Response<?> resp=subscriptionService.editSubscriptionStatus(MUNCHKIN_ID, SubscriptionStatus.ACTIVE.getStatusCode(),
                null,false);
        Assert.assertTrue(resp.getErrors().size() > 0);
    }

    @Test
    public void testEditSubscriptionStatusUrl() throws Exception{
        when(featureLocatorService.getPodFeatureLocation(podLocation, null, MUNCHKIN_ID)).thenReturn("dummyhost".intern());
        Response<?> resp=subscriptionService.editSubscriptionStatus(MUNCHKIN_ID, SubscriptionStatus.CANCEL.getStatusCode(),
                podLocation,false);
        Assert.assertNull(resp.getResult());
    }

    @Test
    public void testEditSubscriptionStatusUrlForActiveStatus() throws Exception{
        when(featureLocatorService.getPodFeatureLocation(podLocation, null, MUNCHKIN_ID)).thenReturn("dummyhost");
        Response<?> resp=subscriptionService.editSubscriptionStatus(MUNCHKIN_ID, SubscriptionStatus.ACTIVE.getStatusCode(),
                podLocation,false);
        Assert.assertTrue(resp.getErrors().size() > 0);

    }

    @Test
    public void testEditSubscriptionStatusUrlForCancelDormantStatus() throws Exception{
        when(featureLocatorService.getPodFeatureLocation(podLocation, null, MUNCHKIN_ID)).thenReturn("dummyhost");
        Response<?> resp=subscriptionService.editSubscriptionStatus(MUNCHKIN_ID, SubscriptionStatus.CANCEL_DORMANT.getStatusCode(),
                podLocation,false);
        Assert.assertTrue(resp.getErrors().size() > 0);

    }

    @Test
    public void testEditSubscriptionStatusUrlForCancelStatusSeriusCancelled() throws Exception{
        when(featureLocatorService.getPodFeatureLocation(podLocation, null, MUNCHKIN_ID)).thenReturn("dummyhost");
        Response<?> resp=subscriptionService.editSubscriptionStatus(MUNCHKIN_ID, SubscriptionStatus.CANCEL.getStatusCode(),
                podLocation,true);
        Assert.assertNull(resp.getResult());


    }

    @Test
    public void testEditSubscriptionStatusUrlForActiveStatusScenario_2() throws Exception{
        when(featureLocatorService.getPodFeatureLocation(podLocation, null, MUNCHKIN_ID)).thenReturn("dummyhost");
        when(siriusProvisioningHandler.reactivateSirius(MUNCHKIN_ID, podLocation)).thenReturn(true);
        Response<?> resp=subscriptionService.editSubscriptionStatus(MUNCHKIN_ID, SubscriptionStatus.ACTIVE.getStatusCode(),
                podLocation,false);
        Assert.assertTrue(resp.isSuccess());


    }

    @Test
    public void testEditSubscriptionStatusUrlForCancelDormantStatusScenario_2() throws Exception{
        when(featureLocatorService.getPodFeatureLocation(podLocation, null, MUNCHKIN_ID)).thenReturn("dummyhost");
        SubscriptionPodInfo subscriptionPodInfo=mock(SubscriptionPodInfo.class);
        when(subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId(MUNCHKIN_ID))).thenReturn(subscriptionPodInfo);
        when(siriusProvisioningHandler.cancelDormantSirius(MUNCHKIN_ID, podLocation)).thenReturn(true);
        when(jsonUtility.getFieldValueFromJsonString(null, "JSONResults", "isActive" )).thenReturn(false);
        Response<?> resp=subscriptionService.editSubscriptionStatus(MUNCHKIN_ID, SubscriptionStatus.CANCEL_DORMANT.getStatusCode(),
                podLocation,false);
        Assert.assertTrue(resp.isSuccess());


    }


    @Test
    public void updateEncryptionFlagScenario_1() throws Exception{
        MultiValueMap<String, String> data = spy(new LinkedMultiValueMap<>());
        data.add("status","invalid");
        data.add("token", UUID.randomUUID().toString());
        Response response =subscriptionService.updateEncryptionFlag(MUNCHKIN_ID,data);
        Assert.assertFalse(response.isSuccess());
    }


    @Test
    public void updateEncryptionFlagScenario_2() throws Exception{
        MultiValueMap<String, String> data = spy(new LinkedMultiValueMap<>());
        data.add("status","");
        data.add("token", UUID.randomUUID().toString());
        Response response =subscriptionService.updateEncryptionFlag(MUNCHKIN_ID,data);
        Assert.assertFalse(response.isSuccess());
    }

    @Test
    public void updateEncryptionFlagScenario_4() throws Exception{
        MultiValueMap<String, String> data = spy(new LinkedMultiValueMap<>());
        data.add("status","enable");
        data.add("token", "");
        Response response =subscriptionService.updateEncryptionFlag(MUNCHKIN_ID,data);
        Assert.assertFalse(response.isSuccess());
    }

    @Test
    public void updateEncryptionFlagScenario_3() throws Exception{
        SubscriptionPodInfo subscriptionPodInfo=mock(SubscriptionPodInfo.class);
        when(subscriptionPodInfo.getInternalApiUrl()).thenReturn("https://mlm");
        when(subscriptionServiceManager.getSubscriptionPodInfo(any(SubscriptionId.class))).thenReturn(subscriptionPodInfo);
        MultiValueMap<String, String> data = spy(new LinkedMultiValueMap<>());
        data.add("status","enable");
        data.add("token", UUID.randomUUID().toString());
        String mlmResponse="{\n" +
                "    \"JSONResults\": {\n" +
                "        \"sections\": [\n" +
                "            \"success\",\n" +
                "            \"message\"\n" +
                "        ],\n" +
                "        \"success\": true,\n" +
                "        \"message\": \"successfully updated encryption flag for "+MUNCHKIN_ID+"\"\n" +
                "    },\n" +
                "    \"HTMLResults\": {\n" +
                "        \"begin\": true,\n" +
                "        \"sections\": []\n" +
                "    }\n" +
                "}";
        when(restClient.postForm("https://mlm/administrator/updateDataEncryptionFlag?tenantId=123-ABC-456",data)).thenReturn(mlmResponse);
        Response response = subscriptionService.updateEncryptionFlag(MUNCHKIN_ID,data);
        Assert.assertTrue(response.isSuccess());
    }


    //@Test
    public void updateDeliverabilityPartnerEnabledStatusScenario_1() {
        MultiValueMap<String, String> data = spy(new LinkedMultiValueMap<>());
        data.add("status","invalid");
        Response response =subscriptionService.updateDeliverabilityPartnerEnabled(MUNCHKIN_ID,data);
        Assert.assertFalse(response.isSuccess());
    }


    //@Test
    public void updateDeliverabilityPartnerEnabledStatusScenario_2() {
        MultiValueMap<String, String> data = spy(new LinkedMultiValueMap<>());
        data.add("status","");
        Response response =subscriptionService.updateDeliverabilityPartnerEnabled(MUNCHKIN_ID,data);
        Assert.assertFalse(response.isSuccess());
    }

    @Test
    public void updateDeliverabilityPartnerEnabledStatusScenario_3() throws Exception{
        SubscriptionPodInfo subscriptionPodInfo=mock(SubscriptionPodInfo.class);
        when(subscriptionPodInfo.getInternalApiUrl()).thenReturn("https://mlm");
        when(subscriptionServiceManager.getSubscriptionPodInfo(any(SubscriptionId.class))).thenReturn(subscriptionPodInfo);
        doNothing().when(subscriptionServiceHelper).validateInputParameters(anyString());
        MultiValueMap<String, String> data = spy(new LinkedMultiValueMap<>());
        data.add("status","enable");
        String mlmResponse="{\n" +
                "    \"JSONResults\": {\n" +
                "        \"sections\": [\n" +
                "            \"success\",\n" +
                "            \"message\"\n" +
                "        ],\n" +
                "        \"success\": true,\n" +
                "        \"message\": \"Successfully updated Deliverability Partner Enabled status for : "+MUNCHKIN_ID+"\"\n" +
                "    },\n" +
                "    \"HTMLResults\": {\n" +
                "        \"begin\": true,\n" +
                "        \"sections\": []\n" +
                "    }\n" +
                "}";
        when(restClient.postForm("https://mlm/administrator/editDeliverabilityPartnerEnabled?tenantId=123-ABC-456",data)).thenReturn(mlmResponse);
        Response response = subscriptionService.updateDeliverabilityPartnerEnabled(MUNCHKIN_ID,data);
        Assert.assertTrue(response.isSuccess());
    }
}
