package com.marketo.subscriptionservice.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/**
 * Created by asawhney on 3/9/17.
 */
@ContextConfiguration(locations = { "classpath:subscription-test-config.xml" })
public class RestClientTest extends AbstractTestNGSpringContextTests {

    @Value("${orion.auth.token}")
    private String ORION_AUTH_TOKEN;

    @Autowired
    private RestClient restClient;

    //@Test
    public void testPostJosn() {
        String url = "http://int-bbconsole-ws.marketo.org/console/rest/v2/backlog/jobs";
        String data = "{\n" +
                "  \"job\": {\n" +
                "    \"component\": \"scenarios\",\n" +
                "    \"action\": {\n" +
                "      \"name\": \"test\",\n" +
                "      \"properties\": {\n" +
                "        \"emailNotificationList\": \"sloch@marketo.com\",\n" +
                "        \"munchkinId\": \"344-XSF-868\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"description\": \"A test scenario that is a functional no-op\",\n" +
                "    \"user\": \"sloch\"\n" +
                "  }\n" +
                "}";


        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
        String response = restClient.postJson(url, data, headers);

    }

    //@Test
    public void testRestJson() {

        String url = "http://int-bbconsole-ws.marketo.org/console/rest/v2/backlog/jobs/b1c63405-304e-4d8d-a1ff-24b9d6041790";
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
        String response = restClient.getJson(url, headers);
    }
}
