package com.marketo.subscriptionservice.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testng.annotations.BeforeMethod;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundleFeatures;
import com.marketo.subscriptionservice.controller.SubscriptionController;

public class SubscriptionControllerTest {
    
    MockMvc mockMvc;
    
    @InjectMocks
    SubscriptionController controller;
    
    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
                
        this.mockMvc = standaloneSetup(controller).setMessageConverters(new MappingJackson2HttpMessageConverter(), new FormHttpMessageConverter()).build();
    }
    
    //@Test
    public void testGetFeatureInfoForTenant() throws Exception {
        
        MvcResult result = this.mockMvc.perform(get("/v1/rest/subscriptions/{tenantId}/feature.json?name=test", "123-XXX-456")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        
        String response = result.getResponse().getContentAsString();
        
        ObjectMapper mapper = new ObjectMapper();
        JavaType type = mapper.getTypeFactory().constructParametricType(Response.class, MarketingFeature.class);
        Response<MarketingFeature> responseObj = mapper.readValue(response, type);
        
        assertThat(responseObj.getResult().get(0).getId(), equalTo(1));
        assertThat(responseObj.getResult().get(0).getName(), equalTo("test"));
    }
    
    //@Test
    public void testGetBundleInfoForTenant() throws Exception {
        
        MvcResult result = this.mockMvc.perform(get("/v1/rest/subscriptions/{tenantId}/bundle.json?name=test", "123-XXX-456")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        
        String response = result.getResponse().getContentAsString();
        
        ObjectMapper mapper = new ObjectMapper();
        JavaType type = mapper.getTypeFactory().constructParametricType(Response.class, MarketingBundle.class);
        Response<MarketingBundle> responseObj = mapper.readValue(response, type);
        
        assertThat(responseObj.getResult().get(0).getId(), equalTo(1));
        assertThat(responseObj.getResult().get(0).getName(), equalTo("test"));        
    }
    
    //@Test
    public void testRequestValidation() throws Exception {
        
        MvcResult result = this.mockMvc.perform(get("/v1/rest/subscriptions/{tenantId}/feature.json", "123-XXX-456")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        
        String response = result.getResponse().getContentAsString();
        
        ObjectMapper mapper = new ObjectMapper();
        JavaType type = mapper.getTypeFactory().constructParametricType(Response.class, MarketingFeature.class);
        Response<MarketingFeature> responseObj = mapper.readValue(response, type);
        
        assertThat(responseObj.getResult().get(0).getId(), equalTo(1));
        assertThat(responseObj.getResult().get(0).getName(), equalTo("test"));
    }
    
    //@Test
    public void testGetAllBundlesForTenant() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/v1/rest/subscriptions/{tenantId}.json", "123-ABC-456")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();

        String response = result.getResponse().getContentAsString();
        
        ObjectMapper mapper = new ObjectMapper();
        JavaType type = mapper.getTypeFactory().constructParametricType(Response.class, SubscriptionBundleFeatures.class);
        Response<SubscriptionBundleFeatures> responseObj = mapper.readValue(response, type);

        List<SubscriptionBundleFeatures> bundleFeatures = responseObj.getResult();
        
        assertThat(true, equalTo(true));        
    }
}
