package com.marketo.subscriptionservice.utility;

import org.apache.commons.lang3.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.security.NoSuchAlgorithmException;

public class SecurityUtilsTest {


	private String featureTenantId = "123-ABC-456";
	private String serviceUrl = "https://api.250ok.com";
	private String apiKey = "A70ffe7b9ff5455a879abaf3f770942ed45e91fd";
	private String communityToken = "m@Rk3t0Fly3r";
	private String hashMethod = "sha1";

	@InjectMocks
	@Spy
	private SecurityUtils securityUtils;

	@BeforeClass
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void testHashingPassword() {
		String hashValue = securityUtils.hashPassword(featureTenantId, communityToken, hashMethod);
		Assert.assertNotNull(hashValue);
	}

	@Test
	public void testHashMethod() throws Exception {
		String hashValue = securityUtils.hash(hashMethod, communityToken);
		Assert.assertNotNull(hashValue);
	}

	@Test
	public void testHashMethodWithError() throws Exception {
		String hashValue = securityUtils.hashPassword(featureTenantId, communityToken, null);
		Assert.assertTrue(StringUtils.isEmpty(hashValue));
	}

}
