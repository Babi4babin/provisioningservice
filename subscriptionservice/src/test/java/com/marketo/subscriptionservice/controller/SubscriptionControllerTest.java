package com.marketo.subscriptionservice.controller;


import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.provisioningservice.service.ProvisioningService;
import com.marketo.subscriptionservice.model.BundleFeaturesRequest;
import com.marketo.subscriptionservice.model.SubscriptionStatus;
import com.marketo.subscriptionservice.rest.ProvisioningResponse;
import com.marketo.subscriptionservice.rest.Response;
import com.marketo.subscriptionservice.service.SubscriptionService;
import org.junit.AfterClass;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;
import java.util.UUID;

import static org.mockito.Mockito.*;

public class SubscriptionControllerTest {


    @InjectMocks
    private SubscriptionController subscriptionController;

    @Mock
    private ProvisioningService provisioningService;

    @Mock
    private ProvisioningContext provisioningcontext;

    @Mock
    private SubscriptionService subscriptionService;

    @Mock
    private ProvisioningResponse provisioningResponse;

    private String requestId;

    private String munchkinId="123-ABC-235";


    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        requestId=UUID.randomUUID().toString();;

    }


    @Test
    public void testProvisionSubscription() throws Exception{
        MultiValueMap<String, String> data=spy(new LinkedMultiValueMap<>());
        when(provisioningcontext.getRequestUUID()).thenReturn(requestId);
        when(provisioningService.provisionSubscription(data)).thenReturn(provisioningResponse);
        Assert.assertEquals(1,subscriptionController.provisionSubscription(data).getResult().size());

    }


    @Test
    public void testNewProvisionSubscription() throws Exception{
        MultiValueMap<String, String> data=spy(new LinkedMultiValueMap<>());
        when(provisioningcontext.getRequestUUID()).thenReturn(requestId);
        when(provisioningService.newProvisionSubscription(data)).thenReturn(provisioningResponse);
        Assert.assertEquals(1,subscriptionController.newProvisionSubscription(data).getResult().size());
    }


    @Test
    public void testPostSubscriptionCreated() throws Exception{
        MultiValueMap<String, String> data=spy(new LinkedMultiValueMap<>());
        String tenantId= UUID.randomUUID().toString();
        when(provisioningcontext.getRequestUUID()).thenReturn(requestId);
        when(provisioningService.doPostSubscriptionCreated(tenantId,data)).thenReturn(provisioningResponse);
        Assert.assertEquals(1,subscriptionController.postSubscriptionCreated(tenantId,data).getResult().size());
    }


    @Test
    public void testMigrate() throws Exception{
        MultiValueMap<String, String> data=spy(new LinkedMultiValueMap<>());
        when(provisioningcontext.getRequestUUID()).thenReturn(requestId);
        when(provisioningService.migrate(data)).thenReturn(provisioningResponse);
        Assert.assertEquals(1,subscriptionController.migrate(data).getResult().size());
    }


    @Test
    public void tesProvisionFeature() throws Exception{
        String configParams=UUID.randomUUID().toString();
        Integer id=new Random().nextInt();
        Assert.assertEquals(1,subscriptionController.provisionFeature(munchkinId,id,configParams).getResult().size());
    }


    @Test
    public void testDeleteFeature() throws Exception{
        Assert.assertEquals(1,subscriptionController.deleteFeature(munchkinId,new Random().nextInt()).getResult().size());
    }

    @Test
    public void tesUpdateProvisionFeature() throws Exception{
        String configParams=UUID.randomUUID().toString();
        Integer id=new Random().nextInt();
        Assert.assertEquals(1,subscriptionController.updateFeature(munchkinId,id,configParams).getResult().size());
    }


    @Test
    public void testUpdateBundle() throws Exception{
        Assert.assertEquals(1,subscriptionController.updateBundle(munchkinId,new Random().nextInt()).getResult().size());
    }


    @Test
    public void testUpdateBundleWithRequest() throws Exception{
        BundleFeaturesRequest bundleFeaturesRequest=mock(BundleFeaturesRequest.class);
        Assert.assertEquals(1,subscriptionController.updateBundle(munchkinId,new Random().nextInt(),bundleFeaturesRequest).getResult().size());
    }


    @Test
    public void testProvisionBundleWithRequest() throws Exception{
        BundleFeaturesRequest bundleFeaturesRequest=mock(BundleFeaturesRequest.class);
        Assert.assertEquals(1,subscriptionController.provisionBundle(munchkinId,new Random().nextInt(),bundleFeaturesRequest).getResult().size());
    }


    @Test
    public void testUpgradeSubscription() throws Exception{
        MultiValueMap<String, String> data=spy(new LinkedMultiValueMap<>());
        when(provisioningService.upgradeSubscription(munchkinId,data)).thenReturn(provisioningResponse);
        when(provisioningcontext.getRequestUUID()).thenReturn(requestId);
        Assert.assertEquals(1,subscriptionController.upgradeSubscription(munchkinId,data).getResult().size());
    }

    @Test
    public void testDowngradeSubscription() throws Exception{
        MultiValueMap<String, String> data=spy(new LinkedMultiValueMap<>());
        when(provisioningService.downgradeSubscription(munchkinId,data)).thenReturn(provisioningResponse);
        when(provisioningcontext.getRequestUUID()).thenReturn(requestId);
        Assert.assertEquals(1,subscriptionController.downgradeSubscription(munchkinId,data).getResult().size());
    }

    @Test
    public void testModifySubscription() throws Exception{
        MultiValueMap<String, String> data=spy(new LinkedMultiValueMap<>());
        when(provisioningService.modifySubscriptionConfig(munchkinId,data)).thenReturn(provisioningResponse);
        when(provisioningcontext.getRequestUUID()).thenReturn(requestId);
        Assert.assertEquals(1,subscriptionController.modifySubscription(munchkinId,data).getResult().size());
    }

    @Test
    public void testDeprovisionBundle() throws Exception{
        Assert.assertEquals(1,subscriptionController.deprovisionBundle(munchkinId,new Random().nextInt()).getResult().size());
    }

    @Test
    public void testGetMarketingFeatureStatus() throws Exception{
        Integer bundleId=new Random().nextInt();
        String code=UUID.randomUUID().toString();
        String datacenter=UUID.randomUUID().toString();
        String response=UUID.randomUUID().toString();
        when(provisioningService.getMarketingFeatureStatus(munchkinId, code, datacenter)).thenReturn(response);
        Assert.assertEquals(1,subscriptionController.getMarketingFeatureStatus(munchkinId,code,datacenter).getResult().size());
    }

    @Test
    public void testCancelSub() throws Exception{
        String podLocation=UUID.randomUUID().toString();
        Boolean siriusCancelled=Boolean.TRUE;
        Response response=mock(Response.class);
        when(response.isSuccess()).thenReturn(true);
        when(subscriptionService.editSubscriptionStatus(munchkinId, SubscriptionStatus.CANCEL.getStatusCode(),
                podLocation, siriusCancelled)).thenReturn(response);
        Assert.assertEquals(response.isSuccess(),subscriptionController.cancelSub(munchkinId,podLocation,siriusCancelled).isSuccess());
    }

    @Test
    public void testCancelDormantSub() throws Exception{
        String podLocation=UUID.randomUUID().toString();
        Response response=mock(Response.class);
        when(response.isSuccess()).thenReturn(true);
        when(subscriptionService.editSubscriptionStatus(munchkinId, SubscriptionStatus.CANCEL_DORMANT.getStatusCode(), podLocation, false)
        ).thenReturn(response);
        Assert.assertEquals(response.isSuccess(),subscriptionController.cancelDormantSub(munchkinId,podLocation).isSuccess());
    }


    @Test
    public void testActivateSub() throws Exception{
        String podLocation=UUID.randomUUID().toString();
        Response response=mock(Response.class);
        when(response.isSuccess()).thenReturn(true);
        when(subscriptionService.editSubscriptionStatus(munchkinId, SubscriptionStatus.ACTIVE.getStatusCode(), podLocation, false)).thenReturn(response);
        Assert.assertEquals(response.isSuccess(),subscriptionController.activateSub(munchkinId,podLocation).isSuccess());
    }


    @Test
    public void testUpdateEncryptionFlag() throws Exception{
        Response response=mock(Response.class);
        when(response.isSuccess()).thenReturn(true);

        MultiValueMap<String, String> data = spy( new LinkedMultiValueMap<>());
        data.add("status","enable");
        data.add("token", UUID.randomUUID().toString());

        when(subscriptionService.updateEncryptionFlag(munchkinId,data)).thenReturn(response);
        Assert.assertEquals(response.isSuccess(),subscriptionController.updateEncryptionFlag(munchkinId,data).isSuccess());

    }



    @AfterClass
    public void tearDown() throws Exception{
        munchkinId=null;
        requestId=null;
    }





}

