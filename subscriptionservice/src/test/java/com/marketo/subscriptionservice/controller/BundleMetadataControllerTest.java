package com.marketo.subscriptionservice.controller;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.provisioningservice.service.ProvisioningContext;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BundleMetadataControllerTest {


    @InjectMocks
    private BundleMetadataController bundleMetadataController;

    @Mock
    private BundleMetadataService bundleMetadataService;

    @Mock
    private ProvisioningContext provisioningContext;


    @BeforeClass
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetMarketingBundle() throws Exception{
        MarketingBundle marketingBundle= mock(MarketingBundle.class);
        String bundleName= UUID.randomUUID().toString();
        when(bundleMetadataService.getMarketingBundle(bundleName)).thenReturn(marketingBundle);
        org.testng.Assert.assertEquals(1,bundleMetadataController.getMarketingBundle(bundleName).getResult().size());

    }

    @Test
    public void testGetBundleIsRoot() throws Exception {
        List<MarketingBundle> proxyBundles = new ArrayList<MarketingBundle>();
        when(bundleMetadataService.getRootBundles()).thenReturn(proxyBundles);
        org.testng.Assert.assertEquals(1,bundleMetadataController.getBundles(Boolean.TRUE).getResult().size());

    }
    @Test
    public void testGetBundleIsNonRoot() throws Exception {
        List<MarketingBundle> proxyBundles = new ArrayList<MarketingBundle>();
        when(bundleMetadataService.getAllBundles()).thenReturn(proxyBundles);
        org.testng.Assert.assertEquals(1,bundleMetadataController.getBundles(Boolean.FALSE).getResult().size());

    }

}
