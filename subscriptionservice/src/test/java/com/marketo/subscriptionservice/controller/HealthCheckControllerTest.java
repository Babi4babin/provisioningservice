package com.marketo.subscriptionservice.controller;

import com.marketo.subscriptionservice.model.Status;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class HealthCheckControllerTest{


    @InjectMocks
    private HealthCheckController healthCheckController;


    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testStatusUp() throws Exception{
        Assert.assertEquals(Status.UP.toString(),healthCheckController.status());
    }

    @Test
    public void testVersion() throws Exception{
        Assert.assertNotNull(healthCheckController.version());
    }


    @Test
    public void testInfo() throws Exception{
        Assert.assertNotNull(healthCheckController.info());
    }

    @Test
    public void testSmokeTest() throws Exception{
        Assert.assertEquals("Success",healthCheckController.smokeTest());
    }




}


