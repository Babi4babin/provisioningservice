package com.marketo.subscriptionservice.controller;

import com.marketo.packageupdater.model.PackageUpdateRequest;
import com.marketo.packageupdater.model.PackageUpdateResponse;
import com.marketo.packageupdater.model.UpdateTrackResponse;
import com.marketo.packageupdater.service.PackageUpdateService;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Random;

import  static org.mockito.Mockito.mock;
import  static org.mockito.Mockito.when;
public class PackageUpdateControllerTest {


    @InjectMocks
    private PackageUpdateController packageUpdateController;

    @Mock
    private PackageUpdateService packageUpdateService;


    @BeforeClass
    public void setUp(){

        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testUpdatePackages() throws Exception{
        PackageUpdateRequest packageUpdateRequest=mock(PackageUpdateRequest.class);
        PackageUpdateResponse packageUpdateResponse=mock(PackageUpdateResponse.class);
        when(packageUpdateService.updatePackages(packageUpdateRequest)).thenReturn(packageUpdateResponse);
        Assert.assertEquals(Boolean.TRUE,packageUpdateController.updatePackages(packageUpdateRequest).isSuccess());
    }

    @Test
    public void testTrackUpdate() throws Exception{
        UpdateTrackResponse updateTrackResponse=mock(UpdateTrackResponse.class);
        Integer trackId=new Random().nextInt();
        when(packageUpdateService.trackUpdate(trackId)).thenReturn(updateTrackResponse);
        Assert.assertEquals(Boolean.TRUE,packageUpdateController.trackUpdate(trackId).isSuccess());
    }
}

