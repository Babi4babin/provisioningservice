package com.marketo.subscriptionservice.controller;


import com.marketo.bundlemetadata.rest.model.BundlesFeatures;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundleFeatures;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundles;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import org.junit.AfterClass;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import com.marketo.subscriptionservice.rest.Response;


public class SubscriptionBundleMapControllerTest {


    @InjectMocks
    private SubscriptionBundleMapController subscriptionBundleMapController;

    @Mock
    private SubscriptionMetadataService subscriptionMetadataService;

    @Mock
    private ProvisioningContext provisioningContext;


    private String tenantUUID="123-ABC-235";


    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }



    @Test
    public void testGetProvisionedBundlesFeatures() throws Exception{
        SubscriptionBundleFeatures subscriptionBundleFeatures=mock(SubscriptionBundleFeatures.class);
        when(subscriptionMetadataService.getBundlesFeatures(tenantUUID)).thenReturn(subscriptionBundleFeatures);
        Assert.assertEquals(1,subscriptionBundleMapController.getProvisionedBundlesFeatures(tenantUUID).getResult().size());
    }

    @Test
    public void testGetAvailableBundlesFeatures() throws Exception{
        BundlesFeatures bundlesFeatures=mock(BundlesFeatures.class);
        when(subscriptionMetadataService.getAvailablesBundlesFeatures(tenantUUID)).thenReturn(bundlesFeatures);
        Assert.assertEquals(1,subscriptionBundleMapController.getAvailableBundlesFeatures(tenantUUID).getResult().size());
    }

    @Test
    public void testGetAvailableModulesFeatures() throws Exception{
        SubscriptionBundles subscriptionBundles=mock(SubscriptionBundles.class);
        when(subscriptionMetadataService.getAvailableModules(tenantUUID)).thenReturn(subscriptionBundles);
        Assert.assertEquals(1,subscriptionBundleMapController.getAvailableModulesFeatures(tenantUUID).getResult().size());

    }

    @Test
    public void testGetProvisionedModulesFeatures() throws Exception{
        SubscriptionBundles subscriptionBundles=mock(SubscriptionBundles.class);
        when(subscriptionMetadataService.getProvisionedModules(tenantUUID)).thenReturn(subscriptionBundles);
        Assert.assertEquals(1,subscriptionBundleMapController.getProvisionedModulesFeatures(tenantUUID).getResult().size());
    }


    @Test
    public void testAddBundle() throws Exception{
        String name= UUID.randomUUID().toString();
        Response response=subscriptionBundleMapController.addBundle(tenantUUID,name);
        Assert.assertEquals(true,response.isSuccess());
    }

    @Test
    public void testAddFeature() throws Exception{
        String name= UUID.randomUUID().toString();
        String featureConfig= UUID.randomUUID().toString();
        Response response=subscriptionBundleMapController.addFeature(tenantUUID,name,featureConfig);
        Assert.assertEquals(true,response.isSuccess());
    }

    @Test
    public void testAddFeatureWithOnlyTenantId() throws Exception{
        Response response=subscriptionBundleMapController.addFeature(tenantUUID);
        Assert.assertEquals(true,response.isSuccess());
    }


    @Test
    public void testUpdateFeatureConfig() throws Exception{
        String name= UUID.randomUUID().toString();
        String featureConfig= UUID.randomUUID().toString();
        Response response=subscriptionBundleMapController.updateFeatureConfig(tenantUUID,name,featureConfig);
        Assert.assertEquals(true,response.isSuccess());
    }


    @AfterClass
    public void tearDown() throws Exception{
        tenantUUID=null;
    }


}


