package com.marketo.provisioningservice.features;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.RestClient;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class RCAProvisionHandlerTest {

  @InjectMocks
  private RCAProvisioningHandler rcaProvisioningHandler;

  @Mock
  private SubscriptionMetadataService subscriptionMetadataService;

  @Mock
  private RestClient restClient;

  private String tenantUUID = "123-ABC-456";

  @BeforeClass
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  private MarketingFeature setupFeature() {

    MarketingFeature marketingFeature = new MarketingFeature();
    marketingFeature.setId(1);
    marketingFeature.setStatusURI("/customerProvision/getProvisionMpaSubscriptionStatus");
    marketingFeature.setCode("rca");
    return marketingFeature;
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testFeatureEmptyConfig() {

    MarketingFeature feature = setupFeature();
    SubscriptionBundleMap subscriptionBundleMap = new SubscriptionBundleMap();
    when(subscriptionMetadataService.getMarketingFeature(tenantUUID, feature)).thenReturn(subscriptionBundleMap);

    rcaProvisioningHandler.getFeatureStatus(tenantUUID, feature);
  }

  @Test(expectedExceptions = ProvisioningException.class)
  public void testFeatureEmptyResponse() {

    MarketingFeature feature = setupFeature();
    SubscriptionBundleMap subscriptionBundleMap = new SubscriptionBundleMap();
    subscriptionBundleMap.setSubscriptionFeatureConfig("{\"podName\":\"rtp-podint\"}");
    when(subscriptionMetadataService.getMarketingFeature(tenantUUID, feature)).thenReturn(subscriptionBundleMap);
    String resultUrl = "http://rtp-podintfe1.marketo.org:8080/customerProvision/getProvisionMpaSubscriptionStatus?tenantUUID=123-ABC-456";
    when(restClient.getJson(resultUrl, new HttpHeaders())).thenReturn(null);

    rcaProvisioningHandler.getFeatureStatus(tenantUUID, feature);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testFeatureResponseFailure() {

    MarketingFeature feature = setupFeature();
    SubscriptionBundleMap subscriptionBundleMap = new SubscriptionBundleMap();
    subscriptionBundleMap.setSubscriptionFeatureConfig("{\"podName\":\"rtp-podint\"}");
    when(subscriptionMetadataService.getMarketingFeature(tenantUUID, feature)).thenReturn(subscriptionBundleMap);
    String resultUrl = "http://rtp-podintfe1.marketo.org:8080/customerProvision/getProvisionMpaSubscriptionStatus?tenantUUID=123-ABC-456";
    when(restClient.getJson(resultUrl, new HttpHeaders())).thenReturn("{\"status\":\"\"}");

    rcaProvisioningHandler.getFeatureStatus(tenantUUID, feature);
  }


  @Test
  public void testFeatureResponseSuccessForFailedState() {

    MarketingFeature feature = setupFeature();
    SubscriptionBundleMap subscriptionBundleMap = new SubscriptionBundleMap();
    subscriptionBundleMap.setSubscriptionFeatureConfig("{\"podName\":\"rtp-podint\"}");
    when(subscriptionMetadataService.getMarketingFeature(tenantUUID, feature)).thenReturn(subscriptionBundleMap);
    String resultUrl = "http://rtp-podintfe1.marketo.org:8080/customerProvision/getProvisionMpaSubscriptionStatus?tenantUUID=123-ABC-456";

    when(restClient.getJson(resultUrl, new HttpHeaders())).thenReturn("{\"status\":\"failed\"}");
    doNothing().when(subscriptionMetadataService).addMarketingFeature(tenantUUID, feature, "{\"status\":\"failed\"}");
    doNothing().when(subscriptionMetadataService).deleteMarketingFeature(tenantUUID, feature.getId());

    String response = rcaProvisioningHandler.getFeatureStatus(tenantUUID, feature);
    Assert.assertEquals(response, "failed");
  }


  @Test
  public void testFeatureResponseSuccessForPendingState() {

    MarketingFeature feature = setupFeature();
    SubscriptionBundleMap subscriptionBundleMap = new SubscriptionBundleMap();
    subscriptionBundleMap.setSubscriptionFeatureConfig("{\"podName\":\"rtp-podint\"}");
    when(subscriptionMetadataService.getMarketingFeature(tenantUUID, feature)).thenReturn(subscriptionBundleMap);
    String resultUrl = "http://rtp-podintfe1.marketo.org:8080/customerProvision/getProvisionMpaSubscriptionStatus?tenantUUID=123-ABC-456";

    when(restClient.getJson(resultUrl, new HttpHeaders())).thenReturn("{\"status\":\"pending\"}");
    doNothing().when(subscriptionMetadataService).addMarketingFeature(tenantUUID, feature, "{\"status\":\"pending\"}");
    doNothing().when(subscriptionMetadataService).deleteMarketingFeature(tenantUUID, feature.getId());

    String response = rcaProvisioningHandler.getFeatureStatus(tenantUUID, feature);
    Assert.assertEquals(response, "pending");
  }
}
