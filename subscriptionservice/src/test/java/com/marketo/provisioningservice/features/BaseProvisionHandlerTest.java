package com.marketo.provisioningservice.features;

import com.marketo.subscriptionservice.model.FeatureCode;
import com.marketo.subscriptionservice.model.ProvisionSource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.locatorservice.exception.LocatorServiceException;
import com.marketo.provisioningservice.service.FeatureLocatorService;
import com.marketo.provisioningservice.service.Operation;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.RestClient;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;


public class BaseProvisionHandlerTest {
    
    @InjectMocks
    private BaseFeatureProvisioningHandler baseFeatureProvisionHandler;
    
    @Mock
    private FeatureLocatorService subFeatureLocator;
    
    @Mock
    private ProvisioningContext provisioningContext;
    
    @Mock
    private RestClient restClient;
    
    @Mock
    private SubscriptionMetadataService subscriptionMetadataService;
        
    @BeforeClass
    public void setup() {
        
        MockitoAnnotations.initMocks(this);        
    }

    private MarketingFeature setupFeature() throws LocatorServiceException {
        
        String tenantId = "123-ABC-456";
        
        String json = "{ \"message\": \"Invalid munchkin ID (20017)\", \"status\": \"ERROR\",\"errorCode\": null }";
        
        MarketingFeature feature = new MarketingFeature();
        feature.setId(1);
        feature.setName("json");
        feature.setLocatorServiceId("mlmpod");
        feature.setProvisioningURI("/rest/v1/featureHandler/operation/provisionSSO/munchkinId/{tenantUUID}");
        feature.setDeprovisioningURI("/rest/v1/featureHandler/operation/deprovisionSSO/munchkinId/{tenantUUID}");
        feature.setUpdateURI("/rest/v1/featureHandler/operation/updateSSO/munchkinId/{tenantUUID}");
        feature.setActive(1);
        feature.setMarketingFeatureConfig("{}");
        
        when(subFeatureLocator.getSubscriptionFeatureLocation(tenantId, feature)).thenReturn("http://trunk.marketo.com");
                
        when(provisioningContext.getTenantId()).thenReturn(tenantId);
                
        return feature;
    }
    
    private String getSuccessResponse() {
        return "{ \"success\" : \"SUCCESS\" }";
    }
    
    private String getFailureResponse() {
        return "{ \"message\": \"Invalid munchkin ID (20017)\", \"status\": \"ERROR\",\"errorCode\": null }";
    }    
    
    @Test
    public void testBuildFeatureUrl() {
        
        String url = baseFeatureProvisionHandler.buildFeatureUrl("http://www.trunk.com", 
                "/rest/v1/featureHandler/operation/provisionSocialPromotion/munchkinId/{tenantUUID}", "123-ABC-456");
        
        assertThat(url, equalTo("http://www.trunk.com/rest/v1/featureHandler/operation/provisionSocialPromotion/munchkinId/123-ABC-456")); 
        
        url = baseFeatureProvisionHandler.buildFeatureUrl("http://www.trunk.com", 
                "/rest/v1/featureHandler/operation/provisionRCA", "123-ABC-456");
        
        assertThat(url, equalTo("http://www.trunk.com/rest/v1/featureHandler/operation/provisionRCA"));
    }
    
    @Test
    public void testFeatureLocator() {  
    }
    
    @Test(expectedExceptions = ProvisioningException.class)
    public void testFailedProvisionFeature() throws LocatorServiceException {
        
        MarketingFeature feature = setupFeature();
        
        when(restClient.postForm(null, null)).thenReturn(getFailureResponse());
        
        baseFeatureProvisionHandler.doOperation(feature, Operation.PROVISION, provisioningContext);    
    }
    
    @Test(expectedExceptions = ProvisioningException.class)
    public void testFailedDeleteFeature() throws LocatorServiceException {
        
        MarketingFeature feature = setupFeature();
        
        when(restClient.postForm(null, null)).thenReturn(getFailureResponse());
        
        baseFeatureProvisionHandler.doOperation(feature, Operation.DEPROVISION, provisioningContext);   
    }
   
    
    @Test(expectedExceptions = ProvisioningException.class)
    public void testFailedUpdateFeature() throws LocatorServiceException {
        
        MarketingFeature feature = setupFeature();
        
        when(restClient.postForm(null, null)).thenReturn(getFailureResponse());
        
        when(subscriptionMetadataService.isFeatureProvisioned(provisioningContext.getTenantId(), feature.getId())).thenReturn(false);
        
        baseFeatureProvisionHandler.doOperation(feature, Operation.UPDATE, provisioningContext);
        
        when(subscriptionMetadataService.isFeatureProvisioned(provisioningContext.getTenantId(), feature.getId())).thenReturn(true);
        
        baseFeatureProvisionHandler.doOperation(feature, Operation.UPDATE, provisioningContext);
    }  
    
    @Test(expectedExceptions = ProvisioningException.class)
    public void testProvisionFeature() throws LocatorServiceException {
        
        MarketingFeature feature = setupFeature();
        
        when(restClient.postForm(null, null)).thenReturn(getSuccessResponse());
        
        baseFeatureProvisionHandler.doOperation(feature, Operation.PROVISION, provisioningContext);    
    }

    @Test(expectedExceptions = ProvisioningException.class)
    public void testDeleteFeature() throws LocatorServiceException {
        
        MarketingFeature feature = setupFeature();
        
        when(restClient.postForm(null, null)).thenReturn(getSuccessResponse());
        
        baseFeatureProvisionHandler.doOperation(feature, Operation.DEPROVISION, provisioningContext);   
    }

    @Test(expectedExceptions = ProvisioningException.class)
    public void testUpdateFeature() throws LocatorServiceException {
        
        MarketingFeature feature = setupFeature();
        
        when(restClient.postForm(null, null)).thenReturn(getSuccessResponse());
        
        when(subscriptionMetadataService.isFeatureProvisioned(provisioningContext.getTenantId(), feature.getId())).thenReturn(false);
        
        baseFeatureProvisionHandler.doOperation(feature, Operation.UPDATE, provisioningContext);
        
        when(subscriptionMetadataService.isFeatureProvisioned(provisioningContext.getTenantId(), feature.getId())).thenReturn(true);
        
        baseFeatureProvisionHandler.doOperation(feature, Operation.UPDATE, provisioningContext);
    }

    @Test
    public void testUpdateFeatureConfig() {

        MarketingFeature feature = new MarketingFeature();
        feature.setCode(FeatureCode.ORIONBase.toString());
        String config = "{\"podName\":\"rtp-podint\"}";
        feature.setMarketingFeatureConfig(config);

        ProvisioningContext provisioningContext = new ProvisioningContext();
        MultiValueMap<String, String> testConfigParams = new LinkedMultiValueMap<>();
        testConfigParams.add("mlm", "true");
        provisioningContext.setParams(testConfigParams);

        String featureConfig = baseFeatureProvisionHandler.updateFeatureConfig(feature, provisioningContext);
        Assert.assertNull(featureConfig);

        feature.setCode(FeatureCode.RCA.toString());
        provisioningContext.setProvisionSource(ProvisionSource.SPA.toString());
        featureConfig = baseFeatureProvisionHandler.updateFeatureConfig(feature, provisioningContext);
        Assert.assertNotNull(featureConfig);
    }

    @Test(expectedExceptions = ProvisioningException.class)
    public void testTenantIdNotFound() {

        when(provisioningContext.getTenantId()).thenReturn(null);
        baseFeatureProvisionHandler.doOperation(null, Operation.PROVISION, provisioningContext);
    }

    @Test(expectedExceptions = ProvisioningException.class)
    public void testFailedFetchFutureLocator() throws LocatorServiceException{

        MarketingFeature feature = setupFeature();
        when(provisioningContext.getActivePod()).thenReturn("mlmpod");
        when(subFeatureLocator.getSubscriptionFeatureLocation("123-ABC-456", feature)).thenReturn(null);
        when(subFeatureLocator.getPodFeatureLocation("mlmpod", feature, "123-ABC-456")).thenReturn(null);

        baseFeatureProvisionHandler.doOperation(feature, Operation.PROVISION, provisioningContext);
    }

    @Test
    public void testProvisionFeatureSuccessResponse() throws LocatorServiceException{

        MarketingFeature feature = setupFeature();
        String url = baseFeatureProvisionHandler.buildFeatureUrl("http://trunk.marketo.com", feature.getProvisioningURI(), "123-ABC-456");
        when(restClient.postJson(url, "{}")).thenReturn(getSuccessResponse());
        baseFeatureProvisionHandler.doOperation(feature, Operation.PROVISION, provisioningContext);
        Assert.assertEquals(restClient.postJson(url, "{}"), getSuccessResponse());
        verify(restClient, atLeastOnce()).postJson(url, "{}");
    }


    @Test(expectedExceptions = ProvisioningException.class)
    public void testProvisionFeatureFailureResponse() throws LocatorServiceException{

        MarketingFeature feature = setupFeature();
        String url = baseFeatureProvisionHandler.buildFeatureUrl("http://trunk.marketo.com", feature.getProvisioningURI(), "123-ABC-456");
        when(restClient.postJson(url, "{}")).thenReturn(getFailureResponse());
        baseFeatureProvisionHandler.doOperation(feature, Operation.PROVISION, provisioningContext);
    }
    
}
