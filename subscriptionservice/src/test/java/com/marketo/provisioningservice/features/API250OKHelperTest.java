package com.marketo.provisioningservice.features;

import com.marketo.provisioningservice.helper.API250OkHelper;
import com.marketo.subscriptionservice.model.ChildAccount;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import com.marketo.subscriptionservice.utility.SecurityUtils;
import org.mockito.*;
import org.mockito.Mock;
import org.springframework.http.*;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;


public class API250OKHelperTest {


	@Spy
	@InjectMocks
	private API250OkHelper api250OkHelper;

	@Mock
	private RestTemplate restTemplate;

	@Mock
	private SecurityUtils securityUtils;

	private String featureTenantId = "";
	private String serviceUrl = "https://api.250ok.com";
	private String apiKey = "A70ffe7b9ff5455a879abaf3f770942ed45e91fd";
	private String communityToken = "m@Rk3t0Fly3r";
	private String hashMethod = "sha1";
	String relativeUrl = "/api/1.0/account/childaccount";


	@BeforeClass
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(api250OkHelper,
				"restTemplate", restTemplate);
		ReflectionTestUtils.setField(api250OkHelper,
				"serviceUrl", serviceUrl);
		ReflectionTestUtils.setField(api250OkHelper,
				"apiKey", apiKey);
		ReflectionTestUtils.setField(api250OkHelper,
				"communityToken", communityToken);
		ReflectionTestUtils.setField(api250OkHelper,
				"hashMethod", hashMethod);

	}

	@BeforeMethod
	public void beforeMethod() {
		featureTenantId = UUID.randomUUID().toString();
	}

	@Test(expectedExceptions = ProvisioningException.class)
	public void testWhenAccessDenied() throws Exception {
		RestClientException ex = new HttpClientErrorException(HttpStatus.FORBIDDEN);
		Mockito.when(securityUtils.hashPassword(featureTenantId, communityToken, hashMethod)).
				thenThrow(ex);
		api250OkHelper.fetchChildAccountForTenant(featureTenantId, relativeUrl);
	}

	@Test(expectedExceptions = ProvisioningException.class)
	public void testWhenAuthenticationFailed() throws Exception {
		RestClientException ex = new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
		Mockito.when(securityUtils.hashPassword(featureTenantId, communityToken, hashMethod)).
				thenThrow(ex);
		api250OkHelper.fetchChildAccountForTenant(featureTenantId, relativeUrl);
	}

	@Test(expectedExceptions = ProvisioningException.class)
	public void testWhenInternalErrorAt250() throws Exception {
		RestClientException ex = new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
		Mockito.when(securityUtils.hashPassword(featureTenantId, communityToken, hashMethod)).
				thenThrow(ex);
		api250OkHelper.fetchChildAccountForTenant(featureTenantId, relativeUrl);
	}

	@Test(expectedExceptions = ProvisioningException.class)
	public void testFailedCreatingChild() throws Exception {
		Mockito.when(securityUtils.hashPassword(featureTenantId, communityToken, hashMethod)).
				thenThrow(new NullPointerException());
		api250OkHelper.fetchChildAccountForTenant(featureTenantId, relativeUrl);
	}

	@Test
	public void testSuccessCreatingChild() {
		String url = serviceUrl.concat(relativeUrl);
		UriComponents builder = UriComponentsBuilder.fromHttpUrl(url)
				.queryParam("external_id", securityUtils.hashPassword(featureTenantId,
						communityToken, hashMethod))
				.build();

		HttpHeaders headers = new HttpHeaders();
		headers.add("x-api-key", apiKey);

		ResponseEntity<ChildAccount[]> responseEntity = Mockito.mock(ResponseEntity.class);
		Mockito.when(responseEntity.getBody()).thenReturn(new ChildAccount[10]);
		Mockito.when(responseEntity.getStatusCode()).thenReturn(HttpStatus.OK);

		Assert.assertTrue(api250OkHelper.fetchChildAccountForTenant(featureTenantId, relativeUrl) == null);
	}

	@Test
	public void testCreatingRestTemplate() {
		Assert.assertNotNull(api250OkHelper.restTemplate());
	}
}
