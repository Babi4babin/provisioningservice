package com.marketo.provisioningservice.features;

import com.marketo.subscriptionservice.model.Response;
import com.marketo.subscriptionservice.model.SubscriptionFeatureConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by asawhney on 6/8/17.
 */
@ContextConfiguration(locations = { "classpath:subscription-test-config.xml" })
public class RCAProvisioningHandlerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private RCAProvisioningHandler rcaProvisioningHandler;

    //@Test
    public void testParseProvisioningResponse() {

        String rcaResponse = "{\"status\":\"pending\"}";
        Response response = rcaProvisioningHandler.parseProvisioningResponse(rcaResponse);
        Assert.assertEquals(response.getStatus(), "pending");
    }

    //@Test
    public void testParseSubscriptionFeatureConfig() {

        String subscriptionFeatureConfig = "{\"podName\":\"rtp-podint\"}";
        SubscriptionFeatureConfig config = rcaProvisioningHandler.parseSubscriptionFeatureConfig(subscriptionFeatureConfig);
        Assert.assertEquals(config.getPodName(), "rtp-podint");
    }

    //@Test
    public void testBuildRCAUrl() {
        String endpoint = "/customerProvision/getProvisionMpaSubscriptionStatus";
        String url = rcaProvisioningHandler.buildRCAUrl(endpoint, "000-000-000", "qe");
        Assert.assertEquals(url, "http://qefe1.marketo.org:8080/customerProvision/getProvisionMpaSubscriptionStatus?tenantUUID=000-000-000");
    }

    //@Test
    public void testGetFeatureStatus() {

    }
}
