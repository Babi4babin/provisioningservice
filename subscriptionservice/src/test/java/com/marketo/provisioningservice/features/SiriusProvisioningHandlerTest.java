package com.marketo.provisioningservice.features;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.model.FeatureStatus;
import com.marketo.subscriptionservice.model.sirius.SiriusJobStatus;
import com.marketo.subscriptionservice.model.sirius.SiriusResponse;
import com.marketo.subscriptionservice.rest.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.Assert;

/**
 * Created by asawhney on 3/9/17.
 */
@ContextConfiguration(locations = { "classpath:subscription-test-config.xml" })
public class SiriusProvisioningHandlerTest extends AbstractTestNGSpringContextTests {

    @Value("${orion.auth.token}")
    private String ORION_AUTH_TOKEN;

    @Autowired
    private RestClient restClient;

    @Autowired
    private SiriusProvisioningHandler siriusProvisioningHandler;

    @Autowired
    private SubscriptionMetadataService subscriptionMetadataService;

    //@Test
    public void testGetFeatureStatus() {

        String url = "http://int-bbconsole-ws.marketo.org/console/rest/v2/backlog/jobs/b1c63405-304e-4d8d-a1ff-24b9d6041790";
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
        String response = restClient.getJson(url, headers);

        SiriusResponse siriusResponse = siriusProvisioningHandler.parseProvisioningResponse(response);

        String status = siriusResponse.getBacklogJob().getStatus();
        MarketingFeature feature = new MarketingFeature();
        feature.setName("Orion Base");

        if (status.equalsIgnoreCase(SiriusJobStatus.KILLED.toString())) {
            subscriptionMetadataService.addMarketingFeature("622-LME-718", feature,
                    FeatureStatus.COMPLETED.toString());
        }

    }

    //@Test
    public void testCreatePayload() {

        MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
        configParams.add("leadDbSize", "1000");

        ProvisioningContext provisioningContext = new ProvisioningContext();
        provisioningContext.setParams(configParams);
        provisioningContext.setTenantId("622-LME-718");
        MarketingFeature feature = new MarketingFeature();
        feature.setCode("orionBase");
        String mlmInternalEndPoint = "test";
        String data = siriusProvisioningHandler.createPayload(feature, provisioningContext, mlmInternalEndPoint);

        String url = "http://int-bbconsole-ws.marketo.org/console/rest/v2/backlog/jobs";
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
        String response = restClient.postJson(url, data, headers);
        Assert.assertNotNull(response);
    }

    //@Test
    public void testParseProvisioningResponse() {

        String postResponse = postResponse();
        SiriusResponse responseI = siriusProvisioningHandler.parseProvisioningResponse(postResponse);
        String backlogIdI = responseI.getBacklogJob().getBacklogId();
        String statusI = responseI.getBacklogJob().getStatus();
        Assert.assertNotNull(backlogIdI);
        Assert.assertNotNull(statusI);

        String getResponse = getResponse();
        SiriusResponse responseII = siriusProvisioningHandler.parseProvisioningResponse(getResponse);
        String backlogIdII = responseII.getBacklogJob().getBacklogId();
        String statusII = responseII.getBacklogJob().getStatus();
        Assert.assertNotNull(backlogIdII);
        Assert.assertNotNull(statusII);

    }

    private String postResponse() {
        String url = "http://int-bbconsole-ws.marketo.org/console/rest/v2/backlog/jobs";
        String data = "{\n" +
                "  \"job\": {\n" +
                "    \"component\": \"scenarios\",\n" +
                "    \"action\": {\n" +
                "      \"name\": \"test\",\n" +
                "      \"properties\": {\n" +
                "        \"emailNotificationList\": \"sloch@marketo.com\",\n" +
                "        \"munchkinId\": \"344-XSF-868\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"description\": \"A test scenario that is a functional no-op\",\n" +
                "    \"user\": \"sloch\"\n" +
                "  }\n" +
                "}";

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
        String response = restClient.postJson(url, data, headers);
        return response;
    }

    private String getResponse() {

        String url = "http://int-bbconsole-ws.marketo.org/console/rest/v2/backlog/jobs/b1c63405-304e-4d8d-a1ff-24b9d6041790";
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
        String response = restClient.getJson(url, headers);
        return response;
    }
}
