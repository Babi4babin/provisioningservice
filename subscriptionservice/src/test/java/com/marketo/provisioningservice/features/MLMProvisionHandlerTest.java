package com.marketo.provisioningservice.features;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.provisioningservice.service.FeatureLocatorService;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionservice.rest.RestClient;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;
import org.mockito.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doReturn;

public class MLMProvisionHandlerTest {

  @Mock
  private FeatureLocatorService featureLocatorService;

  @Mock
  private RestClient restClient;

  @Spy
  @InjectMocks
  private MLMProvisioningHandler mlmProvisioningHandler;

  @BeforeClass
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  private MarketingFeature setupFeature() {

    MarketingFeature feature = new MarketingFeature();
    feature.setId(1);
    feature.setName("Base");
    feature.setProvisioningURI("/administrator/subscriptionProvisioningAPI?doAction=provBaseFeature");
    feature.setActive(1);
    feature.setMarketingFeatureConfig("{}");

    when(featureLocatorService.getPodFeatureLocation("mlmpod", feature, null)).thenReturn("http://trunk.marketo.com");

    return feature;
  }

  private ProvisioningContext setupProvisioningContext() {

    MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
    configParams.add("activePod", "mlmpod");
    ProvisioningContext provisioningContext = new ProvisioningContext();
    provisioningContext.setParams(configParams);
    return provisioningContext;
  }

  private String getSuccessResponse() {
    return "{ \"JSONResults\" : { \"munchkinId\" : \"123-ABC-456\" } }";
  }

  private String getFailureResponse() {
    return "{ \"JSONResults\" : { \"isError\" : true } }";
  }

  private String getFailureResponse2() {
    return "{ \"JSONResults\" : { \"isError\" : true, \"errorMessage\" : \"Error provisioning feature\"} }";
  }

  @Test(expectedExceptions = ProvisioningInputException.class)
  public void testProvisionInvalidPod() {

    MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
    ProvisioningContext provisioningContext = new ProvisioningContext();
    provisioningContext.setParams(configParams);
    MarketingFeature feature = setupFeature();
    mlmProvisioningHandler.provision(feature, provisioningContext);
  }

  @Test(expectedExceptions = ProvisioningException.class)
  public void testProvisionInvalidResponse() {

    MarketingFeature feature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    when(restClient.postForm("http://trunk.marketo.com"+feature.getProvisioningURI(), provisioningContext.getParams())).thenReturn(null);

    mlmProvisioningHandler.provision(feature, provisioningContext);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testProvisionInvalidResponse2() {

    MarketingFeature feature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    when(restClient.postForm("http://trunk.marketo.com"+feature.getProvisioningURI(), provisioningContext.getParams())).thenReturn("{}");

    mlmProvisioningHandler.provision(feature, provisioningContext);
  }


  @Test
  public void testProvisionSuccessResponse() {

    MarketingFeature feature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    when(restClient.postForm("http://trunk.marketo.com"+feature.getProvisioningURI(), provisioningContext.getParams())).thenReturn(getSuccessResponse());

    mlmProvisioningHandler.provision(feature, provisioningContext);
    Assert.assertNotNull(provisioningContext.getTenantId());
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testProvisionErrorResponse() {

    MarketingFeature feature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    when(restClient.postForm("http://trunk.marketo.com"+feature.getProvisioningURI(), provisioningContext.getParams())).thenReturn(getFailureResponse());

    mlmProvisioningHandler.provision(feature, provisioningContext);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testProvisionErrorResponse2() {

    MarketingFeature feature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    when(restClient.postForm("http://trunk.marketo.com"+feature.getProvisioningURI(), provisioningContext.getParams())).thenReturn(getFailureResponse2());

    mlmProvisioningHandler.provision(feature, provisioningContext);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testDeprovisionFeature() {

    MarketingFeature feature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    mlmProvisioningHandler.deprovision(feature, provisioningContext);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testMigrateTenantIdNotFound() {

    MarketingFeature feature = setupFeature();
    ProvisioningContext provisioningContext = new ProvisioningContext();
    mlmProvisioningHandler.migrate(feature, provisioningContext);
  }


  @Test
  public void testMigrateSuccessResponse() {

    MarketingFeature feature = setupFeature();
    ProvisioningContext provisioningContext = new ProvisioningContext();
    provisioningContext.setTenantId("123-ABC-456");
    doReturn("http://trunk.marketo.com").when(mlmProvisioningHandler).getFeatureLocator(feature, provisioningContext);
    when(restClient.postJson("http://trunk.marketo.com/rest/v1/featureHandler/operation/migrateFeatures/munchkinId/123-ABC-456", "")).thenReturn(getSuccessResponse());

    String response = mlmProvisioningHandler.migrate(feature, provisioningContext);
    Assert.assertEquals("123-ABC-456", response);
  }
}
