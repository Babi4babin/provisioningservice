package com.marketo.provisioningservice.features;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.provisioningservice.service.FeatureLocatorService;
import com.marketo.provisioningservice.service.Operation;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.model.FeatureStatus;
import com.marketo.subscriptionservice.rest.RestClient;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

public class SiriusProvisionHandlerTest {

  @InjectMocks
  private SiriusProvisioningHandler siriusProvisioningHandler;

  @Mock
  private RestClient restClient;

  @Mock
  private FeatureLocatorService subFeatureLocator;

  @Mock
  private SubscriptionMetadataService subscriptionMetadataService;

  @Value("${orion.auth.token}")
  private String ORION_AUTH_TOKEN;

  private String featureTenantId = "123-ABC-456";

  private String featureJobId = "1111";

  private String featurePod = "mlmpod";


  @BeforeClass
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  private MarketingFeature setupFeature() {

    MarketingFeature feature = new MarketingFeature();
    feature.setActive(1);
    feature.setCode("orionBase");
    feature.setProvisioningURI("/rest/v2/backlog/jobs");
    feature.setStatusURI("/rest/v2/backlog/jobs/{backlogId}");
    feature.setMarketingFeatureConfig("{}");
    when(subFeatureLocator.getSubscriptionFeatureLocation(featureTenantId, feature)).thenReturn("http://trunk.marketo.com");
    return feature;
  }

  private ProvisioningContext setupProvisioningContext() {

    MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
    configParams.add("leadDbSize", "1000");
    ProvisioningContext provisioningContext = new ProvisioningContext();
    provisioningContext.setParams(configParams);
    provisioningContext.setTenantId(featureTenantId);
    provisioningContext.setActivePod(featurePod);
    return provisioningContext;
  }

  private String getUrl() {
    return "https://123-abc-456-bbconsole-ws.marketo.org/console/rest/v2/backlog/jobs";
  }

  private String getData() {
    return "{\"job\":{\"component\":\"scenarios\",\"action\":{\"name\":\"orion_base\",\"properties\":{\"munchkinId\":\"123-ABC-456\",\"mlmInternalEndPoint\":" +
            "\"http://trunk.marketo.com\",\"leadDbSize\":\"1000\",\"podName\":\"mlmpod\"}},\"description\":\"Orion Base Provision\",\"user\":\"bbconsole\"}}";
  }

  private String getCancelDormantSiriusData() {
    return "{\"job\":{\"component\":\"scenarios\",\"action\":{\"name\":\"orion_base_deprovision_for_cancelled_dormant\",\"properties\":{\"munchkinId\":\"123-ABC-456\"," +
            "\"mlmInternalEndPoint\":\"none\",\"leadDbSize\":\"none\",\"podName\":\"none\"}},\"description\":null,\"user\":\"bbconsole\"}}";
  }

  private String getCancelSiriusData() {
    return "{\"job\":{\"component\":\"scenarios\",\"action\":{\"name\":\"orion_base_deprovision_for_cancelled\",\"properties\":{\"munchkinId\":\"123-ABC-456\"," +
            "\"mlmInternalEndPoint\":\"none\",\"leadDbSize\":\"none\",\"podName\":\"none\"}},\"description\":null,\"user\":\"bbconsole\"}}";
  }

  private String getReactivateSiriusData() {
    return "{\"job\":{\"component\":\"scenarios\",\"action\":{\"name\":\"orion_base_reactivation_for_cancelled_dormant\",\"properties\":{\"munchkinId\":" +
            "\"123-ABC-456\",\"mlmInternalEndPoint\":\"none\",\"leadDbSize\":\"none\",\"podName\":\"none\"}},\"description\":null,\"user\":\"bbconsole\"}}";
  }

  private String getSuccessResponse() {
    return "{ \"success\" : true, \"backlogJob\" : { \"backlogId\" : null, \"status\" : \"SUCCEEDED\" } }";
  }

  private String getFeatureSuccessStatus() {
    return "{ \"success\" : true, \"backlogJob\" : { \"backlogId\" : \"1111\", \"status\" : \"SUCCEEDED\" } }";
  }


  private String getFeatureFailedStatus() {
    return "{ \"success\" : true, \"backlogJob\" : { \"backlogId\" : \"1111\", \"status\" : \"FAILED\" } }";
  }

  private String getFeatureRunningStatus() {
    return "{ \"success\" : true, \"backlogJob\" : { \"backlogId\" : \"1111\", \"status\" : \"RUNNING\" } }";
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testFeatureLocatorNotFound() {

    MarketingFeature marketingFeature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    provisioningContext.setActivePod("");
    when(subFeatureLocator.getSubscriptionFeatureLocation(provisioningContext.getTenantId(), marketingFeature)).thenReturn("");
    siriusProvisioningHandler.doOperation(marketingFeature, Operation.PROVISION, provisioningContext);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testFeatureLocatorNotFound2() {

    MarketingFeature marketingFeature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    when(subFeatureLocator.getSubscriptionFeatureLocation(provisioningContext.getTenantId(), marketingFeature)).thenReturn("");
    when(subFeatureLocator.getPodFeatureLocation(provisioningContext.getActivePod(), marketingFeature, provisioningContext.getTenantId())).thenReturn("");
    siriusProvisioningHandler.doOperation(marketingFeature, Operation.PROVISION, provisioningContext);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testFeatureLeadDbSizeNull() {

    MarketingFeature marketingFeature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    provisioningContext.getParams().remove("leadDbSize");
    siriusProvisioningHandler.doOperation(marketingFeature, Operation.PROVISION, provisioningContext);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testFeatureDatacenterNull() {

    MarketingFeature marketingFeature = setupFeature();
    marketingFeature.setCode("");
    ProvisioningContext provisioningContext = setupProvisioningContext();
    when(subFeatureLocator.getSubscriptionDC(provisioningContext.getTenantId())).thenReturn("");
    when(subFeatureLocator.getDCFeatureLocation(provisioningContext.getActivePod())).thenReturn("");
    siriusProvisioningHandler.doOperation(marketingFeature, Operation.PROVISION, provisioningContext);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testFeatureEmptyResponse() {

    MarketingFeature marketingFeature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    String tempTenantId = provisioningContext.getTenantId();
    when(subFeatureLocator.getSubscriptionDC(provisioningContext.getTenantId())).thenReturn(tempTenantId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
    when(restClient.postJson(getUrl(), getData(), headers)).thenReturn("");
    siriusProvisioningHandler.doOperation(marketingFeature, Operation.PROVISION, provisioningContext);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testFeatureFailedResponse() {

    MarketingFeature marketingFeature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    String tempTenantId = provisioningContext.getTenantId();
    when(subFeatureLocator.getSubscriptionDC(provisioningContext.getTenantId())).thenReturn(tempTenantId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
    when(restClient.postJson(getUrl(), getData(), headers)).thenReturn("{\"success\" : false}");
    siriusProvisioningHandler.doOperation(marketingFeature, Operation.PROVISION, provisioningContext);
  }


  @Test
  public void testFeatureSuccessResponse() {

    MarketingFeature marketingFeature = setupFeature();
    ProvisioningContext provisioningContext = setupProvisioningContext();
    String tempTenantId = provisioningContext.getTenantId();

    when(subFeatureLocator.getSubscriptionDC(provisioningContext.getTenantId())).thenReturn(tempTenantId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
    when(restClient.postJson(getUrl(), getData(), headers)).thenReturn(getSuccessResponse());
    doNothing().when(subscriptionMetadataService).addMarketingFeature(tempTenantId, marketingFeature, "{\"status\":\"SUCCEEDED\"}");

    siriusProvisioningHandler.doOperation(marketingFeature, Operation.PROVISION, provisioningContext);
    verify(restClient, atLeastOnce()).postJson(getUrl(), getData(), headers);
  }


  @Test
  public void testFeatureStatusSucceeded() {

    MarketingFeature marketingFeature = setupFeature();
    when(subFeatureLocator.getSubscriptionDC(featureTenantId)).thenReturn(featureTenantId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
    when(restClient.getJson("https://123-abc-456-bbconsole-ws.marketo.org/console/rest/v2/backlog/jobs/1111", headers)).thenReturn(getFeatureSuccessStatus());
    doNothing().when(subscriptionMetadataService).addMarketingFeature(featureTenantId, marketingFeature, "{\"backlogId\":\"1111\",\"status\":\"SUCCEEDED\"}");

    String status = siriusProvisioningHandler.getFeatureStatus(featureTenantId, marketingFeature, featureJobId, null);
    Assert.assertEquals(status, FeatureStatus.COMPLETED.toString());
  }


  @Test
  public void testFeatureStatusFailed() {

    MarketingFeature marketingFeature = setupFeature();
    when(subFeatureLocator.getSubscriptionDC(featureTenantId)).thenReturn(featureTenantId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
    when(restClient.getJson("https://123-abc-456-bbconsole-ws.marketo.org/console/rest/v2/backlog/jobs/1111", headers)).thenReturn(getFeatureFailedStatus());

    String status = siriusProvisioningHandler.getFeatureStatus(featureTenantId, marketingFeature, featureJobId, null);
    Assert.assertEquals(status, FeatureStatus.FAILED.toString());
  }


  @Test
  public void testFeatureStatusRunning() {

    MarketingFeature marketingFeature = setupFeature();
    when(subFeatureLocator.getSubscriptionDC(featureTenantId)).thenReturn(featureTenantId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
    when(restClient.getJson("https://123-abc-456-bbconsole-ws.marketo.org/console/rest/v2/backlog/jobs/1111", headers)).thenReturn(getFeatureRunningStatus());

    String status = siriusProvisioningHandler.getFeatureStatus(featureTenantId, marketingFeature, featureJobId, null);
    Assert.assertEquals(status, FeatureStatus.PROGRESS.toString());
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void testCancelDormantSiriusInvalidStatusResponse() {

    when(subFeatureLocator.getSubscriptionDC(featureTenantId)).thenReturn(featureTenantId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
    when(restClient.postJson(getUrl(), getCancelDormantSiriusData(), headers)).thenReturn(getFeatureFailedStatus());
    siriusProvisioningHandler.cancelDormantSirius(featureTenantId, featurePod);
  }


  @Test
  public void testCancelDormantSiriusSuccessResponse() {

    when(subFeatureLocator.getSubscriptionDC(featureTenantId)).thenReturn(featureTenantId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
    when(restClient.postJson(getUrl(), getCancelDormantSiriusData(), headers)).thenReturn(getFeatureSuccessStatus());
    boolean result = siriusProvisioningHandler.cancelDormantSirius(featureTenantId, featurePod);
    Assert.assertTrue(result);
  }


  @Test
  public void testCancelSiriusSuccessResponse() {

    when(subFeatureLocator.getSubscriptionDC(featureTenantId)).thenReturn(featureTenantId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
    when(restClient.postJson(getUrl(), getCancelSiriusData(), headers)).thenReturn(getFeatureSuccessStatus());
    boolean result = siriusProvisioningHandler.cancelSirius(featureTenantId, featurePod);
    Assert.assertTrue(result);
  }


  @Test
  public void testReactivateSiriusSuccessResponse() {

    when(subFeatureLocator.getSubscriptionDC(featureTenantId)).thenReturn(featureTenantId);
    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Auth-Token", ORION_AUTH_TOKEN);
    when(restClient.postJson(getUrl(), getReactivateSiriusData(), headers)).thenReturn(getFeatureSuccessStatus());
    boolean result = siriusProvisioningHandler.reactivateSirius(featureTenantId, featurePod);
    Assert.assertTrue(result);
  }

}
