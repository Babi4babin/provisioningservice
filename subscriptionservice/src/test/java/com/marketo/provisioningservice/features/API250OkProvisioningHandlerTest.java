package com.marketo.provisioningservice.features;


import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.provisioningservice.helper.API250OkHelper;
import com.marketo.provisioningservice.service.Operation;
import com.marketo.provisioningservice.service.ProvisioningContext;
import com.marketo.subscriptionservice.model.ChildAccount;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import com.marketo.subscriptionservice.service.SubscriptionService;
import com.marketo.subscriptionservice.utility.SubscriptionConstants;
import org.mockito.*;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class API250OkProvisioningHandlerTest {


	@Spy
	@InjectMocks
	private API250OkProvisioningHandler api250OkProvisioningHandler;


	@Mock
	private API250OkHelper api250OkHelper;

	@Mock
	private SubscriptionService subscriptionService;

	private String featureTenantId = "123-ABC-456";

	private String featurePod = "api250ok";

	private String fetchUrl = "/api/1.0/account/childaccount";


	@BeforeClass
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	private MarketingFeature initFeature(){
		MarketingFeature feature = new MarketingFeature();
		feature.setActive(1);
		feature.setProvisioningURI("/api/1.0/account/childaccount");
		feature.setStatusURI("/api/1.0/account/childaccount");
		feature.setUpdateURI("/api/1.0/account/childaccount");
		return feature;
	}

	private MarketingFeature setupEmailDeliverabilityFeature() {
		MarketingFeature feature = initFeature();
		feature.setCode("emailDeliverability");
		feature.setMarketingFeatureConfig("{\"quantity\":1,\"max_inbox\":15,\"inbox_expiration\":\"2024-12-31\",\"inbox_period\":\"monthly\",\"max_design\":15,\"design_expiration\":\"2024-12-31\"}");
		feature.setDeveloperConfig("[{\"name\":{\"field\":\"quantity\",\"displayField\":\"Quantity\"},\"type\":\"number\",\"required" +
				"\":false,\"value\":1},{\"name\":{\"field\":\"max_inbox\",\"displayField\":\"max_inbox\"},\"type\":\"number\",\"required\":false,\"value\":15},{\"name\":{\"field\":\"" +
				"inbox_expiration\",\"displayField\":\"Inbox Expiration\"},\"type\":\"string\",\"required\":false,\"value\":\"2024-12-31\"},{\"name\":{\"field\":\"inbox_period\",\"displayField\"" +
				":\"Inbox Period\"},\"type\":\"string\",\"required\":false,\"value\":\"monthly\"},{\"name\":{\"field\":\"max_design\",\"displayField\":\"Max Design\"},\"type\":\"number\",\"required\":false,\"value\":15}," +
				"{\"name\":{\"field\":\"design_expiration\",\"displayField\":\"Design Expiration\"},\"type\":\"string\",\"required\":false,\"value\":\"2024-12-31\"}]");
		return feature;
	}

	private MarketingFeature setupEmailReputationFeature() {
		MarketingFeature feature = initFeature();
		feature.setCode("emailReputation");
		feature.setDeveloperConfig("[{\"name\":{\"field\":\"quantity\",\"displayField\":\"Quantity\"},\"type\":\"number\"," +
			"\"required\":false,\"value\":1},{\"name\":{\"field\":\"reputation_expiration\",\"displayField\":\"Reputation Expiration\"}," +
			"\"type\":\"string\",\"required\":false,\"value\":\"2024-12-31\"},{\"name\":{\"field\":\"max_blacklist\"," +
			"\"displayField\":\"Max Blacklist\"},\"type\":\"number\",\"required\":false,\"value\":1},{\"name\":{\"field\":" +
			"\"blacklist_expiration\",\"displayField\":\"BlackList Expiration\"},\"type\":\"string\",\"required\":false," +
			"\"value\":\"2024-12-31\"},{\"name\":{\"field\":\"max_dmarc\",\"displayField\":\"Max Dmarc\"},\"type\":\"number\"," +
			"\"required\":false,\"value\":1},{\"name\":{\"field\":\"dmarc_expiration\",\"displayField\":\"Dmarc Expiration\"}," +
			"\"type\":\"string\",\"required\":false,\"value\":\"2024-12-31\"}]");
		feature.setMarketingFeatureConfig("{\"quantity\":1,\"reputation_expiration\":\"2024-12-31\",\"max_blacklist\":1," +
			"\"blacklist_expiration\":\"2024-12-31\",\"max_dmarc\":1,\"dmarc_expiration\":\"2024-12-31\"}");
		return feature;
	}

	private MarketingFeature setupEmailInformantFeature() {
		MarketingFeature feature = initFeature();
		feature.setCode("emailInformant");
		feature.setDeveloperConfig("[{\"name\":{\"field\":\"quantity\",\"displayField\":\"Quantity\"},\"type\":\"number\"," +
			"\"required\":false,\"value\":1},{\"name\":{\"field\":\"max_emailinformant\",\"displayField\":\"Maximum Email Informant\"}," +
			"\"type\":\"number\",\"required\":false,\"value\":1000000},{\"name\":{\"field\":\"emailinformant_expiration\"," +
			"\"displayField\":\"Email Informant Expiration\"},\"type\":\"string\",\"required\":false,\"value\":\"2024-12-31\"}]");
		feature.setMarketingFeatureConfig("{\"quantity\":1,\"max_emailinformant\":1000000,\"emailinformant_expiration\":\"2024-12-31\"}");
		return feature;
	}

	private ProvisioningContext setupProvisioningContext() {

		MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
		configParams.add("emailInformant", "{\n" +
				"         “emailInformantMonthlyEvents”: 1000000,\n" +
				"         “expirationDate” : “12/31/2100”\n" +
				"        }");
		ProvisioningContext provisioningContext = new ProvisioningContext();
		provisioningContext.setParams(configParams);
		provisioningContext.setTenantId(featureTenantId);
		provisioningContext.setActivePod(featurePod);
		return provisioningContext;
	}

	private ChildAccount setupAccount(){
		ChildAccount account = new ChildAccount();
		account.setEmail("mocktest1@adobe.com");
		account.setId("123");
		account.setExternal_id("1234567890");
		account.setCompany("Adobe");
		account.setEmailinformant_max("10");
		account.setEmailinformant_expires("2024-12-31");
		account.setDesign_expires("2024-12-31");
		account.setDmarc_max("10");
		account.setHas_dmarc("y");
		account.setDmarc_expires("2024-12-31");
		account.setHas_emailinformant("y");
		account.setBlacklist_max("10");
		account.setHas_designinformant("y");
		account.setHas_reputation("y");
		account.setReputation_expires("2024-12-31");
		account.setHas_blacklistinformant("y");
		account.setBlacklist_expires("2024-12-31");
		account.setHas_inboxinformant("y");
	  return account;
	}

	private MultiValueMap<String, String> setupRequestBodyEmailDeliverability(ChildAccount account, Integer quantity){
		MultiValueMap<String, String> requestBodyMap = new LinkedMultiValueMap<String, String>();
		requestBodyMap.add("inbox_period", "monthly");
		requestBodyMap.add("quantity", String.valueOf(quantity));
		requestBodyMap.add("inbox_expiration", "2024-12-31");
		requestBodyMap.add("design_expiration", "2024-12-31");
		requestBodyMap.add("inbox", "y");
		requestBodyMap.add("design", "y");
		requestBodyMap.add("blacklist", "y");
		requestBodyMap.add("reputation", "y");
		requestBodyMap.add("dmarc", "y");
		requestBodyMap.add("emailinformant", "y");
		requestBodyMap.add("max_inbox", String.valueOf(quantity * 15));
		requestBodyMap.add("max_design", String.valueOf(quantity * 15));
		requestBodyMap.add("blacklist_expiration", account.getBlacklist_expires());
		requestBodyMap.add("max_blacklist", account.getBlacklist_max());
		requestBodyMap.add("reputation_expiration", account.getReputation_expires());
		requestBodyMap.add("emailinformant_expiration", account.getEmailinformant_expires());
		requestBodyMap.add("max_emailinformant", account.getEmailinformant_max());
		requestBodyMap.add("dmarc_expiration", account.getDmarc_expires());
		requestBodyMap.add("max_dmarc", account.getDmarc_max());
		requestBodyMap.add("id", account.getId());
		requestBodyMap.add("email", account.getEmail());
		requestBodyMap.add("name", account.getCompany());
		requestBodyMap.add("external_id", account.getExternal_id());
		return requestBodyMap;
	}

	@Test
	public void testProvision250OKAPIBundle() throws Exception {
		MarketingFeature feature = setupEmailDeliverabilityFeature();

		MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
		configParams.add("newBundle", "{'emailDeliverability':null}");
		ProvisioningContext provisioningContext = new ProvisioningContext();
		provisioningContext.setParams(configParams);
		provisioningContext.setTenantId(featureTenantId);
		provisioningContext.setActivePod(featurePod);

		ChildAccount[] accounts = new ChildAccount[1];
		accounts[0] = Mockito.mock(ChildAccount.class);
		Mockito.when(api250OkHelper.fetchChildAccountForTenant(featureTenantId, fetchUrl)).thenReturn(accounts);
		api250OkProvisioningHandler.doOperation(feature, Operation.PROVISION, provisioningContext);
		verify(api250OkProvisioningHandler, times(1)).provision(feature, provisioningContext);
	}


	@Test
	public void testProvision250OKAPIBundle_Scenario_2() throws Exception {
		MarketingFeature feature = setupEmailDeliverabilityFeature();

		MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
		configParams.add("newBundle", "{'emailDeliverability':{'quantity':2}}");
		ProvisioningContext provisioningContext = new ProvisioningContext();
		provisioningContext.setParams(configParams);
		provisioningContext.setTenantId(featureTenantId);
		provisioningContext.setActivePod(featurePod);

		ChildAccount[] accounts = new ChildAccount[1];
		accounts[0] = Mockito.mock(ChildAccount.class);
		Mockito.when(api250OkHelper.fetchChildAccountForTenant(featureTenantId, fetchUrl)).thenReturn(accounts);
		api250OkProvisioningHandler.doOperation(feature, Operation.PROVISION, provisioningContext);
		verify(api250OkProvisioningHandler, times(1)).provision(feature, provisioningContext);
	}


	//@Test
	public void testDeProvision250OKAPIBundle() {
		MarketingFeature feature = setupEmailDeliverabilityFeature();
		ProvisioningContext provisioningContext = setupProvisioningContext();
		api250OkProvisioningHandler.doOperation(feature, Operation.DEPROVISION, provisioningContext);
		verify(api250OkProvisioningHandler, times(1)).deprovision(feature, provisioningContext);
	}

	@Test
	public void testUpdate250OKAPIBundle() {
		ChildAccount account = setupAccount();
		ChildAccount[] accounts = new ChildAccount[1];
		accounts[0] = account;
		MarketingFeature feature = setupEmailDeliverabilityFeature();
		MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
		configParams.add("newBundle", "{'emailDeliverability':null}");
		ProvisioningContext provisioningContext = new ProvisioningContext();
		provisioningContext.setParams(configParams);
		provisioningContext.setTenantId(featureTenantId);
		provisioningContext.setActivePod(featurePod);
		Mockito.when(api250OkHelper.fetchChildAccountForTenant(featureTenantId, fetchUrl)).thenReturn(accounts);

		api250OkProvisioningHandler.doOperation(feature, Operation.UPDATE, provisioningContext);
		verify(api250OkProvisioningHandler, times(1)).update(feature, provisioningContext);

	}

	@Test(expectedExceptions = ProvisioningException.class)
	public void testUpdateWhenChildAccountDoesNoExist() {
		ChildAccount childAccount = null;
		MarketingFeature feature = setupEmailDeliverabilityFeature();
		ProvisioningContext provisioningContext = setupProvisioningContext();

		Mockito.when(api250OkProvisioningHandler.fetchByMunchkinId(featureTenantId, feature.getStatusURI())).thenReturn(childAccount);
		api250OkProvisioningHandler.update(feature, provisioningContext);
	}

	@Test
	public void testUpdateWithEmailDeliverability() {
		ChildAccount account = setupAccount();
		ChildAccount[] accounts = new ChildAccount[1];
		accounts[0] = account;
		MarketingFeature feature = setupEmailDeliverabilityFeature();
		List<String> api250okFields = Arrays.asList(SubscriptionConstants.API250OK_FIELD_INBOX, SubscriptionConstants.API250OK_FIELD_BLACKLIST, SubscriptionConstants.API250OK_FIELD_REPUTATION
				, SubscriptionConstants.API250OK_FIELD_DESIGN, SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT, SubscriptionConstants.API250OK_FIELD_DMARC);
		MultiValueMap<String, String> requestBodyMap = setupRequestBodyEmailDeliverability(account, 1);
		MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
		configParams.add("newBundle", "{'emailDeliverability':null}");
		ProvisioningContext provisioningContext = new ProvisioningContext();
		provisioningContext.setParams(configParams);
		provisioningContext.setTenantId(featureTenantId);
		provisioningContext.setActivePod(featurePod);

		Mockito.when(api250OkHelper.fetchChildAccountForTenant(featureTenantId, fetchUrl)).thenReturn(accounts);
		Mockito.when(api250OkHelper.getApi250okFields()).thenReturn(api250okFields);
		api250OkProvisioningHandler.update(feature, provisioningContext);
		verify(api250OkHelper, times(1)).provisionModulesForChildAccount(provisioningContext.getTenantId(), HttpMethod.PUT, requestBodyMap, feature.getUpdateURI());
	}

	@Test
	public void testUpdateWithEmailDeliverabilityWithCustomQuantity() {
		Integer quantity = 10;
		ChildAccount account = setupAccount();
		ChildAccount[] accounts = new ChildAccount[1];
		accounts[0] = account;
		MarketingFeature feature = setupEmailDeliverabilityFeature();
		List<String> api250okFields = Arrays.asList(SubscriptionConstants.API250OK_FIELD_INBOX, SubscriptionConstants.API250OK_FIELD_BLACKLIST, SubscriptionConstants.API250OK_FIELD_REPUTATION
				, SubscriptionConstants.API250OK_FIELD_DESIGN, SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT, SubscriptionConstants.API250OK_FIELD_DMARC);
		MultiValueMap<String, String> requestBodyMap = setupRequestBodyEmailDeliverability(account, quantity);
		MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
		configParams.add("newBundle", "{\"emailDeliverability\":{\"quantity\":10}}");
		ProvisioningContext provisioningContext = new ProvisioningContext();
		provisioningContext.setParams(configParams);
		provisioningContext.setTenantId(featureTenantId);
		provisioningContext.setActivePod(featurePod);

		Mockito.when(api250OkHelper.fetchChildAccountForTenant(featureTenantId, fetchUrl)).thenReturn(accounts);
		Mockito.when(api250OkHelper.getApi250okFields()).thenReturn(api250okFields);
		api250OkProvisioningHandler.update(feature, provisioningContext);
		verify(api250OkHelper, times(1)).provisionModulesForChildAccount(provisioningContext.getTenantId(), HttpMethod.PUT, requestBodyMap, feature.getUpdateURI());
	}

	@Test
	public void testUpdateWithEmailReputation() {
		ChildAccount account = setupAccount();
		ChildAccount[] accounts = new ChildAccount[1];
		accounts[0] = account;
		MarketingFeature feature = setupEmailReputationFeature();

		List<String> api250okFields = Arrays.asList(SubscriptionConstants.API250OK_FIELD_INBOX, SubscriptionConstants.API250OK_FIELD_BLACKLIST, SubscriptionConstants.API250OK_FIELD_REPUTATION,
				SubscriptionConstants.API250OK_FIELD_DESIGN, SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT, SubscriptionConstants.API250OK_FIELD_DMARC);
		MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
		configParams.add("newBundle", "{'emailReputation':null}");
		ProvisioningContext provisioningContext = new ProvisioningContext();
		provisioningContext.setParams(configParams);
		provisioningContext.setTenantId(featureTenantId);
		provisioningContext.setActivePod(featurePod);

		Mockito.when(api250OkHelper.getApi250okFields()).thenReturn(api250okFields);
		Mockito.when(api250OkHelper.fetchChildAccountForTenant(featureTenantId, fetchUrl)).thenReturn(accounts);
		try {
			api250OkProvisioningHandler.update(feature, provisioningContext);
		} catch (Exception e) {
			Assert.fail("Exception occured: " + e);
		}
	}

	@Test
	public void testUpdateWithEmailInformant() {
		ChildAccount account = setupAccount();
		ChildAccount[] accounts = new ChildAccount[1];
		accounts[0] = account;
		MarketingFeature feature = setupEmailInformantFeature();

		List<String> api250okFields = Arrays.asList(SubscriptionConstants.API250OK_FIELD_INBOX, SubscriptionConstants.API250OK_FIELD_BLACKLIST, SubscriptionConstants.API250OK_FIELD_REPUTATION
				, SubscriptionConstants.API250OK_FIELD_DESIGN, SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT, SubscriptionConstants.API250OK_FIELD_DMARC);
		MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
		configParams.add("newBundle", "{'emailInformant':null}");
		ProvisioningContext provisioningContext = new ProvisioningContext();
		provisioningContext.setParams(configParams);
		provisioningContext.setTenantId(featureTenantId);
		provisioningContext.setActivePod(featurePod);

		Mockito.when(api250OkHelper.fetchChildAccountForTenant(featureTenantId, fetchUrl)).thenReturn(accounts);
		Mockito.when(api250OkHelper.getApi250okFields()).thenReturn(api250okFields);
		try {
			api250OkProvisioningHandler.update(feature, provisioningContext);
		} catch (Exception e) {
			Assert.fail("Exception occured: " + e);
		}
	}

	@Test
	public void testUpdateWithEmailInformantWhenParamsEmpty() {
		ChildAccount account = setupAccount();
		ChildAccount[] accounts = new ChildAccount[1];
		accounts[0] = account;
		MarketingFeature feature = setupEmailInformantFeature();

		List<String> api250okFields = Arrays.asList(SubscriptionConstants.API250OK_FIELD_INBOX, SubscriptionConstants.API250OK_FIELD_BLACKLIST, SubscriptionConstants.API250OK_FIELD_REPUTATION
				, SubscriptionConstants.API250OK_FIELD_DESIGN, SubscriptionConstants.API250OK_FIELD_EMAIL_INFORMANT, SubscriptionConstants.API250OK_FIELD_DMARC);
		MultiValueMap<String, String> configParams = new LinkedMultiValueMap<>();
		configParams.add("newBundle", "{'emailInformant':null}");
		ProvisioningContext provisioningContext = new ProvisioningContext();
		provisioningContext.setTenantId(featureTenantId);
		provisioningContext.setActivePod(featurePod);

		Mockito.when(api250OkHelper.fetchChildAccountForTenant(featureTenantId, fetchUrl)).thenReturn(accounts);
		Mockito.when(api250OkHelper.getApi250okFields()).thenReturn(api250okFields);
		try {
			api250OkProvisioningHandler.update(feature, provisioningContext);
		} catch (Exception e) {
			Assert.fail("Exception occured: " + e);
		}
	}



}
