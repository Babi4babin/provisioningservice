package com.marketo.provisioningservice.features;

import java.lang.reflect.Method;
import java.util.Collections;

import com.marketo.subscriptionservice.model.NonProdDC;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.locatorservice.client.SubscriptionServiceManager;
import com.marketo.locatorservice.entities.SubscriptionId;
import com.marketo.locatorservice.entities.SubscriptionPodInfo;
import com.marketo.locatorservice.entities.SubscriptionPodInfo.SubscriptionPodInfoBuilder;
import com.marketo.locatorservice.exception.LocatorServiceException;
import com.marketo.provisioningservice.service.FeatureLocatorService;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

//@ContextConfiguration(locations = { "classpath:subscription-test-config.xml" })
public class SubscriptionFeatureLocatorTest{

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionFeatureLocatorTest.class);
    
    @InjectMocks
    private FeatureLocatorService subFeatureLocator;
    
    @Mock
    private SubscriptionServiceManager subscriptionServiceManager;     
    
    @BeforeClass
    public void init() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void getFeatureLocationBySub() throws LocatorServiceException {
                
        String tenantId = "123-ABC-456";
        
        MarketingFeature feature = new MarketingFeature();
        feature.setId(1);
        feature.setName("json");
        feature.setLocatorServiceId("mlmpod");
        feature.setProvisioningURI("/rest/v1/featureHandler/operation/provisionSSO/munchkinId/{tenantUUID}");
        feature.setActive(1);
                
        
        SubscriptionPodInfo podInfo = SubscriptionPodInfoBuilder.newBuilder().internalApiUrl("http://trunk.marketo.com").build();   
        
        when(subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId(tenantId))).thenReturn(podInfo);        
        
        String location = subFeatureLocator.getSubscriptionFeatureLocation(tenantId, feature);
        
        assertThat(location, equalTo("http://trunk.marketo.com"));
    }
    
    @Test
    public void getFeatureLocation() throws LocatorServiceException {
        
        MarketingFeature feature = new MarketingFeature();
        feature.setLocatorServiceId("mlmpod");        
        String tenantUUID = "123-ABC-456";
        String activePod = "trunk";
        
        SubscriptionPodInfo podInfo = SubscriptionPodInfoBuilder.newBuilder().
                internalApiUrl("http://trunk.marketo.com").podName(activePod).build();
        
        when(subscriptionServiceManager.getAllSubscriptionPodInfo()).thenReturn(Collections.singletonList(podInfo));
        
        
        String location = subFeatureLocator.getPodFeatureLocation(activePod, feature, tenantUUID);
        
        assertThat(location, equalTo("http://trunk.marketo.com"));

    }

    @Test
    public void getDCTestINT() throws Exception {
        String dc = NonProdDC.SJ.toString();
        String podName = "sjint";
        Class flsClass = Class.forName("com.marketo.provisioningservice.service.FeatureLocatorService");
        Method getDCMethod = flsClass.getDeclaredMethod("getDC", new Class[] { String.class, String.class });
        getDCMethod.setAccessible(true);
        Object flsClassInstance = flsClass.newInstance();
        Object result = getDCMethod.invoke(flsClassInstance, new Object[] { dc,podName});
        assertThat(result.toString(), equalTo(NonProdDC.INT.toString()));

    }

    @Test
    public void getDCTestQEPods() throws Exception {
        String dc = NonProdDC.SJ.toString();
        String podName1 = "sjqe2";
        Class flsClass = Class.forName("com.marketo.provisioningservice.service.FeatureLocatorService");
        Method getDCMethod = flsClass.getDeclaredMethod("getDC", new Class[] { String.class, String.class });
        getDCMethod.setAccessible(true);
        Object flsClassInstance = flsClass.newInstance();
        Object result1 = getDCMethod.invoke(flsClassInstance, new Object[] { dc,podName1});
        assertThat(result1.toString(), equalTo(NonProdDC.QE.toString()));

        String podName2 = "sjqe";
        Object result2 = getDCMethod.invoke(flsClassInstance, new Object[] { dc,podName2});
        assertThat(result2.toString(), equalTo(NonProdDC.QE.toString()));
    }

    @Test
    public void getDCTestGCPINT() throws Exception {
        String dc = NonProdDC.SJ.toString();
        String podName = "gusw1int001";
        Class flsClass = Class.forName("com.marketo.provisioningservice.service.FeatureLocatorService");
        Method getDCMethod = flsClass.getDeclaredMethod("getDC", new Class[] { String.class, String.class });
        getDCMethod.setAccessible(true);
        Object flsClassInstance = flsClass.newInstance();
        Object result = getDCMethod.invoke(flsClassInstance, new Object[] { dc,podName});
        assertThat(result.toString(), equalTo(NonProdDC.INT.toString()));
    }
}
