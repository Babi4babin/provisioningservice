package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.provisioningservice.features.RCAProvisioningHandler;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

public class ProvisioningServiceGetFeatureMockTest {

  @Mock
  private BundleMetadataService bundleMetadataService;

  @Mock
  private MarketingFeature marketingFeature;

  @Mock
  private SubscriptionMetadataDAO subscriptionMetadataDAO;

  @Mock
  private RCAProvisioningHandler rcaProvisioningHandler;

  @InjectMocks
  private ProvisioningService provisioningService;

  private static final String MUNCHKIN_ID = "123-LME-456";

  @BeforeClass
  public void setUp() {
    MockitoAnnotations.initMocks(this);

  }


  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Invalid TenantUUID Recieved")
  public void testGetMarketingFeatureStatusWithEmptyException() {
    provisioningService.getMarketingFeatureStatus("","rca","sj");
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Feature: rca not found in the Bundle Metadata")
  public void testGetMarketingFeatureStatusWithNullException() {
    when(bundleMetadataService.getMarketingFeatureByCode("rca")).thenReturn(null);
    provisioningService.getMarketingFeatureStatus(MUNCHKIN_ID,"rca","sj");
  }


  @Test
  public void testGetMarketingFeatureWithUnknownStatus() {
    when(bundleMetadataService.getMarketingFeatureByCode("rca")).thenReturn(marketingFeature);
    when( subscriptionMetadataDAO.getMarketingFeatureStatus(MUNCHKIN_ID, marketingFeature)).thenReturn(null);
    when(marketingFeature.getCode()).thenReturn("orionBase").thenReturn("unknown");
    String status = provisioningService.getMarketingFeatureStatus(MUNCHKIN_ID, "rca", "sj");

    Assert.assertEquals(status, "unknown");
  }

  @Test
  public void testGetMarketingFeatureWithUnknownStatus1() {

    String data = "{\"status\": \"\\tactive\\t\",\"\\tbacklogId\\t\": \"id\"}";
    when(bundleMetadataService.getMarketingFeatureByCode("rca")).thenReturn(marketingFeature);
    when( subscriptionMetadataDAO.getMarketingFeatureStatus(MUNCHKIN_ID, marketingFeature)).thenReturn(data);
    when(marketingFeature.getCode()).thenReturn("revenueCycleAnalytics");
    String status = provisioningService.getMarketingFeatureStatus(MUNCHKIN_ID, "rca", "sj");

    verify(rcaProvisioningHandler,times(1)).getFeatureStatus(MUNCHKIN_ID,marketingFeature);
  }

}
