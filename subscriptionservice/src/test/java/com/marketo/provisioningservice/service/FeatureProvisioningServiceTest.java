package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.provisioningservice.features.API250OkProvisioningHandler;
import com.marketo.provisioningservice.features.DefaultFeatureProvisioningHandler;
import com.marketo.provisioningservice.features.MLMProvisioningHandler;
import com.marketo.provisioningservice.features.SiriusProvisioningHandler;
import com.marketo.subscriptionservice.model.ClientType;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FeatureProvisioningServiceTest {

    @Mock
    private MarketingFeature marketingFeature;

    @Mock
    private ProvisioningContext provisioningContext;

    private Operation operation;

    @Mock
    private MLMProvisioningHandler mlmFeatureHandler;

    @Mock
    private DefaultFeatureProvisioningHandler defaultFeatureHandler;

    @Mock
    private SiriusProvisioningHandler siriusProvisioningHandler;

    @Mock
    private API250OkProvisioningHandler api250OkProvisioningHandler;

    @InjectMocks
    private FeatureProvisioningService featureProvisioningService;

    private static final String TENANT_ID = "622-LME-718";

    @BeforeMethod
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testMlmFeatureHandlerDoOperation() {
       when(marketingFeature.getName()).thenReturn("Base");
       when(provisioningContext.getTenantId()).thenReturn(TENANT_ID);
       featureProvisioningService.doOperation(marketingFeature, operation
                                             , provisioningContext);

       verify(defaultFeatureHandler,times(0)).doOperation(marketingFeature, operation
                                                          , provisioningContext);

       verify(mlmFeatureHandler,times(1)).doOperation(marketingFeature, operation
                                                      , provisioningContext);
    }

    @Test
    public void testSiriusProvisioningHandlerDoOperation() {
       when(marketingFeature.getClientType()).thenReturn(ClientType.SIRIUS.toString());
       featureProvisioningService.doOperation(marketingFeature, operation
                                             , provisioningContext);

       verify(mlmFeatureHandler,times(0)).doOperation(marketingFeature, operation
                                                      , provisioningContext);

       verify(defaultFeatureHandler,times(0)).doOperation(marketingFeature, operation
                                                         , provisioningContext);

       verify(siriusProvisioningHandler,times(1)).doOperation(marketingFeature, operation
                                                             , provisioningContext);
    }

    @Test
    public void testDefaultHandlerDoOperation() {
       when(marketingFeature.getClientType()).thenReturn(null);
       featureProvisioningService.doOperation(marketingFeature, operation
                                              , provisioningContext);

       verify(mlmFeatureHandler,times(0)).doOperation(marketingFeature, operation
                                                      , provisioningContext);

       verify(defaultFeatureHandler,times(1)).doOperation(marketingFeature, operation
                                                         , provisioningContext);
    }

    @Test
    public void test250OKProvisioningHandlerDoOperation() {
        when(marketingFeature.getClientType()).thenReturn(ClientType.API250OK.toString());
        featureProvisioningService.doOperation(marketingFeature, operation
                , provisioningContext);

        verify(mlmFeatureHandler, times(0)).doOperation(marketingFeature, operation
                , provisioningContext);

        verify(defaultFeatureHandler, times(0)).doOperation(marketingFeature, operation
                , provisioningContext);

        verify(siriusProvisioningHandler, times(0)).doOperation(marketingFeature, operation
                , provisioningContext);
        verify(api250OkProvisioningHandler, times(1)).doOperation(marketingFeature, operation
                , provisioningContext);
    }
}