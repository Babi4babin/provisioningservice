package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.exception.BundleMetadataException;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.provisioningservice.features.MLMProvisioningHandler;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.ProvisioningResponse;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class ProvisioningServiceMigrateMockTest {

  @Mock
  private ProvisioningContext provisioningContext;

  @Mock
  private BundleMetadataService bundleMetadataService;

  @Mock
  private MarketingBundle marketingBundle;

  @Mock
  private MarketingBundleFeature marketingBundleFeature;

  @Mock
  private FeatureProvisioningService featureProvisioningService;

  @Mock
  private MarketingFeature marketingFeature;

  @Mock
  private SubscriptionMetadataDAO subscriptionMetadataDAO;

  @Mock
  private MLMProvisioningHandler mlmFeatureHandler;

  @Mock
  private SubscriptionMetadataService subscriptionMetadataService;

  @InjectMocks
  private ProvisioningService provisioningService;

  private static List<String> multiValuedMapToken;

  private List<String> multiValuedMapMlm;

  private List<String> multiValuedMapBundle;

  private static final String TOKEN = "token";

  private static final String MLM = "mlm";

  private static final String BUNDLE = "bundle";

  private static final String BASE_MODULE = "baseModule";

  private static final String MUNCHKIN_ID = "123-LME-456";

  private MultiValueMap<String, String> configParams;

  //static content to be loaded first
  {
    multiValuedMapToken = new ArrayList<>();
    multiValuedMapMlm  = new ArrayList<>();
    multiValuedMapBundle = new ArrayList<>();

    multiValuedMapToken.add(TOKEN);
    multiValuedMapMlm.add(MLM);
    multiValuedMapBundle.add(BASE_MODULE);
  }

  @BeforeClass
  public void setUp() {
    MockitoAnnotations.initMocks(this);

  }

  @BeforeMethod
  public void init() {
    configParams = new LinkedMultiValueMap<>();
    configParams.put(TOKEN, multiValuedMapToken);
    configParams.put(MLM, multiValuedMapMlm);
    configParams.put(BUNDLE, multiValuedMapBundle);
  }

  @Test
  public void testMigrate() {
    List<SubscriptionBundleMap> subscriptionBundleList = new ArrayList<>();
    List<MarketingBundleFeature> marketingBundleFeatureList = new ArrayList<>();
    marketingBundleFeatureList.add(marketingBundleFeature);
    configParams.add("tenantUUID", MUNCHKIN_ID);
    when(provisioningContext.getTenantId()).thenReturn(MUNCHKIN_ID);
    when(bundleMetadataService.getMarketingBundleByCode("baseModule")).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getAllBundleFeatures(MUNCHKIN_ID)).thenReturn(subscriptionBundleList);
    when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatureList);
    when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
    when(marketingBundleFeature.getMarketingFeatureConfig()).thenReturn("Base-Config");

    ProvisioningResponse provisioningResponse =   provisioningService.migrate(configParams);
    Assert.assertNotNull(provisioningResponse);
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Missing subscription tenantUUID")
  public void testMigrateWithEmptyTenantUUID() {
    provisioningService.migrate(configParams);
  }

  @Test(expectedExceptions = BundleMetadataException.class)
  public void testMigrateWithAlreadyExistingTenantUUID() {
    List<SubscriptionBundleMap> subscriptionBundleList = new ArrayList<>();
    subscriptionBundleList.add(new SubscriptionBundleMap());
    configParams.add("tenantUUID", MUNCHKIN_ID);
    when(subscriptionMetadataDAO.getAllBundleFeatures(MUNCHKIN_ID)).thenReturn(subscriptionBundleList);
    provisioningService.migrate(configParams);
  }

}
