package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.subscriptionservice.model.ClientType;
import com.marketo.subscriptionservice.model.SubscriptionFeatureInfo;
import junit.framework.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

/**
 * Created by asawhney on 3/9/17.
 */
@ContextConfiguration(locations = { "classpath:subscription-test-config.xml" })
public class ProvisioningServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ProvisioningService provisioningService;

    //@Test
    public void testParseSubscriptionFeatureInfo() {

        String subscriptionInfo = "{\"status\": \"Completed\"," +
                "\"jobId\": \"1111\"}";

        SubscriptionFeatureInfo info = provisioningService.parseSubscriptionFeatureInfo(subscriptionInfo);
        Assert.assertNotNull(info.getBacklogId());
        Assert.assertNotNull(info.getStatus());
    }

    //@Test
    public void testGetFeatureCurrentStatus() {

        String subscriptionInfo = "{\"status\": \"SUCCEEDED\"," +
                "\"jobId\": \"1111\"}";
        MarketingFeature feature = new MarketingFeature();
        feature.setClientType(ClientType.SIRIUS.toString());
        String status = provisioningService.getFeatureCurrentStatus("622-LME-718", feature, subscriptionInfo, null);
    }
}
