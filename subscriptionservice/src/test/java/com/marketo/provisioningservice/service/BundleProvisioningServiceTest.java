package com.marketo.provisioningservice.service;

import org.testng.annotations.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BundleProvisioningServiceTest {

    /*
    for (MarketingBundleFeature bundleFeature : bundle.getChildBundleFeatures()) {
        
        try {
            if (bundleFeature.isBundle()) {
                doOperation(bundleFeature.getBundle(), operation, context);
            } else if (bundleFeature.isFeature()) {
                String featureConfig = bundleFeature.getMarketingFeatureConfig();
                MarketingFeature feature = bundleFeature.getFeature();
                
                if (!StringUtils.isEmpty(featureConfig)) {
                    feature.setMarketingFeatureConfig(featureConfig);                        
                } else {
                    logger.info("Feature configuration not present for feature: " 
                            + feature.getName() + " for parent bundle " + bundle.getName());
                }
                
                featureProvisioningService.doOperation(feature, operation, context);
            }                
        } catch (ProvisioningException e) {
            
            if (bundleFeature.isBundle()) {
                logger.error("Failed Provisoning Markering Bundle: "
                        + bundleFeature.getBundle().getName() + ". Context: " + context, e);
            } else {
                logger.error("Failed Provisoning Markering Feature: "
                        + bundleFeature.getFeature().getName() + ". Context: " + context, e);                                        
            }
            
            context.addError(e);
    */
    
    @Test
    public void testProvisionBundle() {
        
        
        
    }

    @Test
    public void testIsBundleRequirePodSelection() {
        ProvisioningService provisioningService = new ProvisioningService();
        assertThat(provisioningService.isBundleRequirePodSelection("test"), equalTo(false));
        assertThat(provisioningService.isBundleRequirePodSelection("programAnalysis"), equalTo(true));
    }

    @Test
    public void testParsePodSelection() {
        ProvisioningContext context = new ProvisioningContext();

        assertThat(context.parsePodSelectionFromParam("").size(), equalTo(0));
        assertThat(context.parsePodSelectionFromParam(null).size(), equalTo(0));
        assertThat(context.parsePodSelectionFromParam("{\"rtp\":\"pod1\"}").size(), equalTo(1));

    }
}
