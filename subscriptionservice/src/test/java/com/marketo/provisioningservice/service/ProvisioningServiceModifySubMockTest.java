package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.dao.BundleMetadataDAO;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundles;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.ProvisioningResponse;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class ProvisioningServiceModifySubMockTest {


  @Mock
  private ProvisioningContext provisioningContext;

  @Mock
  private MarketingBundle marketingBundle;

  @Mock
  private MarketingBundleFeature marketingBundleFeature;

  @Mock
  private MarketingFeature marketingFeature;

  @Mock
  private SubscriptionMetadataDAO subscriptionMetadataDAO;

  @Mock
  private SubscriptionMetadataService subscriptionMetadataService;

  @Mock
  private BundleProvisioningService bundleProvisioningService;

  @Mock
  private BundleMetadataDAO bundleMetadataDAO;

  @Mock
  private SubscriptionBundleMap subscriptionBundle;

  @Mock
  private SubscriptionBundles subscriptionBundles;

  @InjectMocks
  private ProvisioningService provisioningService;

  private static List<String> multiValuedMapToken;

  private List<String> multiValuedMapMlm;

  private List<String> multiValuedMapBundle;

  private static final String TOKEN = "token";

  private static final String MLM = "mlm";

  private static final String BUNDLE = "bundle";

  private static final String BASE_MODULE = "baseModule";

  private static final String MUNCHKIN_ID = "123-LME-456";

  private MultiValueMap<String, String> configParams;

  //static content to be loaded first
  {
    multiValuedMapToken = new ArrayList<>();
    multiValuedMapMlm  = new ArrayList<>();
    multiValuedMapBundle = new ArrayList<>();

    multiValuedMapToken.add(TOKEN);
    multiValuedMapMlm.add(MLM);
    multiValuedMapBundle.add(BASE_MODULE);
  }

  @BeforeClass
  public void setUp() {
    MockitoAnnotations.initMocks(this);

  }

  @BeforeMethod
  public void init() {
    configParams = new LinkedMultiValueMap<>();
    configParams.put(TOKEN, multiValuedMapToken);
    configParams.put(MLM, multiValuedMapMlm);
    configParams.put(BUNDLE, multiValuedMapBundle);
  }
  @Test(expectedExceptions = ProvisioningException.class)
  public void TestModifySubscriptionConfigWithUnprovisionedModule() {
    JSONObject bundleConfig = new JSONObject("{\"rca\":{}}}");
    List<MarketingBundle> marketingBundlesList = new ArrayList<>();
    List<MarketingBundleFeature> marketingBundleFeatureList = new ArrayList<>();
    marketingBundleFeatureList.add(marketingBundleFeature);
    MarketingBundle marketingBundle1 = new MarketingBundle();
    marketingBundle1.setCode("searchEngineOptimization");
    marketingBundle1.setChildBundleFeatures(marketingBundleFeatureList);
    marketingBundlesList.add(marketingBundle1);
    when(provisioningContext.getNewBundleConfig()).thenReturn(bundleConfig);
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    when(subscriptionBundles.getBundles()).thenReturn(marketingBundlesList);
    when(bundleMetadataDAO.getMarketingBundleByCode("rca")).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, marketingBundle.getId())).thenReturn(subscriptionBundle);
    when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatureList);
    when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
    ProvisioningResponse provisioningResponse = provisioningService.modifySubscriptionConfig(MUNCHKIN_ID, configParams);
    Assert.assertNotNull(provisioningResponse);
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Missing subscription tenantUUID")
  public void testModifySubscriptionConfigWithEmptyTenantUUID() {
    provisioningService.modifySubscriptionConfig(null,configParams);
  }

  @Test(expectedExceptions = ProvisioningException.class)
  public void TestModifySubscriptionConfigWithNoProvisionedModule() {
    JSONObject bundleConfig = new JSONObject("{\"rca\":{}}}");
    when(provisioningContext.getNewBundleConfig()).thenReturn(bundleConfig);
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    provisioningService.modifySubscriptionConfig(MUNCHKIN_ID, configParams);
  }

  @Test(expectedExceptions = ProvisioningException.class)
  public void TestModifySubscriptionConfigWithModuleProvisioningFailure() {
    JSONObject bundleConfig = new JSONObject("{\"rca\":{}}}");
    List<MarketingBundle> marketingBundlesList = new ArrayList<>();
    MarketingBundle marketingBundle1 = new MarketingBundle();
    marketingBundlesList.add(marketingBundle1);
    when(provisioningContext.getNewBundleConfig()).thenReturn(bundleConfig);
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    when(subscriptionBundles.getBundles()).thenReturn(marketingBundlesList);
    when(bundleMetadataDAO.getMarketingBundleByCode("rca")).thenReturn(null);
    provisioningService.modifySubscriptionConfig(MUNCHKIN_ID, configParams);
  }

  @Test()
  public void TestModifySubscriptionConfig() {
    JSONObject bundleConfig = new JSONObject("{\"rca\":{}}}");
    List<MarketingBundle> marketingBundlesList = new ArrayList<>();
    List<MarketingBundleFeature> marketingBundleFeatureList = new ArrayList<>();
    marketingBundleFeatureList.add(marketingBundleFeature);
    MarketingBundle marketingBundle1 = new MarketingBundle();
    marketingBundle1.setCode("rca");
    marketingBundle1.setChildBundleFeatures(marketingBundleFeatureList);
    marketingBundlesList.add(marketingBundle1);
    when(provisioningContext.getNewBundleConfig()).thenReturn(bundleConfig);
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    when(subscriptionBundles.getBundles()).thenReturn(marketingBundlesList);
    when(bundleMetadataDAO.getMarketingBundleByCode("rca")).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, marketingBundle.getId())).thenReturn(subscriptionBundle);
    when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatureList);
    when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
    when(marketingFeature.getMarketingFeatureConfig()).thenReturn(configParams.getFirst(BUNDLE));

    ProvisioningResponse provisioningResponse = provisioningService.modifySubscriptionConfig(MUNCHKIN_ID, configParams);
    Assert.assertNotNull(provisioningResponse);
  }

}
