package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.rest.model.BundleDiffInfo;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundleFeatures;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.bundlemetadata.service.MarketingFeatureConfigurator;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.model.BundleFeaturesRequest;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;
import org.apache.commons.lang3.RandomStringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class ProvisioningServiceUpdateTest {

  @Mock
  private ProvisioningContext provisioningContext;

  @Mock
  private BundleMetadataService bundleMetadataService;

  @Mock
  private MarketingBundle marketingBundle;

  @Mock
  private FeatureProvisioningService featureProvisioningService;

  @Mock
  private MarketingFeature marketingFeature;

  @Mock
  private SubscriptionMetadataDAO subscriptionMetadataDAO;

  @Mock
  private SubscriptionMetadataService subscriptionMetadataService;

  @Mock
  private SubscriptionBundleMap subscriptionBundle;

  @Mock
  private SubscriptionBundleFeatures subscriptionBundleFeatures;

  @Mock
  private BundleDiffInfo bundleDiffInfo;

  @Mock
  private MarketingFeatureConfigurator featureConfigurator;

  @InjectMocks
  private ProvisioningService provisioningService;

  private static final String MUNCHKIN_ID = "123-LME-456";

  @BeforeClass
  public void setUp() {
    MockitoAnnotations.initMocks(this);

  }

  @Test
  public void testUpdateBundle() {

    List<MarketingFeature> marketingFeatureList = new ArrayList<>();
    List<MarketingFeature> marketingCommonFeatureList = new ArrayList<>();
    marketingCommonFeatureList.add(marketingFeature);
    when(subscriptionMetadataService.getBundlesFeatures(MUNCHKIN_ID)).thenReturn(subscriptionBundleFeatures);
    when(bundleMetadataService.getMarketingBundle(1)).thenReturn(marketingBundle);
    when(bundleDiffInfo.getCommonFeatures()).thenReturn(marketingFeatureList);
    when(subscriptionBundleFeatures.getBundle()).thenReturn(marketingBundle);
    when(bundleMetadataService.compareBundles(marketingBundle, marketingBundle)).thenReturn(bundleDiffInfo);
    when(bundleDiffInfo.getCommonFeatures()).thenReturn(marketingCommonFeatureList);

    try{
      provisioningService.updateBundle(MUNCHKIN_ID, 1);
    }
    catch (Exception e){
      Assert.fail("Exception occured: "+ e);
    }

  }

  @Test (expectedExceptions = ProvisioningInputException.class)
  public void testUpdateBundleWithInvalidBundleId() {
    int invalidBundleId = Integer.parseInt(RandomStringUtils.randomNumeric(4));
    when(subscriptionMetadataService.getBundlesFeatures(MUNCHKIN_ID)).thenReturn(subscriptionBundleFeatures);
    when(bundleMetadataService.getMarketingBundle(invalidBundleId)).thenReturn(null);
    provisioningService.updateBundle(MUNCHKIN_ID, invalidBundleId);
  }

  @Test
  public void testUpdateBundleRubiks() {
    List<MarketingFeature> marketingFeatureList = new ArrayList<>();
    marketingFeatureList.add(marketingFeature);
    BundleFeaturesRequest bundleFeaturesRequest = new BundleFeaturesRequest();
    bundleFeaturesRequest.setFeatures(marketingFeatureList);
    when(bundleMetadataService.getMarketingBundle(1)).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, 1)).thenReturn(subscriptionBundle);
    when(subscriptionMetadataService.getBundlesFeatures(MUNCHKIN_ID)).thenReturn(subscriptionBundleFeatures);
    when(bundleMetadataService.getMarketingFeature(0)).thenReturn(marketingFeature);
    when(featureConfigurator.getDefaultFeatureConfig(MUNCHKIN_ID, marketingFeature)).thenReturn("{\"featureConfig\":{}}");
    when(marketingFeature.getMarketingFeatureConfig()).thenReturn("");
    try{
      provisioningService.updateBundle(MUNCHKIN_ID, 1, bundleFeaturesRequest);
    }
    catch (Exception e){
      Assert.fail("Exception occured: "+ e);
    }
  }

  @Test (expectedExceptions = ProvisioningInputException.class)
  public void testUpdateBundleRubiksForInvalidBundleId() {
    int invalidBundleId = Integer.parseInt(RandomStringUtils.randomNumeric(4));
    BundleFeaturesRequest bundleFeaturesRequest = new BundleFeaturesRequest();
    when(bundleMetadataService.getMarketingBundle(invalidBundleId)).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, invalidBundleId)).thenReturn(null);
    when(subscriptionMetadataService.getBundlesFeatures(MUNCHKIN_ID)).thenReturn(subscriptionBundleFeatures);

    provisioningService.updateBundle(MUNCHKIN_ID, invalidBundleId, bundleFeaturesRequest);
  }

  @Test (expectedExceptions = ProvisioningInputException.class)
  public void testUpdateBundleRubiksForSubNotProvisioned() {
    BundleFeaturesRequest bundleFeaturesRequest = new BundleFeaturesRequest();
    when(bundleMetadataService.getMarketingBundle(1)).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, 1)).thenReturn(subscriptionBundle);
    when(subscriptionMetadataService.getBundlesFeatures(MUNCHKIN_ID)).thenReturn(null);

    provisioningService.updateBundle(MUNCHKIN_ID, 1, bundleFeaturesRequest);
  }

  @Test
  public void testUpdateFeature() {
    String data = "{\"status\": \"\\tactive\\t\",\"\\tbacklogId\\t\": \"id\"}";
    when( bundleMetadataService.getMarketingFeature(1)).thenReturn(marketingFeature);
    try {
      provisioningService.updateFeature(MUNCHKIN_ID, 1, data);
    }
    catch (Exception e){
      Assert.fail("Exception occured: "+ e);
    }
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Invalid TenantUUID Recieved")
  public void updateFeatureWithProvisioningInputException() {
    when( bundleMetadataService.getMarketingFeature(1)).thenReturn(marketingFeature);
    provisioningService.updateFeature("", 1 , null);
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Feature: 1 not found in the Bundle Metadata")
  public void updateFeatureWithProvisioningInputEx() {
    when( bundleMetadataService.getMarketingFeature(1)).thenReturn(null);
    provisioningService.updateFeature(MUNCHKIN_ID, 1 , null );
  }

  @Test
  public void testUpdateBundle2() {

    List<MarketingFeature> marketingFeatureList = new ArrayList<>();
    List<MarketingFeature> marketingCommonFeatureList = new ArrayList<>();
    marketingCommonFeatureList.add(marketingFeature);
    when(subscriptionMetadataService.getBundlesFeatures(MUNCHKIN_ID)).thenReturn(subscriptionBundleFeatures);
    when(bundleMetadataService.getMarketingBundle(1)).thenReturn(marketingBundle);
    when(bundleDiffInfo.getCommonFeatures()).thenReturn(marketingFeatureList);
    when(subscriptionBundleFeatures.getBundle()).thenReturn(marketingBundle);
    when(bundleMetadataService.compareBundles(marketingBundle, marketingBundle)).thenReturn(bundleDiffInfo);
    when(bundleDiffInfo.getCommonFeatures()).thenReturn(marketingCommonFeatureList);
    try{
      provisioningService.updateBundle(MUNCHKIN_ID, 1);
    }
    catch (Exception e){
      Assert.fail("Exception occured: "+ e);
    }

  }
}
