package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.dao.BundleMetadataDAO;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundles;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.ProvisioningResponse;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;
import org.apache.commons.lang3.RandomStringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class ProvisioningServiceDowngradeMockTest {

  @Mock
  private ProvisioningContext provisioningContext;

  @Mock
  private BundleMetadataService bundleMetadataService;

  @Mock
  private MarketingBundle marketingBundle;

  @Mock
  private SubscriptionMetadataDAO subscriptionMetadataDAO;

  @Mock
  private SubscriptionMetadataService subscriptionMetadataService;

  @Mock
  private BundleMetadataDAO bundleMetadataDAO;

  @Mock
  private SubscriptionBundleMap subscriptionBundle;

  @Mock
  private SubscriptionBundles subscriptionBundles;

  @InjectMocks
  private ProvisioningService provisioningService;

  private static List<String> multiValuedMapToken;

  private List<String> multiValuedMapMlm;

  private List<String> multiValuedMapBundle;

  private static final String TOKEN = "token";

  private static final String MLM = "mlm";

  private static final String BUNDLE = "bundle";

  private static final String BASE_MODULE = "baseModule";

  private static final String MUNCHKIN_ID = "123-LME-456";

  private MultiValueMap<String, String> configParams;

  //static content to be loaded first
  {
    multiValuedMapToken = new ArrayList<>();
    multiValuedMapMlm  = new ArrayList<>();
    multiValuedMapBundle = new ArrayList<>();

    multiValuedMapToken.add(TOKEN);
    multiValuedMapMlm.add(MLM);
    multiValuedMapBundle.add(BASE_MODULE);
  }

  @BeforeClass
  public void setUp() {
    MockitoAnnotations.initMocks(this);

  }

  @BeforeMethod
  public void init() {
    configParams = new LinkedMultiValueMap<>();
    configParams.put(TOKEN, multiValuedMapToken);
    configParams.put(MLM, multiValuedMapMlm);
    configParams.put(BUNDLE, multiValuedMapBundle);
  }


  @Test(expectedExceptions = ProvisioningException.class)
  public void TestDowngradeSubscriptionBaseModule() {
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    when(bundleMetadataDAO.getMarketingBundleByCode("baseModule")).thenReturn(marketingBundle);
    provisioningService.downgradeSubscription(MUNCHKIN_ID, configParams);
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Missing subscription tenantUUID")
  public void TestDowngradeSubscriptionWithEmptyTenantUUID() {
    provisioningService.downgradeSubscription(null, configParams);
  }

  @Test(expectedExceptions = ProvisioningException.class)
  public void TestDowngradeSubscriptionWithInvalidModule() {
    configParams.remove(BUNDLE);
    configParams.add(BUNDLE, RandomStringUtils.randomAlphabetic(4));
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    provisioningService.downgradeSubscription(MUNCHKIN_ID, configParams);
  }


  @Test()
  public void TestDowngradeSubscription() {
    configParams.remove(BUNDLE);
    configParams.add(BUNDLE, "rca");
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    when(bundleMetadataDAO.getMarketingBundleByCode("rca")).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, marketingBundle.getId())).thenReturn(subscriptionBundle);
    when(bundleMetadataService.getMarketingBundle(0)).thenReturn(marketingBundle);

    ProvisioningResponse provisioningResponse = provisioningService.downgradeSubscription(MUNCHKIN_ID, configParams);
    Assert.assertNotNull(provisioningResponse);
  }
}
