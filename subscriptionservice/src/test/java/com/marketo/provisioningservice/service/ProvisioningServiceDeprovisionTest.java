package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;
import org.apache.commons.lang3.RandomStringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

public class ProvisioningServiceDeprovisionTest {

  @Mock
  private ProvisioningContext provisioningContext;

  @Mock
  private BundleMetadataService bundleMetadataService;

  @Mock
  private MarketingBundle marketingBundle;

  @Mock
  private MarketingBundleFeature marketingBundleFeature;

  @Mock
  private FeatureProvisioningService featureProvisioningService;

  @Mock
  private MarketingFeature marketingFeature;

  @Mock
  private SubscriptionMetadataDAO subscriptionMetadataDAO;

  @Mock
  private SubscriptionMetadataService subscriptionMetadataService;

  @Mock
  private SubscriptionBundleMap subscriptionBundle;

  @InjectMocks
  private ProvisioningService provisioningService;

  private static final String MUNCHKIN_ID = "123-LME-456";

  @BeforeClass
  public void setUp() {
    MockitoAnnotations.initMocks(this);

  }

  @Test
  public void testDeprovisionFeature() {
    when(subscriptionMetadataService.getAddonFeatureInfoForSubsription(MUNCHKIN_ID, 1)).thenReturn(marketingFeature);
    try {
      provisioningService.deprovisionFeature(MUNCHKIN_ID, 1);
    }
    catch (Exception e){
      Assert.fail("Exception occured: "+ e);
    }
  }

  @Test(expectedExceptions = ProvisioningInputException.class)
  public void testDeprovisionFeatureWithInvalidFeatureId() {
    provisioningService.deprovisionFeature(MUNCHKIN_ID, Integer.parseInt(RandomStringUtils.randomNumeric(8)));
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Invalid TenantUUID Recieved")
  public void testDeprovisionFeatureWithEmptyTenantUUID() {
    when(subscriptionMetadataService.getAddonFeatureInfoForSubsription(null, 1)).thenReturn(marketingFeature);
    provisioningService.deprovisionFeature(null, 1);
  }

  @Test
  public void testDeprovisionBundle() {
    List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();
    Map<Integer, Integer> featureUsedByCount= new HashMap<>();
    featureUsedByCount.put(1,1);
    marketingBundleFeatures.add(marketingBundleFeature);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, 1)).thenReturn(subscriptionBundle);
    when(bundleMetadataService.getMarketingBundle(1)).thenReturn(marketingBundle);
    when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
    when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
    when(marketingFeature.getId()).thenReturn(1);
    when(subscriptionMetadataDAO.getFeatureInfoForSubscription(MUNCHKIN_ID, 1)).thenReturn(marketingFeature);
    when(subscriptionMetadataService.getFeatureUsedByCount(MUNCHKIN_ID)).thenReturn(featureUsedByCount);
    try {
      provisioningService.deprovisionBundle(MUNCHKIN_ID, 1);
    }
    catch (Exception e){
      Assert.fail("Exception occured: "+ e);
    }
  }

  @Test(expectedExceptions = ProvisioningInputException.class)
  public void testDeprovisionBundleWithInvalidBundleId() {
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, 1)).thenReturn(null);
    provisioningService.deprovisionBundle(MUNCHKIN_ID, 1);
  }


  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Invalid TenantUUID Received")
  public void testDeprovisionBundleWithEmptyTenantUUID() {
    provisioningService.deprovisionBundle(null, 1);
  }

}
