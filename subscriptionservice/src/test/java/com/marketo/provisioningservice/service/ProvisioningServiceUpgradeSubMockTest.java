package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.dao.BundleMetadataDAO;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundles;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.rest.ProvisioningResponse;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class ProvisioningServiceUpgradeSubMockTest {

  @Mock
  private ProvisioningContext provisioningContext;

  @Mock
  private MarketingBundle marketingBundle;

  @Mock
  private SubscriptionMetadataDAO subscriptionMetadataDAO;

  @Mock
  private SubscriptionMetadataService subscriptionMetadataService;

  @Mock
  private BundleProvisioningService bundleProvisioningService;

  @Mock
  private BundleMetadataDAO bundleMetadataDAO;

  @Mock
  private SubscriptionBundleMap subscriptionBundle;

  @Mock
  private SubscriptionBundles subscriptionBundles;

  @InjectMocks
  private ProvisioningService provisioningService;

  private static List<String> multiValuedMapToken;

  private List<String> multiValuedMapMlm;

  private List<String> multiValuedMapBundle;

  private static final String TOKEN = "token";

  private static final String MLM = "mlm";

  private static final String BUNDLE = "bundle";

  private static final String BASE_MODULE = "baseModule";

  private static final String MUNCHKIN_ID = "123-LME-456";

  private MultiValueMap<String, String> configParams;

  //static content to be loaded first
  {
    multiValuedMapToken = new ArrayList<>();
    multiValuedMapMlm  = new ArrayList<>();
    multiValuedMapBundle = new ArrayList<>();

    multiValuedMapToken.add(TOKEN);
    multiValuedMapMlm.add(MLM);
    multiValuedMapBundle.add(BASE_MODULE);
  }

  @BeforeClass
  public void setUp() {
    MockitoAnnotations.initMocks(this);

  }

  @BeforeMethod
  public void init() {
    configParams = new LinkedMultiValueMap<>();
    configParams.put(TOKEN, multiValuedMapToken);
    configParams.put(MLM, multiValuedMapMlm);
    configParams.put(BUNDLE, multiValuedMapBundle);
  }


  @Test
  public void TestUpgradeSubscription() {
    JSONObject bundleConfig = new JSONObject("{\"rca\":{}}}");
    List<MarketingBundle> marketingBundlesList = new ArrayList<>();
    MarketingBundle marketingBundle1 = new MarketingBundle();
    marketingBundlesList.add(marketingBundle1);
    when(provisioningContext.getNewBundleConfig()).thenReturn(bundleConfig);
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    when(subscriptionBundles.getBundles()).thenReturn(marketingBundlesList);
    when(bundleMetadataDAO.getMarketingBundleByCode("rca")).thenReturn(marketingBundle);
    try {
      provisioningService.upgradeSubscription(MUNCHKIN_ID, configParams);
    }
    catch (Exception e) {
      Assert.fail("Exception occured: "+ e);
    }
  }

  @Test(expectedExceptions = ProvisioningException.class)
  public void TestUpgradeSubscriptionWithNoProvisionedModule() {
    JSONObject bundleConfig = new JSONObject("{\"rca\":{}}}");
    when(provisioningContext.getNewBundleConfig()).thenReturn(bundleConfig);
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    provisioningService.upgradeSubscription(MUNCHKIN_ID, configParams);
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Base Module cannot be present for addition to existing modules")
  public void TestUpgradeSubscriptionWithConfigHavingBaseModule() {
    JSONObject bundleConfig = new JSONObject("{\"baseModule\":{}}}");
    when(provisioningContext.getNewBundleConfig()).thenReturn(bundleConfig);
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    provisioningService.upgradeSubscription(MUNCHKIN_ID, configParams);
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Missing subscription tenantUUID")
  public void TestUpgradeSubscriptionWithEmptyTenantUUID() {
    provisioningService.upgradeSubscription(null, configParams);
  }

  @Test(expectedExceptions = ProvisioningException.class)
  public void TestUpgradeSubscriptionWithModuleProvisioningFailure() {
    JSONObject bundleConfig = new JSONObject("{\"rca\":{}}}");
    List<MarketingBundle> marketingBundlesList = new ArrayList<>();
    MarketingBundle marketingBundle1 = new MarketingBundle();
    marketingBundlesList.add(marketingBundle1);
    when(provisioningContext.getNewBundleConfig()).thenReturn(bundleConfig);
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    when(subscriptionBundles.getBundles()).thenReturn(marketingBundlesList);
    when(bundleMetadataDAO.getMarketingBundleByCode("rca")).thenReturn(null);
    provisioningService.upgradeSubscription(MUNCHKIN_ID, configParams);
  }

  @Test()
  public void TestUpgradeSubscriptionForModuleAlreadyProvisioned() {
    JSONObject bundleConfig = new JSONObject("{\"rca\":{}}}");
    List<MarketingBundle> marketingBundlesList = new ArrayList<>();
    MarketingBundle marketingBundle1 = new MarketingBundle();
    marketingBundlesList.add(marketingBundle1);
    when(provisioningContext.getNewBundleConfig()).thenReturn(bundleConfig);
    when(subscriptionMetadataService.getProvisionedModules(MUNCHKIN_ID)).thenReturn(subscriptionBundles);
    when(subscriptionBundles.getBundles()).thenReturn(marketingBundlesList);
    when(bundleMetadataDAO.getMarketingBundleByCode("rca")).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, marketingBundle.getId())).thenReturn(subscriptionBundle);
    ProvisioningResponse provisioningResponse = provisioningService.upgradeSubscription(MUNCHKIN_ID, configParams);
    Assert.assertNotNull(provisioningResponse);
  }
}
