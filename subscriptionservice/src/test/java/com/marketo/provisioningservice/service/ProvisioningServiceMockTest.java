package com.marketo.provisioningservice.service;

import com.marketo.bundlemetadata.dao.BundleMetadataDAO;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.provisioningservice.features.MLMProvisioningHandler;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import com.marketo.subscriptionmetadata.service.SubscriptionMetadataService;
import com.marketo.subscriptionservice.model.BundleFeaturesRequest;
import com.marketo.subscriptionservice.rest.ProvisioningResponse;
import com.marketo.subscriptionservice.rest.exception.ProvisioningException;
import com.marketo.subscriptionservice.rest.exception.ProvisioningInputException;

import org.apache.commons.lang3.RandomStringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;

public class ProvisioningServiceMockTest {

  @Mock
  private ProvisioningContext provisioningContext;

  @Mock
  private BundleMetadataService bundleMetadataService;

  @Mock
  private MarketingBundle marketingBundle;

  @Mock
  private MarketingBundleFeature marketingBundleFeature;

  @Mock
  private FeatureProvisioningService featureProvisioningService;

  @Mock
  private MarketingFeature marketingFeature;

  @Mock
  private SubscriptionMetadataDAO subscriptionMetadataDAO;

  @Mock
  private MLMProvisioningHandler mlmFeatureHandler;

  @Mock
  private SubscriptionMetadataService subscriptionMetadataService;

  @Mock
  private BundleProvisioningService bundleProvisioningService;

  @Mock
  private BundleMetadataDAO bundleMetadataDAO;

  @Mock
  private SubscriptionBundleMap subscriptionBundle;

  @InjectMocks
  private ProvisioningService provisioningService;

  private static List<String> multiValuedMapToken;

  private List<String> multiValuedMapMlm;

  private List<String> multiValuedMapBundle;

  private static final String TOKEN = "token";

  private static final String MLM = "mlm";

  private static final String BUNDLE = "bundle";

  private static final String BASE_MODULE = "baseModule";

  private static final String MUNCHKIN_ID = "123-LME-456";

  private  MultiValueMap<String, String> configParams;

  //static content to be loaded first
  {
    multiValuedMapToken = new ArrayList<>();
    multiValuedMapMlm  = new ArrayList<>();
    multiValuedMapBundle = new ArrayList<>();

    multiValuedMapToken.add(TOKEN);
    multiValuedMapMlm.add(MLM);
    multiValuedMapBundle.add(BASE_MODULE);
  }

  @BeforeClass
  public void setUp() {
    MockitoAnnotations.initMocks(this);

  }

  @BeforeMethod
  public void init() {
    configParams = new LinkedMultiValueMap<>();
    configParams.put(TOKEN, multiValuedMapToken);
    configParams.put(MLM, multiValuedMapMlm);
    configParams.put(BUNDLE, multiValuedMapBundle);
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Missing Token")
  public void testProvisioningServiceException() {
    List<String> multiValuedMapWithEmpty = new ArrayList<>();
    multiValuedMapWithEmpty.add("");

    configParams.put(TOKEN, multiValuedMapWithEmpty);
    provisioningService.provisionSubscription(configParams);
  }


  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Bundle name not found in the parameters")
  public void testProvisioningServiceExcept() {
    configParams.remove(BUNDLE);
    when(provisioningContext.getParams()).thenReturn(configParams);

    provisioningService.provisionSubscription(configParams);

  }

  @Test
  public void testProvisioningService() {
    configParams.remove(BUNDLE);
    configParams.add(BUNDLE, BUNDLE);
    List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();
    marketingBundleFeatures.add(marketingBundleFeature);

    when(provisioningContext.getParams()).thenReturn(configParams);
    when(bundleMetadataService.getMarketingBundle(BUNDLE)).thenReturn(marketingBundle);
    when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
    when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
    when(marketingBundleFeature.getMarketingFeatureConfig()).thenReturn("Base-Config");
    when(provisioningContext.getTenantId()).thenReturn(MUNCHKIN_ID);

    ProvisioningResponse provisioningResponse = provisioningService.provisionSubscription(configParams);
    Assert.assertNotNull(provisioningResponse);
  }

  @Test
  public void testIsBundleRequirePodSelection() {
    boolean resultVal =  provisioningService.isBundleRequirePodSelection("marketingPerformanceInsights");
    Assert.assertTrue(resultVal);
  }

  @Test
  public void testIsBundleRequirePodSelectionFalse() {
    boolean resultVal =  provisioningService.isBundleRequirePodSelection(MLM);
    Assert.assertFalse(resultVal);
  }

  @Test
  public void testProvisionBundle() {
    when(bundleMetadataService.getMarketingBundle(MLM)).thenReturn(marketingBundle);
    when(provisioningContext.getTenantId()).thenReturn(MUNCHKIN_ID);
    try {
      provisioningService.provisionBundle(MUNCHKIN_ID, MLM, TOKEN);
    }
    catch (Exception e){
      Assert.fail("Exception occured: "+ e);
    }
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Bundle: rca not found in the bundle metadata")
  public void testProvisionBundleException() {
    when(bundleMetadataService.getMarketingBundle(MLM)).thenReturn(null);
    when(provisioningContext.getTenantId()).thenReturn(MUNCHKIN_ID);
    provisioningService.provisionBundle(MUNCHKIN_ID, "rca", TOKEN);

    verify( subscriptionMetadataService, times(1)).addMarketingBundle(provisioningContext.getTenantId()
      , marketingBundle);
  }


  @Test
  public void testNewProvisioningService() {
    List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();
    configParams.add("sandboxCopyEnable","1" );
    configParams.add("mlm", "true");

    marketingBundleFeatures.add(marketingBundleFeature);

    when(provisioningContext.getParams()).thenReturn(configParams);
    when(bundleMetadataService.getMarketingBundle(BUNDLE)).thenReturn(marketingBundle);
    when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
    when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
    when(marketingBundleFeature.getMarketingFeatureConfig()).thenReturn("{\"Base-Config\":{}}}");
    when(provisioningContext.getTenantId()).thenReturn(MUNCHKIN_ID);
    when(bundleMetadataService.getMarketingBundleByCode("baseModule")).thenReturn(marketingBundle);

    ProvisioningResponse provisioningResponse = provisioningService.newProvisionSubscription(configParams);
    Assert.assertNotNull(provisioningResponse);
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Missing Token")
  public void testNewProvisioningServiceWithEmptyToken() {
    configParams.remove("token");
    configParams.add("token","" );

    provisioningService.newProvisionSubscription(configParams);
  }

  @Test
  public void testDoPostSubscriptionCreated() {
    List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();

    marketingBundleFeatures.add(marketingBundleFeature);
    configParams.add("mlm", "true");
    configParams.add("activePod", "mlmpod");
    when(provisioningContext.getParams()).thenReturn(configParams);
    when(bundleMetadataDAO.getMarketingBundleByCode("baseModule")).thenReturn(marketingBundle);
    when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
    when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
    when(marketingFeature.getName()).thenReturn("Base");
    ProvisioningResponse provisioningResponse = provisioningService.doPostSubscriptionCreated(MUNCHKIN_ID, configParams);
    Assert.assertNotNull(provisioningResponse);
  }

  @Test
  public void testDoPostSubscriptionCreatedWithEmptyBundle() {
    List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();

    marketingBundleFeatures.add(marketingBundleFeature);
    configParams.add("mlm", "true");
    configParams.add("activePod", "mlmpod");
    when(provisioningContext.getParams()).thenReturn(configParams);
    when(bundleMetadataDAO.getMarketingBundleByCode("baseModule")).thenReturn( null);
    ProvisioningResponse provisioningResponse = provisioningService.doPostSubscriptionCreated(MUNCHKIN_ID, configParams);
    Assert.assertNotNull(provisioningResponse);
  }

  @Test
  public void testDoPostSubscriptionCreatedWithNonBaseFeature() {
    List<MarketingBundleFeature> marketingBundleFeatures = new ArrayList<>();

    marketingBundleFeatures.add(marketingBundleFeature);
    configParams.add("mlm", "true");
    configParams.add("activePod", "mlmpod");
    when(provisioningContext.getParams()).thenReturn(configParams);
    when(bundleMetadataDAO.getMarketingBundleByCode("baseModule")).thenReturn(marketingBundle);
    when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
    when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
    when(marketingFeature.getName()).thenReturn("rca");
    ProvisioningResponse provisioningResponse = provisioningService.doPostSubscriptionCreated(MUNCHKIN_ID, configParams);
    Assert.assertNotNull(provisioningResponse);
  }

  @Test
  public void testProvisionBundleRubiks() {
    int bundleId = 1;
    when(bundleMetadataService.getMarketingBundle(bundleId)).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, bundleId)).thenReturn(null);
    BundleFeaturesRequest bundleFeaturesRequest = new BundleFeaturesRequest();
    try {
      provisioningService.provisionBundle(MUNCHKIN_ID, bundleId, bundleFeaturesRequest);
    }
    catch (Exception e){
      Assert.fail("Exception occured: "+ e);
    }
  }

  @Test(expectedExceptions = ProvisioningInputException.class)
  public void testProvisionBundleRubiksAlreadyProvisioned() {
    int bundleId = 1;
    when(bundleMetadataService.getMarketingBundle(bundleId)).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, bundleId)).thenReturn(subscriptionBundle);
    BundleFeaturesRequest bundleFeaturesRequest = new BundleFeaturesRequest();
    provisioningService.provisionBundle(MUNCHKIN_ID, bundleId, bundleFeaturesRequest);
  }

  @Test(expectedExceptions = ProvisioningException.class)
  public void testProvisionBundleRubiksWithFailure() {
    int bundleId = 1;
    List<Exception> errors = new ArrayList<>();
    errors.add(new ProvisioningInputException("Bundle not found"));
    when(bundleMetadataService.getMarketingBundle(bundleId)).thenReturn(marketingBundle);
    when(subscriptionMetadataDAO.getSubscriptionBundleEntry(MUNCHKIN_ID, bundleId)).thenReturn(subscriptionBundle);
    when(provisioningContext.getErrors()).thenReturn(errors);
    BundleFeaturesRequest bundleFeaturesRequest = new BundleFeaturesRequest();
    provisioningService.provisionBundle(null, bundleId, bundleFeaturesRequest);
  }

  @Test
  public void testProvisionFeature() {
    when( bundleMetadataService.getMarketingFeature(1)).thenReturn(marketingFeature);
    try {
      provisioningService.provisionFeature(MUNCHKIN_ID, 1, "Base");
    }
    catch (Exception e){
      Assert.fail("Exception occured: "+ e);
    }
  }

  @Test(expectedExceptions = ProvisioningInputException.class,
    expectedExceptionsMessageRegExp = "Invalid TenantUUID Recieved")
  public void testProvisionFeatureWithEmptyTenantUUID() {
    when( bundleMetadataService.getMarketingFeature(1)).thenReturn(marketingFeature);
    provisioningService.provisionFeature(null, 1, "Base");
  }

  @Test(expectedExceptions = ProvisioningInputException.class)
  public void testProvisionFeatureWithInvalidFeatureId() {
    provisioningService.provisionFeature(MUNCHKIN_ID, Integer.parseInt(RandomStringUtils.randomNumeric(8)), "Base");
  }
  
}

