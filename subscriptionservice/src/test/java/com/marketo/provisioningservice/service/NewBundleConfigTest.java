package com.marketo.provisioningservice.service;


import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import junit.framework.Assert;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.annotations.Test;

/**
 * Created by rmalani on 5/15/17.
 */
@ContextConfiguration(locations = {"classpath:subscription-test-config.xml"})
public class NewBundleConfigTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ProvisioningContext provisioningContext;

    @Autowired
    private BundleProvisioningService bundleProvisioningService;

    @Autowired
    private BundleMetadataService bundleMetadataService;


    /**
     * Test to parse the newBundle parameter in the request
     */
    //@Test
    public void testParseNewBundle() {

        // setting up the test parameters
        String newBundleString1 = "{'bundle':{'baseModule':{'subscriptionTypeCode':'base'," +
                "'dedicatedSendingIPs':0,'deliverabilityPartnerEnabled':false,'webServiceDailyQuota':50000," +
                "'deliverabilityPartnerUser':'','deliverabilityPartnerGroup':'','mkDenialEnabled':false," +
                "'mtaOptions':2,'nurtureProgramLimit':100,'nurtureTracksLimit':15,'maxReportsPerSub':100," +
                "'maxRecordsPerReport':100000,'maxCustomActivity':10,'maxCustomActivityFields':20}," +
                "'searchEngineOptimization':{'sku':'1000.000'}," +
                "'programAnalysis':{},'advancedReportBuilder':{},'lifeCycleModeler':{'rcmLimit':1}," +
                "'marketingCalendar':{'calendarSeats':5},'salesInsight':{'sisSeats':5,'lissSeats':5}," +
                "'emailPlugin':{'lissSeats':5},'adTargeting':{},'websiteRetargeting':{},'socialMarketing':{}," +
                "'eventsAndWebinars':{},'mobileEngagement':{},'workspacesAndPartitions':{}," +
                "'customObjects':{'maxCustomObjects':10,'maxCustomObjectFields':50," +
                "'maxCustomObjectData':2000000},'nativeSfdcMsdCrmConnector':{},'webPersonalization':{}," +
                "'accountBasedMarketingCore':{'paidSeats':10,'accountsLimit':1000},'predictiveContent':{}," +
                "'accountBasedMarketing':{}}}";
        String newBundleString2 = "{\"baseModule\": null,\"searchEngineOptimization\": {\"sku\": \"1000.000\"}}";
        MultiValueMap<String, String> testConfigParams = new LinkedMultiValueMap<>();
        testConfigParams.add("newBundle", newBundleString2);
        provisioningContext.setParams(testConfigParams);
        // running the test function
        JSONObject bundles = provisioningContext.getNewBundleConfig();

        // verifying the results
        Assert.assertEquals(((JSONObject)bundles.get("baseModule")).get("subscriptionTypeCode"), "base");
        Assert.assertEquals(((JSONObject)bundles.get("customObjects")).get("maxCustomObjects"), 10);
        Assert.assertEquals((((JSONObject)bundles.get("searchEngineOptimization")).get("sku")), "SEO 1000");
    }

    /**
     * Test to check if the feature default configuration gets overridden correctly
     * by the configuration from newBundle
     */
    //@Test
    public void testOverrideDefaultFeatureConfig() {
        // setting up the test parameters
        MarketingBundle bundle = bundleMetadataService.getMarketingBundleByCode("baseModule");
        MarketingBundleFeature bundleFeature = bundle.getChildBundleFeatures().get(0);
        MarketingFeature feature = bundleFeature.getFeature();
        String featureConfig = bundleFeature.getMarketingFeatureConfig();
        feature.setMarketingFeatureConfig(featureConfig);

        String newBundleString = "{'baseModule':{'subscriptionTypeCode':'base'," +
                "'dedicatedSendingIPs':7,'deliverabilityPartnerEnabled':false,'webServiceDailyQuota':50000," +
                "'deliverabilityPartnerUser':'','deliverabilityPartnerGroup':'','mkDenialEnabled':false," +
                "'mtaOptions':2,'nurtureProgramLimit':100,'nurtureTracksLimit':15,'maxReportsPerSub':100," +
                "'maxRecordsPerReport':100000,'maxCustomActivity':10,'maxCustomActivityFields':20}," +
                "'searchEngineOptimization':{'sku':'500.000'}," +
                "'programAnalysis':{},'advancedReportBuilder':{},'lifeCycleModeler':{'rcmLimit':1}," +
                "'marketingCalendar':{'calendarSeats':5},'salesInsight':{'sisSeats':5,'lissSeats':5}," +
                "'emailPlugin':{'lissSeats':5},'adTargeting':{},'websiteRetargeting':{},'socialMarketing':{}," +
                "'eventsAndWebinars':{},'mobileEngagement':{},'workspacesAndPartitions':{}," +
                "'customObjects':{'maxCustomObjects':10,'maxCustomObjectFields':50," +
                "'maxCustomObjectData':2000000},'nativeSfdcMsdCrmConnector':{},'webPersonalization':{}," +
                "'accountBasedMarketingCore':{'paidSeats':10,'accountsLimit':1000},'predictiveContent':{}," +
                "'accountBasedMarketing':{}}";
        MultiValueMap<String, String> testConfigParams = new LinkedMultiValueMap<>();
        testConfigParams.add("newBundle", newBundleString);
        provisioningContext.setParams(testConfigParams);
        JSONObject bundles = provisioningContext.getNewBundleConfig();

        // running the test
        bundleProvisioningService.overrideDefaultFeatureConfig(feature, bundle, provisioningContext);

        //verifying the result
        Assert.assertEquals(new JSONObject(feature.getMarketingFeatureConfig()).get("dedicatedSendingIPs"), 7);

    }

    /**
     * Test to check the spa to provision utility map
     */
    //@Test
    public void testSpaToProvisioningMapUtility() {

        // setting up the test parameters
        String newBundleString2 ="{'baseModule': null,'searchEngineOptimization': {'sku': '1500.000'}}";
        MultiValueMap<String, String> testConfigParams = new LinkedMultiValueMap<>();
        testConfigParams.add("newBundle", newBundleString2);
        provisioningContext.setParams(testConfigParams);
        // running the test function
        JSONObject bundles = provisioningContext.getNewBundleConfig();
        // verifying the results
        Assert.assertEquals(((JSONObject)bundles.get("searchEngineOptimization")).get("sku"), "SEO 1000 PLUS");
    }

    /**
     * Test for blank newBundle
     */
    //@Test
    public void testBlankNewBundle() {

        String newBundleString = "  ";
        MultiValueMap<String, String> testConfigParams = new LinkedMultiValueMap<>();
        testConfigParams.add("newBundle", newBundleString);
        provisioningContext.setParams(testConfigParams);
        JSONObject bundles = provisioningContext.getNewBundleConfig();
        Assert.assertEquals(bundles.toString(), "{}");
    }

    /**
     * Test if no newBundle is present in request
     */
    //@Test
    public void testNoNewBundle() {

        MultiValueMap<String, String> testConfigParams = new LinkedMultiValueMap<>();
        provisioningContext.setParams(testConfigParams);
        JSONObject bundles = provisioningContext.getNewBundleConfig();
        Assert.assertEquals(bundles.toString(), "{}");
    }

}

