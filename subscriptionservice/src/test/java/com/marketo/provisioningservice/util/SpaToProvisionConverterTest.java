package com.marketo.provisioningservice.util;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.mockito.Mockito.*;

public class SpaToProvisionConverterTest {

  @Spy
  JSONObject newBundleConfig;

  @InjectMocks
  SpaToProvisionConverter spaToProvisionConverter = new SpaToProvisionConverter();

  @BeforeClass
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }


  @Test
  public void testConvertSpaToProvisionValues() {

    String spaName = "searchEngineOptimization";
    HashMap<String, String> spaValue = new HashMap<>();
    spaValue.put("sku", "1000.000");
    newBundleConfig.put(spaName, spaValue);
    when(newBundleConfig.get(spaName)).thenReturn(new JSONObject("{\"sku\": \"1000.000\"}"));
    spaToProvisionConverter.convertSpaToProvisionValues(newBundleConfig);
    verify(newBundleConfig, atLeastOnce()).get(spaName);
  }
}
