package com.marketo.updatetracker.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.marketo.updatetracker.model.PackageUpdateTracker;
import com.marketo.updatetracker.model.SubscriptionUpdateTracker;

@Repository
@Transactional
public class PackageUpdateTrackerDAO {

    @PersistenceContext
    private EntityManager entityManager;  
    
    /**
     * Get Last Run
     */
    
    public PackageUpdateTracker getLastRun() {
                
        Session session = entityManager.unwrap(Session.class);
        
        PackageUpdateTracker updateTracker = (PackageUpdateTracker)session.createCriteria(PackageUpdateTracker.class)
                .addOrder(Order.desc("startTime"))
                .setMaxResults(1)
                .uniqueResult();
        
        return updateTracker;   
    }

    /**
     * Get Last Completed Run
     * @return
     */
    public PackageUpdateTracker getLastCompletedRun() {
        
        Session session = entityManager.unwrap(Session.class);
        
        PackageUpdateTracker updateTracker = (PackageUpdateTracker)session.createCriteria(PackageUpdateTracker.class)
                .add(Restrictions.isNotNull("startTime"))
                .add(Restrictions.isNotNull("endTime"))
                .addOrder(Order.desc("startTime"))
                .setMaxResults(1)
                .uniqueResult();
        
        return updateTracker;   
    }
    
    /**
     * Add update tracker run.
     * @param tag
     * @return
     */
    public PackageUpdateTracker addUpdateRun(String tag) {
            
        PackageUpdateTracker updateTracker = new PackageUpdateTracker();
        updateTracker.setStatus(PackageUpdateTracker.STARTED);
        updateTracker.setTag(tag);
        
        PackageUpdateTracker savedTracker = entityManager.merge(updateTracker);
        return savedTracker;
    }
    
    /**
     * Set the state of tracker run.
     * @param trackerId
     * @param state
     */
    public void updateTrackerState(int trackerId, int state) {
        
        PackageUpdateTracker updateTrackerRun = entityManager.find(PackageUpdateTracker.class, trackerId);
        
        if (updateTrackerRun != null) {

            updateTrackerRun.setStatus(state);
            
            entityManager.merge(updateTrackerRun); 
        } else {
            throw new RuntimeException("Invalid Tracker Id " + trackerId);
        }
    }
    
    /**
     * Mark the tracker thread as finished
     * @param trackerId
     */
    public void updateSuccessRun(int trackerId) {
     
        PackageUpdateTracker updateTrackerRun = entityManager.find(PackageUpdateTracker.class, trackerId);
        
        if (updateTrackerRun != null) {

            updateTrackerRun.setStatus(PackageUpdateTracker.FINSHED);
            updateTrackerRun.setEndTime(new Date());
            
            entityManager.merge(updateTrackerRun); 
        } else {
            throw new RuntimeException("Invalid Tracker Id " + trackerId);
        }
    }
    
    /**
     * Mark the tracker thread as finished
     * @param trackerId
     */
    public void updateFailureRun(int trackerId) {
     
        PackageUpdateTracker updateTrackerRun = entityManager.find(PackageUpdateTracker.class, trackerId);
        
        if (updateTrackerRun != null) {

            updateTrackerRun.setStatus(PackageUpdateTracker.FAILED);
            updateTrackerRun.setEndTime(new Date());
            
            entityManager.merge(updateTrackerRun); 
        } else {
            throw new RuntimeException("Invalid Tracker Id " + trackerId);
        }
    }

    /**
     * Get subscriptions done for this tracker.
     * @param trackerId
     * @return
     */
    public List<String> getCompletedSubscriptionsForTracker(int trackerId) {
     
        Session session = entityManager.unwrap(Session.class);
        
        List<String> subscriptions = (List<String>)session.createCriteria(SubscriptionUpdateTracker.class)
                .add(Restrictions.eq("trackerId", trackerId))
                .setProjection(Projections.property("tenantUUID"))
                .list();

        return subscriptions;       
    }
    
    /**
     * Add Subscription Update Tracker Entry.
     * @param trackerId
     * @param tenantUUID
     * @param success
     */
    public SubscriptionUpdateTracker addSubscriptionEntry(int trackerId, String tenantUUID, boolean success) {
        
        SubscriptionUpdateTracker subUpdateEntry = new SubscriptionUpdateTracker();
        subUpdateEntry.setTenantUUID(tenantUUID);
        subUpdateEntry.setTrackerId(trackerId);
        
        if (success) {
            subUpdateEntry.setStatus(1);
        } else {
            subUpdateEntry.setStatus(0);
        }

        SubscriptionUpdateTracker updatedSubTracker = entityManager.merge(subUpdateEntry);
        return updatedSubTracker;
    }
}
