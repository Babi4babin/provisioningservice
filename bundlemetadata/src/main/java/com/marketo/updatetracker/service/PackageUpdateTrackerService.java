package com.marketo.updatetracker.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.marketo.updatetracker.dao.PackageUpdateTrackerDAO;
import com.marketo.updatetracker.model.PackageUpdateTracker;
import com.marketo.updatetracker.model.SubscriptionUpdateTracker;

@Component
public class PackageUpdateTrackerService {

    @Autowired
    private PackageUpdateTrackerDAO packageUpdateTrackerDAO;

    /**
     * Get last completed run
     * @return
     */
    public PackageUpdateTracker getLastRun() {
        return packageUpdateTrackerDAO.getLastRun(); 
    }

    public PackageUpdateTracker getLastCompletedRun() {
        return packageUpdateTrackerDAO.getLastCompletedRun(); 
    }
    
    public PackageUpdateTracker addUpdateRun(String tag) {
        return packageUpdateTrackerDAO.addUpdateRun(tag);
    }
    
    public void updateTrackerState(int trackerId, int status) {
        packageUpdateTrackerDAO.updateTrackerState(trackerId, status);
    }
    
    public void updateSuccessRun(int trackerId) {
        packageUpdateTrackerDAO.updateSuccessRun(trackerId);
    }
    
    public void updateFailureRun(int trackerId) {
        packageUpdateTrackerDAO.updateFailureRun(trackerId);
    }
    
    public List<String> getCompletedSubscriptionsForTracker(int trackerId) {
        return packageUpdateTrackerDAO.getCompletedSubscriptionsForTracker(trackerId);
    }
    
    public SubscriptionUpdateTracker addSubscriptionEntry(String tenantUUID, int trackerId, boolean success) {
        return packageUpdateTrackerDAO.addSubscriptionEntry(trackerId, tenantUUID, success);
    }
    
}
