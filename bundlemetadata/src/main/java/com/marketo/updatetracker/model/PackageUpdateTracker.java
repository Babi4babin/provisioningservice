package com.marketo.updatetracker.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.marketo.bundlemetadata.model.BaseEntity;

@Entity
@Table(name="packaging_update_tracker")
public class PackageUpdateTracker extends BaseEntity implements Serializable {
  
    // Update Tracker States
    public static final int STARTED = 1;
    
    public static final int IN_PROGRESS = 2;
    
    public static final int FINSHED = 3;
    
    public static final int FAILED = 4;
    
    /**
     * 
     */
    private static final long serialVersionUID = -496973140798485892L;

    private String tag;
    
    @Column(name = "start_time", updatable = false)
    private Date startTime;
    
    @Column(name = "end_time")
    private Date endTime;
    
    private int status;
    
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    @PrePersist
    public final void onAdd() {
      this.startTime = new Date();
    }

}
