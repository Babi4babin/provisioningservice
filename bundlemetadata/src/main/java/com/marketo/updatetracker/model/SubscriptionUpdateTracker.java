package com.marketo.updatetracker.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.marketo.bundlemetadata.model.BaseEntity;

@Entity
@Table(name="subscription_update_tracker")
public class SubscriptionUpdateTracker extends BaseEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6528568617916295873L;
    
    @Column(name = "tracker_id", updatable = false)
    private int trackerId;
    
    @Column(name = "tenant_uuid")
    private String tenantUUID;

    private int status;

    public int getTrackerId() {
        return trackerId;
    }

    public void setTrackerId(int trackerId) {
        this.trackerId = trackerId;
    }

    public String getTenantUUID() {
        return tenantUUID;
    }

    public void setTenantUUID(String tenantUUID) {
        this.tenantUUID = tenantUUID;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
