package com.marketo.bundlemetadata.tools;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyAccessor;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;
import org.yaml.snakeyaml.Yaml;

import com.google.common.base.Splitter;

public class SeedDataReader {
    
    private static Logger logger = LoggerFactory.getLogger(SeedDataReader.class);
    
    private Map<String, Object> symbolMap = new HashMap<>();
    
    private ApplicationContext context;
    
    public SeedDataReader(ApplicationContext context) {
        this.context = context;
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, Map<String, Object>> parseMap(Map<String, Object> yamlMap) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        
        Map<String, Map<String, Object>> resultMap = new LinkedHashMap<>();
        
        // Assume keys point to the model classes
        for (String modelClass : yamlMap.keySet()) {
            
            // Instantiate 
            Class<?> c = Class.forName(modelClass);

            Map<String, Object> objectMap = new LinkedHashMap<>();
            
            Map<String, Object> yamlClassMap = (Map<String, Object>) yamlMap.get(modelClass);
            
            for (String objRef : yamlClassMap.keySet()) {

                Object o = c.newInstance();
                PropertyAccessor myAccessor = PropertyAccessorFactory.forDirectFieldAccess(o);
                
                Map<String, Object> objectValues = (Map<String, Object>) yamlClassMap.get(objRef);
                for (String name : objectValues.keySet()) {
                    myAccessor.setPropertyValue(name, parseValue(objectValues.get(name)));                
                }
                objectMap.put(objRef, o);
                symbolMap.put(objRef, o);
            }
            
            resultMap.put(modelClass, objectMap);
        }
        return resultMap;
    }
    
    public Object parseValue(Object value) {
        
        // If the value is a reference return the reference then
        if (value instanceof String) {
            String valueStr = (String) value;
                        
            if (symbolMap.containsKey(valueStr)) {
                return symbolMap.get(valueStr);
            }
        } else if (value instanceof Map) {
            // Its a map, so value needs to be pulled in.
            Map<String, String> refMap = (Map<String, String>) value;
            
            String className = refMap.get("class");
            String method = refMap.get("method");
            String param = refMap.get("param");
            
            if (StringUtils.isEmpty(className)) {
                throw new RuntimeException("Valid Class name not found in one of the references.");
            }
            
            if (StringUtils.isEmpty(method)) {
                throw new RuntimeException("Valid Method not found in one of the references.");
            }
            
            if (StringUtils.isEmpty(param)) {
                throw new RuntimeException("Valid Parameter was not found for one of the references.");
            }
            
            try {
                // Create a class
                //Object c = context.getBean(className);
                Class<?> c = Class.forName(className);

                String[] params = param.split(",");
                List<String> newParams = new ArrayList<>();
                Class[] types = new Class[params.length];
                int i=0;
                for (String p : params) {
                    newParams.add(p.trim());
                    types[i++] = String.class;
                }
                
                Method methodObj = c.getMethod(method, types);
                Object o = c.newInstance();
                context.getAutowireCapableBeanFactory().autowireBean(o);                               
                
                EntityManagerFactory entityManagerFactory = context.getBean(EntityManagerFactory.class);
                EntityManager em = entityManagerFactory.createEntityManager();
                
                c.getMethod("setEntityManager", EntityManager.class).invoke(o, em);
                
                return methodObj.invoke(o, (Object[])(newParams.toArray(new String[0])));
            } catch (ClassNotFoundException cnf) {
                throw new RuntimeException("Class: " + className + " not found. Check the Yaml file", cnf);
            } catch (NoSuchMethodException nsm) {
                throw new RuntimeException("Method: " + method + " not found. Check the Yaml file", nsm);
            } catch (InvocationTargetException it) {
                throw new RuntimeException("Failed invocating method " + method + " on class" + className, it);
            } catch (IllegalAccessException ia) {
                throw new RuntimeException("Access Modifier on method " + method + " is not valid", ia);
            } catch(InstantiationException ie) {
                throw new RuntimeException("Could not instantiate class " + className, ie);
            }
        }
        
        logger.info("Value is " + value);
        return value;
    }
    
    public Map<String, Map<String, Object>> loadData(InputStream is) {

        Yaml yaml = new Yaml();

        Map<String, Object> map = (Map<String, Object>)yaml.load(is);
        
        try {
            return parseMap(map);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Invalid class name in the yaml file.", e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Possibly Invalid property names. Check yaml file and corresponding classes.", e);
        }
    }
}
