package com.marketo.bundlemetadata.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="marketing_feature")
@JsonInclude(Include.NON_NULL)
public class MarketingFeature extends BaseEntity {
        
    private String name;

    private String code;

    private String description;

    //@JsonIgnore
    private int active;
    
    @Column(name = "developer_config")
    private String developerConfig;
    
    @Column(name = "provisioning_URI")
    //@JsonIgnore
    private String provisioningURI;
    
    @Column(name = "deprovisioning_URI")
    //@JsonIgnore
    private String deprovisioningURI;

    @Column(name = "update_URI")
    //@JsonIgnore
    private String updateURI;

    @Column(name = "status_URI")
    //@JsonIgnore
    private String statusURI;
    
    @Column(name = "locator_service_id")
    //@JsonIgnore
    private String locatorServiceId;

    @Column(name = "client_type")
    //@JsonIgnore
    private String clientType;
    
    @Transient
    private String marketingFeatureConfig;
    
    @Transient
    private String isDeletable;
        
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getDeveloperConfig() {
        return developerConfig;
    }

    public void setDeveloperConfig(String developerConfig) {
        this.developerConfig = developerConfig;
    }
    
    public String getProvisioningURI() {
        return provisioningURI;
    }

    public void setProvisioningURI(String provisioningURI) {
        this.provisioningURI = provisioningURI;
    }

    public String getStatusURI() {
        return statusURI;
    }

    public void setStatusURI(String statusURI) {
        this.statusURI = statusURI;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getLocatorServiceId() {
        return locatorServiceId;
    }

    public void setLocatorServiceId(String locatorServiceId) {
        this.locatorServiceId = locatorServiceId;
    }
    
    public String getMarketingFeatureConfig() {
        return marketingFeatureConfig;
    }

    public void setMarketingFeatureConfig(String marketingFeatureConfig) {
        this.marketingFeatureConfig = marketingFeatureConfig;
    }
    
    public String getDeprovisioningURI() {
        return deprovisioningURI;
    }

    public String getUpdateURI() {
        return updateURI;
    }
    
    public void setDeprovisioningURI(String deprovisioningURI) {
        this.deprovisioningURI = deprovisioningURI;
    }

    public void setUpdateURI(String updateURI) {
        this.updateURI = updateURI;
    }
}
