package com.marketo.bundlemetadata.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "marketing_bundle_detail")
@JsonInclude(Include.NON_NULL)
public class MarketingBundleFeature extends BaseEntity {
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "marketing_bundle_id")
    @JsonIgnore
    private MarketingBundle parentBundle;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "child_marketing_bundle_id")
    @JsonProperty
    private MarketingBundle bundle;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "marketing_feature_id")
    @JsonProperty
    private MarketingFeature feature;
    
    @Column(name = "bundle_feature_config", length = 2000)
    //@JsonIgnore
    private String marketingFeatureConfig;
    
    private int active;

    @Column(name = "is_optional")
    private int isOptional;

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getIsOptional() {
        return isOptional;
    }

    public void setIsOptional(int isOptional) {
        this.isOptional =  isOptional;
    }

    @JsonIgnore
    public boolean isActive() {
        return this.active == 1;
    }

    @JsonIgnore
    public boolean isOptional() {
        return this.isOptional == 1;
    }
    
    public String getMarketingFeatureConfig() {
        return marketingFeatureConfig;
    }

    public void setMarketingFeatureConfig(String marketingFeatureConfig) {
        this.marketingFeatureConfig = marketingFeatureConfig;
    }
    
    public MarketingBundle getParentBundle() {
        return parentBundle;
    }

    public void setParentBundle(MarketingBundle parentBundle) {
        this.parentBundle = parentBundle;
    }

    public MarketingBundle getBundle() {
        return bundle;
    }

    public void setBundle(MarketingBundle bundle) {
        this.bundle = bundle;
    }

    public MarketingFeature getFeature() {
        return feature;
    }

    public void setFeature(MarketingFeature feature) {
        this.feature = feature;
    }
    
    @JsonIgnore
    public boolean isBundle() {
        return bundle != null && feature == null;
    }

    @JsonIgnore
    public boolean isFeature() {
        return bundle == null && feature != null;
    }
}