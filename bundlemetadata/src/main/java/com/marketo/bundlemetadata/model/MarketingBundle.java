package com.marketo.bundlemetadata.model;

import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name="marketing_bundle")
public class MarketingBundle extends BaseEntity {
    
    private String name;
    
    private String description;

    private String code;

    private int active;
        
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "marketing_bundle_id")
    @OrderBy("id")
    private List<MarketingBundleFeature> childBundleFeatures;
        
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
    
    public List<MarketingBundleFeature> getChildBundleFeatures() {
        return Collections.unmodifiableList(childBundleFeatures);
    }

    public void setChildBundleFeatures(List<MarketingBundleFeature> childBundleFeatures) {
        this.childBundleFeatures = childBundleFeatures;
    }
}
