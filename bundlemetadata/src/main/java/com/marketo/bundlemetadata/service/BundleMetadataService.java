package com.marketo.bundlemetadata.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.marketo.bundlemetadata.dao.BundleMetadataDAO;
import com.marketo.bundlemetadata.exception.BundleMetadataException;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.rest.model.BundleDiffInfo;

@Component
public class BundleMetadataService {
    
    private static final Logger logger = LoggerFactory.getLogger(BundleMetadataService.class);
        
    @Autowired
    private BundleMetadataDAO bundleMetadataDao;
        
    /**
     * 
     * @param id
     * @return
     */
    public MarketingFeature getMarketingFeature(int id) {
       return bundleMetadataDao.getMarketingFeature(id); 
    }


    /**
     *
     * @param code
     * @return
     */
    public MarketingFeature getMarketingFeatureByCode(String code) {
        return bundleMetadataDao.getMarketingFeatureByCode(code);
    }

    /**
     *
     * @param name
     * @return
     */
    public MarketingFeature getMarketingFeature(String name) {
        return bundleMetadataDao.getMarketingFeature(name);
    }

    public MarketingBundle getMarketingBundle(String bundleName) {
        MarketingBundle bundle = bundleMetadataDao.getMarketingBundle(bundleName);
        if (bundle == null) {
            throw new BundleMetadataException("Could not find Bundle, with name: " + bundleName);
        }
        return bundle;
    }

    public MarketingBundle getMarketingBundleByCode(String code) {
        MarketingBundle bundle = bundleMetadataDao.getMarketingBundleByCode(code);
        if (bundle == null) {
            throw new BundleMetadataException("Could not find Bundle, with code: " + code);
        }
        return bundle;
    }
    
    public MarketingBundle getMarketingBundle(int bundleId) {
        MarketingBundle bundle = bundleMetadataDao.getMarketingBundle(bundleId);
        if (bundle == null) {
            throw new BundleMetadataException("Could not find Bundle, with id: " + bundleId);
        }
        return bundle;
    }
    
    public List<MarketingBundle> getAllBundles() {
        throw new UnsupportedOperationException("Request to get all bundles not supported");
    }
    
    public List<MarketingBundle> getRootBundles() {        
        return bundleMetadataDao.getRootBundles();
    }
    
    /**
     * Was feature present as part of the bundle
     * @param bundle
     * @param featureId
     * @return
     */
    public MarketingBundleFeature getFeatureInBundle(MarketingBundle bundle, int featureId) {
        
        MarketingBundleFeature feature = null;
        if (bundle != null) {
            for (MarketingBundleFeature bundleFeature : bundle.getChildBundleFeatures()) {
                if (bundleFeature.isBundle()) {
                    feature = getFeatureInBundle(bundleFeature.getBundle(), featureId);
                } else {
                    if (bundleFeature.getFeature().getId() == featureId) {
                        feature = bundleFeature;
                    }
                }
            }
        }
        
        return feature;
    }
    
    /**
     * 
     * @param oldBundle
     * @param newBundle
     * @return
     */
    public BundleDiffInfo compareBundles(MarketingBundle oldBundle, MarketingBundle newBundle) {
        
        Map<Integer, MarketingBundle> oldBundles = new LinkedHashMap<>(); 
        Map<Integer, MarketingFeature> oldFeatures = new LinkedHashMap<>();
        
        List<MarketingBundle> newBundles = new ArrayList<>();
        List<MarketingBundle> commonBundles = new ArrayList<>();
        List<MarketingFeature> newFeatures = new ArrayList<>();
        List<MarketingFeature> commonFeatures = new ArrayList<>();
        
        List<MarketingBundleFeature> bundleFeatures = oldBundle.getChildBundleFeatures();
        for (MarketingBundleFeature bundleFeature : bundleFeatures) {
            
            if (bundleFeature.isBundle()) {
                oldBundles.put(bundleFeature.getBundle().getId(), bundleFeature.getBundle());
            } else if (bundleFeature.isFeature()) {
                oldFeatures.put(bundleFeature.getFeature().getId(), bundleFeature.getFeature());
            }            
        }
        
        bundleFeatures = newBundle.getChildBundleFeatures();
        for (MarketingBundleFeature bundleFeature : bundleFeatures) {
            if (bundleFeature.isBundle()) {
                if (oldBundles.containsKey(bundleFeature.getBundle().getId())) {
                    commonBundles.add(bundleFeature.getBundle());
                    oldBundles.remove(bundleFeature.getBundle().getId());
                } else {
                    newBundles.add(bundleFeature.getBundle());
                }
            } else if (bundleFeature.isFeature()) {
                
                MarketingFeature feature = bundleFeature.getFeature();
                feature.setMarketingFeatureConfig(bundleFeature.getMarketingFeatureConfig());
                
                if (oldFeatures.containsKey(bundleFeature.getFeature().getId())) {
                    commonFeatures.add(feature);
                    oldFeatures.remove(feature.getId());
                } else {
                    newFeatures.add(feature);
                }
            }            
        }        
        
        BundleDiffInfo bundleDiff = new BundleDiffInfo();
        bundleDiff.setCommonChildBundles(commonBundles);
        bundleDiff.setCommonFeatures(commonFeatures);
        bundleDiff.setNewChildBundles(newBundles);
        bundleDiff.setNewFeatures(newFeatures);
        bundleDiff.setOldChildBundles(new ArrayList<>(oldBundles.values()));
        bundleDiff.setOldFeatures(new ArrayList<>(oldFeatures.values()));
        
        return bundleDiff;
    }
 
    /**
     * Get updates to marketing_bundle_detail in the given interval
     * @param startTime
     * @param endTime
     */
    public List<MarketingBundleFeature> getBundleUpdatesBetween(Date startTime, Date endTime) {        
        return bundleMetadataDao.getBundleUpdatesBetween(startTime, endTime);        
    }
    
    public MarketingBundle getRootBundle(MarketingBundle childBundle) {
         return bundleMetadataDao.getRootBundle(childBundle);       
    }
}
