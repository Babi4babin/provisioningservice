package com.marketo.bundlemetadata.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.common.ObjectMapperFactory;

@Component
public class MarketingFeatureConfigurator {

    private static final Logger logger = LoggerFactory.getLogger(MarketingFeatureConfigurator.class);
    
    // Developer config Types
    private static final String PICKLIST_TYPE = "picklist";
    private static final String NUMBER_TYPE = "number";
    private static final String BOOLEAN_TYPE = "boolean";
    private static final String TEXT_TYPE = "text";
    
    @Autowired
    private PodListService podListService;
    
    /**
     * @param tenantUUID
     * @param features
     */
    public void initializeFeatures(String tenantUUID, List<MarketingFeature> features) {
        
        for (MarketingFeature feature : features) {
            if (!StringUtils.isEmpty(feature.getDeveloperConfig())) {
                feature.setMarketingFeatureConfig(initializeFeatureConfig(tenantUUID, feature.getDeveloperConfig()));
                String developerConfig = transformDeveloperConfig(tenantUUID, feature.getDeveloperConfig(), new HashMap<String, String>());
                feature.setDeveloperConfig(developerConfig);
            }            
        }
    }

    /**
     * @param tenantUUID
     * @param feature
     */
    public void initializeFeature(String tenantUUID, MarketingFeature feature, Map<String, String> configuredPods) {

        if (!StringUtils.isEmpty(feature.getDeveloperConfig())) {
            feature.setMarketingFeatureConfig(initializeFeatureConfig(tenantUUID, feature.getDeveloperConfig(), configuredPods));
            String developerConfig = transformDeveloperConfig(tenantUUID, feature.getDeveloperConfig(), configuredPods);
            feature.setDeveloperConfig(developerConfig);
        }
    }
        
    @SuppressWarnings("unchecked")
    public String transformDeveloperConfig(String tenantUUID, String developerConfig, Map<String, String> configuredPods) {
   
        ObjectMapper mapper = ObjectMapperFactory.getInstance();
        try {
            List<Object> configs = mapper.readValue(developerConfig, List.class);
            // each config is a map.
            for (Object o : configs) {
                Map<String, Object> config = (Map<String, Object>) o;
                               
                Map<String, String> name = (Map<String, String>)config.get("name");
                
                // get the name of the field                
                String field = name.get("field");
                               
                if (StringUtils.endsWithIgnoreCase(field, "podName")) {
                    
                    Map<String, String> metadata = (Map<String, String>)config.get("metadata");
                    List<String> activePods = podListService.getActivePods(tenantUUID, metadata.get("tag"));
                    // Only add the pod selection values if the pod was not configured already for this subscription (check configuredPods)
                    if (!configuredPods.containsKey(metadata.get("tag"))) {
                        config.put("value", activePods);
                    }
                }
            }
            
            return mapper.writeValueAsString(configs);
        } catch (Exception e) {
            logger.error("Error initializing Marketing Feature Config: ", e);
        }
      
        return null;
    }

    public String initializeFeatureConfig(String tenantUUID, String developerConfig) {
        return initializeFeatureConfig(tenantUUID, developerConfig, new HashMap<String, String>());
    }
    
    
    @SuppressWarnings("unchecked")
    public String initializeFeatureConfig(String tenantUUID, String developerConfig, Map<String, String> configuredPods) {
        
        Map<String, Object> featureConfig = new HashMap<>();
        String marketingFeatureConfig = null;
        
        if (!StringUtils.isEmpty(developerConfig)) {
            ObjectMapper mapper = ObjectMapperFactory.getInstance();
            try {
                List<Object> configs = mapper.readValue(developerConfig, List.class);
                // each config is a map.
                for (Object o : configs) {
                    Map<String, Object> config = (Map<String, Object>) o;
                    Map<String, String> name = (Map<String, String>)config.get("name");
                    
                    // get the name of the field                
                    String field = name.get("field");
                    String type = (String) config.get("type");
                    String fieldValue = null;
                    if (StringUtils.endsWithIgnoreCase(field, "podName")) {
                        Map<String, String> metadata = (Map<String, String>)config.get("metadata");
                        fieldValue = configuredPods.containsKey(metadata.get("tag")) ? configuredPods.get(metadata.get("tag")) : null;
                    }
                    else if (StringUtils.endsWithIgnoreCase(type, PICKLIST_TYPE) && config.containsKey("defaultValue")) {
                        String submitField = (String) config.get("submitField");
                        Map<String, String> defaultValue = (Map<String, String>) config.get("defaultValue");
                        if(!StringUtils.isEmpty(submitField) && defaultValue != null && defaultValue.size() > 0) {
                            fieldValue = defaultValue.get(submitField);
                        }
                    }
                    featureConfig.put(field, fieldValue);
                }
                marketingFeatureConfig = mapper.writeValueAsString(featureConfig);
            } catch (Exception e) {
                logger.error("Error initializing Marketing Feature Config: ", e);
            }            
        }
        return marketingFeatureConfig;
    }
    
    /**
     * Construct the default feature config for this feature.
     * @param tenantUUID
     * @param feature
     * @return
     */
    public String getDefaultFeatureConfig(String tenantUUID, MarketingFeature feature) {
        return initializeFeatureConfig(tenantUUID, feature.getDeveloperConfig());
    }
    
    /*private Object getValue(Map<String, Object> config) {
        
        String type = (String)config.get("type");
        
        switch(type) {
        
            case PICKLIST_TYPE:
                return config.get("defaultValue") != null ? config.get("defaultValue") : config.get("values");
            case NUMBER_TYPE:
            case BOOLEAN_TYPE:
            case TEXT_TYPE:
            default:
                return config.get("value");        
        }        
    }*/
    
    /**
     * 
     * @param type
     * @return
     *//*
    private Object getPlaceholderValue(String type) {
        
        switch (type) {
            case PICKLIST_TYPE:
                return "";
            case NUMBER_TYPE:
                return 0;
            case BOOLEAN_TYPE:
                return false;
            case TEXT_TYPE:
                return "";
            default:
                return "";
        }
    }*/
    
}
