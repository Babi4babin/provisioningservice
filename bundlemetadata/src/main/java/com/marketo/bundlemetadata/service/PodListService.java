package com.marketo.bundlemetadata.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.marketo.locatorservice.client.LocatorServiceClient;
import com.marketo.locatorservice.client.LocatorServiceClientConfig;
import com.marketo.locatorservice.client.impl.JsonResponse;
import com.marketo.locatorservice.client.impl.LocatorServiceClientImpl;
import com.marketo.locatorservice.client.utils.JsonUtil;

@Component
@Configuration
public class PodListService {

    private static final Logger logger = LoggerFactory.getLogger(PodListService.class);
    
    private static final String GET_PRODUCT_CLUSTER_QRY = "/clusters.json?search=metadata.product==\'%s\'";
        
    @Autowired
    private LocatorServiceClientConfig locatorServiceClientConfig;
    
    private LocatorServiceClient locatorServiceClient;
        
    @Autowired
    private RCAPodListService rcaPodListService;
        
    /**
     * Initialize locator service client. Registers init and destroy method
     * 
     * @return locator service client
     */
    @Bean(initMethod = "init", destroyMethod = "destroy")
    public LocatorServiceClient locatorServiceClient() {
        return new LocatorServiceClientImpl(this.locatorServiceClientConfig);
    }

    /**
     * Get Active Pods for a given product
     * @param product
     * @return
     */
    public List<String> getActivePods(String tenantUUID, String product) {
        
        List<String> pods = new ArrayList<>();       
        if ("rca".equals(product)) {
            pods = rcaPodListService.getRCAPods(tenantUUID);
        } else {
            // get from locator service
            pods = getPodsFromLocator(product);
        }        
        return pods;
    }
      
    /**
     * Get Pod list for product from locator.
     * @param product
     * @return
     */
    private List<String> getPodsFromLocator(String product) {
        
        List<String> clusters = new ArrayList<>();
        
        this.locatorServiceClient = locatorServiceClient();
        try {
            
            String query = String.format(GET_PRODUCT_CLUSTER_QRY, product);            
            HttpGet getRequest = new HttpGet(getUrl(query));            
            JsonResponse response = locatorServiceClient.sendRequest(getRequest);
            if (!response.isError()) {
                for (JsonNode node : response.getEntries()) {
                    clusters.add(JsonUtil.getStringValue(node, "clusterId"));
                }
            }           
        } catch (Exception e) {
            logger.error("Could not retrieve active pods for: " + product + "." + e.getMessage());
        }
        
        return clusters;
    }
    
    private String getUrl(final String actionUrl) {
        StringBuffer buf = new StringBuffer(this.locatorServiceClient.getConfig().getLocatorServiceUrl());
        buf.append(actionUrl);

        return buf.toString();
    }
}
