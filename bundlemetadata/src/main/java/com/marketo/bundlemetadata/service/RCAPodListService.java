package com.marketo.bundlemetadata.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketo.locatorservice.client.SubscriptionServiceManager;
import com.marketo.locatorservice.entities.SubscriptionId;
import com.marketo.locatorservice.entities.SubscriptionPodInfo;

@Component
public class RCAPodListService {

    private static final Logger logger = LoggerFactory.getLogger(RCAPodListService.class);
    
    private static final String GET_PODS_URL = "%s/administrator/getMPAPodList?tenantId=%s&srcPrvnService=%s";
    
    @Autowired
    private SubscriptionServiceManager subscriptionServiceManager;
    
    /**
     * @param tenantUUID
     * @return
     */
    public List<String> getRCAPods(String tenantUUID) {
        
        List<String> pods = Collections.emptyList();
        
        try {
            SubscriptionPodInfo subPod = subscriptionServiceManager.getSubscriptionPodInfo(new SubscriptionId(tenantUUID));
            String srcPrvnService = "1";
            String url = buildUrl(subPod.getInternalApiUrl(), tenantUUID, srcPrvnService);
            String response = sendRequest(url);
            pods = parseRCAPodListResponse(response);
        } catch (Exception e) {
            logger.error("Error in retrieving RCA Pods. for tenantUUID: " + tenantUUID + ". " + e.getMessage());
        }
        
        return pods;
    }
    
    /**
     * 
     * @param appUrl
     * @return
     */
    private String buildUrl(String appUrl, String tenantId, String srcPrvnService) {
       
       return String.format(GET_PODS_URL, appUrl, tenantId, srcPrvnService);
    }
    
    /**
     * Do post request with url encoded form data
     * @param url
     * @param data
     * @return
     * TODO: Implement retries here
     */
    private String sendRequest(String url) {
                
        String response = null;
        RestTemplate restTemplate = new RestTemplate();
        
        try {
            
            logger.info("Sending request to URL: " + url);
                    
            response = restTemplate.getForObject(url, String.class);
    
            logger.info("Response recieved: " + response);
            
        } catch (Exception e) {
          logger.error("Error retrieving response: for " + url + ". " + e.getMessage());      
        }
        return response;
    }
    
    @SuppressWarnings("unchecked")
    private List<String> parseRCAPodListResponse(String response) {
        
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> responseObj = null;
        List<String> podList = new ArrayList<>();
        
        if (response != null) {
            try {
                responseObj = mapper.readValue(response, Map.class);
            } catch (Exception e) {
                throw new RuntimeException("Unabled to parse response received");
            }
            
            if (responseObj.containsKey("JSONResults")) {
                
                Map<String, Object> jsonSection = (Map<String, Object>)responseObj.get("JSONResults");
                
                if (jsonSection.containsKey("isError")) {
                    throw new RuntimeException((String)jsonSection.get("errorMessage"));
                }
                
                podList.addAll(((Map<String,String>)jsonSection.get("pods")).keySet());
                
            } else {
                throw new RuntimeException("Invalid Response Recieved. " + response);
            }     
        }
        return podList;
    }
}
