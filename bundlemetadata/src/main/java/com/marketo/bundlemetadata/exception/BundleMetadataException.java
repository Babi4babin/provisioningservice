package com.marketo.bundlemetadata.exception;

public class BundleMetadataException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -2568941298640336751L;

    public BundleMetadataException(String message) {
        super(message);
    }
    
}
