package com.marketo.bundlemetadata.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import lombok.Data;
import lombok.ToString;

import java.util.*;

@Data
@ToString
public class SubscriptionBundles {

    private String tenantUUID;

    private List<MarketingBundle> bundles = new ArrayList<>();

    @JsonIgnore
    private Map <Integer, Integer> bundleMap = new HashMap<>();

    public void addBundle(MarketingBundle bundle) {
        bundles.add(bundle);
        bundleMap.put(bundle.getId(), bundles.size()-1);
    }

    public void removeBundle(MarketingBundle bundle) {
        if(bundleMap.containsKey(bundle.getId())) {
            bundles.set(bundleMap.get(bundle.getId()).intValue(), null);
            bundleMap.remove(bundle.getId());
        }
    }

    public void removeEmptyBundles() {
        bundles.removeAll(Collections.singleton(null));
    }

    public void setTenantUUID(String tenantUUID) {
        this.tenantUUID = tenantUUID;
    }

    public List<MarketingBundle> getBundles() {
        return bundles;
    }

    public Map<Integer, Integer> getBundleMap() {
        return bundleMap;
    }
}
