package com.marketo.bundlemetadata.rest.model;

import java.util.List;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;

public class BundlesFeatures {
    
    private List<MarketingBundle> bundles;
    
    private List<MarketingFeature> features;
    
    public BundlesFeatures(List<MarketingBundle> bundles, List<MarketingFeature> features) {
        this.bundles = bundles;
        this.features = features;
    }

    public List<MarketingBundle> getBundles() {
        return bundles;
    }

    public List<MarketingFeature> getFeatures() {
        return features;
    }
}
