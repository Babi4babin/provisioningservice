package com.marketo.bundlemetadata.rest.model;

import java.util.ArrayList;
import java.util.List;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SubscriptionBundleFeatures {

    private String tenantUUID;
    
    // Top level bundle
    private MarketingBundle bundle;

    // Addon features/bundles
    private List<MarketingBundleFeature> addOns = new ArrayList<>();
    
    public void addAddon(MarketingBundleFeature bundleFeature) {
        addOns.add(bundleFeature);
    }

    public void setBundle(MarketingBundle bundle) {
        this.bundle = bundle;
    }

    public void setTenantUUID(String tenantUUID) {
        this.tenantUUID = tenantUUID;
    }

    public MarketingBundle getBundle() {
        return bundle;
    }

    public List<MarketingBundleFeature> getAddOns() {
        return addOns;
    }
}
