package com.marketo.bundlemetadata.rest.model;

import java.util.List;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;

/**
 * Represents diff between two Bundles
 * @author vraman
 *
 */
public class BundleDiffInfo {
    
    private List<MarketingFeature> oldFeatures;
    
    private List<MarketingBundle> oldChildBundles;
    
    private List<MarketingFeature> newFeatures;

    private List<MarketingBundle> newChildBundles;
    
    private List<MarketingFeature> commonFeatures;
    
    private List<MarketingBundle> commonChildBundles;

    public List<MarketingFeature> getOldFeatures() {
        return oldFeatures;
    }

    public void setOldFeatures(List<MarketingFeature> oldFeatures) {
        this.oldFeatures = oldFeatures;
    }

    public List<MarketingBundle> getOldChildBundles() {
        return oldChildBundles;
    }

    public void setOldChildBundles(List<MarketingBundle> oldChildBundles) {
        this.oldChildBundles = oldChildBundles;
    }

    public List<MarketingFeature> getNewFeatures() {
        return newFeatures;
    }

    public void setNewFeatures(List<MarketingFeature> newFeatures) {
        this.newFeatures = newFeatures;
    }

    public List<MarketingBundle> getNewChildBundles() {
        return newChildBundles;
    }

    public void setNewChildBundles(List<MarketingBundle> newChildBundles) {
        this.newChildBundles = newChildBundles;
    }

    public List<MarketingFeature> getCommonFeatures() {
        return commonFeatures;
    }

    public void setCommonFeatures(List<MarketingFeature> commonFeatures) {
        this.commonFeatures = commonFeatures;
    }

    public List<MarketingBundle> getCommonChildBundles() {
        return commonChildBundles;
    }

    public void setCommonChildBundles(List<MarketingBundle> commonChildBundles) {
        this.commonChildBundles = commonChildBundles;
    }
}
