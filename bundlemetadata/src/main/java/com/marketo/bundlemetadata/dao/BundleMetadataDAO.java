package com.marketo.bundlemetadata.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;

@Repository
@Transactional(readOnly = true)
public class BundleMetadataDAO {

    @PersistenceContext
    private EntityManager entityManager;   
    
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    /**
     * 
     * @param bundleName
     * @return
     */
    public MarketingBundle getMarketingBundle(String bundleName) {
        
        Session session = entityManager.unwrap(Session.class);
            
        MarketingBundle bundle = (MarketingBundle)session.createCriteria(MarketingBundle.class)
               .add(Restrictions.eq("name", bundleName))
               .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
               //.addOrder("")
               .uniqueResult();
        
        return bundle;           
    }

    /**
     *
     * @param bundleCode
     * @return
     */
    public MarketingBundle getMarketingBundleByCode(String code) {

        Session session = entityManager.unwrap(Session.class);

        MarketingBundle bundle = (MarketingBundle)session.createCriteria(MarketingBundle.class)
                .add(Restrictions.eq("code", code))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .uniqueResult();

        return bundle;
    }
    /**
     * 
     * @param bundleName
     * @return
     */
    public MarketingBundle getMarketingBundle(int bundleId) {
        
        Session session = entityManager.unwrap(Session.class);
        
        MarketingBundle bundle = (MarketingBundle)session.createCriteria(MarketingBundle.class)
               .add( Restrictions.eq("id", bundleId))
               .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
               .uniqueResult();
        
        return bundle;           
    }
    
    /**
     * 
     * @param id
     * @return
     */
    
    public MarketingFeature getMarketingFeature(String name) {
      
        Session session = entityManager.unwrap(Session.class);

        MarketingFeature feature = (MarketingFeature) session.createCriteria(MarketingFeature.class)
                .add( Restrictions.eq("name", name))
                .uniqueResult();
        return feature;   
    }
    
    /**
     * 
     * @param id
     * @return
     */
    
    public MarketingFeature getMarketingFeature(int id) {
        
        Session session = entityManager.unwrap(Session.class);
        
        MarketingFeature feature = (MarketingFeature) session.createCriteria(MarketingFeature.class)
                .add( Restrictions.eq("id", id))
                .uniqueResult();

        return feature;   
    }

    /**
     *
     * @param code
     * @return
     */
    public MarketingFeature getMarketingFeatureByCode(String code) {

        Session session = entityManager.unwrap(Session.class);

        MarketingFeature feature = (MarketingFeature) session.createCriteria(MarketingFeature.class)
                .add( Restrictions.eq("code", code))
                .uniqueResult();

        return feature;
    }

    /**
     * 
     * @param name
     * @return
     */
    public int getMarketingFeatureId(String name) {
        
        MarketingFeature feature = getMarketingFeature(name);
        if (feature != null) {
            return feature.getId();
        } else {
            throw new RuntimeException("Marketing Feature " + name + " not found");
        }
    }
    
    /**
     * Add Marketing Bundle to Metadata
     * @param bundle
     */
    public void addMarketingBundle(MarketingBundle bundle) {                  
        entityManager.persist(bundle);
    }    
    
    public boolean isChildBundle(int parentBundleId, int childBundleId) {
        
        Session session = entityManager.unwrap(Session.class);
                    
        MarketingBundleFeature bundleFeature = (MarketingBundleFeature)session.createCriteria(MarketingBundleFeature.class)
               .add( Restrictions.eq("parentBundle.id", parentBundleId))
               .add(Restrictions.eq("bundle.id", childBundleId))
               .uniqueResult();
        
        return bundleFeature != null ? false : true;           
    }
    
    public boolean isChildFeature(int parentBundleId, int childFeatureId) {
        
        Session session = entityManager.unwrap(Session.class);
                   
        MarketingBundleFeature bundleFeature = (MarketingBundleFeature)session.createCriteria(MarketingBundleFeature.class)
               .add( Restrictions.eq("parentBundle.id", parentBundleId))
               .add(Restrictions.eq("feature.id", childFeatureId))
               .uniqueResult();
        
        return bundleFeature != null ? true : false;           
    }
    
    /**
     * Get all root bundles
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<MarketingBundle> getRootBundles() {
        
        Session session = entityManager.unwrap(Session.class);
                        
        DetachedCriteria subQuery = DetachedCriteria.forClass(MarketingBundleFeature.class)
                                    .add(Restrictions.isNotNull("parentBundle"))
                                    .add(Restrictions.isNull("feature"))
                                    .setProjection(Projections.distinct(Projections.property("bundle.id")));
        
        List<MarketingBundle> bundles = (List<MarketingBundle>)session.createCriteria(MarketingBundle.class)
               .setFetchMode("childBundleFeatures", FetchMode.SELECT)
               .add(Property.forName("id").notIn(subQuery))
               .add(Restrictions.eq("active", 1))
               .list();
        
        return bundles;
    }
    
    @SuppressWarnings("unchecked")
    public List<MarketingBundle> getAllBundles() {
        
        Session session = entityManager.unwrap(Session.class);
                    
        List<MarketingBundle> bundles = (List<MarketingBundle>)session.createCriteria(MarketingBundle.class)
               .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
               .list();
        
        return bundles;
    }
    
    /**
     * Get all non-root bundles
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<MarketingBundle> getAvailableBundles() {
        
        Session session = entityManager.unwrap(Session.class);
        
        List<MarketingBundle> bundles = (List<MarketingBundle>)session.createCriteria(MarketingBundleFeature.class)
               .add(Restrictions.isNotNull("parentBundle.id"))
               .add(Restrictions.isNull("feature.id"))
               .setProjection(Projections.distinct(Projections.property("bundle")))
               .list();
      
        return bundles;
    }
    
    /**
     * Get all features that do not belong to other non root bundles, 
     * thus available to be shown in UI as separate entities
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<MarketingFeature> getAvailableFeatures() {
               
        // Get all features that do not belong to other bundles
        Session session = entityManager.unwrap(Session.class);
                    
        DetachedCriteria subQuery1 = DetachedCriteria.forClass(MarketingBundleFeature.class)
                .add(Restrictions.isNotNull("parentBundle.id"))
                .add(Restrictions.isNull("feature.id"))
                .setProjection(Projections.distinct(Projections.property("bundle.id")));
                    
        DetachedCriteria subQuery2 = DetachedCriteria.forClass(MarketingBundleFeature.class)
               .createAlias("feature", "feature")
               .add(Property.forName("parentBundle.id").in(subQuery1))
               .setProjection(Projections.distinct(Projections.property("feature")));
        
        List<MarketingFeature> features = (List<MarketingFeature>)session.createCriteria(MarketingFeature.class)
                .add(Property.forName("id").notIn(subQuery2)).list();
                              
        return features;
    }

    /**
     * 
     * @param startTime
     * @param endTime
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<MarketingBundleFeature> getBundleUpdatesBetween(Date startTime, Date endTime) {
        
        Session session = entityManager.unwrap(Session.class);
        
        List<MarketingBundleFeature> bundleFeatures = session.createCriteria(MarketingBundleFeature.class)
                .add(Restrictions.between("updatedAt", startTime, endTime))
                .addOrder(Order.desc("updatedAt"))
                .addOrder(Order.desc("id"))
                .list();
                
        return bundleFeatures;
    }
    
    /**
     * Get root bundle for a given bundle
     * @param childBundle
     * @return
     */
    public MarketingBundle getRootBundle(MarketingBundle childBundle) {
        
        Session session = entityManager.unwrap(Session.class);
        
        MarketingBundle bundle = childBundle;
        MarketingBundle parentBundle = null;
        do {
            parentBundle = bundle;
            
            bundle = (MarketingBundle)session.createCriteria(MarketingBundleFeature.class)
                .add(Restrictions.eq("bundle.id", childBundle.getId()))
                .setProjection(Property.forName("parentBundle"))
                .uniqueResult();

        } while(bundle != null);
        
        return parentBundle;        
    }
    
    public int getMarketingBundleFeatureId(String parentBundleName, String childFeature) {
        
        Session session = entityManager.unwrap(Session.class);
        
        String hql = String.format("FROM MarketingBundleFeature bf where bf.parentBundle.name = :bundleName and bf.feature.name = :featureName");
                        
        Query query = session.createQuery(hql).setParameter("bundleName", parentBundleName).setParameter("featureName", childFeature);        
        
        MarketingBundleFeature bundleFeature = (MarketingBundleFeature)query.list().get(0);    
        
        return bundleFeature.getId();
    }
}
