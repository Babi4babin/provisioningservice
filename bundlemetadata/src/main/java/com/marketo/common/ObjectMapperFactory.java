package com.marketo.common;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperFactory {
    private static final ObjectMapper mapper = new ObjectMapper();
    
    public static ObjectMapper getInstance() {
        return mapper;
    }
}
