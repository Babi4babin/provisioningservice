package com.marketo.subscriptionmetadata.dao;

import com.marketo.bundlemetadata.dao.BundleMetadataDAO;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class SubscriptionMetadataDAO {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionMetadataDAO.class);
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    private BundleMetadataDAO bundleMetadataDAO;
    
    /**
     * Get all bundles/features
     * @param tenantId
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<SubscriptionBundleMap> getAllBundleFeatures(String tenantId) {
        
        Session session = entityManager.unwrap(Session.class);          
        
        String hql = String.format("FROM SubscriptionBundleMap s where s.tenantUUID = :tenantID order by s.id asc");
        
        Query query = session.createQuery(hql).setParameter("tenantID", tenantId);
        
        List<SubscriptionBundleMap> subscriptionBundles = query.list();

        // Set the feature configuration
        for (SubscriptionBundleMap bundleEntry : subscriptionBundles) {
            if (bundleEntry.getMarketingFeature() != null) {
                bundleEntry.getMarketingFeature().setMarketingFeatureConfig(bundleEntry.getSubscriptionFeatureConfig());
            }
        }
        
        return subscriptionBundles;   
    }
    
    /**
     * Add feature info to subscription_bundle_map, update if it already exists
     * @param tenantId
     * @param feature
     */
    public void addMarketingFeature(String tenantId, MarketingFeature feature) {
        
        Session session = entityManager.unwrap(Session.class);
        
        SubscriptionBundleMap subscriptionFeature = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingFeature.id", feature.getId()))
                .uniqueResult();            

        if (subscriptionFeature != null) {
            // Set updated config
            subscriptionFeature.setSubscriptionFeatureConfig(feature.getMarketingFeatureConfig());                
        } else {
            subscriptionFeature = new SubscriptionBundleMap();
            subscriptionFeature.setMarketingFeature(feature);
            subscriptionFeature.setSubscriptionFeatureConfig(feature.getMarketingFeatureConfig());
            subscriptionFeature.setTenantUUID(tenantId);
        }
        
        entityManager.merge(subscriptionFeature);
        entityManager.flush();
    }

    /**
     *
     * Get Marketing Feature Status
     * @param tenantId
     * @param feature
     * @return
     */
    public String getMarketingFeatureStatus(String tenantId, MarketingFeature feature) {

        Session session = entityManager.unwrap(Session.class);

        SubscriptionBundleMap subscriptionFeature = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingFeature.id", feature.getId()))
                .uniqueResult();

        if (subscriptionFeature == null || subscriptionFeature.getsubscriptionFeatureInfo() == null)
            return null;

        return subscriptionFeature.getsubscriptionFeatureInfo();
    }

    /**
     *
     * @param tenantId
     * @param feature
     * @return
     */
    public SubscriptionBundleMap getMarketingFeature(String tenantId, MarketingFeature feature) {

        Session session = entityManager.unwrap(Session.class);

        SubscriptionBundleMap subscriptionFeature = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingFeature.id", feature.getId()))
                .uniqueResult();

        if (subscriptionFeature == null)
            return null;

        return subscriptionFeature;
    }

    /**
     * Add feature info to subscription_bundle_map, update if it already exists
     * @param tenantId
     * @param feature
     */
    public void addMarketingFeature(String tenantId, MarketingFeature feature, String status) {

        Session session = entityManager.unwrap(Session.class);

        SubscriptionBundleMap subscriptionFeature = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingFeature.id", feature.getId()))
                .uniqueResult();

        if (subscriptionFeature != null) {
            // Set updated config
            subscriptionFeature.setSubscriptionFeatureConfig(feature.getMarketingFeatureConfig());
            subscriptionFeature.setSubscriptionFeatureInfo(status);
        } else {
            subscriptionFeature = new SubscriptionBundleMap();
            subscriptionFeature.setMarketingFeature(feature);
            subscriptionFeature.setSubscriptionFeatureConfig(feature.getMarketingFeatureConfig());
            subscriptionFeature.setSubscriptionFeatureInfo(status);
            subscriptionFeature.setTenantUUID(tenantId);
        }

        entityManager.merge(subscriptionFeature);
        entityManager.flush();
    }


    /**
     * Update feature config in subscription_bundle_map
     * @param tenantId
     * @param feature
     */
    public void updateMarketingFeatureConfig(String tenantId, MarketingFeature feature, String featureConfig) {

        Session session = entityManager.unwrap(Session.class);

        SubscriptionBundleMap subscriptionFeature = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingFeature.id", feature.getId()))
                .uniqueResult();

        if (subscriptionFeature != null) {
            subscriptionFeature.setSubscriptionFeatureConfig(featureConfig);
            entityManager.merge(subscriptionFeature);
            entityManager.flush();
        }

    }

    /**
     * Get provisioned bundle for tenantId
     * @param tenantId
     * @param bundle
     * @return
     */
    public SubscriptionBundleMap getProvisionBundle(String tenantId, MarketingBundle bundle) {

        Session session = entityManager.unwrap(Session.class);

        SubscriptionBundleMap subscriptionBundle = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingBundle.id", bundle.getId()))
                .uniqueResult();

        return subscriptionBundle;
    }
    
    /**
     * Add bundle info to subscription_bundle_map
     * @param tenantId
     * @param bundle
     */
    public void addMarketingBundle(String tenantId, MarketingBundle bundle) {
        
        SubscriptionBundleMap subBundleMap = new SubscriptionBundleMap();
        subBundleMap.setMarketingBundle(bundle);
        subBundleMap.setTenantUUID(tenantId);

        entityManager.persist(subBundleMap);        
    }

    /**
     * 
     * @param tenantId
     * @param featureId
     */
    public void deleteMarketingFeature(String tenantId, int featureId) {
       
        Session session = entityManager.unwrap(Session.class);
            
        SubscriptionBundleMap subscriptionBundle = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingFeature.id", featureId))
                .uniqueResult();
        
        if (subscriptionBundle != null) {
          entityManager.remove(subscriptionBundle);
        }
    }
    
    /**
     * Update Marketing Bundle
     * @param tenantId
     * @param newBundleId
     * @param oldBundleId
     */
    public void updateMarketingBundle(String tenantId, int newBundleId, int oldBundleId) {

        MarketingBundle newBundle = bundleMetadataDAO.getMarketingBundle(newBundleId);
                
        Session session = entityManager.unwrap(Session.class);
        
        SubscriptionBundleMap subscriptionBundle = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingBundle.id", oldBundleId))
                .uniqueResult();            
        
        subscriptionBundle.setMarketingBundle(newBundle);
        
        entityManager.merge(subscriptionBundle);
    }
    
    /**
     * 
     * @param tenantId
     * @param featureId
     */
    public MarketingFeature getFeatureInfoForSubscription(String tenantId, int featureId) {
        
        SubscriptionBundleMap subscriptionBundle = getSubscriptionFeatureEntry(tenantId, featureId);
        
        MarketingFeature feature = null;
        if (subscriptionBundle != null) {
            feature = subscriptionBundle.getMarketingFeature();
            feature.setMarketingFeatureConfig(subscriptionBundle.getSubscriptionFeatureConfig());
        }
        
        return feature;        
    }

    /**
     *
     * @param tenantId
     * @param bundleId
     */
    public void deleteMarketingBundle(String tenantId, int bundleId) {

        Session session = entityManager.unwrap(Session.class);

        SubscriptionBundleMap subscriptionBundle = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingBundle.id", bundleId))
                .uniqueResult();

        entityManager.remove(subscriptionBundle);
    }

    /**
     * Delete all subscription's bundles and features for a given subscription
     * Will be used mainly for migration v2 to Rubiks
     * @param tenantId
     */
    public void deleteBundlesAndFeatures(String tenantId) {

        Session session = entityManager.unwrap(Session.class);

        String hql = String.format("DELETE FROM SubscriptionBundleMap s where s.tenantUUID = :tenantID");

        Query query = session.createQuery(hql).setParameter("tenantID", tenantId);

        query.executeUpdate();
    }

    /**
     * 
     * @param tenantId
     * @param featureId
     */
    public SubscriptionBundleMap getSubscriptionFeatureEntry(String tenantId, int featureId) {
                
        Session session = entityManager.unwrap(Session.class);
        
        SubscriptionBundleMap subscriptionBundle = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingFeature.id", featureId))
                .uniqueResult();

        return subscriptionBundle;           
    }
    
    public SubscriptionBundleMap getSubscriptionBundleEntry(String tenantId, int bundleId) {
        Session session = entityManager.unwrap(Session.class);
        
        SubscriptionBundleMap subscriptionBundle = (SubscriptionBundleMap)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("tenantUUID", tenantId))
                .add(Restrictions.eq("marketingBundle.id", bundleId))
                .uniqueResult();

        return subscriptionBundle;
    }

    /**
     * Get all the subscriptions for the bundle
     * @param bundleId
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<String> getSubscriptionsForBundle(int bundleId) {
        
        Session session = entityManager.unwrap(Session.class);
        
        List<String> subscriptions = (List<String>)session.createCriteria(SubscriptionBundleMap.class)
                .add(Restrictions.eq("marketingBundle.id", bundleId))
                .setProjection(Property.forName("tenantUUID"))
                .list();
                
        return subscriptions;
    }

    /**
     * Given tenantId, Get all provisioned features which have podName configured already.
     * @param tenantId
     * @return
     */
    public List<SubscriptionBundleMap> getProvisionedPodFeatures(String tenantId) {

        Session session = entityManager.unwrap(Session.class);

        String hql = String.format("FROM SubscriptionBundleMap s " +
                " where s.marketingBundle.id is null" +
                "  and s.marketingFeature.developerConfig like :podName " +
                "  and tenantUUID = :tenantUUID");

        Query query = session.createQuery(hql);
        query.setParameter("podName", "%podName%");
        query.setParameter("tenantUUID", tenantId);

        List<SubscriptionBundleMap> subscriptionBundleMaps = query.list();

        return subscriptionBundleMaps;
    }
}
