package com.marketo.subscriptionmetadata.service;

import com.marketo.bundlemetadata.dao.BundleMetadataDAO;
import com.marketo.bundlemetadata.exception.BundleMetadataException;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.rest.model.BundlesFeatures;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundleFeatures;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundles;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.bundlemetadata.service.MarketingFeatureConfigurator;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

@Component
public class SubscriptionMetadataService {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionMetadataService.class);
    @Autowired
    private SubscriptionMetadataDAO subscriptionMetadataDAO;

    @Autowired
    private BundleMetadataService bundleMetadataService;

    @Autowired
    private BundleMetadataDAO bundleMetadataDAO;

    @Autowired
    private MarketingFeatureConfigurator featureConfigurator;

    /**
     * Get feature info for a tenantId and named feature.
     * 
     * @param tenantId
     * @param featureId
     */
    public MarketingFeature getAddonFeatureInfoForSubsription(String tenantId, int featureId) {
        return subscriptionMetadataDAO.getFeatureInfoForSubscription(tenantId, featureId);
    }

    /**
     * Get feature info for a tenantId and named bundle.
     * 
     * @param tenantId
     * @param bundleName
     */
    public MarketingBundle getBundleInfoForSubscription(String tenantId, String bundleName) {
        throw new UnsupportedOperationException("Request to get BundleInfo for a subscription not supported.");
    }

    /**
     * Get all the subscriptions which have this bundle.
     * 
     * @param bundleId
     * @return
     */
    public List<String> getSubscriptionsForBundle(int bundleId) {

        return subscriptionMetadataDAO.getSubscriptionsForBundle(bundleId);
    }

    /**
     * Compute bundle and addons that are provisioned for this subscription.
     * 
     * @param tenantId
     * @return
     */
    public SubscriptionBundleFeatures getBundlesFeatures(String tenantId) {

        List<SubscriptionBundleMap> subBundleMaps = subscriptionMetadataDAO.getAllBundleFeatures(tenantId);

        if (subBundleMaps.size() == 0) {
            throw new BundleMetadataException("No Subscripton Bundle Mapping found for: " + tenantId);
        }

        MarketingBundle rootBundle = subBundleMaps.get(0).getMarketingBundle();

        if (rootBundle == null) {
            for (SubscriptionBundleMap bundleMap : subBundleMaps) {
                if (bundleMap.getMarketingBundle() != null) {
                    rootBundle = bundleMap.getMarketingBundle();
                    break;
                }
            }
        }

        SubscriptionBundleFeatures subBundleFeatures = new SubscriptionBundleFeatures();
        subBundleFeatures.setTenantUUID(tenantId);
        subBundleFeatures.setBundle(rootBundle);

        for (int i = 1; i < subBundleMaps.size(); i++) {
            SubscriptionBundleMap bundleMap = subBundleMaps.get(i);
            boolean isBundle = bundleMap.getMarketingBundle() != null;
            MarketingBundle bundle = bundleMap.getMarketingBundle();
            MarketingFeature feature = bundleMap.getMarketingFeature();
            MarketingBundleFeature bundleFeature = new MarketingBundleFeature();

            if (isBundle) {
                bundleFeature.setBundle(bundle);
                subBundleFeatures.addAddon(bundleFeature);
            } else {
                // check if this feature comes as part of the bundle and if yes
                // copy the config to feature in bundle
                // we do this, as config in subscription_bundle_map overrides
                // what is available in bundle for this feature
                MarketingBundleFeature f = bundleMetadataService.getFeatureInBundle(rootBundle, feature.getId());
                if (f != null) {
                    f.setMarketingFeatureConfig(feature.getMarketingFeatureConfig());
                } else {
                    bundleFeature.setFeature(feature);
                    subBundleFeatures.addAddon(bundleFeature);
                }
            }
        }

        return subBundleFeatures;
    }

    /**
     * Compute modules (bundles) that are provisioned for this subscription. With overridden feature config if any
     *
     * @param tenantId
     * @return
     */
    public SubscriptionBundles getAvailableModules(String tenantId) {

        List<MarketingBundle> allBundles = bundleMetadataDAO.getAllBundles();
        List<SubscriptionBundleMap> subBundleMaps = subscriptionMetadataDAO.getAllBundleFeatures(tenantId);
        List<MarketingFeature> features = new ArrayList<>();

        // At least we should find the base module/bundle
        if (subBundleMaps.size() == 0) {
            throw new BundleMetadataException("No Subscription Bundle Mapping found for: " + tenantId);
        }

        SubscriptionBundles availableBundles = new SubscriptionBundles();
        availableBundles.setTenantUUID(tenantId);
        for (Iterator<MarketingBundle> iterator = allBundles.iterator(); iterator.hasNext();) {
            availableBundles.addBundle(iterator.next());
        }

        for (Iterator<SubscriptionBundleMap> iterator = subBundleMaps.iterator(); iterator.hasNext();) {
            SubscriptionBundleMap bundleMap = iterator.next();
            MarketingBundle bundle = bundleMap.getMarketingBundle();
            MarketingFeature feature = bundleMap.getMarketingFeature();

            if (bundleMap.isBundle()) {
                availableBundles.removeBundle(bundle);
            }
            else if(bundleMap.isFeature()) {
                features.add(feature);
            }
        }

        //  check if this feature comes as part of the bundle and if yes copy the config to feature in bundle
        //  we do this, as config in subscription_bundle_map overrides what is available in bundle for this feature
        for (Iterator<MarketingFeature> iterator = features.iterator(); iterator.hasNext();) {
            MarketingFeature feature = iterator.next();
            for (Iterator<MarketingBundle> bundleItr = availableBundles.getBundles().iterator(); bundleItr.hasNext();) {
                MarketingBundleFeature bundleFeature = bundleMetadataService.getFeatureInBundle(bundleItr.next(), feature.getId());
                if (bundleFeature != null) {
                    bundleFeature.getFeature().setMarketingFeatureConfig(feature.getMarketingFeatureConfig());
                }
            }
        }

        /*
         * For features which require pod selection, if the pod with same tag e.g rtp or rca was selected and provisioned
         * for this subscription part of a different feature then force preselect the pod as feature with same tag can not provisioned on different pods
         */
        Map<String,String > configuredPods = getConfiguredPods(tenantId);

        for (Iterator<MarketingBundle> bundleItr = availableBundles.getBundles().iterator(); bundleItr.hasNext();) {
            MarketingBundle bundle = bundleItr.next();
            if(bundle != null) {
                for (Iterator<MarketingBundleFeature> bundleFeatureItr = bundle.getChildBundleFeatures().iterator(); bundleFeatureItr.hasNext(); ) {
                    featureConfigurator.initializeFeature(tenantId, bundleFeatureItr.next().getFeature(), configuredPods);
                }
            }
        }
        availableBundles.removeEmptyBundles();
        return availableBundles;
    }

    /**
     * Compute modules (bundles) that are provisioned for this subscription. With overridden feature config if any
     *
     * @param tenantId
     * @return
     */
    public SubscriptionBundles getProvisionedModules(String tenantId) {

        List<SubscriptionBundleMap> subBundleMaps = subscriptionMetadataDAO.getAllBundleFeatures(tenantId);

        if (subBundleMaps.size() == 0) {
            throw new BundleMetadataException("No Subscription Bundle Mapping found for: " + tenantId);
        }

        SubscriptionBundles subBundles = new SubscriptionBundles();
        subBundles.setTenantUUID(tenantId);

        for (Iterator<SubscriptionBundleMap> iterator = subBundleMaps.iterator(); iterator.hasNext();) {
            SubscriptionBundleMap bundleMap = iterator.next();
            MarketingBundle bundle = bundleMap.getMarketingBundle();

            if (bundleMap.isBundle()) {
                subBundles.addBundle(bundle);
            }
        }

        return subBundles;
    }

    /**
     * Get all available bundles/features
     * 
     * @param tenantUUID
     * @return
     */
    public BundlesFeatures getAvailablesBundlesFeatures(String tenantUUID) {

        // Get existing bundles/feature for this sub
        SubscriptionBundleFeatures subBundleFeatures = getBundlesFeatures(tenantUUID);

        Set<Integer> existingBundles = new HashSet<>();
        Set<Integer> existingFeatures = new HashSet<>();

        List<MarketingBundleFeature> bundleFeatures = new ArrayList<>();
        bundleFeatures.addAll(subBundleFeatures.getBundle().getChildBundleFeatures());
        bundleFeatures.addAll(subBundleFeatures.getAddOns());

        for (MarketingBundleFeature bundleFeature : bundleFeatures) {
            if (bundleFeature.isBundle()) {
                existingBundles.add(bundleFeature.getBundle().getId());
            } else {
                existingFeatures.add(bundleFeature.getFeature().getId());
            }
        }

        List<MarketingBundle> bundles = bundleMetadataDAO.getAvailableBundles();
        for (Iterator<MarketingBundle> iterator = bundles.iterator(); iterator.hasNext();) {
            if (existingBundles.contains(iterator.next().getId())) {
                iterator.remove();
            }
        }

        List<MarketingFeature> features = bundleMetadataDAO.getAvailableFeatures();
        for (Iterator<MarketingFeature> iterator = features.iterator(); iterator.hasNext();) {
            if (existingFeatures.contains(iterator.next().getId())) {
                iterator.remove();
            }
        }

        featureConfigurator.initializeFeatures(tenantUUID, features);

        return new BundlesFeatures(bundles, features);
    }


    /**
     * Get all subscriptions which have this feature.
     *
     * @param featureName
     */
    public List<String> getSubscriptionsForFeature(String featureName) {
        return null;
    }

    public void addMarketingFeature(String tenantId, MarketingFeature feature) {
        subscriptionMetadataDAO.addMarketingFeature(tenantId, feature);
    }

    public SubscriptionBundleMap getMarketingFeature(String tenantId, MarketingFeature feature) {
        return subscriptionMetadataDAO.getMarketingFeature(tenantId, feature);
    }


    public void deleteBundlesAndFeatures(String tenantId) {
        subscriptionMetadataDAO.deleteBundlesAndFeatures(tenantId);
    }

    public void addMarketingFeature(String tenantId, MarketingFeature feature, String status) {
        subscriptionMetadataDAO.addMarketingFeature(tenantId, feature, status);
    }

    /**
     * Add Feature to Subscription Bundle Map
     * @param tenantId
     * @param featureName
     * @param featureConfig
     */
    public void addMarketingFeature(String tenantId, String featureName, String featureConfig, String status) {
        
        MarketingFeature feature = bundleMetadataDAO.getMarketingFeature(featureName);
        if (feature != null) {
            feature.setMarketingFeatureConfig(featureConfig);
            if (isFeatureProvisioned(tenantId, feature.getId())) {
                // If feature is already provisioned this sub, do not do anything if there is no config.
                if (!StringUtils.isEmpty(featureConfig)) {
                    subscriptionMetadataDAO.addMarketingFeature(tenantId, feature, status);
                }
            } else {
                subscriptionMetadataDAO.addMarketingFeature(tenantId, feature, status);
            }           
        } else {
            throw new RuntimeException("Invalid Marketing Feature: " + featureName);
        }        
    }
    
    public void addMarketingBundle(String tenantId, MarketingBundle bundle) {
        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle);
    }

    public void addMarketingBundle(String tenantId, String bundleName) {
        
        MarketingBundle bundle = bundleMetadataDAO.getMarketingBundle(bundleName);
        
        if (bundle != null) {            
            if (subscriptionMetadataDAO.getSubscriptionBundleEntry(tenantId, bundle.getId()) == null) {
                subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle);           
            }            
        } else {
            throw new RuntimeException("Invalid Marketing Bundle: " + bundleName);
        }        
     }

    /**
     * 
     * @param tenantId
     * @param featureId
     */
    public void deleteMarketingFeature(String tenantId, int featureId) {
        subscriptionMetadataDAO.deleteMarketingFeature(tenantId, featureId);
    }

    public void updateMarketingBundle(String tenantId, int newBundleId, int oldBundleId) {
        subscriptionMetadataDAO.updateMarketingBundle(tenantId, newBundleId, oldBundleId);
    }

    /**
     * Is Feature Provisioned
     * @param tenantId
     * @param featureId
     * @return
     */
    public boolean isFeatureProvisioned(String tenantId, int featureId) {

        SubscriptionBundleFeatures bundleFeatures = getBundlesFeatures(tenantId);

        MarketingBundleFeature feature = bundleMetadataService
                .getFeatureInBundle(bundleFeatures.getBundle(), featureId);

        MarketingFeature addOnFeature = getAddonFeatureInfoForSubsription(tenantId, featureId);

        boolean isProvisioned = (feature != null);

        isProvisioned |= (addOnFeature != null);

        return isProvisioned;
    }

    /**
     * Feature usage featureId -> count (in how many PROVISIONED bundle it has been used given tenantId )
     * @param tenantId
     * @return
     */
    public Map<Integer, Integer> getFeatureUsedByCount(String tenantId) {
        // Feature usage featureId -> count (in how many bundle it has been used and provisioned)
        Map<Integer, Integer> featuresUsedByCount = new HashMap<>();

        // Get all provisioned bundles for this subscription
        List<SubscriptionBundleMap> subBundleMaps = subscriptionMetadataDAO.getAllBundleFeatures(tenantId);

        for (Iterator<SubscriptionBundleMap> subBundleMapItr = subBundleMaps.iterator(); subBundleMapItr.hasNext();) {

            SubscriptionBundleMap subBundleMap = subBundleMapItr.next();

            if (subBundleMap.isFeature()) continue; // skip features (already part of bundles)

            for(MarketingBundleFeature bundleFeature: subBundleMap.getMarketingBundle().getChildBundleFeatures()) {
                Integer featureId = bundleFeature.getFeature().getId();
                Integer currCount = featuresUsedByCount.get(featureId);
                currCount = (currCount == null) ? 0 : currCount;
                featuresUsedByCount.put(featureId, currCount + 1);
            }
        }
        return featuresUsedByCount;
    }

    /**
     * Given tenantId, return configured pods as a MAP key is the tag (e.g. rca, rtp)
     * and the value is the pod selection which was made in UI at the provisioning time
     * @param tenantId
     * @return
     */
    public Map<String, String> getConfiguredPods(String tenantId) {

        List<SubscriptionBundleMap> subBundleMaps = subscriptionMetadataDAO.getProvisionedPodFeatures(tenantId);

        Map<String, String> configuredPods = new HashMap<>();
        try {
            for (Iterator<SubscriptionBundleMap> itr = subBundleMaps.iterator(); itr.hasNext();) {
                SubscriptionBundleMap bundleMap = itr.next();
                MarketingFeature feature = bundleMap.getMarketingFeature();
                JSONArray devConfigArray = new JSONArray(feature.getDeveloperConfig());
                JSONObject subFeatureConfig = new JSONObject(bundleMap.getSubscriptionFeatureConfig());

                for (Object obj : devConfigArray) {
                    JSONObject config = (JSONObject) obj;
                    JSONObject nameObj =  config.getJSONObject("name");
                    if (nameObj.get("field").equals("podName")) {
                        JSONObject metadata = config.getJSONObject("metadata");
                        configuredPods.put(metadata.getString("tag"), subFeatureConfig.getString("podName"));
                    }
                }
            }
        }
        catch (Exception e) {
            logger.error("Error getting configured pods info", e);
        }

        return configuredPods;
    }

    /**
     * updateConfig provisioned feature config in subscription_bundle_map,
     * e.g. used to update the RCA pod after move, could be called from CPT move tool
     * @param tenantId
     * @param code, feature code
     * @param featureConfig
     */
    public void updateMarketingFeatureConfig(String tenantId, String code, String featureConfig) {
        MarketingFeature feature = bundleMetadataDAO.getMarketingFeatureByCode(code);
        if (feature != null && !StringUtils.isEmpty(featureConfig)) {
            subscriptionMetadataDAO.updateMarketingFeatureConfig(tenantId, feature, featureConfig);
        } else {
            throw new RuntimeException("Invalid Marketing Feature, code: " + code);
        }
    }

}
