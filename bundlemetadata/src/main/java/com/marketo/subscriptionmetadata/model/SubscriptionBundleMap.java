package com.marketo.subscriptionmetadata.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.marketo.bundlemetadata.model.BaseEntity;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;

@Entity
@Table(name="subscription_bundle_map")
public class SubscriptionBundleMap extends BaseEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7428632980653611758L;

    @OneToOne
    @JoinColumn(name = "marketing_app_bundle_id")
    private MarketingBundle marketingBundle;
    
    @OneToOne
    @JoinColumn(name = "marketing_app_feature_id")
    private MarketingFeature marketingFeature;
    
    @Column(name = "tenant_uuid")
    private String tenantUUID;
    
    @Column(name = "subscription_feature_config", length = 2000)
    private String subscriptionFeatureConfig;

    @Column(name = "subscription_feature_info", length = 2000)
    private String subscriptionFeatureInfo;
    
    public int getId() {
        return id;
    }

    public MarketingBundle getMarketingBundle() {
        return marketingBundle;
    }

    public void setMarketingBundle(MarketingBundle marketingBundle) {
        this.marketingBundle = marketingBundle;
    }

    public MarketingFeature getMarketingFeature() {
        return marketingFeature;
    }

    public void setMarketingFeature(MarketingFeature marketingFeature) {
        this.marketingFeature = marketingFeature;
    }

    public String getTenantUUID() {
        return tenantUUID;
    }

    public void setTenantUUID(String tenantUUID) {
        this.tenantUUID = tenantUUID;
    }

    public String getSubscriptionFeatureConfig() {
        return subscriptionFeatureConfig;
    }

    public String getsubscriptionFeatureInfo() {
        return subscriptionFeatureInfo;
    }

    public void setSubscriptionFeatureInfo(String subscriptionFeatureInfo) {
        this.subscriptionFeatureInfo = subscriptionFeatureInfo;
    }

    public void setSubscriptionFeatureConfig(String subscriptionFeatureConfig) {
        this.subscriptionFeatureConfig = subscriptionFeatureConfig;
    }

    public boolean isBundle() {
        return marketingBundle != null && marketingFeature == null;
    }

    public boolean isFeature() {
        return marketingBundle == null && marketingFeature != null;
    }
}
