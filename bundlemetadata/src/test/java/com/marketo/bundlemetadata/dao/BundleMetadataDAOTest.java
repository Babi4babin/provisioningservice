package com.marketo.bundlemetadata.dao;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;

@ContextConfiguration(locations = { "classpath:bundlemetadata-test-config.xml" })
public class BundleMetadataDAOTest extends AbstractTestNGSpringContextTests {


  public static final Integer BASE_BUNDLE_ID = 1;
  public static final String BASE_BUNDLE_NAME = "Base Module";
  public static final String BASE_BUNDLE_CODE = "baseModule";
  public static final Integer BASE_FEATURE_ID = 1;
  public static final String BASE_FEATURE_NAME = "Base";
  public static final String BASE_FEATURE_CODE = "baseFeature";
  public static final String PROGRAM_ANALYSIS_BUNDLE_NAME= "Program Analysis";



  @Autowired
  BundleMetadataDAO bundleMetadataDAO;

  @Test
  public void testGetMarketingBundleById(){
    MarketingBundle bundle = bundleMetadataDAO.getMarketingBundle(BASE_BUNDLE_ID);
    Assert.assertEquals(bundle.getName(), BASE_BUNDLE_NAME);
  }

  @Test
  public void testGetMarketingBundleByName(){
    MarketingBundle bundle = bundleMetadataDAO.getMarketingBundle(BASE_BUNDLE_NAME);
    Assert.assertEquals((Integer)bundle.getId(), BASE_BUNDLE_ID);
  }

  @Test
  public void testGetMarketingBundleByCode(){
    MarketingBundle bundle = bundleMetadataDAO.getMarketingBundleByCode(BASE_BUNDLE_CODE);
    Assert.assertEquals(bundle.getName(), BASE_BUNDLE_NAME);
  }

  @Test
  public void testGetMarketingFeatureByName(){
    MarketingFeature feature = bundleMetadataDAO.getMarketingFeature(BASE_FEATURE_NAME);
    Assert.assertEquals(feature.getName(), BASE_FEATURE_NAME);
  }

  @Test
  public void testGetMarketingFeatureById(){
    MarketingFeature feature = bundleMetadataDAO.getMarketingFeature(BASE_FEATURE_ID);
    Assert.assertEquals(feature.getName(), BASE_FEATURE_NAME);
  }

  @Test
  public void testGetMarketingFeatureByCode(){
    MarketingFeature feature = bundleMetadataDAO.getMarketingFeatureByCode(BASE_FEATURE_CODE);
    Assert.assertEquals(feature.getName(), BASE_FEATURE_NAME);
  }

  @Test
  public void testGetMarketingFeatureId(){
    Integer featureId = bundleMetadataDAO.getMarketingFeatureId(BASE_FEATURE_NAME);
    Assert.assertEquals(featureId, BASE_FEATURE_ID);
  }

  @Test(expectedExceptions = RuntimeException.class)
  public void testGetMarketingFeatureIdForNonExistingFeature(){
    bundleMetadataDAO.getMarketingFeatureId(RandomStringUtils.random(5));
  }

  @Test
  public void testIsChildBundle(){
    boolean isChild = bundleMetadataDAO.isChildBundle(BASE_BUNDLE_ID, BASE_FEATURE_ID);
    Assert.assertTrue(isChild);
  }

  @Test
  public void testIsChildFeature(){
    boolean isChild = bundleMetadataDAO.isChildFeature(BASE_BUNDLE_ID, BASE_FEATURE_ID);
    Assert.assertTrue(isChild);
  }

  @Test
  public void testGetRootBundles(){
    List<MarketingBundle> marketingBundleList;
    marketingBundleList = bundleMetadataDAO.getRootBundles();
    Assert.assertTrue(marketingBundleList.size()>0);
    Assert.assertEquals(marketingBundleList.get(0).getName(), BASE_BUNDLE_NAME);
  }

  @Test
  public void testGetAllBundles(){
    List<MarketingBundle> marketingBundleList;
    marketingBundleList = bundleMetadataDAO.getAllBundles();
    Assert.assertTrue(marketingBundleList.size()>0);
    Assert.assertEquals(marketingBundleList.get(0).getName(), BASE_BUNDLE_NAME);
  }

  @Test
  public void testGetAvailableFeatures(){
    List<MarketingFeature> marketingFeatureList;
    marketingFeatureList = bundleMetadataDAO.getAvailableFeatures();
    Assert.assertTrue(marketingFeatureList.size()>0);
    Assert.assertEquals(marketingFeatureList.get(0).getName(), BASE_FEATURE_NAME);
  }

  @Test
  public void testGetBundleUpdatesBetween(){
    List<MarketingBundleFeature> marketingBundleFeatureList;
    Date dateNow = new Date();
    Date dateEarlier = new Date(0);
    marketingBundleFeatureList = bundleMetadataDAO.getBundleUpdatesBetween(dateEarlier, dateNow);
    Assert.assertTrue(marketingBundleFeatureList.size()>0);
  }

  @Test
  public void testGetRootBundle(){
    MarketingBundle childBundle = bundleMetadataDAO.getMarketingBundle(BASE_BUNDLE_NAME);
    MarketingBundle marketingBundle = bundleMetadataDAO.getRootBundle(childBundle);
    Assert.assertEquals(marketingBundle.getName(), BASE_BUNDLE_NAME);
  }

  @Test
  public void testGetMarketingBundleFeatureId(){
    int marketingBundleFeatureId = bundleMetadataDAO.getMarketingBundleFeatureId(BASE_BUNDLE_NAME, "Dynamic Content");
    Assert.assertEquals(marketingBundleFeatureId, 1);
  }

  @Test
  public void testGetRootBundle2(){
    MarketingBundle childBundle = bundleMetadataDAO.getMarketingBundle(PROGRAM_ANALYSIS_BUNDLE_NAME);
    MarketingBundle parentBundle = bundleMetadataDAO.getRootBundle(childBundle);
    Assert.assertEquals(parentBundle.getName(), PROGRAM_ANALYSIS_BUNDLE_NAME);
  }

  @Test
  public void testIsChildFeature2(){
    boolean isChild = bundleMetadataDAO.isChildFeature(BASE_FEATURE_ID, 50);
    Assert.assertFalse(isChild);
  }
}
