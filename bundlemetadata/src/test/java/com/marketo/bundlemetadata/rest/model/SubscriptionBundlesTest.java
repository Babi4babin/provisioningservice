package com.marketo.bundlemetadata.rest.model;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

/**
 * Created by talkhateeb on 12/16/15.
 */
public class SubscriptionBundlesTest {

    @Test
    public void testAddBundle() throws Exception {
        MarketingBundle bundle = new MarketingBundle();
        MarketingBundle bundle1 = new MarketingBundle();
        bundle.setId(10);
        bundle1.setId(20);
        bundle.setName("Test Bundle");
        bundle1.setName("Test Bundle1");
        SubscriptionBundles bundles = new SubscriptionBundles();
        bundles.addBundle(bundle);
        bundles.addBundle(bundle1);
        assertThat(bundles.getBundleMap().get(bundle.getId()), equalTo(0));
        assertThat(bundles.getBundleMap().get(bundle1.getId()), equalTo(1));
    }

    @Test
    public void removeBundle() {
        MarketingBundle bundle = new MarketingBundle();
        MarketingBundle bundle1 = new MarketingBundle();
        bundle.setId(10);
        bundle1.setId(20);
        bundle.setName("Test Bundle");
        bundle1.setName("Test Bundle1");

        SubscriptionBundles bundles = new SubscriptionBundles();
        bundles.addBundle(bundle);
        bundles.addBundle(bundle1);

        bundles.removeBundle(bundle1);

        assertThat(bundles.getBundleMap().get(bundle1.getId()), equalTo(null));
        bundles.removeEmptyBundles();
        assertThat(bundles.getBundles().size(), equalTo(1));
    }
}