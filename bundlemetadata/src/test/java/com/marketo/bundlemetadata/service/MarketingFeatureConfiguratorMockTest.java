package com.marketo.bundlemetadata.service;

import com.marketo.bundlemetadata.model.MarketingFeature;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.*;

import static org.mockito.Mockito.*;

public class MarketingFeatureConfiguratorMockTest {


    @Mock
    private PodListService podListService;

    @InjectMocks
    private  MarketingFeatureConfigurator marketingFeatureConfigurator;

    private String munchkinId="";


    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @BeforeMethod
    public void init(){
        //need different munchkinid for verify call
        munchkinId=UUID.randomUUID().toString();
    }

    @Test
    public void testInitializeFeaturesEmpty() throws Exception{
        List<MarketingFeature> features=spy(new ArrayList<MarketingFeature>());
        marketingFeatureConfigurator.initializeFeatures(munchkinId,features);
        verify(podListService,times(0)).getActivePods(munchkinId,"tag1");
    }

    @Test
    public void testInitializeFeaturesWhenDeveloperCfgEmpty() throws Exception{

        List<MarketingFeature> features=spy(new ArrayList<MarketingFeature>());
        MarketingFeature marketingFeature=mock(MarketingFeature.class);
        features.add(marketingFeature);
        marketingFeatureConfigurator.initializeFeatures(munchkinId,features);
        verify(podListService,times(0)).getActivePods(munchkinId,"tag1");
    }

    @Test
    public void testInitializeFeaturesScenario1() throws Exception{

        List<MarketingFeature> features=spy(new ArrayList<MarketingFeature>());
        MarketingFeature marketingFeature=mock(MarketingFeature.class);

        String developerConfig="[{\"metadata\":{\"tag\":\"tag1\"},\"defaultValue\":" +
                "{\"testVal\":\"testVal\"},\"" +
                "name\":{\"field\":\"fieldVal\"},\"submitField\":\"" +
                "testVal\",\"type\":\"picklist\"}]\n";

        marketingFeature.setDeveloperConfig(developerConfig);
        when(marketingFeature.getDeveloperConfig()).thenReturn(developerConfig);
        features.add(marketingFeature);
        marketingFeatureConfigurator.initializeFeatures(munchkinId,features);
        verify(podListService,times(0)).getActivePods(munchkinId,"tag1");



    }

    @Test
    public void testInitializeFeaturesScenario2() throws Exception{

        List<MarketingFeature> features=spy(new ArrayList<MarketingFeature>());
        MarketingFeature marketingFeature=mock(MarketingFeature.class);

        String developerConfig="[{\"metadata\":{\"tag\":\"tag1\"},\"defaultValue\":" +
                "{\"testVal\":\"testVal\"},\"" +
                "name\":{\"field\":\"podName\"},\"submitField\":\"" +
                "testVal\",\"type\":\"picklist\"}]\n";

        marketingFeature.setDeveloperConfig(developerConfig);
        when(marketingFeature.getDeveloperConfig()).thenReturn(developerConfig);
        features.add(marketingFeature);
        marketingFeatureConfigurator.initializeFeatures(munchkinId,features);
        verify(podListService,times(1)).getActivePods(munchkinId,"tag1");


    }

    @Test
    public void testInitializeFeaturesScenario3() throws Exception{

        List<MarketingFeature> features=spy(new ArrayList<MarketingFeature>());
        MarketingFeature marketingFeature=mock(MarketingFeature.class);

        String developerConfig="[{\"metadata\":{\"tag\":\"tag1\"},\"defaultValue\":" +
                "{\"testVal\":\"testVal\"},\"" +
                "name\":null,\"submitField\":\"" +
                "testVal\",\"type\":\"picklist\"}]\n";

        marketingFeature.setDeveloperConfig(developerConfig);
        when(marketingFeature.getDeveloperConfig()).thenReturn(developerConfig);
        features.add(marketingFeature);
        marketingFeatureConfigurator.initializeFeatures(munchkinId,features);
        verify(podListService,times(0)).getActivePods(munchkinId,"tag1");



    }

    @Test
    public void testInitializeFeatureWithPods() throws Exception{
        MarketingFeature marketingFeature=mock(MarketingFeature.class)
                ;
        String developerConfig="[{\"metadata\":{\"tag\":\"tag1\"},\"defaultValue\":" +
                "{\"testVal\":\"testVal\"},\"" +
                "name\":{\"field\":\"podName\"},\"submitField\":\"" +
                "testVal\",\"type\":\"picklist\"}]\n";

        marketingFeature.setDeveloperConfig(developerConfig);
        when(marketingFeature.getDeveloperConfig()).thenReturn(developerConfig);
        Map<String, String> configuredPods=new HashMap<>();
        marketingFeatureConfigurator.initializeFeature(munchkinId,marketingFeature,configuredPods);
        verify(podListService,times(1)).getActivePods(munchkinId,"tag1");

    }

    @Test
    public void testGetDefaultFeatureConfig() throws Exception{
        MarketingFeature marketingFeature=mock(MarketingFeature.class);

        String developerConfig="[{\"metadata\":{\"tag\":\"tag1\"},\"defaultValue\":" +
                "{\"testVal\":\"testVal\"},\"" +
                "name\":{\"field\":\"fieldVal\"},\"submitField\":\"" +
                "testVal\",\"type\":\"picklist\"}]\n";

        marketingFeature.setDeveloperConfig(developerConfig);
        when(marketingFeature.getDeveloperConfig()).thenReturn(developerConfig);
        Assert.assertEquals("{\"fieldVal\":\"testVal\"}"
                ,marketingFeatureConfigurator.getDefaultFeatureConfig(munchkinId,marketingFeature));
        verify(podListService,times(0)).getActivePods(munchkinId,"tag1");
    }





}
