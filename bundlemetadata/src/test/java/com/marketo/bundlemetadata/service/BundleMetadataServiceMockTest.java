package com.marketo.bundlemetadata.service;

import com.marketo.bundlemetadata.dao.BundleMetadataDAO;
import com.marketo.bundlemetadata.exception.BundleMetadataException;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingBundleFeature;
import com.marketo.bundlemetadata.model.MarketingFeature;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.*;

import static org.mockito.Mockito.*;

public class BundleMetadataServiceMockTest {


   @Mock
   private BundleMetadataDAO bundleMetadataDAO;

   @Mock
   private MarketingFeature marketingFeature;

   @Mock
   private MarketingBundle marketingBundle;

   @InjectMocks
   private BundleMetadataService bundleMetadataService;


   @BeforeClass
   public void setUp(){
       MockitoAnnotations.initMocks(this);
   }


   @Test
    public void testGetMarketingFeature() throws Exception{
       Integer featureId=new Random().nextInt();
       when(bundleMetadataDAO.getMarketingFeature(featureId)).thenReturn(marketingFeature);
       Assert.assertEquals(marketingFeature,bundleMetadataService.getMarketingFeature(featureId));

   }

    @Test
    public void testGetMarketingFeatureByCode() throws Exception{
        String featureCode= UUID.randomUUID().toString();
        when(bundleMetadataDAO.getMarketingFeatureByCode(featureCode)).thenReturn(marketingFeature);
        Assert.assertEquals(marketingFeature,bundleMetadataService.getMarketingFeatureByCode(featureCode));
    }

    @Test
    public void testGetMarketingFeatureCharId() throws Exception{
        String featureId=UUID.randomUUID().toString();
        when(bundleMetadataDAO.getMarketingFeature(featureId)).thenReturn(marketingFeature);
        Assert.assertEquals(marketingFeature,bundleMetadataService.getMarketingFeature(featureId));

    }

    @Test
    public void testGetMarketingBundle() throws Exception{
       String bundleName=UUID.randomUUID().toString();
       when(bundleMetadataDAO.getMarketingBundle(bundleName)).thenReturn(marketingBundle);
       Assert.assertEquals(marketingBundle,bundleMetadataService.getMarketingBundle(bundleName));

    }

    @Test(expectedExceptions = BundleMetadataException.class)
    public void testGetMarketingBundleWithEmptyBundle() throws Exception{
        String bundleName=UUID.randomUUID().toString();
        MarketingBundle marketingBundle=null;
        when(bundleMetadataDAO.getMarketingBundle(bundleName)).thenReturn(marketingBundle);
        bundleMetadataService.getMarketingBundle(bundleName);
    }


    @Test
    public void testGetMarketingBundleByCode() throws Exception{
        String bundleName=UUID.randomUUID().toString();
        when(bundleMetadataDAO.getMarketingBundleByCode(bundleName)).thenReturn(marketingBundle);
        Assert.assertEquals(marketingBundle,bundleMetadataService.getMarketingBundleByCode(bundleName));

    }

    @Test(expectedExceptions = BundleMetadataException.class)
    public void testGetMarketingBundleByCodeWithEmptyBundle() throws Exception{
        String bundleName=UUID.randomUUID().toString();
        MarketingBundle marketingBundle=null;
        when(bundleMetadataDAO.getMarketingBundleByCode(bundleName)).thenReturn(marketingBundle);
        bundleMetadataService.getMarketingBundleByCode(bundleName);

    }

    @Test
    public void testGetMarketingBundleByBundleId() throws Exception{
       Integer bundleId=new Random().nextInt();
       when(bundleMetadataDAO.getMarketingBundle(bundleId)).thenReturn(marketingBundle);
       Assert.assertEquals(marketingBundle,bundleMetadataService.getMarketingBundle(bundleId));
    }

    @Test(expectedExceptions = BundleMetadataException.class)
    public void testGetMarketingBundleByBundleIdExceptionScenario() throws Exception{
        Integer bundleId=new Random().nextInt();
        MarketingBundle marketingBundle=null;
        when(bundleMetadataDAO.getMarketingBundle(bundleId)).thenReturn(marketingBundle);
        bundleMetadataService.getMarketingBundle(bundleId);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testGetAllBundles() throws Exception{
        bundleMetadataService.getAllBundles();
    }


    @Test
    public void testGetRootBundles() throws Exception{
        List<MarketingBundle> marketingBundles=spy(new ArrayList<MarketingBundle>());
        when(bundleMetadataDAO.getRootBundles()).thenReturn(marketingBundles);
        bundleMetadataService.getRootBundles();
        verify(bundleMetadataDAO,times(1)).getRootBundles();
    }


    @Test
    public void testGetFeatureInBundleScenario1() throws Exception{
        Integer featureId=new Random().nextInt();
        MarketingBundleFeature marketingBundleFeature=mock(MarketingBundleFeature.class);
        when(marketingBundleFeature.isBundle()).thenReturn(true);
        List<MarketingBundleFeature> marketingBundleFeatures=spy(new ArrayList<MarketingBundleFeature>());
        marketingBundleFeatures.add(marketingBundleFeature);
        when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
        Assert.assertNull(bundleMetadataService.getFeatureInBundle(marketingBundle,featureId));
    }

    @Test
    public void testGetFeatureInBundleScenario2() throws Exception{
        Integer featureId=new Random().nextInt();
        MarketingBundleFeature marketingBundleFeature=mock(MarketingBundleFeature.class);
        MarketingFeature marketingFeature=mock(MarketingFeature.class);
        when(marketingFeature.getId()).thenReturn(featureId);
        when(marketingBundleFeature.isBundle()).thenReturn(false);
        when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
        List<MarketingBundleFeature> marketingBundleFeatures=spy(new ArrayList<MarketingBundleFeature>());
        marketingBundleFeatures.add(marketingBundleFeature);
        when(marketingBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
        Assert.assertNotNull(bundleMetadataService.getFeatureInBundle(marketingBundle,featureId));
    }


    @Test
    public void testCompareBundlesWithOldBundlesScenario1() throws Exception{
        MarketingBundle oldBundle=mock(MarketingBundle.class);
        MarketingBundle newBundle=mock(MarketingBundle.class);

        List<MarketingBundleFeature> marketingBundleFeatures=spy(new ArrayList<MarketingBundleFeature>());

        MarketingBundleFeature marketingBundleFeature=mock(MarketingBundleFeature.class);
        MarketingBundle marketingBundle=mock(MarketingBundle.class);
        when(marketingBundle.getId()).thenReturn(new Random().nextInt());

        when(marketingBundleFeature.getBundle()).thenReturn(marketingBundle);
        when(marketingBundleFeature.isBundle()).thenReturn(true);

        marketingBundleFeatures.add(marketingBundleFeature);
        when(oldBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
        Assert.assertNotNull(bundleMetadataService.compareBundles(oldBundle,newBundle));
    }

    @Test
    public void testCompareBundlesWithOldBundlesScenario2() throws Exception{
        MarketingBundle oldBundle=mock(MarketingBundle.class);
        MarketingBundle newBundle=mock(MarketingBundle.class);

        List<MarketingBundleFeature> marketingBundleFeatures=spy(new ArrayList<MarketingBundleFeature>());

        MarketingBundleFeature marketingBundleFeature=mock(MarketingBundleFeature.class);
        MarketingFeature marketingFeature=mock(MarketingFeature.class);
        when(marketingBundle.getId()).thenReturn(new Random().nextInt());

        when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
        when(marketingBundleFeature.isBundle()).thenReturn(false);
        when(marketingBundleFeature.isFeature()).thenReturn(true);


        marketingBundleFeatures.add(marketingBundleFeature);
        when(oldBundle.getChildBundleFeatures()).thenReturn(marketingBundleFeatures);
        Assert.assertNotNull(bundleMetadataService.compareBundles(oldBundle,newBundle));
    }

    @Test
    public void testCompareBundlesWithOldBundlesScenario3() throws Exception{
        MarketingBundle oldBundle=mock(MarketingBundle.class);
        MarketingBundle newBundle=mock(MarketingBundle.class);

        List<MarketingBundleFeature> marketingOldBundleFeatures=spy(new ArrayList<MarketingBundleFeature>());
        List<MarketingBundleFeature> marketingNewBundleFeatures=spy(new ArrayList<MarketingBundleFeature>());

        MarketingBundleFeature marketingBundleFeature=mock(MarketingBundleFeature.class);

        MarketingBundle marketingBundle=mock(MarketingBundle.class);
        Integer bundleId=new Random().nextInt();
        when(marketingBundle.getId()).thenReturn(bundleId);
        when(marketingBundleFeature.getBundle()).thenReturn(marketingBundle);
        when(marketingBundleFeature.isBundle()).thenReturn(true);

        MarketingBundleFeature marketingBundleFeature2=mock(MarketingBundleFeature.class);

        MarketingBundle marketingBundleItem2=mock(MarketingBundle.class);
        when(marketingBundleItem2.getId()).thenReturn(++bundleId);
        when(marketingBundleFeature2.getBundle()).thenReturn(marketingBundleItem2);
        when(marketingBundleFeature2.isBundle()).thenReturn(true);

        marketingNewBundleFeatures.add(marketingBundleFeature);
        marketingNewBundleFeatures.add(marketingBundleFeature2);
        marketingOldBundleFeatures.add(marketingBundleFeature);

        when(newBundle.getChildBundleFeatures()).thenReturn(marketingNewBundleFeatures);
        when(oldBundle.getChildBundleFeatures()).thenReturn(marketingOldBundleFeatures);
        Assert.assertNotNull(bundleMetadataService.compareBundles(oldBundle,newBundle));
    }


    @Test
    public void testCompareBundlesWithOldBundlesScenario4() throws Exception{
        MarketingBundle oldBundle=mock(MarketingBundle.class);
        MarketingBundle newBundle=mock(MarketingBundle.class);

        List<MarketingBundleFeature> marketingOldBundleFeatures=spy(new ArrayList<MarketingBundleFeature>());
        List<MarketingBundleFeature> marketingNewBundleFeatures=spy(new ArrayList<MarketingBundleFeature>());

        MarketingBundleFeature marketingBundleFeature=mock(MarketingBundleFeature.class);

        MarketingFeature marketingFeature=mock(MarketingFeature.class);
        Integer featureId=new Random().nextInt();
        when(marketingFeature.getId()).thenReturn(featureId);
        when(marketingBundleFeature.getFeature()).thenReturn(marketingFeature);
        when(marketingBundleFeature.isBundle()).thenReturn(false);
        when(marketingBundleFeature.isFeature()).thenReturn(true);

        MarketingBundleFeature marketingBundleFeature2=mock(MarketingBundleFeature.class);

        MarketingFeature marketingFeatureItem2=mock(MarketingFeature.class);
        when(marketingFeatureItem2.getId()).thenReturn(++featureId);
        when(marketingBundleFeature2.getFeature()).thenReturn(marketingFeatureItem2);
        when(marketingBundleFeature2.isBundle()).thenReturn(false);
        when(marketingBundleFeature2.isFeature()).thenReturn(true);


        marketingNewBundleFeatures.add(marketingBundleFeature);
        marketingNewBundleFeatures.add(marketingBundleFeature2);
        marketingOldBundleFeatures.add(marketingBundleFeature);

        when(newBundle.getChildBundleFeatures()).thenReturn(marketingNewBundleFeatures);
        when(oldBundle.getChildBundleFeatures()).thenReturn(marketingOldBundleFeatures);
        Assert.assertNotNull(bundleMetadataService.compareBundles(oldBundle,newBundle));
    }



    @Test
    public void testGetBundleUpdatesBetween() throws Exception{
        List<MarketingBundleFeature> marketingBundleFeatures=spy(new ArrayList<MarketingBundleFeature>());
        when(bundleMetadataDAO.getBundleUpdatesBetween(any(Date.class), any(Date.class))).thenReturn(marketingBundleFeatures);
        Assert.assertNotNull(bundleMetadataService.getBundleUpdatesBetween(new Date(),new Date()));

    }

    @Test
    public void testGetRootBundle() throws Exception{
        when(bundleMetadataDAO.getRootBundle(marketingBundle)).thenReturn(marketingBundle);
        Assert.assertEquals(marketingBundle,bundleMetadataService.getRootBundle(marketingBundle));
    }

}
