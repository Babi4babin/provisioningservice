package com.marketo.bundlemetadata.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.rest.model.BundleDiffInfo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


@ContextConfiguration(locations = { "classpath:bundlemetadata-test-config.xml" })
public class BundleMetadataServiceTest extends AbstractTestNGSpringContextTests {
    
    @Autowired
    private BundleMetadataService bundleMetadataService;

    @Value("${orion.auth.token}")
    private String ORION_AUTH_TOKEN;

    @Test
    public void testGetBundle() {

        Assert.assertEquals(ORION_AUTH_TOKEN,"#ORION_AUTH_TOKEN#");

        String bundleName = "Base Module";

        MarketingBundle bundle = bundleMetadataService.getMarketingBundle(bundleName);

        assertThat(bundle.getName(), equalTo("Base Module"));
        assertThat(bundle.getCode(), equalTo("baseModule"));
        assertThat(bundle.getDescription(), equalTo("A module for all non-paid features"));
        assertThat(bundle.getActive(), equalTo(1));

        assertThat(bundle.getChildBundleFeatures().size(), equalTo(14));
        assertThat(bundle.getChildBundleFeatures().get(1).getFeature().getName(), equalTo("Dynamic Content"));
        assertThat(bundle.getChildBundleFeatures().get(2).getFeature().getName(), equalTo("SSO"));
        assertThat(bundle.getChildBundleFeatures().get(3).getFeature().getName(), equalTo("Email AB Testing"));
        assertThat(bundle.getChildBundleFeatures().get(4).getFeature().getName(), equalTo("Landing Page AB Test"));
    }

    @Test
    public void testGetFeature() {

        int featureId = 1;
        MarketingFeature feature = bundleMetadataService.getMarketingFeature(featureId);

        assertThat(feature.getName(), equalTo("Base"));
        assertThat(feature.getCode(), equalTo("baseFeature"));
        assertThat(feature.getActive(), equalTo(1));
    }

    @Test
    public void testGetRootBundles() {

        List<MarketingBundle> bundles = bundleMetadataService.getRootBundles();
        assertThat(bundles.size(), equalTo(18));

        assertThat(bundles.get(0).getName(), equalTo("Base Module"));
        assertThat(bundles.get(1).getName(), equalTo("Search Engine Optimization"));
        assertThat(bundles.get(2).getName(), equalTo("Program Analysis"));
        assertThat(bundles.get(3).getName(), equalTo("Advanced Report Builder"));
        assertThat(bundles.get(4).getName(), equalTo("Life Cycle Modeler"));
        assertThat(bundles.get(5).getName(), equalTo("Planning and Coordination"));
        assertThat(bundles.get(6).getName(), equalTo("Sales Intelligence"));
        assertThat(bundles.get(7).getName(), equalTo("Sales Intelligence Add Ons (plug-ins)"));
        assertThat(bundles.get(8).getName(), equalTo("Ad Personalization"));
        assertThat(bundles.get(9).getName(), equalTo("Social Marketing"));
        assertThat(bundles.get(10).getName(), equalTo("Events and Webinars"));
    }

    @Test
    public void testCompareBundles() {

        List<MarketingBundle> bundles = bundleMetadataService.getRootBundles();


        MarketingBundle oldBundle = bundleMetadataService.getMarketingBundle("Advanced Report Builder");

        MarketingBundle newBundle = bundleMetadataService.getMarketingBundle("Program Analysis");

        BundleDiffInfo bundlesDiff = bundleMetadataService.compareBundles(oldBundle, newBundle);

        List<MarketingFeature> commonFeatures = bundlesDiff.getCommonFeatures();
        List<MarketingFeature> oldFeatures = bundlesDiff.getOldFeatures();
        List<MarketingFeature> newFeatures = bundlesDiff.getNewFeatures();

        assertThat(commonFeatures.size(), equalTo(1));
        assertThat(commonFeatures.get(0).getName(), equalTo("RCA"));

        assertThat(oldFeatures.size(), equalTo(1));
        assertThat(oldFeatures.get(0).getName(), equalTo("RCE"));

        assertThat(newFeatures.size(), equalTo(1));
        assertThat(newFeatures.get(0).getName(), equalTo("Program Analyzer"));

    }
    
    /*
    @Test
    public void testGetUpdatesBundlesBetween() {
        
        Date endTime = new Date();
        
        Calendar cal = Calendar.getInstance();
        
        cal.set(2014, 9, 10);
        
        Date startTime = cal.getTime();
        
        List<MarketingBundleFeature> bundleFeatures = bundleMetadataService.getBundleUpdatesBetween(startTime, endTime);
        
        logger.info("Id is " + bundleFeatures.get(0).getId());
        
        assertThat(bundleFeatures.size(), equalTo(3));
    }*/
    
}
