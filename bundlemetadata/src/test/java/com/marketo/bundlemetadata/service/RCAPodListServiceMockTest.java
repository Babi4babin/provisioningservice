package com.marketo.bundlemetadata.service;

import com.marketo.locatorservice.client.SubscriptionServiceManager;
import com.marketo.locatorservice.entities.SubscriptionId;
import com.marketo.locatorservice.entities.SubscriptionPodInfo;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class RCAPodListServiceMockTest {


    @Mock
    private SubscriptionServiceManager subscriptionServiceManager;

    @Mock
    private SubscriptionPodInfo subscriptionPodInfo;

    private String  munchkinId= "123-ABC-456";

    @InjectMocks
    private RCAPodListService rcaPodListService;

    @BeforeClass
    public void setUp(){

        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testGetRCAPodsScenario_1() throws Exception{
        String url="http://int-locator-ws.marketo.org/locatorservice/";
        when(subscriptionPodInfo.getInternalApiUrl()).thenReturn(url);
        when(subscriptionServiceManager.getSubscriptionPodInfo(any(SubscriptionId.class))).thenReturn(subscriptionPodInfo);
        rcaPodListService.getRCAPods(munchkinId);
        verify(subscriptionServiceManager,times(1)).getSubscriptionPodInfo(new SubscriptionId(munchkinId));

    }

    @Test(dependsOnMethods = "testGetRCAPodsScenario_1")
    public void testGetRCAPodsScenario_2() throws Exception{
        String url="http://int-locator-ws.marketo.org/locatorservice/";
        when(subscriptionPodInfo.getInternalApiUrl()).thenReturn(url);
        when(subscriptionServiceManager.getSubscriptionPodInfo(any(SubscriptionId.class))).thenThrow(new NullPointerException());
        rcaPodListService.getRCAPods(munchkinId);
        verify(subscriptionServiceManager,atLeastOnce()).getSubscriptionPodInfo(new SubscriptionId(munchkinId));

    }

    @Test
    public void testGetRCAPodsScenario_3() throws Exception{
        String url="http://int-locator-ws.marketo.org/locatorservice//clusters.json?search=metadata.product=='rca'";
        when(subscriptionPodInfo.getInternalApiUrl()).thenReturn(url);
        when(subscriptionServiceManager.getSubscriptionPodInfo(any(SubscriptionId.class))).thenReturn(subscriptionPodInfo);
        rcaPodListService.getRCAPods(munchkinId);
        verify(subscriptionServiceManager,atLeastOnce()).getSubscriptionPodInfo(new SubscriptionId(munchkinId));

    }

}
