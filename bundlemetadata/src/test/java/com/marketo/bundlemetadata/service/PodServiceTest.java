package com.marketo.bundlemetadata.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ContextConfiguration(locations = { "classpath:bundlemetadata-test-config.xml" })
public class PodServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private PodListService podService;
    
    @Test
    public void testGetPods() {
        
        List<String> pods = podService.getActivePods("123-ABC-456", "seo");
        
        assertThat(pods.size(), equalTo(1));
        assertThat(pods.get(0), equalTo("seo-podint"));
    }
}
