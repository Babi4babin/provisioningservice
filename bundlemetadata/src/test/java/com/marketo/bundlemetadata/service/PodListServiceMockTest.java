package com.marketo.bundlemetadata.service;

import com.marketo.locatorservice.client.LocatorServiceClient;
import com.marketo.locatorservice.client.LocatorServiceClientConfig;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;

public class PodListServiceMockTest {

    @Mock
    private LocatorServiceClientConfig locatorServiceClientConfig;

    @Mock
    private RCAPodListService rcaPodListService;


    @InjectMocks
    private PodListService podListService;


    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testLocatorServiceClient() throws Exception{
        Assert.assertTrue(podListService.locatorServiceClient() instanceof LocatorServiceClient);
    }

    @Test
    public void testGetActivePodsScenario_1() throws Exception{
        String tenantUUID= UUID.randomUUID().toString();
        String product="rca";
        List<String> pods=spy(new ArrayList<String>());
        pods.add("A");
        pods.add("B");
        when(rcaPodListService.getRCAPods(tenantUUID)).thenReturn(pods);
        Assert.assertNotNull(podListService.getActivePods(tenantUUID,product));
        verify(rcaPodListService,times(1)).getRCAPods(tenantUUID);

    }
    @Test
    public void testGetActivePodsScenario_2() throws Exception{
        String tenantUUID= UUID.randomUUID().toString();
        String product="non_rca";
        List<String> pods=spy(new ArrayList<String>());
        pods.add("A");
        pods.add("B");
        when(locatorServiceClientConfig.getLocatorServiceUrl()).thenReturn(UUID.randomUUID().toString());
        when(rcaPodListService.getRCAPods(tenantUUID)).thenReturn(pods);
        Assert.assertNotNull(podListService.getActivePods(tenantUUID,product));
        verify(rcaPodListService,times(0)).getRCAPods(tenantUUID);

    }

    @Test
    public void testGetActivePodsScenario_3() throws Exception{
        String tenantUUID= "123-ABC-456";
        String product="seo";
        String url="http://int-locator-ws.marketo.org/locatorservice/";
        List<String> pods=spy(new ArrayList<String>());
        pods.add("A");
        pods.add("B");
        when(locatorServiceClientConfig.getLocatorServiceUrl()).thenReturn(url);
        when(rcaPodListService.getRCAPods(tenantUUID)).thenReturn(pods);
        Assert.assertNotNull(podListService.getActivePods(tenantUUID,product));
        verify(rcaPodListService,times(0)).getRCAPods(tenantUUID);

    }
}
