package com.marketo.bundlemetadata.exception;

import com.marketo.bundlemetadata.service.BundleMetadataService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;


@ContextConfiguration(locations = { "classpath:bundlemetadata-test-config.xml" })
public class bundleMetadataExceptionTest  extends AbstractTestNGSpringContextTests {

  @Autowired
  private BundleMetadataService bundleMetadataService;

  @Test(expectedExceptions = BundleMetadataException.class,
  expectedExceptionsMessageRegExp = "Could not find Bundle, with id: .*")
  public void testBundleMetadataException() {
    int bundleId = Integer.parseInt(RandomStringUtils.randomNumeric(5));
    bundleMetadataService.getMarketingBundle(bundleId);
  }
}
