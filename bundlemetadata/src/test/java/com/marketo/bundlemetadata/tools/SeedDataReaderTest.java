package com.marketo.bundlemetadata.tools;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.util.Map;

public class SeedDataReaderTest {

  private InputStream inputStream;

  SeedDataReader seedDataReader = new SeedDataReader(new ClassPathXmlApplicationContext("classpath:bundlemetadata-test-config.xml"));

  @Test
  public void testLoadData(){
    inputStream = this.getClass().getResourceAsStream("/seedFiles/seed_data_test.yml");
    Map<String, Map<String, Object>> resultMap = seedDataReader.loadData(inputStream);
    Assert.assertNotNull(resultMap);
    Assert.assertEquals(resultMap.size(), 3);
    Assert.assertTrue(resultMap.containsKey("com.marketo.bundlemetadata.model.MarketingBundle"));
    Assert.assertTrue(resultMap.containsKey("com.marketo.bundlemetadata.model.MarketingFeature"));
    Assert.assertTrue(resultMap.containsKey("com.marketo.bundlemetadata.model.MarketingBundleFeature"));

    Map<String, Object> marketingBundle = resultMap.get("com.marketo.bundlemetadata.model.MarketingBundle");
    Assert.assertNotNull(marketingBundle);
    Assert.assertEquals(marketingBundle.size(), 6);

    Assert.assertTrue(marketingBundle.containsKey("EnterpriseBundleLB"));
    Assert.assertTrue(marketingBundle.containsKey("StandardBundleLB"));
    Assert.assertTrue(marketingBundle.containsKey("SelectBundleLB"));
    Assert.assertTrue(marketingBundle.containsKey("SparkBundleLB"));
    Assert.assertTrue(marketingBundle.containsKey("DialogStandardBundleLB"));
    Assert.assertTrue(marketingBundle.containsKey("MarketoLiteBundleLB"));

    Object standardBundle = marketingBundle.get("StandardBundleLB");
    Assert.assertEquals(((MarketingBundle) standardBundle).getName(), "SMB - Standard");

    Map<String, Object> marketingFeature = resultMap.get("com.marketo.bundlemetadata.model.MarketingFeature");
    Assert.assertNotNull(marketingFeature);
    Assert.assertEquals(marketingFeature.size(), 46);

    Map<String, Object> marketingBundleFeature = resultMap.get("com.marketo.bundlemetadata.model.MarketingBundleFeature");
    Assert.assertNotNull(marketingBundleFeature);
    Assert.assertEquals(marketingBundleFeature.size(), 145);

  }

  @Test
  public void testLoadDataForAccountInsightsMarketingBundleFeature(){
    inputStream = this.getClass().getResourceAsStream("/seedFiles/seed_data_account_insights_test.yml");
    Map<String, Map<String, Object>> resultMap = seedDataReader.loadData(inputStream);
    Assert.assertNotNull(resultMap);
    Assert.assertEquals(resultMap.size(), 2);
    Assert.assertTrue(resultMap.containsKey("com.marketo.bundlemetadata.model.MarketingFeature"));
    Assert.assertTrue(resultMap.containsKey("com.marketo.bundlemetadata.model.MarketingBundleFeature"));

    Map<String, Object> marketingFeature = resultMap.get("com.marketo.bundlemetadata.model.MarketingFeature");
    Assert.assertNotNull(marketingFeature);
    Assert.assertEquals(marketingFeature.size(), 1);

    Assert.assertTrue(marketingFeature.containsKey("AccountInsightPlugInFeatureLB"));

    Object accountInsightPlugInFeatureLB = marketingFeature.get("AccountInsightPlugInFeatureLB");
    Assert.assertEquals(((MarketingFeature) accountInsightPlugInFeatureLB).getName(), "Account Insight Plug-In");
    Assert.assertEquals(((MarketingFeature) accountInsightPlugInFeatureLB).getCode(), "accountInsightPlugIn");
    Assert.assertEquals(((MarketingFeature) accountInsightPlugInFeatureLB).getActive (), 1);

    Map<String, Object> marketingBundleFeature = resultMap.get("com.marketo.bundlemetadata.model.MarketingBundleFeature");
    Assert.assertNotNull(marketingBundleFeature);
    Assert.assertEquals(marketingBundleFeature.size(), 2);

    Assert.assertTrue(marketingBundleFeature.containsKey("SalesInsightBundleAccountInsightPlugInFeatureLB"));
    Assert.assertTrue(marketingBundleFeature.containsKey("BrowserAndEmailPlugInBundleAccountInsightPlugInFeatureLB"));

  }

  @Test(expectedExceptions = RuntimeException.class,
    expectedExceptionsMessageRegExp = "Class: .* not found. Check the Yaml file.*")
  public void testLoadDataWithIncorrectClass() {
    inputStream = this.getClass().getResourceAsStream("/seedFiles/seed_data_negative_test_1.yml");
    seedDataReader.loadData(inputStream);
  }

  @Test(expectedExceptions = RuntimeException.class,
    expectedExceptionsMessageRegExp = "Valid Class name not found in one of the references.")
  public void testLoadDataWithEmptyClass() {
    inputStream = this.getClass().getResourceAsStream("/seedFiles/seed_data_negative_test_2.yml");
    seedDataReader.loadData(inputStream);
  }

  @Test(expectedExceptions = RuntimeException.class,
    expectedExceptionsMessageRegExp = "Method: .* not found. Check the Yaml file.*")
  public void testLoadDataWithIncorrectMethodName() {
    inputStream = this.getClass().getResourceAsStream("/seedFiles/seed_data_negative_test_3.yml");
    seedDataReader.loadData(inputStream);
  }

  @Test(expectedExceptions = RuntimeException.class,
    expectedExceptionsMessageRegExp = "Valid Method not found in one of the references.")
  public void testLoadDataWithEmptyMethod() {
    inputStream = this.getClass().getResourceAsStream("/seedFiles/seed_data_negative_test_4.yml");
    seedDataReader.loadData(inputStream);
  }

  @Test(expectedExceptions = RuntimeException.class,
    expectedExceptionsMessageRegExp = "Valid Parameter was not found for one of the references.")
  public void testLoadDataWithEmptyParamValue() {
    inputStream = this.getClass().getResourceAsStream("/seedFiles/seed_data_negative_test_5.yml");
    seedDataReader.loadData(inputStream);
  }

}
