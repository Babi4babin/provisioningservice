package com.marketo.bundlemetadata.tools;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

public class SeedDataLoaderTest {

  private String filename;

  //TODO: Fix the problem wrt bundlemetadata-config.xml in the method seedData() inside SeedDataLoader class
  // and then modify test cases accordingly.
//  @Test
  public void testLoadData() {
    filename = "/seedFiles/seed_data_test.yml";
    try {
      SeedDataLoader seedDataLoader = new SeedDataLoader();
      seedDataLoader.seedData(filename);
    }
    catch (Exception e){
      Assert.fail("Failed to load seed data for file: " + filename);
    }
  }

//  @Test(expectedExceptions = FileNotFoundException.class)
  public void testLoadDataWithIncorrectFileName() {
    filename = RandomStringUtils.randomAlphabetic(10);
    SeedDataLoader seedDataLoader = new SeedDataLoader();
    seedDataLoader.seedData(filename);
  }

}
