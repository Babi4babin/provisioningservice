package com.marketo.bundlemetadata.tools;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.yaml.snakeyaml.Yaml;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class YamlReaderTest {

    private InputStream is;
    
    @BeforeMethod
    public void setup() {
        is = this.getClass().getResourceAsStream("/multilinejson.yml");
    }
    
    @Test
    public void testYaml() {

        Yaml yaml = new Yaml(); 
        
        Map<String, Object> map = (Map<String, Object>)yaml.load(is);
        
        Map<String, Object> entry = (Map<String, Object>)map.get("com.marketo.bundlemetadata.model.MarketingFeature");
        
        Map<String, Object> fields = (Map<String, Object>)entry.get("RCAFeatureLB");
        
        assertThat((Integer)fields.get("id"), equalTo(3));
        assertThat((String)fields.get("name"), equalTo("RCA"));
        assertThat((String)fields.get("description"), equalTo("Revenue Cycle Analytics"));
        assertThat((Integer)fields.get("active"), equalTo(1));
        assertThat((String)fields.get("provisioningURI"), equalTo("/featureHandler/provision?doAction=rca"));
        assertThat((String)fields.get("locatorServiceId"), equalTo("mlmpod"));
    }
    
    /*
     * Test how SnakeYaml handle multi-line json
     */
    @Test
    public void testMultilineJson() throws JsonParseException, JsonMappingException, IOException {
        
        Yaml yaml = new Yaml(); 
     
        Map<String, Object> map = (Map<String, Object>)yaml.load(is);
        
        Map<String, Object> entry = (Map<String, Object>)map.get("com.marketo.bundlemetadata.model.MarketingFeature");
        
        Map<String, String> fields = (Map<String, String>)entry.get("RCAFeatureLB");
        
        String json = fields.get("marketingFeatureConfig");
        
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> jsonValue = mapper.readValue(json, Map.class);
        
        assertThat((String)jsonValue.get("mpaPodName"), equalTo("pod1"));
        
        Map<String, Boolean> featuresMap = (Map<String, Boolean>)jsonValue.get("features");
        
        assertThat(featuresMap.get("rca"), equalTo(true));
        assertThat(featuresMap.get("optyInfluenceAnalyzer"), equalTo(true));
        assertThat(featuresMap.get("successPathAnalyzer"), equalTo(true));
        assertThat(featuresMap.get("rce"), equalTo(true));
    }
}
