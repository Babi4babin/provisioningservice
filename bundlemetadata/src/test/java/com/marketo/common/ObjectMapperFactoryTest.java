package com.marketo.common;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.testng.annotations.Test;

public class ObjectMapperFactoryTest {

  @Test
  public void testObjectMapper(){
    ObjectMapper mapper = ObjectMapperFactory.getInstance();
    Assert.assertTrue(mapper instanceof ObjectMapper);
    mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
  }
}
