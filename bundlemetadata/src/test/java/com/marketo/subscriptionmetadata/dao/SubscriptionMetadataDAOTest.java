package com.marketo.subscriptionmetadata.dao;

import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundleFeatures;
import org.hamcrest.core.IsNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;

import java.util.List;

@ContextConfiguration(locations = { "classpath:bundlemetadata-test-config.xml" })
public class SubscriptionMetadataDAOTest extends AbstractTestNGSpringContextTests {

    public static final String BASE_MODULE_BUNDLE_NAME = "Base Module";

    @Autowired
    private BundleMetadataService bundleMetadataService;
    
    @Autowired
    private SubscriptionMetadataDAO subscriptionMetadataDAO;
    
    
    @BeforeMethod
    public void setup() {
    }
    
    // Test whether created_at, updated_at are being filled properly
    @Test
    public void testUpdateTime() {
        
        String tenantId = "123-ABC-456";
        int featureId = 2;
        
        MarketingFeature feature = bundleMetadataService.getMarketingFeature(2);
        
        subscriptionMetadataDAO.addMarketingFeature(tenantId, feature);
        
        SubscriptionBundleMap subBundleMap = subscriptionMetadataDAO.getSubscriptionFeatureEntry(tenantId, featureId);
        
        logger.info("Created date " + subBundleMap.getCreatedAt());        
        logger.info("Updated date " + subBundleMap.getUpdatedAt());
                
        try {
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        feature.setMarketingFeatureConfig("{\"num_seats\" : 10}");
        
        subscriptionMetadataDAO.addMarketingFeature(tenantId, feature);
        
        subBundleMap = subscriptionMetadataDAO.getSubscriptionFeatureEntry(tenantId, featureId);
        
        logger.info("Created date " + subBundleMap.getCreatedAt());        
        logger.info("Updated date " + subBundleMap.getUpdatedAt());        
        
        assertThat(subBundleMap.getCreatedAt().equals(subBundleMap.getUpdatedAt()), equalTo(false));    
    }

    @Test
    public void getProvisionedBundle() {
        String tenantId = "ABC-122-XYZ";
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle("Base Module");
        assertThat(bundle.getName(), equalTo("Base Module"));
        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle);
        SubscriptionBundleMap bundleMap = subscriptionMetadataDAO.getProvisionBundle(tenantId, bundle);
        assertThat(bundleMap.getMarketingBundle().getName(), equalTo("Base Module"));
    }

    @Test
    public void testGetAllBundleFeatures() {
        String tenantId = "ABC-124-XYT";
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle("Base Module");
        MarketingBundle bundle2 = bundleMetadataService.getMarketingBundle("Program Analysis");
        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle);
        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle2);

        List<SubscriptionBundleMap> subBundleMaps = subscriptionMetadataDAO.getAllBundleFeatures(tenantId);
        assertThat(subBundleMaps.get(0).getMarketingBundle().getName(), equalTo("Base Module"));
        // base should be the first in the list
        assertThat(subBundleMaps.get(0).getMarketingBundle().getChildBundleFeatures().get(0).getFeature().getName(), equalTo("Base"));
        assertThat(subBundleMaps.get(0).getMarketingBundle().getChildBundleFeatures().get(1).getFeature().getName(), equalTo("Dynamic Content"));

        assertThat(subBundleMaps.get(1).getMarketingBundle().getName(), equalTo("Program Analysis"));
        assertThat(subBundleMaps.get(1).getMarketingBundle().getChildBundleFeatures().get(0).getFeature().getName(), equalTo("RCA"));
        assertThat(subBundleMaps.get(1).getMarketingBundle().getChildBundleFeatures().get(1).getFeature().getName(), equalTo("Program Analyzer"));

        assertThat(subBundleMaps.get(0).getMarketingFeature(), equalTo(null));

    }

    @Test
    public void TestDeleteMarketingBundle() {
        String tenantId = "JKH-123-XYZ";
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle("Advanced Report Builder");

        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle);

        // Read Data back
        List<SubscriptionBundleMap> subBundleMaps = subscriptionMetadataDAO.getAllBundleFeatures(tenantId);
        assertThat(subBundleMaps.get(0).getMarketingBundle().getName(), equalTo("Advanced Report Builder"));

        subscriptionMetadataDAO.deleteMarketingBundle(tenantId, bundle.getId());

        subBundleMaps = subscriptionMetadataDAO.getAllBundleFeatures(tenantId);

        assertThat(subBundleMaps.size(), equalTo(0));
    }


    @Test
    public void testGetSubscriptionBundleEntry(){
        final String TENANT_UUID = "151-ABC-PQR";
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle(BASE_MODULE_BUNDLE_NAME);
        subscriptionMetadataDAO.addMarketingBundle(TENANT_UUID, bundle);
        SubscriptionBundleMap subscriptionBundleMap = subscriptionMetadataDAO.getSubscriptionBundleEntry(TENANT_UUID, bundle.getId());
        Assert.assertEquals(subscriptionBundleMap.getMarketingBundle().getName(), BASE_MODULE_BUNDLE_NAME);
    }

    @Test
    public void testUpdateMarketingFeatureConfig(){
        final String TENANT_UUID = "152-ABC-PQR";
        MarketingFeature feature = bundleMetadataService.getMarketingFeature(2);
        String marketingFeatureConfig = "{\"status\":\"completed\"}";
        subscriptionMetadataDAO.addMarketingFeature(TENANT_UUID, feature);
        subscriptionMetadataDAO.updateMarketingFeatureConfig(TENANT_UUID, feature, marketingFeatureConfig);
        SubscriptionBundleMap subscriptionBundleMap = subscriptionMetadataDAO.getSubscriptionFeatureEntry(TENANT_UUID, feature.getId());
        Assert.assertEquals(subscriptionBundleMap.getSubscriptionFeatureConfig(), marketingFeatureConfig);
    }

    @Test
    public void testUpdateMarketingBundle(){
        final String TENANT_UUID = "153-ABC-PQR";
        MarketingBundle oldBundle = bundleMetadataService.getMarketingBundle("Advanced Report Builder");
        MarketingBundle newBundle = bundleMetadataService.getMarketingBundle("Program Analysis");
        subscriptionMetadataDAO.addMarketingBundle(TENANT_UUID, oldBundle);

        subscriptionMetadataDAO.updateMarketingBundle(TENANT_UUID, newBundle.getId(), oldBundle.getId());
        SubscriptionBundleMap subscriptionBundleMap = subscriptionMetadataDAO.getSubscriptionBundleEntry(TENANT_UUID, newBundle.getId());
        Assert.assertEquals(subscriptionBundleMap.getMarketingBundle().getName(), "Program Analysis");
    }

    @Test
    public void testAddMarketingFeature(){
        final String TENANT_UUID = "154-ABC-PQR";
        MarketingFeature feature = bundleMetadataService.getMarketingFeature(2);
        String status ="{\"status\":\"completed\"}";
        subscriptionMetadataDAO.addMarketingFeature(TENANT_UUID, feature, status);
        String featureStatus = subscriptionMetadataDAO.getMarketingFeatureStatus(TENANT_UUID, feature);
        Assert.assertEquals(status, featureStatus);
    }

    @Test
    public void testDeleteMarketingFeature(){
        final String TENANT_UUID = "155-ABC-PQR";
        MarketingFeature feature = bundleMetadataService.getMarketingFeature(2);
        subscriptionMetadataDAO.addMarketingFeature(TENANT_UUID, feature);
        SubscriptionBundleMap subscriptionBundleMap = subscriptionMetadataDAO.getMarketingFeature(TENANT_UUID, feature);
        Assert.assertEquals(subscriptionBundleMap.getMarketingFeature().getId(), 2);

        subscriptionMetadataDAO.deleteMarketingFeature(TENANT_UUID, feature.getId());
        List<SubscriptionBundleMap> subscriptionBundleMapList = subscriptionMetadataDAO.getAllBundleFeatures(TENANT_UUID);
        boolean isFeaturePresent = false;
        for(SubscriptionBundleMap subscriptionBundleMap1 : subscriptionBundleMapList){
            if(subscriptionBundleMap1.getMarketingFeature().getId()==2){
                isFeaturePresent = true;
            }
        }
        Assert.assertFalse(isFeaturePresent);
    }

    @Test
    public void testDeleteBundlesAndFeature(){
        final String TENANT_UUID = "156-ABC-PQR";
        MarketingFeature feature = bundleMetadataService.getMarketingFeature(2);
        subscriptionMetadataDAO.addMarketingFeature(TENANT_UUID, feature);
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle(BASE_MODULE_BUNDLE_NAME);
        subscriptionMetadataDAO.addMarketingBundle(TENANT_UUID, bundle);

        subscriptionMetadataDAO.deleteBundlesAndFeatures(TENANT_UUID);
        List<SubscriptionBundleMap> subscriptionBundleMapList = subscriptionMetadataDAO.getAllBundleFeatures(TENANT_UUID);

        Assert.assertEquals(subscriptionBundleMapList.size(), 0);
    }

    @Test
      public void testGetSubscriptionsForBundle() {
        final String TENANT_UUID = "157-ABC-PQR";
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle(BASE_MODULE_BUNDLE_NAME);
        subscriptionMetadataDAO.addMarketingBundle(TENANT_UUID, bundle);
        List<String> list = subscriptionMetadataDAO.getSubscriptionsForBundle(bundle.getId());
        Assert.assertTrue(list.size()>0);
    }

}
