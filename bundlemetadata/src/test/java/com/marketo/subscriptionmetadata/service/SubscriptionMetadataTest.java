package com.marketo.subscriptionmetadata.service;

import com.marketo.bundlemetadata.dao.BundleMetadataDAO;
import com.marketo.bundlemetadata.model.MarketingBundle;
import com.marketo.bundlemetadata.model.MarketingFeature;
import com.marketo.bundlemetadata.rest.model.BundlesFeatures;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundleFeatures;
import com.marketo.bundlemetadata.rest.model.SubscriptionBundles;
import com.marketo.bundlemetadata.service.BundleMetadataService;
import com.marketo.subscriptionmetadata.dao.SubscriptionMetadataDAO;
import com.marketo.subscriptionmetadata.model.SubscriptionBundleMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

@ContextConfiguration(locations = { "classpath:bundlemetadata-test-config.xml" })
public class SubscriptionMetadataTest extends AbstractTestNGSpringContextTests {

    public static final String BASE_MODULE_BUNDLE_NAME = "Base Module";
    public static final String RTP_FEATURE_NAME = "RTP Predictive";

    @Autowired
    private BundleMetadataService bundleMetadataService;

    @Autowired
    private SubscriptionMetadataService subscriptionMetadataService;

    @Autowired
    private SubscriptionMetadataDAO subscriptionMetadataDAO;

    @Autowired
    private BundleMetadataDAO bundleMetadataDAO;

    @BeforeMethod
    public void setup() {
    }

    @Test
    public void testAddBundle() {

        MarketingBundle bundle = bundleMetadataService.getMarketingBundle("Base Module");

        subscriptionMetadataService.addMarketingBundle("ABC-124-XYZ", bundle);

        // read back the data.
        SubscriptionBundleFeatures bundleFeatures = subscriptionMetadataService.getBundlesFeatures("ABC-124-XYZ");

        assertThat("Base Module", equalTo(bundleFeatures.getBundle().getName()));
        assertThat("A module for all non-paid features", equalTo(bundleFeatures.getBundle().getDescription()));
        assertThat("baseModule", equalTo(bundleFeatures.getBundle().getCode()));
        assertThat(1, equalTo(bundleFeatures.getBundle().getActive()));
    }
    
    //@Test covered in testAddBundle
    public void testGetAllBundlesFeature() {
        
        SubscriptionBundleFeatures bundleFeatures = subscriptionMetadataService.getBundlesFeatures("123-ABC-456");
        
        // check the top level bundle
        assertThat(true, equalTo(bundleFeatures.getBundle() != null));
        assertThat("Enterprise", equalTo(bundleFeatures.getBundle().getName()));
        // No Addons
        assertThat(0, equalTo(bundleFeatures.getAddOns().size()));
    }

    //@Test revised for Rubiks check testAvailableBundles
    public void testGetAvaialbleBundlesFeatures() {

        BundlesFeatures bundleFeatures = subscriptionMetadataService.getAvailablesBundlesFeatures("123-ABC-456");
        assertThat(bundleFeatures.getBundles().size(), equalTo(1));
        assertThat(bundleFeatures.getFeatures().size(), equalTo(1));

        assertThat(bundleFeatures.getBundles().get(0).getName(), equalTo("Social App"));
        assertThat(bundleFeatures.getFeatures().get(0).getName(), equalTo("Social Promotion"));
    }

    @Test
    public void testGetProvisionedModules() {

        String tenantId = "ABC-124-XMV";
        // RCA is feature in both of the following bundles
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle("Advanced Report Builder");
        MarketingBundle bundle2 = bundleMetadataService.getMarketingBundle("Program Analysis");
        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle);
        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle2);

        MarketingFeature f = bundleMetadataDAO.getMarketingFeature("RCA");
        if (subscriptionMetadataService.isFeatureProvisioned(tenantId, f.getId())) {
            subscriptionMetadataService.deleteMarketingFeature(tenantId, f.getId());
        }
        f.setMarketingFeatureConfig("{test}");
        subscriptionMetadataService.addMarketingFeature(tenantId, f);

        SubscriptionBundles provisioned = subscriptionMetadataService.getProvisionedModules(tenantId);

        // Verify feature config got updated in both bundles
        assertThat(provisioned.getBundles().get(0).getChildBundleFeatures().get(0).getFeature().getName(), equalTo("RCA"));
        assertThat(provisioned.getBundles().get(1).getChildBundleFeatures().get(0).getFeature().getName(), equalTo("RCA"));
        assertThat(provisioned.getBundles().get(0).getChildBundleFeatures().get(0).getFeature().getMarketingFeatureConfig(), equalTo("{test}"));
        assertThat(provisioned.getBundles().get(1).getChildBundleFeatures().get(0).getFeature().getMarketingFeatureConfig(), equalTo("{test}"));
        subscriptionMetadataService.deleteMarketingFeature(tenantId, f.getId());
    }

    @Test void testAvailableBundles() {

        String tenantId = "ABC-124-XYZZ";

        // RCA is feature in both of the following bundles
        Integer allBundlesCount = bundleMetadataDAO.getAllBundles().size();
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle("Advanced Report Builder");
        MarketingBundle bundle2 = bundleMetadataService.getMarketingBundle("Life Cycle Modeler");

        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle);
        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle2);

        MarketingFeature f = bundleMetadataDAO.getMarketingFeature("RCA");
        f.setMarketingFeatureConfig("{test}");
        subscriptionMetadataService.addMarketingFeature(tenantId, f);


        SubscriptionBundles available = subscriptionMetadataService.getAvailableModules(tenantId);

        MarketingBundle programAnalysis = bundleMetadataService.getMarketingBundle("Program Analysis");

        assertThat(available.getBundleMap().containsKey(programAnalysis.getId()), equalTo(true));
        assertThat(available.getBundleMap().containsKey(bundle.getId()), equalTo(false));
        assertThat(available.getBundleMap().containsKey(bundle2.getId()), equalTo(false));

        // Verify for programAnalysis, the RCA feature's config got updated as it's already provisioned part of another module
        Integer index = available.getBundleMap().get(programAnalysis.getId());
        assertThat(available.getBundles().get(index).getChildBundleFeatures().get(0).getFeature().getName(), equalTo("RCA"));
        assertThat(available.getBundles().get(index).getChildBundleFeatures().get(0).getFeature().getMarketingFeatureConfig(), equalTo("{test}"));

        subscriptionMetadataDAO.deleteMarketingBundle(tenantId, bundle.getId());
        subscriptionMetadataDAO.deleteMarketingBundle(tenantId, bundle2.getId());

    }

    @Test
    public void testDisableFeature() {

        String tenantId = "123-ABC-456";
        int featureId = 2;

        subscriptionMetadataService.deleteMarketingFeature(tenantId, featureId);

        MarketingFeature feature = subscriptionMetadataService.getAddonFeatureInfoForSubsription(tenantId, featureId);

        assertThat(feature, nullValue());
    }

    //@Test the use case is different for Rubiks
    public void testUpdateBundle() {

        String tenantId = "123-ABC-456";
        MarketingBundle newBundle = bundleMetadataService.getMarketingBundle("SMB Spark");

        MarketingBundle oldBundle = bundleMetadataService.getMarketingBundle("Enterprise");

        subscriptionMetadataService.updateMarketingBundle(tenantId, newBundle.getId(), oldBundle.getId());

        SubscriptionBundleFeatures bundleFeatures = subscriptionMetadataService.getBundlesFeatures("123-ABC-456");

        assertThat("SMB Spark", equalTo(bundleFeatures.getBundle().getName()));
        assertThat("SMB Spark Bundle", equalTo(bundleFeatures.getBundle().getDescription()));
        assertThat(1, equalTo(bundleFeatures.getBundle().getActive()));
    }

    @Test
    public void testIsFeatureProvisioned() {

        String tenantId = "123-ABC-456";
        MarketingFeature featureRCA = bundleMetadataDAO.getMarketingFeature("RCA");
        MarketingFeature feature2 = bundleMetadataDAO.getMarketingFeature("Calendar Presentations");
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle("Program Analysis");
        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle);

        boolean isProvisioned = subscriptionMetadataService.isFeatureProvisioned(tenantId, featureRCA.getId());
        assertThat(isProvisioned, equalTo(true));


        isProvisioned = subscriptionMetadataService.isFeatureProvisioned(tenantId, feature2.getId());
        assertThat(isProvisioned, equalTo(false));
        subscriptionMetadataDAO.deleteMarketingBundle(tenantId, bundle.getId());
    }

    @Test
    public void testGetFeatureUsedByCount() {

        String tenantId = "ABC-124-XYM";

        // RCA is feature in both of the following bundles
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle("Advanced Report Builder");
        MarketingBundle bundle2 = bundleMetadataService.getMarketingBundle("Program Analysis");
        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle);
        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle2);

        Map<Integer, Integer> featureUsedByCount = subscriptionMetadataService.getFeatureUsedByCount(tenantId);

        MarketingFeature featureRCA = bundleMetadataDAO.getMarketingFeature("RCA");
        MarketingFeature featurePA = bundleMetadataDAO.getMarketingFeature("Program Analyzer");
        MarketingFeature featureRCE = bundleMetadataDAO.getMarketingFeature("RCE");
        MarketingFeature featureSSO = bundleMetadataDAO.getMarketingFeature("SSO");

        assertThat(featureUsedByCount.get(featureRCA.getId()), equalTo(2));
        assertThat(featureUsedByCount.get(featurePA.getId()), equalTo(1));
        assertThat(featureUsedByCount.get(featureRCE.getId()), equalTo(1));
        assertThat(featureUsedByCount.get(featureSSO.getId()), equalTo(null)); // not provisioned for subscription "ABC-124-XYZ"

        subscriptionMetadataDAO.deleteMarketingBundle(tenantId, bundle.getId());
        featureUsedByCount = subscriptionMetadataService.getFeatureUsedByCount(tenantId);
        assertThat(featureUsedByCount.get(featureRCA.getId()), equalTo(1));

        subscriptionMetadataDAO.deleteMarketingBundle(tenantId, bundle2.getId());
    }

    @Test
    public void TestGetProvisionedPodFeatures() {
        String tenantId = "108-EZL-727";
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle("Predictive");

        subscriptionMetadataDAO.addMarketingBundle(tenantId, bundle);

        MarketingFeature feature = bundleMetadataDAO.getMarketingFeature("RTP Predictive");

        feature.setMarketingFeatureConfig("{\"podName\":\"rtp-pod-dummy\"}");
        subscriptionMetadataService.addMarketingFeature(tenantId, feature);

        List<SubscriptionBundleMap> subscriptionBundleMaps = subscriptionMetadataDAO.getProvisionedPodFeatures(tenantId);
        assertThat(1, equalTo(subscriptionBundleMaps.size()));

        assertThat(subscriptionMetadataService.getConfiguredPods(tenantId).get("rtp"), equalTo("rtp-pod-dummy"));
    }

    @Test
    public void TestUpdateFeatureConfig() {
        String tenantId = "108-EZL-727";

        MarketingFeature feature = bundleMetadataDAO.getMarketingFeature("RTP Predictive");
        subscriptionMetadataService.addMarketingFeature(tenantId, feature);
        subscriptionMetadataDAO.updateMarketingFeatureConfig(tenantId, feature, "{\"podName\":\"rtp-pod-dummy2\"}");
        assertThat(subscriptionMetadataService.getConfiguredPods(tenantId).get("rtp"), equalTo("rtp-pod-dummy2"));
    }

    @Test
    public void testGetAvailableBundlesFeatures() {
        final String TENANT_UUID = "ABC-123-XYZ";
        MarketingBundle bundle = bundleMetadataDAO.getMarketingBundle("Predictive");
        subscriptionMetadataService.addMarketingBundle(TENANT_UUID, bundle);
        MarketingFeature feature = bundleMetadataDAO.getMarketingFeature(RTP_FEATURE_NAME);
        subscriptionMetadataService.addMarketingFeature(TENANT_UUID, feature);
        BundlesFeatures bundlesFeatures = subscriptionMetadataService.getAvailablesBundlesFeatures(TENANT_UUID);
        Assert.assertTrue(bundlesFeatures.getFeatures().size() > 0);
    }

    @Test
    public void testGetMarketingFeature() {
        final String TENANT_UUID = "ABC-125-XYZ";
        MarketingFeature feature = bundleMetadataDAO.getMarketingFeature(RTP_FEATURE_NAME);
        subscriptionMetadataService.addMarketingFeature(TENANT_UUID, feature);
        SubscriptionBundleMap subscriptionBundleMap = subscriptionMetadataService.getMarketingFeature(TENANT_UUID, feature);
        Assert.assertEquals(subscriptionBundleMap.getMarketingFeature().getName() , RTP_FEATURE_NAME);
    }

    @Test
    public void testDeleteBundlesAndFeature(){
        final String TENANT_UUID = "ABC-126-XYZ";
        MarketingFeature feature = bundleMetadataService.getMarketingFeature(2);
        subscriptionMetadataService.addMarketingFeature(TENANT_UUID, feature);
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle(BASE_MODULE_BUNDLE_NAME);
        subscriptionMetadataService.addMarketingBundle(TENANT_UUID, bundle);

        subscriptionMetadataService.deleteBundlesAndFeatures(TENANT_UUID);
        List<SubscriptionBundleMap> subscriptionBundleMapList = subscriptionMetadataDAO.getAllBundleFeatures(TENANT_UUID);

        Assert.assertEquals(subscriptionBundleMapList.size(), 0);
    }

    @Test
    public void testAddMarketingFeature(){
        final String TENANT_UUID = "ABC-127-XYZ";
        MarketingFeature feature = bundleMetadataService.getMarketingFeature(2);
        String status ="{\"status\":\"completed\"}";
        subscriptionMetadataService.addMarketingFeature(TENANT_UUID, feature, status);
        SubscriptionBundleMap subscriptionBundleMap = subscriptionMetadataService.getMarketingFeature(TENANT_UUID, feature);
        Assert.assertEquals(subscriptionBundleMap.getMarketingFeature().getId(), 2);
    }

    @Test
    public void testAddMarketingFeatureWithFeatureConfig(){
        final String TENANT_UUID = "ABC-128-XYZ";
        MarketingFeature feature = bundleMetadataService.getMarketingFeature(RTP_FEATURE_NAME);
        String status ="{\"status\":\"completed\"}";
        subscriptionMetadataService.addMarketingFeature(TENANT_UUID, feature, status);
        String featureConfig = "{\"podName\":\"rtp-podtest\"}";
        subscriptionMetadataService.addMarketingFeature(TENANT_UUID, feature.getName(), featureConfig,  status);
        SubscriptionBundleMap subscriptionBundleMap = subscriptionMetadataService.getMarketingFeature(TENANT_UUID, feature);
        Assert.assertEquals(subscriptionBundleMap.getSubscriptionFeatureConfig(), featureConfig);
    }

    @Test
    public void testAddMarketingBundle(){
        final String TENANT_UUID = "ABC-129-XYZ";
        MarketingBundle bundle = bundleMetadataService.getMarketingBundle(BASE_MODULE_BUNDLE_NAME);
        subscriptionMetadataService.addMarketingBundle(TENANT_UUID, bundle);
        SubscriptionBundleFeatures subscriptionBundleFeatures = subscriptionMetadataService.getBundlesFeatures(TENANT_UUID);
        Assert.assertEquals(subscriptionBundleFeatures.getBundle().getName(), BASE_MODULE_BUNDLE_NAME);
    }

    @Test
    public void testUpdateMarketingBundle(){
        final String TENANT_UUID = "ABC-130-XYZ";
        MarketingBundle oldBundle = bundleMetadataService.getMarketingBundle("Advanced Report Builder");
        MarketingBundle newBundle = bundleMetadataService.getMarketingBundle("Program Analysis");
        subscriptionMetadataService.addMarketingBundle(TENANT_UUID, oldBundle);

        subscriptionMetadataService.updateMarketingBundle(TENANT_UUID, newBundle.getId(), oldBundle.getId());
        SubscriptionBundleFeatures subscriptionBundleFeatures = subscriptionMetadataService.getBundlesFeatures(TENANT_UUID);
        Assert.assertEquals(subscriptionBundleFeatures.getBundle().getName(), "Program Analysis");
    }

    @Test
    public void testUpdateMarketingFeatureConfig(){
        final String TENANT_UUID = "ABC-131-XYZ";
        MarketingFeature feature = bundleMetadataService.getMarketingFeature(RTP_FEATURE_NAME);
        String status ="{\"status\":\"completed\"}";
        subscriptionMetadataService.addMarketingFeature(TENANT_UUID, feature, status);
        String featureConfig = "{\"podName\":\"rtp-podtest\"}";
        subscriptionMetadataService.updateMarketingFeatureConfig(TENANT_UUID, feature.getCode(), featureConfig);
        SubscriptionBundleMap subscriptionBundleMap = subscriptionMetadataService.getMarketingFeature(TENANT_UUID, feature);
        Assert.assertEquals(subscriptionBundleMap.getSubscriptionFeatureConfig(), featureConfig);
    }
}
