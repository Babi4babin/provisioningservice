package com.marketo.updatetracker.service;

import com.marketo.updatetracker.model.SubscriptionUpdateTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import com.marketo.updatetracker.model.PackageUpdateTracker;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import scala.collection.immutable.List;

import java.util.Random;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ContextConfiguration(locations = {"classpath:bundlemetadata-test-config.xml"})
public class PackageUpdateTrackerTest extends AbstractTestNGSpringContextTests {

	@Autowired
	private PackageUpdateTrackerService packageUpdateTrackerService;


	PackageUpdateTracker packageUpdateTracker;


	@BeforeMethod
	public void beforeMethod() throws Exception {
		packageUpdateTracker = packageUpdateTrackerService.addUpdateRun("oct2014_release");
		logger.info("Id is " + packageUpdateTracker.getId());
		assertThat(packageUpdateTracker.getId(), is(not(0)));
	}

	@Test
	public void testAddUpdateRun() throws Exception {

		PackageUpdateTracker updateTracker = packageUpdateTrackerService.addUpdateRun("oct2014_release");

		logger.info("Id is " + updateTracker.getId());

		assertThat(updateTracker.getId(), is(not(0)));

		updateTracker = packageUpdateTrackerService.getLastRun();

		assertThat(updateTracker.getTag(), equalTo("oct2014_release"));
		assertThat(updateTracker.getStartTime(), notNullValue());
	}

	@Test
	public void testGetLastRun() throws Exception {

		packageUpdateTrackerService.addUpdateRun("oct2014_release");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		packageUpdateTrackerService.addUpdateRun("dec2014_release");

		PackageUpdateTracker updateTracker = packageUpdateTrackerService.getLastRun();

		assertThat(updateTracker.getTag(), equalTo("dec2014_release"));
		assertThat(updateTracker.getStartTime(), notNullValue());
	}

	@Test
	public void testGetLastCompletedRun() throws Exception {

		packageUpdateTrackerService.updateSuccessRun(packageUpdateTracker.getId());

		packageUpdateTrackerService.updateTrackerState(packageUpdateTracker.getId(), PackageUpdateTracker.FINSHED);

		packageUpdateTracker = packageUpdateTrackerService.getLastCompletedRun();

		assertThat(packageUpdateTracker.getTag(), equalTo("oct2014_release"));

	}

	@Test
	public void testGetLastCompletedRunInvalid() throws Exception {
		boolean updateSucess = true;
		try {
			packageUpdateTrackerService.updateTrackerState(-1, PackageUpdateTracker.FINSHED);

		} catch (Exception e) {
			updateSucess = false;
		}
		Assert.assertFalse(updateSucess);
	}

	@Test
	public void testUpdateFailureRun() throws Exception {

		packageUpdateTrackerService.updateFailureRun(packageUpdateTracker.getId());
		packageUpdateTracker = packageUpdateTrackerService.getLastRun();
		assertThat(packageUpdateTracker.getTag(), equalTo("oct2014_release"));

	}

	@Test
	public void testUpdateFailureRunFailed() throws Exception {
		boolean updateSucess = true;
		try {
			packageUpdateTrackerService.updateFailureRun(-1);
		} catch (Exception e) {
			updateSucess = false;
		}
		Assert.assertFalse(updateSucess);
	}

	@Test
	public void testUpdateSuccessRunFailed() throws Exception {
		boolean updateSucess = true;
		try {
			packageUpdateTrackerService.updateSuccessRun(-1);
		} catch (Exception e) {
			updateSucess = false;
		}
		Assert.assertFalse(updateSucess);
	}

	@Test
	public void testUpdateTrackerStateFailed() throws Exception {
		boolean updateSucess = true;
		try {
			packageUpdateTrackerService.updateTrackerState(-1, PackageUpdateTracker.FINSHED);
		} catch (Exception e) {
			updateSucess = false;
		}
		Assert.assertFalse(updateSucess);
	}

	@Test
	public void testgetCompletedSubscriptionsForTracker() throws Exception {
		packageUpdateTrackerService.updateSuccessRun(packageUpdateTracker.getId());
		Assert.assertTrue(packageUpdateTrackerService.getCompletedSubscriptionsForTracker(packageUpdateTracker.getId()) != null);

	}

	@Test
	public void testAddSubscriptionEntry() throws Exception {

		SubscriptionUpdateTracker subscriptionUpdateTracker =
				packageUpdateTrackerService.addSubscriptionEntry(UUID.randomUUID().toString(), packageUpdateTracker.getId(), true);

		Assert.assertNotNull(subscriptionUpdateTracker);
	}

	@Test
	public void testAddSubscriptionEntry_2() throws Exception {

		SubscriptionUpdateTracker subscriptionUpdateTracker =
				packageUpdateTrackerService.addSubscriptionEntry(UUID.randomUUID().toString(), packageUpdateTracker.getId(), false);

		Assert.assertNotNull(subscriptionUpdateTracker);
	}

	@AfterMethod
	public void cleanUp() throws Exception {
		packageUpdateTracker = null;
	}
}
