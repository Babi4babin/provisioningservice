use provisioningService;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET name = 'Sales Engage', code = 'salesEngage', updated_at = now()
WHERE name = 'Engage Integration';

COMMIT;