use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_bundle
SET name = 'Browser and Email Plug-In', description = 'Browser and Email Plug-In Bundle', updated_at = now()
WHERE name = 'Additional Email Plug-In';

COMMIT;