USE provisioningService;

SET autocommit = 0;
BEGIN;

INSERT INTO marketing_feature (name, code, description, active, developer_config, locator_service_id, provisioning_URI, deprovisioning_URI, update_URI, created_at, updated_at)
VALUES ( "Sales Insight Actions", "salesInsightActions", "Sales Insight Actions Feature", 1,
'[{"name":{"field":"salesEngageSeats","displayField":"Sales Insight Actions Seats"},"type":"number","required":false,"value":0}]',
 'mlmpod', "/rest/v1/featureHandler/operation/enableSalesInsightActions/munchkinId/{tenantUUID}", "/rest/v1/featureHandler/operation/disableSalesInsightActions/munchkinId/{tenantUUID}", "/rest/v1/featureHandler/operation/updateSalesInsightActions/munchkinId/{tenantUUID}", NOW(), NOW());

COMMIT;