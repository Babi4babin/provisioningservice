USE packagingservice;

SET autocommit = 0;
BEGIN;

INSERT INTO marketing_bundle (name, description, code, active, created_at, updated_at)
VALUES ("Sales Insight Actions", "Sales Insight Actions Bundle", "salesInsightActions", 1, NOW(), NOW());


INSERT INTO marketing_feature (name, code, description, active, developer_config, locator_service_id, provisioning_URI, deprovisioning_URI, update_URI, created_at, updated_at)
VALUES ( "Sales Insight Actions", "salesInsightActions", "Sales Insight Actions Feature", 1,
'[{"name":{"field":"salesEngageSeats","displayField":"Sales Insight Actions Seats"},"type":"number","required":false,"value":0}]',
 'mlmpod', "/rest/v1/featureHandler/operation/enableSalesInsightActions/munchkinId/{tenantUUID}", "/rest/v1/featureHandler/operation/disableSalesInsightActions/munchkinId/{tenantUUID}", "/rest/v1/featureHandler/operation/updateSalesInsightActions/munchkinId/{tenantUUID}", NOW(), NOW());

INSERT INTO marketing_bundle_detail (marketing_bundle_id, marketing_feature_id, bundle_feature_config, is_optional, active, created_at, updated_at)
 VALUES ((SELECT id FROM marketing_bundle where code = 'salesInsightActions'),
         (SELECT id FROM marketing_feature where code = 'salesInsightActions'),
         '{"salesEngageSeats":0}',0, 1, NOW(), NOW());

COMMIT;