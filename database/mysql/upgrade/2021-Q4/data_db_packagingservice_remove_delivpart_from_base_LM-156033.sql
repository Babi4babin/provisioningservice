use packagingservice;

SET autocommit = 0;
BEGIN;

-- Marketing Bundle Detail Entries
UPDATE marketing_bundle_detail
SET bundle_feature_config =
        REPLACE(	bundle_feature_config,
                    ', "deliverabilityPartnerEnabled": false,',
                    ','),
    updated_at = NOW()
WHERE marketing_feature_id = 1;

UPDATE marketing_bundle_detail
SET bundle_feature_config =
        REPLACE(	bundle_feature_config,
                    ', "deliverabilityPartnerUser" : "", "deliverabilityPartnerGroup" : "",',
                    ','),
    updated_at = NOW()
WHERE marketing_feature_id = 1;

-- Marketing Feature Entry
UPDATE marketing_feature
SET developer_config =
        REPLACE(	developer_config,
                    ', { "name" : { "field" : "deliverabilityPartnerEnabled", "displayField" : "Deliveribility Partner" }, "type" : "boolean", "required" : false, "value" : false },',
                    ','),
    updated_at = NOW()
WHERE id = 1;

UPDATE marketing_feature
SET developer_config =
        REPLACE(	developer_config,
                    ', { "name" : { "field" : "deliverabilityPartnerUser", "displayField" : "Deliverability User" }, "type" : "text", "required" : false }, { "name" : { "field" : "deliverabilityPartnerGroup", "displayField" : "Deliverability Group" }, "type" : "text", "required" : false },',
                    ','),
    updated_at = NOW()
WHERE id = 1;

COMMIT;