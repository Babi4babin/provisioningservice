use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET updated_at = now(),
    developer_config = '[{ "name" : { "field" : "premiumEvents", "displayField" : "Premium Events" }, "type" : "boolean", "required" : false, "value" : false }]',
    update_uri = '/rest/v1/featureHandler/operation/updateEventAndWebinar/munchkinId/{tenantUUID}'
WHERE name = 'Event and Webinar';

COMMIT;









