SELECT SCHEMA();

SET foreign_key_checks = 0;

ALTER TABLE marketing_feature ADD COLUMN status_URI varchar(255) DEFAULT NULL AFTER update_URI;

ALTER TABLE marketing_feature ADD COLUMN client_type varchar(255) DEFAULT NULL AFTER locator_service_id;

SET foreign_key_checks = 1;
