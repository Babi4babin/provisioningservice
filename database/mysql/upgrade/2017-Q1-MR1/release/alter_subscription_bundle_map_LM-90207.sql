SELECT SCHEMA();

SET foreign_key_checks = 0;

ALTER TABLE subscription_bundle_map ADD COLUMN subscription_feature_info varchar(2000) DEFAULT NULL AFTER subscription_feature_config;

SET foreign_key_checks = 1;
