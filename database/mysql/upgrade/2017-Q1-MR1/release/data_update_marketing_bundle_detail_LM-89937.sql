use packagingservice;

SET autocommit = 0;
BEGIN;

DELETE dt from marketing_bundle_detail as dt
left join marketing_bundle as b on (b.code='baseModule')
left join marketing_feature as f on (f.code='engagementTrend')
where dt.marketing_feature_id = f.id and dt.marketing_bundle_id = b.id;

INSERT into marketing_bundle_detail
(`marketing_bundle_id`, `child_marketing_bundle_id`, `marketing_feature_id`, `bundle_feature_config`, `is_optional`, `active`, `created_at`, `updated_at`)
select b.id, null, f.id, null, 0, 1, now(), now() from marketing_feature f
left join marketing_bundle as b on (b.code='advancedReportBuilder')
where f.code = 'engagementTrend';

COMMIT;
/*ROLLBACK*/