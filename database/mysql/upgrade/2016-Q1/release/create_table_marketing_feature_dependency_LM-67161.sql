SELECT SCHEMA();

SET foreign_key_checks = 0;

USE provisioningService;

CREATE TABLE IF NOT EXISTS marketing_feature_dependency (
    id int NOT NULL AUTO_INCREMENT,
    marketing_feature_id int NOT NULL COMMENT 'Feature with id = marketing_feature_id has to be provisioned AFTER feature with id = dep_marketing_feature_id',
    dep_marketing_feature_id int NOT NULL COMMENT 'Feature with id = dep_marketing_feature_id has to be provisioned BEFORE feature with id = marketing_feature_id',
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT marketing_feature_id_FK_1 FOREIGN KEY (marketing_feature_id) REFERENCES marketing_feature (id),
    CONSTRAINT dep_marketing_feature_id_FK_2 FOREIGN KEY (dep_marketing_feature_id) REFERENCES marketing_feature (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET foreign_key_checks = 1;