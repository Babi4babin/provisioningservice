SELECT SCHEMA();

USE provisioningService;

SET foreign_key_checks = 0;

ALTER TABLE marketing_bundle ADD COLUMN code varchar(255) DEFAULT NULL AFTER description;

ALTER TABLE marketing_feature ADD COLUMN code varchar(255) DEFAULT NULL AFTER name;

ALTER TABLE marketing_bundle_detail ADD COLUMN is_optional tinyint(1) DEFAULT 0 AFTER bundle_feature_config;

SET foreign_key_checks = 1;
