use provisioningService;

SET autocommit = 0;
BEGIN;

DELETE FROM marketing_bundle_detail
WHERE marketing_feature_id in (select id from marketing_feature where name = 'RCE')
AND marketing_bundle_id in (select id from marketing_bundle where name = 'Enterprise');

COMMIT;