use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET developer_config = '[{ "name" : { "field" : "accountInsightPlugIn", "displayField" : "Account Insight Plug-In" }, "type" : "number", "required" : false, "value" : 5 }]', updated_at = now()
WHERE code = 'accountInsightPlugIn';

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{"accountInsightPlugIn":5}'
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE code = 'accountInsightPlugIn' LIMIT 1);

COMMIT;