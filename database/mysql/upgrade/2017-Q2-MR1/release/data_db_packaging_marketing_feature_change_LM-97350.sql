use packagingservice;

SET autocommit = 0;
BEGIN;

update marketing_feature
set status_URI = '/rca-admin-1.0/api/rest/customerProvision/getRCAProvisioningProgress', updated_at = now()
where code = 'revenueCycleAnalytics';

COMMIT;