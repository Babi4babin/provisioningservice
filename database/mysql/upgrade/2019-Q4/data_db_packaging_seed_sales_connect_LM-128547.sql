use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_bundle
SET name = 'Sales Connect', description = 'Sales Connect Integration with ToutApp', updated_at = now()
WHERE code = 'salesEngage';

UPDATE marketing_feature
SET name = 'Sales Connect', description = 'Sales Connect Integration with ToutApp', developer_config = '[{"name":{"field":"salesEngageSeats","displayField":"Sales Connect Seats"},"type":"number","required":false,"value":0},{"name":{"field":"planType","displayField":"Plan Type"},"type":"picklist","required":false,"displayField":"name","submitField":"value","value":[{"name":"Standard","value":"Standard"},{"name":"Premium","value":"Premium"}],"defaultValue":{"name":"Standard","value":"Standard"}}]', updated_at = now()
WHERE code = 'salesEngage';

COMMIT;
