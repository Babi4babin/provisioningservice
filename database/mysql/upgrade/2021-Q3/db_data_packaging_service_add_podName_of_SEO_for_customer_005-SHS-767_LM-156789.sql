use provisioningService;

SET autocommit = 0;
BEGIN;

SELECT * FROM subscription_bundle_map WHERE id = 333734 AND tenant_uuid = "005-SHS-767" AND subscription_feature_config LIKE '%seo%';

UPDATE subscription_bundle_map
SET subscription_feature_config = REPLACE(subscription_feature_config, '"podName":null', '"podName":"sjseo"'),
updated_at = now()
WHERE tenant_uuid = '005-SHS-767' AND id = 333734;

SELECT * FROM subscription_bundle_map WHERE id = 333734 AND tenant_uuid = "005-SHS-767" AND subscription_feature_config LIKE '%seo%';

COMMIT;