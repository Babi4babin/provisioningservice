use packagingservice;

SET autocommit = 0;
BEGIN;


-- Bundle Entries
INSERT INTO marketing_bundle (name, description, code, active, created_at, updated_at)
VALUES ("Email Deliverability", "Email Deliverability Bundle", "emailDeliverability", 1, NOW(), NOW());

INSERT INTO marketing_bundle (name, description, code, active, created_at, updated_at)
VALUES ("Email Reputation", "Email Reputation Bundle", "emailReputation", 1, NOW(), NOW());

INSERT INTO marketing_bundle (name, description, code, active, created_at, updated_at)
VALUES ("Email Informant", "Email Informant Bundle", "emailInformant", 1, NOW(), NOW());


-- Feature Entries
INSERT INTO marketing_feature (name, code, description, active, developer_config, locator_service_id, client_type, provisioning_URI, deprovisioning_URI, update_URI, status_URI, created_at, updated_at)
VALUES ( "Email Deliverability", "emailDeliverability", "Email Deliverability Feature", 1,
'[{"name":{"field":"quantity","displayField":"Quantity"},"type":"number","required":false,"value":1},{"name":{"field":"max_inbox","displayField":"Max Inbox"},"type":"number","required":false,"value":15},{"name":{"field":"inbox_expiration","displayField":"Inbox Expiration"},"type":"text","required":false,"value":"2024-12-31"},{"name":{"field":"inbox_period","displayField":"Inbox Period"},"type":"text","required":false,"value":"monthly"},{"name":{"field":"max_design","displayField":"Max Design"},"type":"number","required":false,"value":15},{"name":{"field":"design_expiration","displayField":"Design Expiration"},"type":"text","required":false,"value":"2024-12-31"}]',
 'mlmpod', 'api250ok', "/api/1.0/account/childaccount", "/api/1.0/account/childaccount", "/api/1.0/account/childaccount", "/api/1.0/account/childaccount", NOW(), NOW());

INSERT INTO marketing_feature (name, code, description, active, developer_config, locator_service_id, client_type, provisioning_URI, deprovisioning_URI, update_URI, status_URI, created_at, updated_at)
VALUES ( "Email Reputation", "emailReputation", "Email Reputation Feature", 1,
'[{"name":{"field":"quantity","displayField":"Quantity"},"type":"number","required":false,"value":1},{"name":{"field":"reputation_expiration","displayField":"Reputation Expiration"},"type":"text","required":false,"value":"2024-12-31"},{"name":{"field":"max_blacklist","displayField":"Max Blacklist"},"type":"number","required":false,"value":1},{"name":{"field":"blacklist_expiration","displayField":"BlackList Expiration"},"type":"text","required":false,"value":"2024-12-31"},{"name":{"field":"max_dmarc","displayField":"Max Dmarc"},"type":"number","required":false,"value":1},{"name":{"field":"dmarc_expiration","displayField":"Dmarc Expiration"},"type":"text","required":false,"value":"2024-12-31"}]',
 'mlmpod', 'api250ok', "/api/1.0/account/childaccount", "/api/1.0/account/childaccount", "/api/1.0/account/childaccount", "/api/1.0/account/childaccount", NOW(), NOW());

INSERT INTO marketing_feature (name, code, description, active, developer_config, locator_service_id, client_type, provisioning_URI, deprovisioning_URI, update_URI, status_URI, created_at, updated_at)
VALUES ( "Email Informant", "emailInformant", "Email Informant Feature", 1,
'[{"name":{"field":"quantity","displayField":"Quantity"},"type":"number","required":false,"value":1},{"name":{"field":"max_emailinformant","displayField":"Maximum Email Informant"},"type":"number","required":false,"value":1000000},{"name":{"field":"emailinformant_expiration","displayField":"Email Informant Expiration"},"type":"text","required":false,"value":"2024-12-31"}]',
'mlmpod', 'api250ok', "/api/1.0/account/childaccount", "/api/1.0/account/childaccount", "/api/1.0/account/childaccount", "/api/1.0/account/childaccount", NOW(), NOW());


-- Marketing Bundle Detail Entries
INSERT INTO marketing_bundle_detail (marketing_bundle_id, marketing_feature_id, bundle_feature_config, is_optional, active, created_at, updated_at)
 VALUES ((SELECT id FROM marketing_bundle where code = 'emailDeliverability'),
         (SELECT id FROM marketing_feature where code = 'emailDeliverability'),'{"quantity":1,"max_inbox":15,"inbox_expiration":"2024-12-31","inbox_period":"monthly","max_design":15,"design_expiration":"2024-12-31"}',
         0, 1, NOW(), NOW());

INSERT INTO marketing_bundle_detail (marketing_bundle_id, marketing_feature_id, bundle_feature_config, is_optional, active, created_at, updated_at)
 VALUES ((SELECT id FROM marketing_bundle where code = 'emailReputation'),
         (SELECT id FROM marketing_feature where code = 'emailReputation'),'{"quantity":1,"reputation_expiration":"2024-12-31","max_blacklist":1,"blacklist_expiration":"2024-12-31","max_dmarc":1,"dmarc_expiration":"2024-12-31"}',
         0, 1, NOW(), NOW());

INSERT INTO marketing_bundle_detail (marketing_bundle_id, marketing_feature_id, bundle_feature_config, is_optional, active, created_at, updated_at)
 VALUES ((SELECT id FROM marketing_bundle where code = 'emailInformant'),
         (SELECT id FROM marketing_feature where code = 'emailInformant'),'{"quantity":1,"max_emailinformant":1000000,"emailinformant_expiration":"2024-12-31"}',
         0, 1, NOW(), NOW());

COMMIT;