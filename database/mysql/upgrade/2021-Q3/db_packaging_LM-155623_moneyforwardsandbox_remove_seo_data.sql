use packagingservice;

SET autocommit = 0;
BEGIN;

SELECT * FROM subscription_bundle_map
WHERE tenant_uuid = "382-VXY-088"
AND marketing_app_feature_id = (SELECT id from marketing_feature where name = "SEO");

SELECT *  FROM subscription_bundle_map
WHERE tenant_uuid = "382-VXY-088"
AND marketing_app_bundle_id = (SELECT id from marketing_bundle where code = "searchEngineOptimization");

DELETE FROM subscription_bundle_map
WHERE tenant_uuid = "382-VXY-088"
AND marketing_app_feature_id = (SELECT id from marketing_feature where name = "SEO");

DELETE  FROM subscription_bundle_map 
WHERE tenant_uuid = "382-VXY-088" 
AND marketing_app_bundle_id = (SELECT id from marketing_bundle where code = "searchEngineOptimization");

SELECT * FROM subscription_bundle_map
WHERE tenant_uuid = "382-VXY-088"
AND marketing_app_feature_id = (SELECT id from marketing_feature where name = "SEO");

SELECT *  FROM subscription_bundle_map
WHERE tenant_uuid = "382-VXY-088"
AND marketing_app_bundle_id = (SELECT id from marketing_bundle where code = "searchEngineOptimization");

COMMIT;