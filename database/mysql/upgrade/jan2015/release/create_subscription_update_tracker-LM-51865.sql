SELECT SCHEMA();

SET foreign_key_checks = 0;

CREATE TABLE IF NOT EXISTS subscription_update_tracker (
    id int NOT NULL AUTO_INCREMENT,
    tracker_id int NOT NULL,
    tenant_uuid varchar(255) NOT NULL,
    status tinyint NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id),
    KEY tracker_id_m1 (tracker_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET foreign_key_checks = 1;