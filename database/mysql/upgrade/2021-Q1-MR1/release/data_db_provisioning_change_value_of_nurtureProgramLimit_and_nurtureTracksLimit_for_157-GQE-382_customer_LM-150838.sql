use provisioningService;

SET autocommit = 0;
BEGIN;

UPDATE subscription_bundle_map
SET subscription_feature_config = REPLACE(subscription_feature_config, 'nurtureProgramLimit":100,"nurtureTracksLimit":15', 'nurtureProgramLimit":400,"nurtureTracksLimit":25'),
updated_at = now()
WHERE tenant_uuid = '157-GQE-382' AND id = 25057;

COMMIT;