use provisioningService;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET name = 'Target Account Management', description = 'Target Account Management feature', updated_at = now()
WHERE code = 'abm';

COMMIT;
