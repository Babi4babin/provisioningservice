use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_bundle
SET name = 'Target Account Management', description = 'Target Account Management bundle', updated_at = now()
WHERE code = 'accountBasedMarketingCore';

UPDATE marketing_bundle
SET name = 'RTP Target Account Management', description = 'RTP Target Account Management Bundle', updated_at = now()
WHERE code = 'accountBasedMarketing';

UPDATE marketing_feature
SET name = 'Target Account Management', description = 'Target Account Management feature', updated_at = now()
WHERE code = 'abm';

UPDATE marketing_feature
SET name = 'RTP Target Account Management', updated_at = now()
WHERE code = 'accountMarketingRTP';

COMMIT;
