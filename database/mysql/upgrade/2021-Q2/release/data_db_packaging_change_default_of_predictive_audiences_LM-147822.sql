use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature 
SET developer_config = '[{ "name" : { "field" : "predictiveAudiences", "displayField" : "Predictive Audiences" }, "type" : "number", "required" : false, "value" : 50 }]' 
WHERE name = "Predictive Audiences"; 

COMMIT;