SELECT SCHEMA();

USE packagingservice;

SET foreign_key_checks = 0;

ALTER TABLE marketing_feature DROP KEY marketing_feature_name_u1;
ALTER TABLE marketing_feature DROP KEY marketing_feature_code_u2;
ALTER TABLE marketing_feature ADD UNIQUE marketing_feature_name_u1 (name);
ALTER TABLE marketing_feature ADD UNIQUE marketing_feature_code_u2 (code);

ALTER TABLE marketing_bundle_detail ADD UNIQUE marketing_bundle_detail_bundle_feature_u1 (marketing_bundle_id, marketing_feature_id);

ALTER TABLE subscription_bundle_map ADD UNIQUE subscription_bundle_map_tenant_bundle_u1 (tenant_uuid,marketing_app_bundle_id);
ALTER TABLE subscription_bundle_map ADD UNIQUE subscription_bundle_map_tenant_feature_u2 (tenant_uuid,marketing_app_feature_id);

SET foreign_key_checks = 1;
