SELECT SCHEMA();

USE provisioningService;

SET foreign_key_checks = 0;

ALTER TABLE marketing_feature DROP KEY name_m1;
ALTER TABLE marketing_feature ADD UNIQUE name_m1 (name);

ALTER TABLE marketing_bundle_detail ADD UNIQUE marketing_bundle_detail_bundle_feature_u1 (marketing_bundle_id, marketing_feature_id);

ALTER TABLE subscription_bundle_map ADD UNIQUE subscription_bundle_map_tenant_bundle_u1 (tenant_uuid,marketing_app_bundle_id);
ALTER TABLE subscription_bundle_map ADD UNIQUE subscription_bundle_map_tenant_feature_u2 (tenant_uuid,marketing_app_feature_id);

SET foreign_key_checks = 1;
