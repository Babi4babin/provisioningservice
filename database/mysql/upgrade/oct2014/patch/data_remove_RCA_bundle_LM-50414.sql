SELECT schema();

SET autocommit=0;

BEGIN;

select * from marketing_bundle_detail where marketing_feature_id in (select id from marketing_feature where name = 'RCA');

delete from marketing_bundle_detail where marketing_feature_id = (select id from marketing_feature where name = 'RCA');

select * from marketing_bundle_detail where marketing_feature_id in (select id from marketing_feature where name = 'RCA');

COMMIT;
/*ROLLBACK;*/
