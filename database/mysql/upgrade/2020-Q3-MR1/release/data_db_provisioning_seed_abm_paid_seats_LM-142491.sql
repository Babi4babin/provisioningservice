use provisioningService;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET developer_config = '[{"name":{"field":"paidSeats","displayField":"Paid Seats"},"type":"number","required":false,"value":25},{"name":{"field":"accountsLimit","displayField":"Accounts Limit"},"type":"number","required":false,"value":1000},{"name":{"field":"indicatorLimit","displayField":"Indicator Limit"},"type":"number","required":false,"value":0}]', updated_at = now()
WHERE code = 'abm';

COMMIT;
