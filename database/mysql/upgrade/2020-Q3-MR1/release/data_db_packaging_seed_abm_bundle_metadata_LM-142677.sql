use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{"paidSeats":25,"accountsLimit":1000,"indicatorLimit":0}', updated_at = now()
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE code = 'abm' LIMIT 1);

COMMIT;
