use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET updated_at = now(),
    developer_config = '[{"name":{"field":"paidSeats","displayField":"Paid Seats"},"type":"number","required":false,"value":10},{"name":{"field":"accountsLimit","displayField":"Accounts Limit"},"type":"number","required":false,"value":1000},{"name":{"field":"indicatorLimit","displayField":"Indicator Limit"},"type":"number","required":false,"value":0}]'
WHERE code = 'abm';

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{"paidSeats":10,"accountsLimit":1000,"indicatorLimit":0}'
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE code = 'abm' LIMIT 1);

COMMIT;