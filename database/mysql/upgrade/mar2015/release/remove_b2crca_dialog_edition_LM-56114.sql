set autocommit=0;
begin;

use provisioningService;

delete from marketing_bundle_detail where marketing_bundle_id = (select id from marketing_bundle where name = 'Dialog Standard') and marketing_feature_id = (select id from marketing_feature where name = 'B2C RCA'); 

update marketing_bundle set name = 'Dialog Edition', description = 'Dialog Edition Bundle', updated_at = now() where name = 'Dialog Standard';

commit;
/*rollback;*/
