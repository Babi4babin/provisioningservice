set autocommit=0;
begin;

use provisioningService;

delete from subscription_bundle_map where marketing_app_feature_id = (select id from marketing_feature where name = 'Launchpoint');

delete from marketing_bundle_detail where marketing_feature_id = (select id from marketing_feature where name = 'Launchpoint');

delete from marketing_feature where name = 'Launchpoint';


commit;
/*rollback;*/