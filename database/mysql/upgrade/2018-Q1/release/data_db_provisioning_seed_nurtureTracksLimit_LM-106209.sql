use provisioningService;

SET autocommit = 0;
BEGIN;

SELECT
  f.name,
  bundle_feature_config,
  replace(bundle_feature_config, '\"nurtureTracksLimit\" \: 15', '\"nurtureTracksLimit\" \: 25')
FROM marketing_bundle_detail dt
  LEFT JOIN marketing_feature f ON (f.id =dt.marketing_feature_id AND f.name = 'Base')
WHERE f.id IS NOT NULL;

UPDATE
  marketing_bundle_detail dt
    LEFT JOIN marketing_feature f
      ON (f.id =dt.marketing_feature_id AND f.name = 'Base')
  SET bundle_feature_config =  replace(bundle_feature_config, '\"nurtureTracksLimit\" \: 15', '\"nurtureTracksLimit\" \: 25')
  WHERE f.id IS NOT NULL;

COMMIT;