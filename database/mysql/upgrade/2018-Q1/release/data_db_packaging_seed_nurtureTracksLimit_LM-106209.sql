use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{ "subscriptionTypeCode": "base", "dedicatedSendingIPs":0, "deliverabilityPartnerEnabled": false, "webServiceDailyQuota": 50000, "deliverabilityPartnerUser" : "", "deliverabilityPartnerGroup" : "", "mkDenialEnabled" : false, "mtaOptions" : 2, "nurtureProgramLimit" : 100, "nurtureTracksLimit" : 25 }'
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE code = 'baseFeature' LIMIT 1);

COMMIT;