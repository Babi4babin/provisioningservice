use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE subscription_bundle_map
SET subscription_feature_config = REPLACE(subscription_feature_config, '"nurtureProgramLimit":100,"nurtureTracksLimit":15', '"nurtureProgramLimit":300,"nurtureTracksLimit":25'),
updated_at = now()
WHERE tenant_uuid = '178-GYD-668' AND id = 131759;

COMMIT;