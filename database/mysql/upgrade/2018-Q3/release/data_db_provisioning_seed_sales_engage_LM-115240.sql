use provisioningService;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET updated_at = now(),
    update_URI = '/rest/v1/featureHandler/operation/updateSalesEngage/munchkinId/{tenantUUID}',
    developer_config = '[{"name":{"field":"salesEngageSeats","displayField":"Sales Engage Seats"},"type":"number","required":false,"value":0},{"name":{"field":"planType","displayField":"Plan Type"},"type":"picklist","required":false,"displayField":"name","submitField":"value","value":[{"name":"Standard","value":"Standard"},{"name":"Premium","value":"Premium"}],"defaultValue":{"name":"Standard","value":"Standard"}}]'
WHERE code = 'salesEngage';

COMMIT;