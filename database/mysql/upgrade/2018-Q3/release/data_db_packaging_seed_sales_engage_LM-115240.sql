use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET updated_at = now(),
    update_URI = '/rest/v1/featureHandler/operation/updateSalesEngage/munchkinId/{tenantUUID}',
    developer_config = '[{"name":{"field":"salesEngageSeats","displayField":"Sales Engage Seats"},"type":"number","required":false,"value":0},{"name":{"field":"planType","displayField":"Plan Type"},"type":"picklist","required":false,"displayField":"name","submitField":"value","value":[{"name":"Standard","value":"Standard"},{"name":"Premium","value":"Premium"}],"defaultValue":{"name":"Standard","value":"Standard"}}]'
WHERE code = 'salesEngage';

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{"salesEngageSeats":0,"planType":"Standard"}'
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE code = 'salesEngage' LIMIT 1);

COMMIT;