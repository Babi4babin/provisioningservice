use packagingservice;

SET autocommit = 0;
BEGIN;


UPDATE marketing_feature
SET developer_config = '[{ "name" : { "field" : "dedicatedSendingIPs", "displayField" : "Dedicated Email Ips" }, "type" : "picklist", "required" : true, "value" : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], "defaultValue" : 0 }, { "name" : { "field" : "deliverabilityPartnerEnabled", "displayField" : "Deliveribility Partner" }, "type" : "boolean", "required" : false, "value" : false }, { "name" : { "field" : "webServiceDailyQuota", "displayField" : "Web Service Daily Limit" }, "type" : "number", "required" : false, "value" : 50000 }, { "name" : { "field" : "deliverabilityPartnerUser", "displayField" : "Deliverability User" }, "type" : "text", "required" : false }, { "name" : { "field" : "deliverabilityPartnerGroup", "displayField" : "Deliverability Group" }, "type" : "text", "required" : false }, { "name" : { "field" : "mkDenialEnabled", "displayField" : "Mk Denial" }, "type" : "boolean", "required" : false, "value" : false }, { "name" : { "field" : "mtaOptions", "displayField" : "MTA" }, "type" : "picklist", "required" : true, "displayField" : "name", "submitField" : "value", "value" : [{ "name" : "Mkt Mail", "value" : 2 }], "defaultValue" : { "name" : "Mkt Mail", "value" : "2" } }, { "name" : { "field" : "nurtureProgramLimit", "displayField" : "Engagement Program Limit" }, "type" : "number", "required" : false, "value" : 100 }, { "name" : { "field" : "nurtureTracksLimit", "displayField" : "Tracks Limit" }, "type" : "number", "required" : false, "value" : 100 }, {"name" : {"field" : "exportDailyQuotaInMB","displayField" : "Bulk Export Daily Quota in MB"},"type" : "number","required" : false,"value" : 500}, { "name" : { "field" : "retentionPeriod", "displayField" : "Retention Period" }, "type" : "picklist", "required" : true, "value" : ["25 month", "37 month"], "defaultValue" : "25 month" }]', updated_at = now()
WHERE code = 'baseFeature';

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{ "subscriptionTypeCode": "base", "dedicatedSendingIPs":0, "deliverabilityPartnerEnabled": false, "webServiceDailyQuota": 50000, "deliverabilityPartnerUser" : "", "deliverabilityPartnerGroup" : "", "mkDenialEnabled" : false, "mtaOptions" : 2, "nurtureProgramLimit" : 100, "nurtureTracksLimit" : 25 , "exportDailyQuotaInMB" : 500, "retentionPeriod" : "25 month"}', updated_at = now()
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE code = 'baseFeature' LIMIT 1);

COMMIT;
