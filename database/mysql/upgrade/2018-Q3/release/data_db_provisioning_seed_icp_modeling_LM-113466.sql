use provisioningService;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET updated_at = now(),
    developer_config = '[{ "name" : { "field" : "paidSeats", "displayField" : "Paid Seats" }, "type" : "number", "required" : false, "value" : 10 }, { "name" : { "field" : "accountsLimit", "displayField" : "Accounts Limit" }, "type" : "number", "required" : false, "value" : 1000 }, { "name" : { "field" : "icpModeling", "displayField" : "Icp Modeling" }, "type" : "picklist", "required" : false, "displayField" : "name", "submitField" : "value", "value" : [ { "name" : "None", "value" : "None" }, { "name" : "Basic", "value" : "Basic" }, { "name" : "Pro", "value" : "Pro" }, { "name" : "Elite", "value" : "Elite" }, { "name" : "Enterprise", "value" : "Enterprise" } ], "defaultValue" : { "name" : "None", "value" : "None" } }]'
WHERE code = 'abm';

COMMIT;
