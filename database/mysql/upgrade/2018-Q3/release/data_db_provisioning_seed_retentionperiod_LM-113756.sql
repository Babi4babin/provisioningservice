use provisioningService;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET developer_config = '[{ "name" : { "field" : "dedicatedSendingIPs","displayField" : "Dedicated Email Ips" },"type" : "picklist","required" : true,"value" : [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],"defaultValue" : 0 },{ "name" : { "field" : "deliverabilityPartnerEnabled","displayField" : "Deliveribility Partner" },"type" : "boolean","required" : false,"value" : false },{ "name" : { "field" : "webServiceDailyQuota","displayField" : "Web Service Daily Limit" },"type" : "number","required" : false,"value" : 10000 },{ "name" : { "field" : "sisSeats","displayField" : "Sales Insight Seats" },"type" : "number","required" : false,"value" : 0 },{ "name" : { "field" : "lissSeats","displayField" : "Outlook Seats" },"type" : "number","required" : false,"value" : 0 },{ "name" : { "field" : "deliverabilityPartnerUser","displayField" : "Deliverability User" },"type" : "text","required" : false },{ "name" : { "field" : "deliverabilityPartnerGroup","displayField" : "Deliverability Group" },"type" : "text","required" : false },{ "name" : { "field" : "mkDenialEnabled","displayField" : "Mk Denial" },"type" : "boolean","required" : false,"value" : false },{ "name" : { "field" : "mtaOptions","displayField" : "MTA" },"type" : "picklist","required" : true,"displayField" : "name","submitField" : "value","value" : [{ "name" : "Strong Mail","value" : 1 },{ "name" : "Mkt Mail","value" : 2 }],"defaultValue" : { "name" : "Mkt Mail","value" : 2 } },{ "name" : { "field" : "nurtureProgramLimit","displayField" : "Engagement Program Limit" },"type" : "number","required" : false,"value" : 100 },{ "name" : { "field" : "nurtureTracksLimit","displayField" : "Tracks Limit" },"type" : "number","required" : false,"value" : 100 },{ "name" : { "field" : "rcmLimit","displayField" : "Approved Models" },"type" : "number","required" : false },{"name" : {"field" : "exportDailyQuotaInMB","displayField" : "Bulk Export Daily Quota in MB"},"type" : "number","required" : false,"value" : 500}, { "name" : { "field" : "retentionPeriod", "displayField" : "Retention Period" }, "type" : "picklist", "required" : true, "value" : ["25 month", "37 month"], "defaultValue" : "25 month" }]', updated_at = now()
WHERE name = 'Base';

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{ "subscriptionTypeCode": "specialcustomer", "webServiceDailyQuota" : 10000, "sisSeats" : 0, "lissSeats" : 10, "dedicatedSendingIPs" : 0, "deliverabilityPartnerEnabled" : false, "deliverabilityPartnerUser" : "", "deliverabilityPartnerGroup" : "", "mkDenialEnabled" : false, "mtaOptions" : 2, "nurtureProgramLimit" : 100, "nurtureTracksLimit" : 100, "rcmLimit" : 1 , "exportDailyQuotaInMB" : 500, "retentionPeriod" : "25 month"}', updated_at = now()
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE name = 'Base' LIMIT 1)
AND marketing_bundle_id = (SELECT id
                           FROM marketing_bundle
                           WHERE name = 'Enterprise' LIMIT 1);

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{ "subscriptionTypeCode": "SMBStandard", "webServiceDailyQuota" : 10000, "sisSeats" : 20, "lissSeats" : 10, "dedicatedSendingIPs" : 0, "deliverabilityPartnerEnabled" : false, "deliverabilityPartnerUser" : "", "deliverabilityPartnerGroup" : "", "mkDenialEnabled" : false, "mtaOptions" : 2, "nurtureProgramLimit" : 100, "nurtureTracksLimit" : 25, "rcmLimit" : 1, "sandboxCopyFrom": "smbbpstandard", "exportDailyQuotaInMB" : 500, "retentionPeriod" : "25 month" }', updated_at = now()
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE name = 'Base' LIMIT 1)
AND marketing_bundle_id = (SELECT id
                           FROM marketing_bundle
                           WHERE name = 'SMB - Standard' LIMIT 1);

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{ "subscriptionTypeCode": "SMBSelect", "webServiceDailyQuota" : 10000, "sisSeats" : 20, "lissSeats" : 10, "dedicatedSendingIPs" : 0, "deliverabilityPartnerEnabled" : false, "deliverabilityPartnerUser" : "", "deliverabilityPartnerGroup" : "", "mkDenialEnabled" : false, "mtaOptions" : 2, "nurtureProgramLimit" : 100, "nurtureTracksLimit" : 25, "rcmLimit" : 1, "sandboxCopyFrom": "smbbpselect", "exportDailyQuotaInMB" : 500, "retentionPeriod" : "25 month" }', updated_at = now()
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE name = 'Base' LIMIT 1)
AND marketing_bundle_id = (SELECT id
                           FROM marketing_bundle
                           WHERE name = 'SMB - Select' LIMIT 1);

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{ "subscriptionTypeCode": "SMBSpark", "webServiceDailyQuota" : 0, "sisSeats" : 5, "lissSeats" : 10, "dedicatedSendingIPs" : 0, "deliverabilityPartnerEnabled" : false, "deliverabilityPartnerUser" : "", "deliverabilityPartnerGroup" : "", "mkDenialEnabled" : false, "mtaOptions" : 2, "nurtureProgramLimit" : 100, "nurtureTracksLimit" : 25, "rcmLimit" : 1, "sandboxCopyFrom" : "smbbpspark", "exportDailyQuotaInMB" : 500, "retentionPeriod" : "25 month" }', updated_at = now()
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE name = 'Base' LIMIT 1)
AND marketing_bundle_id = (SELECT id
                           FROM marketing_bundle
                           WHERE name = 'SMB - Spark' LIMIT 1);


UPDATE marketing_bundle_detail
SET bundle_feature_config = '{ "subscriptionTypeCode": "b2c", "webServiceDailyQuota" : 10000, "sisSeats" : 0, "lissSeats" : 0, "dedicatedSendingIPs" : 0, "deliverabilityPartnerEnabled" : false, "deliverabilityPartnerUser" : "", "deliverabilityPartnerGroup" : "", "mkDenialEnabled" : false, "mtaOptions" : 2, "b2crca" : false, "nurtureProgramLimit" : 100, "nurtureTracksLimit" : 25, "rcmLimit" : 1, "sandboxCopyFrom": "smbbpdialog", "exportDailyQuotaInMB" : 500, "retentionPeriod" : "25 month" }', updated_at = now()
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE name = 'Base' LIMIT 1)
AND marketing_bundle_id = (SELECT id
                           FROM marketing_bundle
                           WHERE name = 'Dialog Edition' LIMIT 1);

UPDATE marketing_bundle_detail
SET bundle_feature_config = '{ "subscriptionTypeCode": "MarketoLite", "webServiceDailyQuota" : 0, "sisSeats" : 0, "lissSeats" : 0, "dedicatedSendingIPs" : 0, "deliverabilityPartnerEnabled" : false, "deliverabilityPartnerUser" : "", "deliverabilityPartnerGroup" : "", "mkDenialEnabled" : false, "mtaOptions" : 2, "nurtureProgramLimit" : 100, "nurtureTracksLimit" : 25, "exportDailyQuotaInMB" : 500, "retentionPeriod" : "25 month" }', updated_at = now()
WHERE marketing_feature_id = (SELECT id
                              FROM marketing_feature
                              WHERE name = 'Base' LIMIT 1)
AND marketing_bundle_id = (SELECT id
                           FROM marketing_bundle
                           WHERE name = 'Marketo Lite' LIMIT 1);
COMMIT;
