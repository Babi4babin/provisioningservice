SELECT SCHEMA();

SET foreign_key_checks = 0;

CREATE TABLE IF NOT EXISTS packaging_update_tracker (
    id int NOT NULL AUTO_INCREMENT,
    tag varchar(255) NOT NULL,
    start_time datetime NOT NULL,
    end_time datetime,
    status tinyint NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY tag_m1 (tag)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET foreign_key_checks = 1;
