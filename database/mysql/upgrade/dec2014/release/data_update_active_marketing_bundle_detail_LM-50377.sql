use provisioningService;

SET autocommit = 0;
BEGIN;

UPDATE marketing_bundle_detail SET active = 1;

COMMIT;
/*ROLLBACK*/