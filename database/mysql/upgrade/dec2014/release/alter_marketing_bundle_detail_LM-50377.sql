SELECT SCHEMA();

SET foreign_key_checks = 0;

ALTER TABLE marketing_bundle_detail ADD COLUMN active tinyint(1) NOT NULL DEFAULT 0 AFTER bundle_feature_config;

SET foreign_key_checks = 1;
