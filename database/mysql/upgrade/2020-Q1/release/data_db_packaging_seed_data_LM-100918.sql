use packagingservice;

SET autocommit = 0;
BEGIN;

UPDATE marketing_feature
SET update_URI = '/rest/v1/featureHandler/operation/updateOutlookPlugIn/munchkinId/{tenantUUID}', updated_at = now()
WHERE code = 'outlookPlugIn';

COMMIT;
