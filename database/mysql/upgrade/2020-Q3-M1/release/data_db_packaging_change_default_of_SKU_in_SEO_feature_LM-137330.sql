use packagingservice;

SET autocommit = 0;
BEGIN;


UPDATE marketing_feature
SET developer_config = '[{ "name" : { "field" : "podName", "displayField" : "SEO Pod" }, "type" : "picklist", "required" : true, "metadata" : {"valueType" : "string", "tag" : "seo"} }, { "name" : { "field" : "sku", "displayField" : "SKU" }, "type" : "picklist", "required" : true, "displayField" : "name", "submitField" : "shortName", "value" : [{"name" : "SEO 500", "shortName" : "SEO 500"}, {"name" : "SEO 1000", "shortName" : "SEO 1000"}, {"name" : "SEO 500 Plus", "shortName" : "SEO 500 Plus"}, {"name" : "SEO 1000 Plus", "shortName" : "SEO 1000 Plus"}], "defaultValue" : {"name" : "SEO 500", "shortName" : "SEO 500"} }]',
updated_at = now()
WHERE code = 'searchEngineOptimization';

COMMIT;
