-- This is used only for vagrant provisioning
DROP DATABASE IF EXISTS packagingservice;
CREATE DATABASE packagingservice CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES on packagingservice.* to 'provisionusr'@'localhost' IDENTIFIED BY 'marketo_password4';

USE packagingservice;

