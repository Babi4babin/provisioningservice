-- This is used only for vagrant provisioning
DROP DATABASE IF EXISTS provisioningservice;
CREATE DATABASE provisioningservice CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES on provisioningservice.* to 'provisionusr'@'localhost' IDENTIFIED BY 'marketo_password4';

USE provisioningservice;

