SELECT SCHEMA();

SET foreign_key_checks = 0;

use provisioningservice;

CREATE TABLE marketing_bundle (
	id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    description varchar(255) DEFAULT NULL,
    code varchar(255) DEFAULT NULL,
    active tinyint(1) NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY name_m1 (name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE marketing_feature (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    code varchar(255) DEFAULT NULL,
    description varchar(255) DEFAULT NULL,
    active tinyint(1) NOT NULL,
    developer_config varchar(4000) DEFAULT NULL,
    locator_service_id varchar(255) NOT NULL,
    client_type varchar(255) DEFAULT NULL,
    provisioning_URI varchar(255) NOT NULL,
    deprovisioning_URI varchar(255) default null,
    status_URI varchar(255) default null,
    update_URI varchar(255) default null,
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY name_m1 (name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE marketing_bundle_detail (
    id int NOT NULL AUTO_INCREMENT,
    marketing_bundle_id int NOT NULL,
    child_marketing_bundle_id int DEFAULT NULL,
    marketing_feature_id int DEFAULT NULL,
    bundle_feature_config varchar(2000) DEFAULT NULL,
    is_optional tinyint(1) DEFAULT 0,
    active tinyint(1) NOT NULL DEFAULT 0,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id),
    KEY marketing_bundle_m1 (marketing_bundle_id),
    UNIQUE KEY marketing_bundle_detail_bundle_feature_u1 (marketing_bundle_id, marketing_feature_id),
    CONSTRAINT marketing_bundle_id_FK_1 FOREIGN KEY (marketing_bundle_id) REFERENCES marketing_bundle (id),
    CONSTRAINT marketing_feature_id_FK_2 FOREIGN KEY (marketing_feature_id) REFERENCES marketing_feature (id),
    CONSTRAINT child_marketing_bundle_id_FK_3 FOREIGN KEY (child_marketing_bundle_id) REFERENCES marketing_bundle (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE subscription_bundle_map (
	id int NOT NULL AUTO_INCREMENT,
	marketing_app_bundle_id int DEFAULT NULL,
	marketing_app_feature_id int DEFAULT NULL,
	tenant_uuid varchar(255) NOT NULL,
	subscription_feature_config varchar(2000) DEFAULT NULL,
	subscription_feature_info varchar(2000) DEFAULT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,    
    PRIMARY KEY (id),
    KEY tenant_uuid_m1 (tenant_uuid),
    UNIQUE KEY subscription_bundle_map_tenant_bundle_u1 (tenant_uuid,marketing_app_bundle_id),
    UNIQUE KEY subscription_bundle_map_tenant_feature_u2 (tenant_uuid,marketing_app_feature_id),
    CONSTRAINT marketing_app_bundle_id_FK1 FOREIGN KEY (marketing_app_bundle_id) REFERENCES marketing_bundle (id),
    CONSTRAINT marketing_app_feature_id_FK2 FOREIGN KEY (marketing_app_feature_id) REFERENCES marketing_feature (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE packaging_update_tracker (
    id int NOT NULL AUTO_INCREMENT,
    tag varchar(255) NOT NULL,
    start_time datetime NOT NULL,
    end_time datetime,
    status tinyint NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY tag_m1 (tag)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS subscription_update_tracker (
    id int NOT NULL AUTO_INCREMENT,
    tracker_id int NOT NULL,
    tenant_uuid varchar(255) NOT NULL,
    status tinyint NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id),
    KEY tracker_id_m1 (tracker_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS marketing_feature_dependency (
    id int NOT NULL AUTO_INCREMENT,
    marketing_feature_id int NOT NULL COMMENT 'Feature with id = marketing_feature_id has to be provisioned AFTER feature with id = dep_marketing_feature_id',
    dep_marketing_feature_id int NOT NULL COMMENT 'Feature with id = dep_marketing_feature_id has to be provisioned BEFORE feature with id = marketing_feature_id',
    created_at DATETIME NOT NULL,
    updated_at DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT marketing_feature_id_FK_1 FOREIGN KEY (marketing_feature_id) REFERENCES marketing_feature (id),
    CONSTRAINT dep_marketing_feature_id_FK_2 FOREIGN KEY (dep_marketing_feature_id) REFERENCES marketing_feature (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET foreign_key_checks = 1;
