use packagingservice;

SET autocommit = 0;
BEGIN;

INSERT INTO subscription_bundle_map(marketing_app_feature_id, tenant_uuid, created_at, updated_at)
VALUES (47, '888-ZZZ-999', NOW(), NOW());

ROLLBACK;
