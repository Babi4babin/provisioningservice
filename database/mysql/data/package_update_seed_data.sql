SELECT schema();

SET autocommit=0;

BEGIN;

insert into packaging_update_tracker (tag, start_time, end_time, status, created_at, updated_at) values ('dec2014_release', '2014-12-12', '2014-12-12', 3, now(), now());

COMMIT;
/*ROLLBACK;*/
