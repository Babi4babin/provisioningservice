#!/usr/bin/env bash
set -e

echo "Running rebuild.sh to determine if DB needs rebuilt..."
# DB Create/Reset
STD_PARAMS="-h$MYSQL_HOST -u$MYSQL_USER -p$MYSQL_PASSWORD -P$MYSQL_PORT"
PROVISIONING_DB_EXISTS=$(mysqlshow $STD_PARAMS provisioningservice || echo "false")
RESET_PROVISIONING_DB="false"
if [ "$PROVISIONING_DB_EXISTS" = "false" ] || [ "$RESET" = "true" ]; then
  RESET_PROVISIONING_DB="true"
fi
if [ "$RESET" = "false" ]; then
  RESET_PROVISIONING_DB="false"
fi
if [ "$RESET_PROVISIONING_DB" = "true" ]; then
  echo "Creating provisioningservice db..."
  mysql $STD_PARAMS < /src/database/mysql/schema/create_db.sql
  echo "Inserting provisioningservice tables..."
  mysql $STD_PARAMS provisioningservice < /src/database/mysql/schema/schema.sql
  echo "Applying seed data"
  java -DdataSource.initialSize=3 -DdataSource.maxActive=30 -DdataSource.maxIdle=5 -DdataSource.password=marketo17 -DdataSource.url=jdbc:mysql://mlm-mysql:3306/provisioningservice -DdataSource.username=root -DdataSource.testWhileIdle=true -DlocatorService.url=http://locator:8080/locatorservice-webapp-1.0-SNAPSHOT/local -jar /src/jars/bundlemetadata.jar /src/database/mysql/data/seed_data.yml provisioningService
fi

PACKAGING_DB_EXISTS=$(mysqlshow $STD_PARAMS packagingservice || echo "false")
RESET_PACKAGING_DB="false"
if [ "$PACKAGING_DB_EXISTS" = "false" ] || [ "$RESET" = "true" ]; then
  RESET_PACKAGING_DB="true"
fi
if [ "$RESET" = "false" ]; then
  RESET_PACKAGING_DB="false"
fi
if [ "$RESET_PACKAGING_DB" = "true" ]; then
  echo "Creating packagingservice db..."
  mysql $STD_PARAMS < /src/database/mysql/schema/create_packaging_db.sql
  echo "Inserting packagingservice tables..."
  mysql $STD_PARAMS provisioningservice < /src/database/mysql/schema/module_schema.sql
  echo "Applying seed data"
  java -DdataSource.initialSize=3 -DdataSource.maxActive=30 -DdataSource.maxIdle=5 -DdataSource.password=marketo17 -DdataSource.url=jdbc:mysql://mlm-mysql:3306/packagingservice -DdataSource.username=root -DdataSource.testWhileIdle=true -DlocatorService.url=http://locator:8080/locatorservice-webapp-1.0-SNAPSHOT/local -jar /src/jars/bundlemetadata.jar /src/database/mysql/data/packaging_seed_data.yml packagingservice
fi

# Rebuild maven
if [ "$REBUILD" = "true" ]; then
  if [ -z "$ARTIFACTORY_USERNAME" ] || [ -z "$ARTIFACTORY_PASSWORD" ]; then
    echo "You must set the environment variables ARTIFACTORY_USERNAME and ARTIFACTORY_PASSWORD."
    echo "Set them with docker-compose environment property or by passing them as options to docker run."
    exit 1
  fi
  echo 'Building provisioning service with Maven... This may require VPN access to artifactory servers.'
  cd /src/
  mvn clean install -DskipTests
  cp /src/subscriptionservice/target/provisioningservice-*-SNAPSHOT.war /usr/local/tomcat/webapps/provisioningservice.war
fi
