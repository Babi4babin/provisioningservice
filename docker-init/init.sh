#!/usr/bin/env bash
set -e

cd /docker-init/conf
envreplace envreplace.json

if [ "$DEV_CONTAINER" = "true" ]; then
  cd /docker-init
  ./rebuild.sh
fi

cd /usr/local/tomcat/bin
echo "Starting tomcat"
catalina.sh run
